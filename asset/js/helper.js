

function isJson(str) {
    try {
        return JSON.parse(str);
    } catch (e) {
        return str
    }
}

  var question_per_element = {};
  var arr = [];
  var count_element = 1;
  var count_question = 1;

//gmp status
var gmp = {
  status : function(status_id){
    switch(status_id) {
      case '0':
          return "Draft";
          break;
      case '1':
          return "Co-Auditor's Review";
          break;
      case '2':
          return "Co-Auditor's Review";
          break;
      case '3':
          return "Submitted to Department Head";
          break;
      case '4':
          return "Submitted to Division Head";
          break;
      case '5':
          return "Approved by Division Head";
          break;
      case '6':
          return "Returned by Department Head";
          break;
      case '7':
          return "Returned by Division Head";
          break;
    }
  },
  audit_date_json : function(data){
    var audit_dates = data.split(",");
    var month_format = ["", "January","February","March","April","May","June","July","August","September","October","November","December"];
    var current_month = 0;
    var month = 0;
    var date = "";
    var month_text = "";
    var year = ""
    var month_array = [];
    var day_array = [];
    var d = null;
    var mount_count = 0;
      $.each(audit_dates, function(index, row){
        d = NewDate(row);
        month = moment(d).format('M');
        year = moment(d).format('Y');
        if(current_month != month){
          mount_count++;
          month_array.push(month);
          if(mount_count == audit_dates.length){
            date = date.trim().substring(0,date.trim().length - 1) + " " + month_format[current_month] + " & " + moment(d).format('DD') + " ";
          } else {
            date = date.trim().substring(0,date.trim().length - 1) + " " + month_format[current_month] + ", " + moment(d).format('DD') + " ";
          }
          current_month = month;
        } else {
          date += moment(d).format('DD') + " ";
        }
      });

      return date.trim().substring(1,date.trim().length) + " " + month_format[current_month] + " " + year;

  },
  audit_date : function(audit_dates){
    var month_format = ["", "January","february","March","April","May","June","July","August","September","October","November","December"];
    var current_month = 0;
    var month = 0;
    var date = "";
    var month_text = "";
    var year = ""
    var month_array = [];
    var day_array = [];
    var mount_count = 0;
      $.each(audit_dates, function(index, row){
        month = moment(row.Date).format('M');
        year = moment(row.Date).format('Y');
        if(current_month != month){

          mount_count++;
          month_array.push(month);
          if(mount_count == audit_dates.length){
            date = date.trim().substring(0,date.trim().length - 1) + " " + month_format[current_month] + " & " + moment(row.Date).format('DD') + " ";
          } else {
            date = date.trim().substring(0,date.trim().length - 1) + " " + month_format[current_month] + ", " + moment(row.Date).format('DD') + " ";
          }


          current_month = month;
        } else {
          date += moment(row.Date).format('DD') + " ";
        }
      });
      return date.trim().substring(1,date.trim().length) + " " + month_format[current_month] + " " + year;

  },
  audit_dates: function(audit_dates){


    console.log(audit_dates);

    var month_format = ["", "January","february","March","April","May","June","July","August","September","October","November","December"];
    var current_month = 0;
    var month = 0;
    var date = "";
    var month_text = "";
    var year = ""
    var month_array = [];
    var day_array = [];
    var mount_count = 0;
      $.each(audit_dates, function(index, row){
        month = moment(row.Date).format('M');
        year = moment(row.Date).format('Y');
        if(current_month != month){

          mount_count++;
          month_array.push(month);
          if(mount_count == audit_dates.length){
            date = date.trim().substring(0,date.trim().length - 1) + " " + month_format[current_month] + " & " + moment(row.Date).format('DD') + ", ";
          } else {
            date = date.trim().substring(0,date.trim().length - 1) + " " + month_format[current_month] + ", " + moment(row.Date).format('DD') + " ";
          }


          current_month = month;
        } else {
          date += moment(row.Date).format('DD') + " ";
        }
      });
      return date.trim().substring(1,date.trim().length) + " " + month_format[current_month] + " " + year;



    // var audit_date = "";
    // var audit_month = "";
    // var audit_year = "";

    // var len = audit_dates.length;
    // audit_month = ""; audit_date = ""; audit_year = "";

    // $.each(audit_dates, function(index, row){
    //   audit_month = moment(row.Date).format('MMMM');
    //     audit_year = moment(row.Date).format('Y');

    //   if (index === len - 1) {
    //     audit_date += " & " + moment(row.Date).format('D') + "";
    //   } else {
    //     audit_date +=  ", " + moment(row.Date).format('D');
    //   }
    // });

    // if(audit_date.trim().charAt(0) == "&") {
    //   audit_date = audit_date.trim().substr(1);
    // }

    // return audit_month + " " + audit_date.substr(1) + ", " + audit_year;
  },
  inspectors: function(inspector){
      var inspectors = "";
      // var len = inspector.length;

      // $.each(inspector, function(index, row){
      //     if (index === len - 1) {
      //       inspectors += " & " + row.inspector + "";
      //     } else {
      //       inspectors +=  ", " + row.inspector;
      //     }
      // });


      // if(inspectors.trim().charAt(0) == "&") {
      //   inspectors = inspectors.trim().substr(1);
      // }

      // inspectors = inspectors.trim().substr(1);

      // var lastChar = inspectors.slice(-1);
      // if (lastChar == '&') {
      //     inspectors = inspectors.slice(0, -1);
      // }
      // return inspectors;

      console.log(inspectors);

  },
  changes: function(ary){
      var string = "";
      var len = ary.length;

      $.each(ary, function(index, row){
          if (index === len - 1) {
            if(row.changes.length > 50){
              string += " & " + row.changes.substring(0,50) + "...";
            } else {
              string += " & " + row.changes+ "";
            }

          } else {
            if(row.changes.length > 55){
              string += ", " + row.changes.substring(0,50) + "...";
            } else {
              string += ", " + row.changes+ "";
            }
          }
      });


      if(string.trim().charAt(0) == "&") {
        string = string.trim().substr(1);
      } else {
        string = string.trim().substr(1);
      }

      return string;
  }
}

    function removelastamp(strVal) {
       var lastChar = strVal.slice(-1);
        if (lastChar == ',') {
            strVal = strVal.slice(0, -1);
        }
        return strVal;
        alert(strVal);
    }

function NewDate(str)
 {str=str.split('-');
  var date=new Date();
  date.setUTCFullYear(str[0], str[1]-1, str[2]);
  date.setUTCHours(0, 0, 0, 0);
  return date;
}


//ajax helper
var aJax = {
  post : function(url,data,callback, complete){

    $.ajax({
      async: true,
      cache: false,
      type: 'POST',
      url:url,
      data:data,
      success: callback
    });

  },
  get : function(url,callback){

    $.ajax({
      type: 'GET',
      url:url,
      success: callback
    });

  },
  postasync : function(url,data,callback, complete){
    $.ajax({
      async: false,
      cache: false,
      type: 'POST',
      url:url,
      data:data,
      success: callback
    });

  },
}

function jsUcfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// table body helper
var TBody = function() {
  this.html = '';
  this.html_final = '';
  this.td_class = '';
  this.tr_class = '';
};

TBody.prototype.set_tdclass = function(style) {
  this.td_class = style;
};
TBody.prototype.set_trclass = function(style) {
  this.tr_class = style;
};
TBody.prototype.td = function(element) {
  this.html += "<td class='" + this.td_class + "'>" + element + "</td>";
};

TBody.prototype.td_norecord = function(span) {
  this.html += "<tr><td style='text-align:center;' colspan='"+span+"' class='" + this.td_class + "'>No Record Found</td></tr>";
};

TBody.prototype.set = function() {
  this.html_final += '<tr class="' + this.tr_class + '">' + this.html + '</tr>';
  this.html = "";
};

TBody.prototype.append = function(element) {
  $(element).html(this.html_final);
};

// list body helper
var UList = function() {
  this.html = '';
  this.html_final = '';
  this.li_class = '';
  this.ul_class = '';
};

UList.prototype.set_liclass = function(style) {
  this.li_class = style;
};
UList.prototype.set_ulclass = function(style) {
  this.ul_class = style;
};
UList.prototype.li = function(element) {
  this.html += "<li class='" + this.li_class + "'>" + element + "</li>";
};

UList.prototype.set = function(element) {
  return "<ul class='" + this.ul_class + "'>" + this.html + "</ul>";
};

// JSON API
function update_audit_report_json(){
    $.ajax({
      type: 'Post',
      url: base_url + "/api/audit_report_list"
      }).done( function(data){
          return "success";
      });
  }




  // TEMPLATE
  var current_element = 1;
  var current_question = 0;
  var Template = function() {
    this.element_value = '';
    this.element_id = ''; // id for editing
    this.element_position = ''; //cnt for ordering

    this.question_value = '';
    this.question_default_yes = '';
    this.question_id = ''; //cnt for ordering
    this.question_position = ''; //cnt for ordering
    this.question_element = ''; //cnt for child
    this.question_required_remarks = ''; //cnt for child

    this.activity_value = '';
    this.activity_position = '';
    this.activity_id = '';

    this.subactivity_value ='';
    this.subactivity_position = '';
    this.subactivity_activity = '';
    this.subactivity_id = '';
  };

  Template.prototype.set_subactivity_id = function(subactivity_id) {
    this.subactivity_id = subactivity_id;
  };
  Template.prototype.set_subactivity_value = function(subactivity_value) {
    this.subactivity_value = subactivity_value;
  };
  Template.prototype.set_subactivity_position = function(subactivity_position) {
    this.subactivity_position = subactivity_position;
  };
  Template.prototype.set_subactivity_activity = function(subactivity_activity) {
    this.subactivity_activity = subactivity_activity;
  };
  Template.prototype.append_subactivity = function(last_btn) {
    var html = '';
    html += '<div class="col-md-12 subactivity subactivity_'+this.subactivity_activity+'_'+this.subactivity_position+'" style="padding-top: 10px;">';
    html += '   <div class="col-md-3 stext" style="text-align:right;">Sub-Activity ' + this.subactivity_position + '</div>';
    html += '   <div class="col-md-4">';
    html += '     <input type="text" value="'+this.subactivity_value+'" val-id="'+this.subactivity_id+'" id="subactivity_'+this.subactivity_activity+'_'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="act_sub_val act_sub_val_'+ this.subactivity_activity+' form-control">';
    html += '     <span class = "er-msg"></span>';
    html += '   </div>';
    html += '   <div class="col-md-4">';
    if(last_btn){
        html += '     <button type="submit" style="border: none; padding: 5px;" position="'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="btn_sub_add btn btn-default btn_sub_add_'+this.subactivity_activity+'_'+this.subactivity_position+'" id="add_sub_activity" title="Add Sub-activity">';
        html += '       <i class="glyphicon glyphicon-plus" ></i>';
        html += '     </button>';
    } else {
        html += '     <button type="submit" style="border: none; padding: 5px;" position="'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="btn_sub_add btn btn-default btn_sub_add_'+this.subactivity_activity+'_'+this.subactivity_position+'" id="add_sub_activity" title="Add Sub-activity">';
        html += '       <i class="glyphicon glyphicon-plus" ></i>';
        html += '     </button>';
    }
    html += '     <button type="submit" style="border: none; padding: 5px;" val-id="'+this.subactivity_id+'" position="'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="btn_sub_add btn btn-default" id="remove_sub_activity" title="Remove Sub-activity">';
    html += '       <i class="glyphicon glyphicon-minus" ></i>';
    html += '     </button>';
    html += '   </div>';
    html += '   <div class="clearfix"></div>';
    html += '</div>';
    $('#activity_sub_'+this.subactivity_activity).append(html);
    arrange_activity();
  };

  Template.prototype.next_subactivity = function(element,last_btn) {
    var html = '';
    html += '<div class="col-md-12 subactivity subactivity_'+this.subactivity_activity+'_'+this.subactivity_position+'" style="padding-top: 10px;">';
    html += '   <div class="col-md-3 stext" style="text-align:right;">Sub-Activity ' + this.subactivity_position + '</div>';
    html += '   <div class="col-md-4">';
    html += '     <input type="text" value="'+this.subactivity_value+'" val-id="'+this.subactivity_id+'" id="subactivity_'+this.subactivity_activity+'_'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="act_sub_val act_sub_val_'+ this.subactivity_activity+' form-control">';
    html += '     <span class = "er-msg"></span>';
    html += '   </div>';
    html += '   <div class="col-md-4">';
    if(last_btn){
      if(this.subactivity_position == last_btn){
        html += '     <button type="submit" style="border: none; padding: 5px;" position="'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="btn_sub_add btn btn-default btn_sub_add_'+this.subactivity_activity+'_'+this.subactivity_position+'" id="add_sub_activity" title="Add Sub-activity">';
        html += '       <i class="glyphicon glyphicon-plus" ></i>';
        html += '     </button>';
      }
    } else {
        html += '     <button type="submit" style="border: none; padding: 5px;" position="'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="btn_sub_add btn btn-default btn_sub_add_'+this.subactivity_activity+'_'+this.subactivity_position+'" id="add_sub_activity" title="Add Sub-activity">';
        html += '       <i class="glyphicon glyphicon-plus" ></i>';
        html += '     </button>';
    }
    html += '     <button type="submit" style="border: none; padding: 5px;" val-id="'+this.subactivity_id+'" position="'+this.subactivity_position+'" activity="'+this.subactivity_activity+'" class="btn_sub_add btn btn-default" id="remove_sub_activity" title="Remove Sub-activity">';
    html += '       <i class="glyphicon glyphicon-minus" ></i>';
    html += '     </button>';
    html += '   </div>';
    html += '   <div class="clearfix"></div>';
    html += '</div>';
    $(html).insertAfter(element);
    arrange_activity();
  };



  Template.prototype.set_activity_value = function(activity_value) {
    this.activity_value = activity_value;
  };
  Template.prototype.set_activity_position = function(activity_position) {
    this.activity_position = activity_position;
  };
  Template.prototype.set_activity_id = function(activity_id) {
    this.activity_id = activity_id;
  };

  Template.prototype.append_activity = function(element, last_btn){
    var html = '';
    html += '<div class="col-md-12 activity_' + this.activity_position + ' activity_div pad-5">';
    html += '     <div class="col-md-2 activity_title" style="text-align: right;">Activity ' + this.activity_position + '</div>';
    html += '     <div class="col-md-4">';
    html += '         <input type="text" id="activity_' + this.activity_position + '"  val-id="'+this.activity_id+'" position="'+this.activity_position+'" value="'+this.activity_value+'" class="act_val form-control">';
    html += '         <span class = "er-msg"></span>';
    html += '     </div>';
    html += '     <div class="col-md-4">';
    html += '       <button type="submit" style="border: none; padding: 5px;" class="btn btn-default btn_activity_add_'+this.activity_position+'" id="add_activity" role="'+this.activity_position+'" position="'+this.activity_position+'" title="Add Activity">';
    html += '         <i class="glyphicon glyphicon-plus "></i> Activity';
    html += '       </button>';
    if(this.activity_position > 1) {
      html += '       <button type="submit" style="border: none; padding: 5px;" val-id="'+this.activity_id+'" class="btn btn-default" id="remove_activity" position="'+this.activity_position+'" title="Remove Activity">';
      html += '         <i class="glyphicon glyphicon-minus "></i> Activity';
      html += '       </button>';
    }
    html += '       <button type="submit" style="border: none; padding: 5px;" class="btn btn-default btn_sub_add_'+this.activity_position+'_0" id="add_sub_activity" activity="'+this.activity_position+'" position="0" title="Add Sub-activity">';
    html += '         <i class="glyphicon glyphicon-plus "></i> Sub-Activity';
    html += '       </button>';
    html += '     </div>';
    html += '     <div class="sub_div" style="margin-top: 5px;" id="activity_sub_'+ this.activity_position+'"></div>';
    html += '</div>';
    $(element).append(html);
    arrange_activity();
  }

  Template.prototype.next_activity = function(element, last_btn){
    var html = '';
    html += '<div class="col-md-12 activity_' + this.activity_position + ' activity_div pad-5">';
    html += '     <div class="col-md-2 activity_title" style="text-align: right;">Activity ' + this.activity_position + '</div>';
    html += '     <div class="col-md-4">';
    html += '         <input type="text" id="activity_' + this.activity_position + '"  val-id="'+this.activity_id+'" position="'+this.activity_position+'" value="'+this.activity_value+'" class="act_val form-control">';
    html += '         <span class = "er-msg"></span>';
    html += '     </div>';
    html += '     <div class="col-md-4">';
    if(last_btn){
      if(this.activity_position == last_btn){
          html += '       <button type="submit" style="border: none; padding: 5px;" class="btn btn-default  btn_activity_add_'+this.activity_position+'" id="add_activity" role="'+this.activity_position+'" position="'+this.activity_position+'" title="Add Activity">';
          html += '         <i class="glyphicon glyphicon-plus "></i> Activity';
          html += '       </button>';
      }
    } else {
      html += '       <button type="submit" style="border: none; padding: 5px;" class="btn btn-default btn_activity_add_'+this.activity_position+'" id="add_activity" role="'+this.activity_position+'" position="'+this.activity_position+'" title="Add Activity">';
      html += '         <i class="glyphicon glyphicon-plus "></i> Activity';
      html += '       </button>';
    }
    if(this.activity_position > 1) {
      html += '       <button type="submit" style="border: none; padding: 5px;" val-id="'+this.activity_id+'" class="btn btn-default" id="remove_activity" position="'+this.activity_position+'" title="Remove Activity">';
      html += '         <i class="glyphicon glyphicon-minus "></i> Activity';
      html += '       </button>';
    }
    html += '       <button type="submit" style="border: none; padding: 5px;" class="btn btn-default btn_sub_add_'+this.activity_position+'_0" id="add_sub_activity" activity="'+this.activity_position+'" position="0" title="Add Sub-activity">';
    html += '         <i class="glyphicon glyphicon-plus "></i> Sub-Activity';
    html += '       </button>';
    html += '     </div>';
    html += '     <div class="sub_div" style="margin-top: 5px;" id="activity_sub_'+ this.activity_position+'"></div>';
    html += '</div>';
    $(html).insertAfter(element);
    arrange_activity();
  }



  Template.prototype.set_element_value = function(element_value) {
    this.element_value = element_value;
  };
  Template.prototype.set_element_id = function(element_id) {
    this.element_id = element_id;
  };
  Template.prototype.set_element_position = function(element_position) {
    this.element_position = element_position;
  };

  Template.prototype.append_element = function(element, last_btn) {
    count_element++;
    var html = '';
    html += '<div id="element_item_'+ this.element_position + '" class="col-md-12 pad-0 element">';
    html += '   <div class="col-md-1 col-xs-1" style="text-align: right;padding-right: 0px;">';
    html += '     <div class="btn-group">';
    html += '       <button style="border: none; padding: 5px;" class="btn btn-default element_down" order="1" position='+ this.element_position+'><span class="glyphicon glyphicon-chevron-down"></span></button>';
    html += '     <button style="border: none; padding: 5px;" class="btn btn-default element_up" order="1" position='+ this.element_position+'><span class="glyphicon glyphicon-chevron-up"></span></button>';
    html += '     </div>';
    html += '   </div>';
    html += '   <div class="col-md-11 col-xs-10 pad-0">';
    html += '       <div class="col-md-12 elem_div pad-5">';
    html += '           <div class="col-md-2 el_text">Element '+ toLetters(this.element_position) + '</div>';
    html += '           <div class="col-md-5">';
    html += '             <input type="text" id="element_value_'+ this.element_position + '" class="elem_val form-control" data-order="" val-id="'+this.element_id+'" position='+ this.element_position+' value="'+this.element_value+'">';
    html += '             <span class= "er-msg"></span>  ';
    html += '           </div>';
    html += '           <div class="col-md-5">';
    html += '             <button style="border: none; padding: 5px;" type="submit" data-div-element="element_item_'+ this.element_position + '" id="add_element" role="'+ this.element_position+'" position='+ this.element_position+' class="btn btn-default text-success element_add btn_element_add_'+ this.element_position+'"><i class="glyphicon glyphicon-plus"></i> Element</button>';
    html += '               <button style="border: none; padding: 5px;" type="submit" id="remove_element" class="btn btn-default text-success"  val-id="'+this.element_id+'" position='+ this.element_position+'><i class="glyphicon glyphicon-minus"></i> Element</button>';
    html += '           </div>';
    html += '           <div class="clearfix"></div>';
    html += '       </div>';
    html += '       <div id="element_question_div" class="element_question_'+ this.element_position + '">';
    html += '       </div>';
    html += '   </div>';
    html += '</div>';

    $(element).append(html);

  };
  Template.prototype.next_element = function(element, last_btn) {
    count_element++;
    var html = '';
    html += '<div id="element_item_'+ this.element_position + '" class="col-md-12 pad-0 element">';
    html += '   <div class="col-md-1 col-xs-1" style="text-align: right;padding-right: 0px;">';
    html += '     <div class="btn-group">';
    html += '       <button style="border: none; padding: 5px;" class="btn btn-default element_down" order="1" position='+ this.element_position+'><span class="glyphicon glyphicon-chevron-down"></span></button>';
    html += '     <button style="border: none; padding: 5px;" class="btn btn-default element_up" order="1" position='+ this.element_position+'><span class="glyphicon glyphicon-chevron-up"></span></button>';
    html += '     </div>';
    html += '   </div>';
    html += '   <div class="col-md-11 col-xs-10 pad-0">';
    html += '       <div class="col-md-12 elem_div pad-5">';
    html += '           <div class="col-md-2 el_text">Element '+ toLetters(this.element_position) + '</div>';
    html += '           <div class="col-md-5">';
    html += '             <input type="text" id="element_value_'+ this.element_position + '" class="elem_val form-control" data-order="" val-id="'+this.element_id+'" position='+ this.element_position+' value="'+this.element_value+'">';
    html += '             <span class= "er-msg"></span>  ';
    html += '           </div>';
    html += '           <div class="col-md-5">';
    html += '             <button style="border: none; padding: 5px;" type="submit" data-div-element="element_item_'+ this.element_position + '" id="add_element" role="'+ this.element_position+'" position='+ this.element_position+' class="btn btn-default text-success element_add btn_element_add_'+ this.element_position+'"><i class="glyphicon glyphicon-plus"></i> Element</button>';
    html += '               <button style="border: none; padding: 5px;" type="submit" id="remove_element" class="btn btn-default text-success"  val-id="'+this.element_id+'" position='+ this.element_position+'><i class="glyphicon glyphicon-minus"></i> Element</button>';
    html += '           </div>';
    html += '           <div class="clearfix"></div>';
    html += '       </div>';
    html += '       <div id="element_question_div" class="element_question_'+ this.element_position + '">';
    html += '       </div>';
    html += '   </div>';
    html += '</div>';

    console.log(element);
    $(html).insertAfter(element);
    arrange_question();

  };

  Template.prototype.set_question_value = function(question_value) {
    this.question_value = question_value;
  };
  Template.prototype.set_question_default_yes = function(question_default_yes) {
    this.question_default_yes = question_default_yes;
  };
  Template.prototype.set_question_id = function(question_id) {
    this.question_id = question_id;
  };
  Template.prototype.set_question_position = function(question_position) {
    this.question_position = question_position;
  };
  Template.prototype.set_question_element = function(question_element) {
    this.question_element = question_element;
  };
  Template.prototype.set_question_required_remarks = function(question_required_remarks) {
    if(question_required_remarks == "1"){
      this.question_required_remarks = "checked";
    } else{
      this.question_required_remarks = "";
    }
  };

  Template.prototype.append_question = function() {

    var id = this.question_element+"_"+this.question_position;
    arr.push({element_question : this.question_element, id: id });

    var html = '';
    html += '<div class="col-md-12 question_div pad-5" data-elem='+this.question_element+' id="question_item_'+ this.question_element + '_' + this.question_position +'">';
    html += ' <div class="col-md-2 col-xs-2 pad-0 quest-title-ctr">';
    html += '   <div class="col-md-4 col-xs-4 pad-0">';
    html += '     <div class="btn-group">'
    if(this.question_position > 1) {
      html += '     <button style="border: none; padding: 5px;" class="btn btn-default question_up" position="'+this.question_position+'" element="'+this.question_element+'" order="1">';
      html += '       <span class="glyphicon glyphicon-chevron-up" ></span>';
      html += '     </button>';
    }
    html += '       <button style="border: none; padding: 5px;" class="btn btn-default question_down" position="'+this.question_position+'" element="'+this.question_element+'" order="1">';
    html += '         <span class="glyphicon glyphicon-chevron-down"></span>';
    html += '       </button>';
    html += '     </div>';
    html += '   </div>';
    html += '   <div class="col-md-8 col-xs-8 pad-0"><span class="qtext">Question '+ toLetters(this.question_element) + this.question_position +'</span></div>';
    html += ' </div>';
    html += ' <div class="col-md-8 col-xs-10 ">';
    html += '   <textarea id="question_value_'+ this.question_element + '_' + this.question_position +'"  data-elem= 1 val-id="'+this.question_id+'" class="ques_val form-control">'+ this.question_value +'</textarea>';
    html += '   <span class = "er-msg"></span>';
    html += '   <br>Default Answer for yes: <input id="default_yes_'+ this.question_element + '_' + this.question_position +'"  val-id="'+this.question_id+'" class="ques_val_yes form-control " type="text" value="'+ this.question_default_yes +'"> ';
    html += '   <span class = "er-msg"></span> ';
    html += '   <input id="required_'+ this.question_element + '_' + this.question_position +'" val-id="'+this.question_id+'" type="checkbox" '+ this.question_required_remarks + ' class="ques_required"> Require Remarks';
    html += ' </div>';
    html += ' <div class="col-md-2">';
    html += '     <div class="btn-group">';
    html += '       <button type="submit" style="border: none; padding: 5px;" id="add_question" role="'+this.question_position +'" position="'+this.question_position+'" element="'+this.question_element+'" class="btn btn-default btn_question_add_'+this.question_element+' btn_question_add_'+this.question_element+'_'+this.question_position+'">';
    html += '         <i class="glyphicon glyphicon-plus " ></i>';
    html += '       </button>';
    if(this.question_position > 1) {
      html += '       <button type="submit" style="border: none; padding: 5px;" id="remove_question" val-id="'+this.question_id+'" position="'+this.question_position+'" element="'+this.question_element+'" class="btn btn-default">';
      html += '         <i class="glyphicon glyphicon-minus " ></i>';
      html += '       </button>';
    }
    html += '     </div>';
    html += ' </div>';
    html += '</div>';

    $('.element_question_'+this.question_element).append(html);
    arrange_question();
  };

  Template.prototype.next_question = function(element_id) {

    var id = this.question_element+"_"+this.question_position;
    arr.push({element_question : this.question_element, id: id });

    var html = '';
    html += '<div class="col-md-12 question_div pad-5" data-elem='+this.question_element+' id="question_item_'+ this.question_element + '_' + this.question_position +'">';
    html += ' <div class="col-md-2 col-xs-2 pad-0 quest-title-ctr">';
    html += '   <div class="col-md-4 col-xs-4 pad-0">';
    html += '     <div class="btn-group">'
    if(this.question_position > 1) {
      html += '     <button style="border: none; padding: 5px;" class="btn btn-default question_up" position="'+this.question_position+'" element="'+this.question_element+'" order="1">';
      html += '       <span class="glyphicon glyphicon-chevron-up" ></span>';
      html += '     </button>';
    }
    html += '       <button style="border: none; padding: 5px;" class="btn btn-default question_down" position="'+this.question_position+'" element="'+this.question_element+'" order="1">';
    html += '         <span class="glyphicon glyphicon-chevron-down"></span>';
    html += '       </button>';
    html += '     </div>';
    html += '   </div>';
    html += '   <div class="col-md-8 col-xs-8 pad-0"><span class="qtext">Question '+ toLetters(this.question_element) + this.question_position +'</span></div>';
    html += ' </div>';
    html += ' <div class="col-md-8 col-xs-10 ">';
    html += '   <textarea id="question_value_'+ this.question_element + '_' + this.question_position +'"  data-elem= 1 val-id="'+this.question_id+'" class="ques_val form-control">'+ this.question_value +'</textarea>';
    html += '   <span class = "er-msg"></span>';
    html += '   <br>Default Answer for yes: <input id="default_yes_'+ this.question_element + '_' + this.question_position +'"  val-id="'+this.question_id+'" class="ques_val_yes form-control " type="text" value="'+ this.question_default_yes +'"> ';
    html += '   <span class = "er-msg"></span> ';
    html += '   <input id="required_'+ this.question_element + '_' + this.question_position +'" val-id="'+this.question_id+'" type="checkbox" '+ this.question_required_remarks + ' class="ques_required"> Require Remarks';
    html += ' </div>';
    html += ' <div class="col-md-2">';
    html += '     <div class="btn-group">';
    html += '       <button type="submit" style="border: none; padding: 5px;" id="add_question" role="'+this.question_position +'" position="'+this.question_position+'" element="'+this.question_element+'" class="btn btn-default btn_question_add_'+this.question_element+' btn_question_add_'+this.question_element+'_'+this.question_position+'">';
    html += '         <i class="glyphicon glyphicon-plus " ></i>';
    html += '       </button>';
    if(this.question_position > 1) {
      html += '       <button type="submit" style="border: none; padding: 5px;" id="remove_question" val-id="'+this.question_id+'" position="'+this.question_position+'" element="'+this.question_element+'" class="btn btn-default">';
      html += '         <i class="glyphicon glyphicon-minus " ></i>';
      html += '       </button>';
    }
    html += '     </div>';
    html += ' </div>';
    html += '</div>';

    // $('.element_question_'+this.question_element).append(html);
    $(html).insertAfter(element_id);
    arrange_question();


  };

  function arrange_question(){
    console.log("arrange question start");
    var element_count = 0;
    var element_order = 0;
    var count = 0;
    var length = $('.element').length;
    var max_element = 25;
    var max_question = 70;
    $('.element').each(function(index, element) {
        element_count++;
        element_order++;

        console.log(element_count);
        //check max
        if(max_element <= element_count){
          $('.element_add').prop("disabled",true);
        } else {
          $('.element_add').prop("disabled",false);
        }

        $(this).attr("data-order",element_order);
        $(this).attr("id","element_item_" + element_order);
        var $this = $(this);
        $(this).find('#add_element').attr("data-order",element_order);
        $(this).find('#add_element').attr("position",element_order);
        $(this).find('#add_element').attr("data-div-element","element_item_" + element_order);

        $(this).find('#remove_elementt').attr("data-orde",element_order);
        $(this).find('#remove_element').attr("position",element_order);

        $(this).find('#element_question_div').attr("class","element_question_" + element_order);
        $(this).find('.question_div').attr("data-elem",element_order);


        $(this).find('.element_down').attr("position",element_order);
        $(this).find('.element_down').attr("order",element_order);

        $(this).find('.element_up').attr("position",element_order);
        $(this).find('.element_up').attr("order",element_order);

        if(element_count == 1){
          $(this).find('.element_up').hide();
          $(this).find('#remove_element').hide();
        } else {
          $(this).find('.element_up').show();
          $(this).find('#remove_element').show();
        }

        if (index == (length - 1)) {
          $(this).find('.element_down').hide();
        } else {
          $(this).find('.element_down').show();
        }


        $(this).find('.el_text').html("Element " + toLetters(element_count));
        $(this).find('.elem_val').attr("data-order",element_order);
        var $items = $this.find(".question_div ");

        count = 0;
        $.each($items, function(n, e)
        {
          count ++;
          console.log(count);
          if(max_question <= count){
            $('.btn_question_add_' + element_count).prop("disabled",true);
          } else {
            $('.btn_question_add_' + element_count).prop("disabled",false);
          }

          
          if(count == 1){
            if($items.length > 1){
              $(this).find('.question_down').show();
            } else {
              $(this).find('.question_down').hide();
            }
            
          } else {
            $(this).find('.question_down').show();
          }

          $(this).find('.qtext').html("Question " + toLetters(element_count) + count);
          $(this).find('.ques_val').attr("data-order",count);
           $(this).find('.question_div').attr("id","question_item_" + element_order + "_" + count);
        });

        if($items.length > 1){
          $($items).last().find('.question_down').hide();
        }
       
    });

    $(".element").last().find('.element_down').hide();
  }

  function arrange_activity(){
    console.log("arrange activity start");
    var element_count = 0;
    var count = 0;
    $('.activity_div').each(function() {
        element_count++;
        var $this = $(this);
        $(this).removeClass().addClass('col-md-12 activity_'+element_count+' activity_div pad-5');
        $(this).find('.activity_title').html("Acivity " + element_count);
        $(this).find('#add_activity').attr("position", element_count);

        $(this).find('.act_val').attr("order", element_count);

        $(this).find('.sub_div').attr("id","activity_sub_"+ element_count);


        $(this).find('#add_sub_activity').attr("activity", element_count);
        $(this).find('#add_sub_activity').removeClass().addClass('btn btn-default btn_sub_add_'+element_count+'_0');
        var $items = $this.find(".sub_div .subactivity");

        count = 0;
        $.each($items, function(n, e)
        {

          $(this).find('.btn_sub_add').attr('position', count);


          count ++;
          $(this).find('.stext').html("Sub-Acivity " + count);
          $(this).find('.act_sub_val').attr('activity', count);
          $(this).find('#add_sub_activity').attr("position", count);
          $(this).removeClass().addClass('col-md-12 subactivity subactivity_'+element_count+'_'+count);
          $(this).find(".act_sub_val").removeClass().addClass('act_sub_val act_sub_val_'+element_count+' form-control');
          $(this).find(".act_sub_val").attr("order",count);

        });

    });
  }

  $(document).on('click', '#add_activity', function() {

      var error = 0;
      var position = $(this).attr('position');
      var my_value = $('#activity_'+position).val();

      $('#activity_'+position).css('border-color', '#ccc');

      $('#activity_'+position).each(function() {
        if ($(this).val() == 0) {
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        } else {
            if($(this).val()  == 0 || $(this).val() == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                error++;
            } else {
              $(this).css('border-color','#ccc');
              $(this).next().hide();
            }
        }
      })

      if (error == 0) {
        position_next = parseInt(position) + 1;
        var activity = new Template();
        activity.set_activity_value("");
        activity.set_activity_id(0);
        activity.set_activity_position(position_next);
        activity.next_activity(".activity_" + position);
      }
  });

  $(document).on('click', '#remove_sub_activity', function() {
    var position = $(this).attr('position');
    var activity = $(this).attr('activity');
    var val_id = $(this).attr('val-id');
    var dialog = bootbox.confirm({
          message: "Are you sure you want to remove this Sub-activity?",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
              if(result == true){

                  if(val_id != 0){
                      //DELETE SUB ACTIVITY
                      aJax.post(
                          "http://localhost/sams/global_controller/trash_data",
                          {
                              query: "sub_item_id = " + val_id,
                              table:"tbl_sub_activities"
                          },
                          function(result){

                          }
                      );
                  }
                  $('.subactivity_'+activity+'_'+position).remove();
                  arrange_activity();
                  // $('.btn_sub_add_'+activity+'_'+parseInt(position-1)).show();
              }
          }
        });
  });

  $(document).on('click', '#remove_activity', function() {
    var position = $(this).attr('position');
    var val_id = $(this).attr('val-id');
    var dialog = bootbox.confirm({
          message: "Are you sure you want to remove this activity?<br> Note: All items under it will also be erased.",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
              if(result == true){

                  if(val_id != 0){
                      //DELETE ACTIVITY
                      aJax.post(
                          base_url + "/global_controller/trash_data",
                          {
                              query: "activity_id = " + val_id,
                              table:"tbl_activities"
                          },
                          function(result){

                          }
                      );

                      //DELETE SUB ACTIVITIES UNDER ACTIVITY
                      aJax.post(
                          base_url + "/global_controller/trash_data",
                          {
                              query: "activity_id = " + val_id,
                              table:"tbl_sub_activities"
                          },
                          function(result){

                          }
                      );
                  }

                  $('.activity_'+position).remove();
                  $('.btn_activity_add_'+parseInt(position-1)).show();
                  arrange_activity();
              }
          }
        });
  });

  $(document).on('click', '#remove_element', function() {
    var position = $(this).attr('position');
    var val_id = $(this).attr('val-id');
    var dialog = bootbox.confirm({
          message: "Are you sure you want to remove this element?<br> Note: All items under it will also be erased.",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
              if(result == true){

                  if(val_id != 0){
                      //DELETE ELEMENT
                      aJax.post(
                          base_url + "/global_controller/trash_data",
                          {
                              query: "element_id = " + val_id,
                              table:"tbl_elements"
                          },
                          function(result){

                          }
                      );

                      //DELETE QUESTIONS UNDER THIS ELEMENT
                      aJax.post(
                          base_url + "/global_controller/trash_data",
                          {
                              query: "element_id = " + val_id,
                              table:"tbl_questions"
                          },
                          function(result){

                          }
                      );
                  }

                  $('#element_item_'+position).remove();
                  $('.btn_element_add_'+parseInt(position-1)).show();
                  arrange_question();
                  count_element = count_element - 1;

              }
          }
        });
  });

  $(document).on('click', '#remove_question', function() {
    var position = $(this).attr('position');
    var element = $(this).attr('element');
    var val_id = $(this).attr('val-id');

    var dialog = bootbox.confirm({
          message: "Are you sure you want to remove this question?",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
              if(result == true){

                  if(val_id != 0){
                      //DELETE QUESTION
                      aJax.post(
                          base_url + "/global_controller/trash_data",
                          {
                              query: "question_id = " + val_id,
                              table:"tbl_questions"
                          },
                          function(result){

                          }
                      );
                  }

                  var id = element+"_"+position;
                  arr.push({element_question : element, id: id });

                  var value = element+'_'+position;
                  arr = arr.filter(function(obj) {return obj.id != value;});
                  console.log(arr);

                  $('#question_item_'+element+'_'+position).remove();
                  $('.btn_question_add_'+element+'_'+parseInt(position-1)).show();
                  arrange_question();
              }
          }
        });
  });



  $(document).on('click', '#add_sub_activity', function() {

        var error = 0;
        var position = $(this).attr('position');
        var activity = $(this).attr('activity');
        var subactivity_value = $('#subactivity_'+activity+'_'+position).val();

        $('#subactivity_'+activity+'_'+position).css('border-color', '#ccc');
        $('#subactivity_'+activity+'_'+position).each(function() {
          if ($(this).val() == 0) {
              $(this).css('border-color', 'red');
              $(this).next().show();
              error++;
          } else {
              if($(this).val()  == 0 || $(this).val() == "0"){
                  $(this).css('border-color','red');
                  $(this).next().html("Invalid Input.");
                  $(this).next().show();
                  error++;
              } else {
                $(this).css('border-color','#ccc');
                $(this).next().hide();
              }
          }
        })

        if (error == 0) {
          var new_position = parseInt(position) + 1;
          var element = new Template();
          element.set_subactivity_value("");
          element.set_subactivity_id(0);
          element.set_subactivity_position(new_position);
          element.set_subactivity_activity(activity);

          console.log('.subactivity_'+activity+'_'+position);

          if(position != 0){
            element.next_subactivity('.subactivity_'+activity+'_'+position);
          } else {
            element.append_subactivity();
          }
        }
  });


  $(document).on('click', '#add_question', function() {

      var error = 0;
      var position = $(this).attr('position');
      var element_id = $(this).attr('element');
      var question_value = $('#question_value_'+position).val();
      var answer_value = $('#default_yes_'+position).val();

      $('#question_value_'+element_id+'_'+position).css('border-color', '#ccc');
      $('#question_value_'+element_id+'_'+position).each(function() {
        if ($(this).val() == 0) {
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        } else {
            if($(this).val()  == 0 || $(this).val() == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                error++;
            } else {
              $(this).css('border-color','#ccc');
              $(this).next().hide();
            }
        }
      })

      $('#default_yes_'+element_id+'_'+position).css('border-color', '#ccc');
      $('#default_yes_'+element_id+'_'+position).each(function() {
        if ($(this).val() == 0) {
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        } else {
            if($(this).val()  == 0 || $(this).val() == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                error++;
            } else {
              $(this).css('border-color','#ccc');
              $(this).next().hide();
            }
        }
      })

      // var current_q = arr.filter(value => value.element_question == element_id).length;
      var current_q = arr.filter(function(value) { return value.element_question == element_id; });
      console.log(current_q.length);

      // if(current_q.length == 70){
      //   error++;
      //   bootbox.alert("Maximun <b>Questions</b> per Element Exceeded!");
      // }

      if (error == 0) {
        var new_position = parseInt(position) + 1;
        var element = new Template();
        element.set_question_value("");
        element.set_question_id(0);
        element.set_question_position(new_position);
        element.set_question_element(element_id);
        element.next_question("#question_item_"+element_id+"_"+position);

        // if($(this).attr('role') != "1"){
        //     $(this).hide();
        // }
      }

  });

  function unique_element (element) {
    var values = {};
    var count = 0;
    var checks = $(element);

    checks.each(function(i, elem)
    {
        if(elem.value in values) {
            count++ ;
            $(elem).css('border-color','red');
            $(elem).next().html("Please enter a Unique Value.");
            $(values[elem.value]).css('border-color','red');
            $(values[elem.value]).next().html("Please enter a Unique Value.");
            $(values[elem.value]).next().show();
        } else {
            values[elem.value] = elem;
        }
    });

    return count;
}

  $(document).on('click', '#add_element', function() {

      var error = 0;
      var position = $(this).attr('data-order');
      var my_value = $('#element_value_'+position).val();

      $('#element_value_'+position).css('border-color', '#ccc');


      $('#element_value_'+position).each(function() {
        if ($(this).val() == "") {
            $(this).css('border-color', 'red');
            $(this).next().show();
            error++;
        } else {
            if($(this).val()  == 0 || $(this).val() == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                error++;
            } else {
              $(this).css('border-color','#ccc');
              $(this).next().hide();
            }
        }
      })

      $('.element_question_'+position+' .question_div').each(function(){
          var question = $(this).find('.ques_val').val();
          var default_yes = $(this).find('.ques_val_yes').val();

          if (question == "") {
              $(this).find('.ques_val').css('border-color', 'red');
              $(this).find('.ques_val').next().show();
              error++;
          } else {
              if(question  == 0 || question == "0"){
                  $(this).find('.ques_val').css('border-color','red');
                  $(this).find('.ques_val').next().html("Invalid Input.");
                  $(this).find('.ques_val').next().show();
                  error++;
              } else {
                $(this).find('.ques_val').css('border-color','#ccc');
                $(this).find('.ques_val').next().hide();
              }
          }

          if (default_yes == "") {
              $(this).find('.ques_val_yes').css('border-color', 'red');
              $(this).find('.ques_val_yes').next().show();
              error++;
          } else {
              if(question  == 0 || question == "0"){
                  $(this).find('.ques_val_yes').css('border-color','red');
                  $(this).find('.ques_val_yes').next().html("Invalid Input.");
                  $(this).find('.ques_val_yes').next().show();
                  error++;
              } else {
                $(this).find('.ques_val_yes').css('border-color','#ccc');
                $(this).find('.ques_val_yes').next().hide();
              }
          }
      });

      // if(count_element == 20){
      //   error++;
      //   bootbox.alert("Maximun Active <b>Element</b> Exceeded!");
      // }

      if (error == 0) {
        var current_div = "#" + $(this).attr("data-div-element");
        var element = new Template();
        element.set_element_value("");
        element.set_element_id(0);
        element.set_element_position(parseInt(position)+ 1);
        element.next_element(current_div);

        arrange_question();

        element.set_question_value("");
        element.set_question_id(0);
        element.set_question_position(1);
        element.set_question_element(parseInt(position)+ 1);
        element.append_question();

      }
  });



  $(document).on('click', '.element_up', function() {
      var position = $(this).attr('position');
      var my_div = "#element_item_" + position;
      var up_div = "#element_item_" + parseInt(position - 1);

      var my_value = $('#element_value_'+position).val();
      var up_value = $('#element_value_'+ parseInt(position - 1)).val();

      $(my_div).detach().insertBefore(up_div);
      arrange_question()

      // $('#element_value_'+position).val(up_value);
      // $('#element_value_'+parseInt(position - 1)).val(my_value);
  });

  $(document).on('click', '.element_down', function() {

      var position = $(this).attr('position');
      var downposition = parseInt(position) + 1;

      var my_div = "#element_item_" + position;
      var down_div = "#element_item_" + downposition;

      // console.log(down_div);

      var my_value = $('#element_value_'+position).val();
      var down_value = $('#element_value_'+ downposition).val();

      $(down_div).detach().insertBefore(my_div);
      arrange_question()
  });

  $(document).on('click', '.question_up', function() {
      var position = $(this).attr('position');
      var element_id = $(this).attr('element');
      var my_value = $('#question_value_'+element_id+'_'+position).val();
      var up_value = $('#question_value_'+element_id+'_'+parseInt(position - 1)).val();

      var my_value_default = $('#default_yes_'+element_id+'_'+position).val();
      var up_value_default = $('#default_yes_'+element_id+'_'+parseInt(position - 1)).val();

      $('#question_value_'+element_id+'_'+position).val(up_value);
      $('#question_value_'+element_id+'_'+parseInt(position - 1)).val(my_value);

      $('#default_yes_'+element_id+'_'+position).val(up_value_default);
      $('#default_yes_'+element_id+'_'+parseInt(position - 1)).val(my_value_default);
  });

  $(document).on('click', '.question_down', function() {

      var position = $(this).attr('position');
      var element_id = $(this).attr('element');
      var downposition = parseInt(position) + 1;


      var my_value = $('#question_value_'+element_id+'_'+position).val();
      var down_value = $('#question_value_'+element_id+'_'+ downposition).val();

      var my_value_default = $('#default_yes_'+element_id+'_'+position).val();
      var down_value_default = $('#default_yes_'+element_id+'_'+ downposition).val();

      $('#question_value_'+element_id+'_'+position).val(down_value);
      $('#question_value_'+element_id+'_'+downposition).val(my_value);

      $('#default_yes_'+element_id+'_'+position).val(down_value_default);
      $('#default_yes_'+element_id+'_'+downposition).val(my_value_default);
  });


  function toLetters(num) {
    var mod = num % 26;
    var pow = num / 26 | 0;
    var out = mod ? String.fromCharCode(64 + mod) : (pow--, 'Z');
    return pow ? toLetters(pow) + out : out;
  }

  function FormValidator(element) {
    var counter = 0;
    $(element).each(function(){
          var input = $(this).val().trim();
          if (input.length == 0) {
            $(this).css('border-color','red');
            $(this).next().show();
            counter++;
          }else{
            if(input == 0 || input == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                counter++;
            } else {
              $(this).css('border-color','#ccc');
              $(this).next().hide();
            }
          }
      });
    return counter;
}

var Geolocation = function() {
  this.country_id = '';
  this.province_id = '';
  this.selected_country = '';
};

Geolocation.prototype.select_country = function(selected_country) {
  this.selected_country = selected_country;
}

Geolocation.prototype.get_countries = function(element) {
  var selectedcountry = this.selected_country;
    $.getJSON( "../asset/geoloc/country.json", function( callback ) {
        var html = "";
        if(this.selected_country != ""){
          html += "<option value='' selected disabled>Select..</option>";
        }
        var result = callback.sort(compare);
        $.each(callback, function(x,y){
            if(y.name != ""){
                if(selectedcountry == y.id) {
                  html += "<option selected value='"+ y.id+"_"+y.code.toUpperCase()+"'>"+ y.name+"</option>";
                } else {
                  html += "<option value='"+ y.id+"_"+y.code.toUpperCase()+"'>"+ y.name+"</option>";
                }

              }
        });
        $(element).html("");
        $(element).html(html);
    });
};

Geolocation.prototype.set_country = function(country_id) {
  var countrycode = country_id.split("_");
  this.country_id = countrycode[0];
}

Geolocation.prototype.get_province = function(element) {
  var count = 0;
  $.getJSON( "../asset/geoloc/regions/" + this.country_id + ".json", function( callback ) {
      var html = "";
      html += '<select id="province" class = "inputcss form-control fullwidth inputs_site">';
      html += "   <option value='' selected disabled>Select..</option>";

      var result = callback.sort(compare);
      $.each(callback, function(x,y){
          if(y.name != ""){
              count++;
              html += "<option value='"+ y.id+"'>"+ y.name+"</option>";
          }
      });
      html += "<option value='xx'>Other</option>";

      html +='</select>';
      html += '<span class = "er-msg">Province should not be empty *</span></div>';

      if(count == 0){
        html = '<input class = "inputcss form-control fullwidth inputs_site" id="province" placeholder="Enter Province/Region Name". />';
        html += '<span class = "er-msg">Province should not be empty *</span></div>';
      }
      $(element).html("");
      $(element).html(html);

  });
}

Geolocation.prototype.set_province = function(province_id) {
  this.province_id = province_id;
}

Geolocation.prototype.get_cities = function(element) {
  var count = 0;
  $.getJSON( "../asset/geoloc/cities/" + this.province_id + ".json", function( callback ) {
      var html = "";
      html += '<select id="city" class = "inputcss form-control fullwidth inputs_site">';
      html += "<option value='' selected disabled>Select..</option>";

      var result = callback.sort(compare);
      $.each(callback, function(x,y){
          if(y.name != ""){
              count++;
              html += "<option value='"+ y.id+"'>"+ y.name+"</option>";
          }
      });

      html += "<option value='xx'>Other</option>";

      html +='</select>';
      html += '<span class = "er-msg">Province should not be empty *</span></div>';

      if(count == 0){
        html = '<input class = "inputcss form-control fullwidth inputs_site" id="city" placeholder="Enter City Name". />';
        html += '<span class = "er-msg">City should not be empty *</span></div>';
      }

      $(element).html("");
      $(element).html(html);

  });
}


function compare(a, b) {
    if (a.name< b.name)
        return -1;
    if (a.name > b.name)
        return 1;
    return 0;
}


var Pagination = function() {
  this.total_page = '';
};

Pagination.prototype.set_total_page = function(total_page) {
  this.total_page = total_page;
};

Pagination.prototype.set = function(element) {
  var htm = "<div class='clearfix'></div>";
  htm += '<div class="btn-group" style="margin-top: 15px;">';
  htm += '  <button type="button" class="btn btn-default first-page">First</button>';
  htm += '  <button type="button" class="btn btn-default prev-page">Prev</button>';
  htm += '  <div class="btn-group dropup">';
  htm += '    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
  htm += '      <span class="pager_no">Page 1</span>';
  htm += '      <span class="glyphicon glyphicon-menu-down"></span>';
  htm += '    </button>';
  htm += '    <ul class="dropdown-menu" style="max-height: 200px; overflow: auto"">';
  for(var x =1; x<=this.total_page; x++){
    var pgno = x;
    htm += '    <li><a style="margin-left: 0px;" class="pg_no" href="#" data-value='+pgno+'>Page '+pgno+'</a></li>';
  }
  htm += '    </ul>';
  htm += '  </div>';
  htm += '  <button type="button" class="btn btn-default next-page">Next</button>';
  htm += '  <button type="button" class="btn btn-default last-page">Last</button>';
  htm += '</div>';

  // htm += '<button class="btn btn-default first-page btn-sm"><< First</button>';
  // htm += '<button class="btn btn-default prev-page btn-sm">< Previous</span></button>';
  htm += '<select class="form-control pager_number input-sm hidden" style="width: 70px;">';
  for(var x =1; x<=this.total_page; x++){
    var pgno = x;
    htm += "<option value='" + pgno + "'>" + pgno + "</option>";
  }
  htm += '</select>';
  // htm += '<button class="btn btn-default next-page btn-sm">Next ></button>';
  // htm += '<button class="btn btn-default last-page btn-sm">Last >></button>';
  $(element).html(htm);

  console.log(this.total_page);
  if(this.total_page < 2){
    $(element).hide();
  } else {
    $(element).show();
  }

};


  $(document).on('click', '.pg_no', function(e){
    e.preventDefault();
    var page_no = $(this).attr("data-value");
    $('.pager_number').val(page_no).change()
  });
