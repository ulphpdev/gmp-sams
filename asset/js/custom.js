
var inuse = 0;
function module_action(module, action, element, id){
	$(element).html("<center>Loading Form.. </center>");
	isLoading(true);
	if(action == "cancel"){
		$('.save').hide();
    	$('.update').hide();
    	$(element).html("");
    	$('.add').show();
	} else { 
		
		aJax.post(
	      	base_url + "global_controller/action_global",
	      	{
	        	id:id, 
	        	module:module, 
	        	type:action
	      	},
	      	function(result){
	      		var html = '';
	      		html += '<div class="panel panel-default">';
	      		html += result;
	      		html += '</div>';

	        	$(element).html(html);
	        	$('html, body').animate({
	          		scrollTop: $(element).offset().top
				});
				
				isLoading(false);

	        	if(action == "add"){
					$('.add').hide();
					$('.save').show();
		    		$('.update').hide();
		    		$('.cancel').show();
				} else {
		    		$('.add').show();
		    		$('.save').hide();
		    		$('.update').show();
		    		$('.cancel').show();
				}
	      	}
	    );
	}
	
}

function change_status_modal(message,datatype,cb){
	bootbox.confirm({
	   message: message,
	   buttons: {
		   confirm: {
			   label: 'Yes',
			   className: 'btn-primary'
			 
		   },
		   cancel: {
			   label: 'No',
			   className: 'btn-danger'
		   }
	   },
	   callback: cb
	}).find('.modal-footer .btn-primary').attr('data-type', datatype);
}

function confirm(message,cb){
	bootbox.confirm({
	   message: message,
	   buttons: {
		   confirm: {
			   label: 'Yes',
			   className: 'btn-primary'
			 
		   },
		   cancel: {
			   label: 'No',
			   className: 'btn-danger'
		   }
	   },
	   callback: cb
	});
}

function Ucfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function validateFields(element) {
    var counter = 0;
    $(element).each(function(){
    	if($(this).val() != null){
    		var input = $(this).val().trim();
	          if (input.length == 0) {
	          	$(this).css('border-color','red');
	            $(this).next().show();
	            counter++;
	          }else{
	            if(input == 0 || input == "0"){
	                $(this).css('border-color','red');
	                $(this).next().html("Invalid Input.");
	                $(this).next().show();
	                counter++;
	            } else {
	              $(this).css('border-color','#ccc');
	              $(this).next().hide();
	            }        	
	          }
    	} else {
    		$(this).css('border-color','red');
            $(this).next().show();
    	}
          
      });
    return counter;
  }

  function isValidEmailAddress(email) {
  	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  	return pattern.test(email);
  };

  function isLoading(visible){
	if(visible){
		bootbox.dialog({ 
			message: "Please wait...", 
			closeButton: false 
		});
	} else {
		$('.bootbox').modal('hide');
	}
  }
