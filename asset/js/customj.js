function substr2(string, count) {
        var s = '';
        var ctr = string.length;
    
        if(ctr > count) {
           s = string.substr(0, count)+'... ';
        } else {
           s = string;
        }

        return s;
}