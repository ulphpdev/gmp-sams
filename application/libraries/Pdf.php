<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf_gothic/tcpdf.php';

class Pdf extends TCPDF
{

    public $report_no;

    public $footer_right;

    public $status;

    public $set_center = 100;

    public function setReportNo($report_no){
        $this->report_no = $report_no;
    }

    public function setFooterRight($footer){
        $this->footer_right = $footer;
    }

    public function setCenter($center){
        $this->set_center = $center;
    }

    public function setStatus($status){
        $this->status = $status;
    }

    function __construct()

    {

        parent::__construct();

    }

    //Page header
    public function Header() {
        // Logo
        $image_file = getcwd().'/asset/img/SAMSULHeader.png';
        $this->Image($image_file, 14, 0, '', '30', 'PNG', '', 'C', false, 400, '', false, false, 0, false, false, false);



    }

    // Page footer
    public function Footer() {


        date_default_timezone_set("Asia/Manila");
        // Position at 15 mm from bottom
        $this->SetY(-20);
        // Set font
        $this->SetFont('font', 'B', 9);
        // Page number
       // $this->Cell(0, 0, "REPORT NO", 0, false, 'C', 0, '', 0, false, 'T', 'C');
        $this->Cell(0, 0, $this->report_no , 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->ln(1);
        $this->Line(5, $this->y + 5, $this->w - 5, $this->y + 5);

        $this->Cell(30, 15, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell($this->set_center, 15, date("d F Y H:i") . "H", 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(50, 15, $this->footer_right, 0, false, 'C', 0, '', 0, false, 'T', 'M');

        // if($this->status != 5){
        //     //watermark
        //     $image_file = getcwd().'/asset/img/draft.png';
        //     $this->Image($image_file, 0, 0, 200, 223, '', '', '', false, 400, 'C', false, false, 0);
        // }


    }

}
