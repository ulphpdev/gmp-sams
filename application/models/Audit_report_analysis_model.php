<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Audit_report_analysis_model extends CI_Model{
	function __construct() {
        parent::__construct();
    }

    public function get_report_analysis_list($limit,$offset){
        $query = $this->db->query('SELECT DISTINCT(a.report_id),a.report_no,b.name,
                                    CONCAT(DATE_FORMAT(a.audit_date_1,"%M")," ",DATE_FORMAT(a.audit_date_1,"%e"),"-",DATE_FORMAT(a.audit_date_2,"%e"),", ",DATE_FORMAT(a.audit_date_2,"%Y")) as audit_date,(SELECT COUNT(question_id) FROM tbl_report_answers where report_id = a.report_id GROUP BY report_id) as question_count 
                                    FROM tbl_report_summary as a
                                    LEFT JOIN tbl_company as b ON a.company_id = b.company_id
                                    LEFT JOIN tbl_report_answers as c ON a.report_id = c.report_id WHERE a.status <> -2 AND a.status <> 3 ORDER BY a.report_id DESC LIMIT '.$offset.','.$limit);
        $result = $query->result();
        return $result;
    }

    public function get_report_analysis_view($report_id){
        $query = $this->db->query('SELECT DISTINCT(a.report_id),a.report_no,b.name,
                                    CONCAT(DATE_FORMAT(a.audit_date_1,"%M")," ",DATE_FORMAT(a.audit_date_1,"%e"),"-",DATE_FORMAT(a.audit_date_2,"%e"),", ",DATE_FORMAT(a.audit_date_2,"%Y")) as audit_date,
                                    (SELECT COUNT(question_id) FROM tbl_report_answers where report_id = a.report_id GROUP BY report_id) as question_count,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 2) as yes,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 1) as no,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 3) as NA,
                                    (SELECT COUNT(answer_id) FROM tbl_report_answers where report_id = '.$report_id.' AND answer_id = 4) as NC,
                                    (SELECT COUNT(category_id) FROM tbl_report_answers where report_id = '.$report_id.' AND category_id = 1) as minor,
                                    (SELECT COUNT(category_id) FROM tbl_report_answers where report_id = '.$report_id.' AND category_id = 2) as major,
                                    (SELECT COUNT(category_id) FROM tbl_report_answers where report_id = '.$report_id.' AND category_id = 3) as critical,
                                    e.limit,e.no_major,e.no_critical
                                    FROM tbl_report_summary as a
                                    LEFT JOIN tbl_company as b ON a.company_id = b.company_id
                                    LEFT JOIN tbl_template as d ON a.template_id = d.template_id
                                    LEFT JOIN tbl_classification as e ON e.classification_id = d.classification_id
                                    LEFT JOIN tbl_report_answers as c ON a.report_id = c.report_id WHERE a.report_id = '.$report_id);
        $result = $query->result();
        return $result;
    }
    public function get_report_analysis_view2($report_id){

        $this->db->select("gmp_answers_count.*, ar_company.name, gmp_computation_coverage.Coverage");
        $this->db->where("gmp_answers_count.report_id", $report_id);
        $this->db->from("gmp_answers_count");
        $this->db->join("ar_company","gmp_answers_count.report_id = ar_company.report_id","LEFT");
        $this->db->join("gmp_computation_coverage","gmp_answers_count.report_id = gmp_computation_coverage.report_id","LEFT");
        $result = $this->db->get();
        return $result->result();
    }

    public function get_report_analysis_view_count($report_id){

        $this->db->select("*");
        $this->db->where("report_id", $report_id);
        $this->db->from("gmp_answers_count");
        $result = $this->db->get();
        return $result->result();
    }
}