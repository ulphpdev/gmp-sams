<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Taipei');

class Global_model extends CI_Model{ 



	function __construct() {

        parent::__construct();

        $this->load->helper('array');

    }



public function get_dashboard($table){

	$this->db->select("*");

	$this->db->from($table);

	$this->db->where("status >= 0 AND status <= 2");
	$result = $this->db->get()->result();

	return $result;

}

public function get_dashboard_filter_status($table, $sort){

	$this->db->select("*");

	$this->db->from($table);
	$this->db->where("status >= 0");
	$this->db->group_by($sort);

	$result = $this->db->get()->result();

	return $result;

}



public function getlist_global_modelx($table,$limit,$offset,$query = null, $order = null){
	$this->db->select('*');
	$this->db->from($table);

	if($query != null){
		$this->db->where($query . " AND (status = 1 OR status = 0)");
	} else {
		$this->db->where("status","1")->or_where("status","0");
	}
	
	if($order != null){
		$this->db->order_by($order, 'ASC');
	}
	$this->db->limit($limit,$offset);
	$result = $this->db->get()->result();
	return $result;
}

public function getlist_global_model_group($table,$limit,$offset,$query = null, $order = null, $status, $group = null){
	$this->db->select($table . '.*, concat(tbl_auditor_info.fname, " ", tbl_auditor_info.lname) as auditor_name');
	$this->db->from($table);
	if($query != null){
		$this->db->where($query);
	}
	
	$this->db->limit($limit,$offset);
	if($group != null){
		$this->db->group_by($group);
	}
	if($order != null){
		$this->db->order_by($order, 'DESC');
	}

	$this->db->join("tbl_auditor_info","tbl_auditor_info.auditor_id = " . $table . ".lead_auditor");

	$result = $this->db->get()->result();
	return $result;
}


public function report_listing_model_archive($limit,$offset,$query=null)
{
    $this->db->limit($limit,$offset);
    $this->db->select('tbl_report_listing.*, concat(tbl_auditor_info.fname, " ", tbl_auditor_info.lname) as auditor_name');
    $this->db->from('tbl_report_listing');
    $this->db->where('tbl_report_listing.status = -1');
    if($query != null){
        $this->db->where($query);
    }
    
    $this->db->group_by("report_id");
    $this->db->order_by("date_modified", 'DESC');
    $this->db->join("tbl_auditor_info","tbl_auditor_info.auditor_id = tbl_report_listing.lead_auditor");
    $result = $this->db->get()->result();
    return $result;
}

public function report_listing_model($limit,$offset,$query=null)
{
	$this->db->limit($limit,$offset);
	$this->db->select('tbl_report_listing.*, concat(tbl_auditor_info.fname, " ", tbl_auditor_info.lname) as auditor_name');
	$this->db->from('tbl_report_listing');
	$this->db->where('tbl_report_listing.status > -1');
	if($query != null){
		$this->db->where($query);
	}
	
	$this->db->group_by("report_id");
	$this->db->order_by("date_modified", 'DESC');
	$this->db->join("tbl_auditor_info","tbl_auditor_info.auditor_id = tbl_report_listing.lead_auditor");
	$result = $this->db->get()->result();
	return $result;
}


public function getlist_global_model_dm($table){

	$this->db->select('*');

	$this->db->from($table);

	$this->db->where("status","1")->or_where("status","0");

	$result = $this->db->get()->result();

	return $result;

}


public function getlist_pagination($table,$query = null,$status = null){
	$this->db->select('*');
	$this->db->from($table);
	

	if($query != null){
		$this->db->where($query . " AND (" . $status . ")");
	} else {
		$this->db->where($status);
	}

	// if($status != null){
	// 	$this->db->where($status);
	// }

	$result = $this->db->get()->result();
	return $result;
}

public function getlist_global_model($table,$limit,$offset,$query = null,$order_by = null, $status= null){

	$this->db->select('*');
	$this->db->from($table);
	
	if($query != null){
		$this->db->where($query)->where("status","1")->where("status","0");
	} else {
		$this->db->where("status","1")->or_where("status","0");
	}

	

	if($query != null){
		$this->db->where($query);
	}

	if($order_by != null){
		$this->db->order_by($order_by, "DESC");
	}


	$this->db->limit($limit,$offset);
	$result = $this->db->get()->result();
	return $result;
}





public function getlist_standard_modal($table,$table2,$limit,$offset,$sort){

	$this->db->select('ref.*, c.classification_name as name');

	$this->db->from($table." ref");

	$this->db->join($table2." c", "c.classification_id = ref.classification_id" ,"left");

	$this->db->where("ref.status","1")->or_where("ref.status","0");

	$this->db->order_by($sort, 'DESC');

	$this->db->limit($limit,$offset);

	$result = $this->db->get()->result();

	return $result;

}



public function delete_data($id, $table, $field){

	$this->db->where($field, $id);

	$this->db->delete($table);

}





function inactive_global($id, $data, $field, $table){

	$this->db->where($field, $id);

	$this->db->update($table, $data);

	// $this->db->update($table1, $data);

	echo 'success';

}



function getedit_global($id,$table,$field){

	$this->db->select('*');

	$this->db->from($table);

	// $this->db->from($table1);

	$this->db->where($field, $id);

	$result = $this->db->get()->result();

	return $result;

}



public function save_data($table, $data) { 

	$this->db->insert($table, $data); 

	$id = $this->db->insert_id();

	return $id;

}



public function update_data($field, $where, $table, $data) { 

	$this->db->where($field, $where);

	$this->db->update($table, $data); 

}



public function get_where($table, $field, $where, $status = null, $sort = null) { 
		$this->db->select('*');
		$this->db->from($table);
		if($status != null){
			$this->db->where('status', $status );
		}
		
		$this->db->where($field, $where);
		if($sort != null){
			$this->db->order_by($sort, "desc" );
		}
		$result = $this->db->get()->result();
		return $result;
}





public function check_exist($table, $where) { 

		$this->db->select("*");
		$this->db->where("email", $where);
		$this->db->where("status <> -2");
		$this->db->from("tbl_auditor_info");
		$q = $this->db->get();
		return $q->result();
}

public function check_exist_approve($table, $where) { 

		$this->db->select("*");
		$this->db->where("email", $where);
		$this->db->where("status <> -2");
		$this->db->from($table);
		$q = $this->db->get();
		return $q->result();
}

public function check_global_exist_model($table,  $field, $where){

		$this->db->select('count(*) as count');

		$this->db->from($table);

		$this->db->where($where);

		$result = $this->db->get()->result();

		return $result;

}



public function getdata(){

	$this->db->select('*');

	$this->db->from('tbl_'.$_POST['table']);

	if($_POST['field'] != 'null'){

		$this->db->where($_POST['field'], $_POST['id']);

	}

	$result = $this->db->get()->result();

	return $result;



}

public function getlist_manage_template($table,$table1,$limit,$offset,$sort,$txt_search){

	if($txt_search == ''){

		$query = $this->db->query("SELECT a.*, b.classification_name, c.standard_name FROM tbl_template as a 

									LEFT JOIN tbl_classification as b ON a.classification_id = b.classification_id JOIN tbl_standard_reference as c ON a.standard_id = c.standard_id 

									WHERE a.status = '1' OR a.status = '0' ORDER BY ".$sort." DESC LIMIT ".$offset.",".$limit."");		

	}else{

		$query = $this->db->query("SELECT a.*, b.classification_name, c.standard_name FROM tbl_template as a 

									LEFT JOIN tbl_classification as b ON a.classification_id = b.classification_id JOIN tbl_standard_reference as c ON a.standard_id = c.standard_id 

									WHERE (a.status = '1' OR a.status = '0') AND (b.classification_name LIKE '%".$txt_search."%' OR c.standard_name LIKE '%".$txt_search."%') ORDER BY ".$sort." DESC LIMIT ".$offset.",".$limit."");		

	}

	$result = $query->result();

    return $result;

    // $this->db->where("a.status","1")->or_where("a.status","0");

	// }

 	//  $this->db->order_by($sort, 'DESC');

	// $this->db->limit($limit,$offset);

   	

	// }

	// 	$this->db->select('cla.*, c.classification_name as name');

	// 	$this->db->from($table." cla");

	// 	$this->db->join($table1." c", "c.template_id = cla.classification_id" ,"left");

	// 	// $this->db->where("ref.status","1")->or_where("ref.status","0");

	// 	$this->db->order_by($sort, 'DESC');

	// 	$this->db->limit($limit,$offset);

	// 	$result = $this->db->get()->result();

	// 	return $result;

	}

	

	function get_no_answer($report_id,$element_id){
		$query = $this->db->query("SELECT c.element_id,d.element_name,a.question_id,a.answer_details,b.category_name FROM tbl_report_answers as a 
								LEFT JOIN tbl_question_category as b ON a.category_id = b.category_id
								LEFT JOIN tbl_questions as c ON a.question_id = c.question_id
								LEFT JOIN tbl_elements as d ON c.element_id = d.element_id
								WHERE a.answer_id = 2 AND a.report_id = $report_id AND c.element_id =$element_id GROUP BY a.question_id" );
		$result = $query->result();
		return $result;
	}

	function get_other_distribution($id){
		$query = $this->db->query("SELECT * FROM tbl_report_other_distribution WHERE report_id = $id");
		$result = $query->result();
		return $result;
	}


	function get_emails($table,$reportid){
		$query = $this->db->query("SELECT * FROM $table WHERE report_id = $reportid");
		$result = $query->result();
		return $result;
	}

	function change_report_status($reportid,$status){
		$this->db->where("report_id",$reportid);
		$this->db->update("tbl_report_summary",array("status"=>$status));
	}

	function get_report_status($reportid){
		$query = $this->db->query("SELECT * FROM tbl_report_summary WHERE report_id = $reportid");
		$data = $query->result_array();
		return $data[0]['status'];
	}

	function get_report_details($reportid){
		$query = $this->db->query("SELECT * FROM qv_join_views_1 WHERE report_id = $reportid");
		$result = $query->result();
		return $result;
	}

	function get_list_data($table){
		$query = $this->db->query("SELECT * FROM $table");
		$result = $query->result();
		return $result;
	}

    function get_list_data_query($table, $query){
        $query = $this->db->query("SELECT * FROM $table WHERE $query");
        $result = $query->result();
        return $result;
    }

	function get_question_classification($id){
		$query = $this->db->query("SELECT * FROM qv_product_type_classification where classification_id = $id");
		$result = $query->result();
		return $result;
	}

	function remove_question_classification($cat_id, $cat_class){
		$this->db->where("classification_id",$cat_class);
		$this->db->where("category_id",$cat_id);
		$this->db->delete("tbl_classification_category");
	}
	function update_classification_category($cat_id, $cat_class, $description){
		$this->db->where("classification_id",$cat_class);
		$this->db->where("category_id",$cat_id);
		$this->db->update("tbl_classification_category", array("description"=>$description));
	}


	function trash_data($query, $table){
		$this->db->where($query);
		$this->db->delete($table);
	}

	function is_checking($table, $field, $item){
		$this->db->select("*");
		$this->db->where($field, $item);
		$this->db->where("status", "1");
		$this->db->from($table);
		$q = $this->db->get();
		return $q->num_rows();

	}

	function check_email_table_same($email, $table, $query)
	{
		$this->db->select("*");
		$this->db->where($query);
		$this->db->from($table);
		$q = $this->db->get();
 
		$data = $q->result_array();
		$existing_email = $data[0]['email'];

		if($existing_email == $email){
			return "same_email";
		} else {
			return "note_same_email";
		}

	}

	function check_email_table_exist($email, $table)
	{
		$this->db->select("*");
		$this->db->where("status", 1);
		$this->db->where("email",$email);
		
		$this->db->from($table);
		$q = $this->db->get();

		return $q->num_rows();
	}

	function report_submission_check($reportid)
	{
		$this->db->select("*");
		$this->db->where("report_id",$reportid);
		$this->db->from("tbl_report_submission_date");
		$q = $this->db->get();

		return $q->num_rows();
	}

	function report_submission_update($reportid,$data)
	{
		$this->db->where("report_id",$reportid);
		$this->db->update("tbl_report_submission_date",$data);
	}

	function report_submission_insert($data)
	{
		$this->db->insert("tbl_report_submission_date",$data);
	}

	function count_active_record($table)
	{
		$this->db->select("*");
		$this->db->where("status",1);
		$this->db->from($table);
		$q = $this->db->get();

		return $q->num_rows();
	}

	function get_name($table,$query)
	{
		$this->db->select("*");
		$this->db->where($query);
		$this->db->from($table);
		$q = $this->db->get();
		$data = $q->result_array();

		return $data[0]['fname'] . " " . $data[0]['lname'];
	}

	function get_list_data_active($table){
		$query = $this->db->query("SELECT * FROM $table WHERE status = 1");
		$result = $query->result();
		return $result;
	}


	function check_if_exist($table, $query)
	{
		$this->db->select("*");
		$this->db->where($query);
		$this->db->from($table);
		$q = $this->db->get();
		return $q->num_rows();
	}

	function getlist_in_use($query)
	{
		$this->db->select("*");
		$this->db->where($query);
		$this->db->from("qv_dm_in_use");
		$q = $this->db->get();
		return $q->num_rows();
	}

	function get_element_answers($reportid,$element)
	{
		$this->db->select("question_id,answer_id,answer_details,default_yes,category_name,answer_id");
		$this->db->where("report_id",$reportid);
		$this->db->where("element_id",$element);
		$this->db->from("qv_question_answers");
		$this->db->group_by("question_id");
		$q = $this->db->get();
		return $q->result();
	}
	function get_element_answers_not_yes($reportid,$element)
	{
		$this->db->select("question_id,answer_id,answer_details,default_yes,category_name,answer_id");
		$this->db->where("report_id",$reportid);
		$this->db->where("element_id",$element);
		// $this->db->where("answer_id <> 1");
		$this->db->from("qv_question_answers");
		$this->db->group_by("question_id");
		$q = $this->db->get();
		return $q->result();
	}

	function check_if_approval($table, $query, $report)
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($query);
		$this->db->where("report_id",$report);
		$q = $this->db->get();
		return $q->num_rows();
	}
	function get_api_logs($limit, $query = null)
	{
		$this->db->limit($limit);
		$this->db->select("tbl_api_logs.*, concat(tbl_auditor_info.fname,' ', tbl_auditor_info.lname) as Name") ;
		$this->db->from("tbl_api_logs");
		$this->db->join("tbl_auditor_info","tbl_api_logs.auditor_id = tbl_auditor_info.auditor_id", "left");
		$this->db->order_by("submission_date","desc");
		if($query != null){
			$this->db->where("report_id",$query);
		}
		$q = $this->db->get();
		return $q->result();
	}

	function dart_logs($id)
	{
		$this->db->select("tbl_api_logs.*, concat(tbl_auditor_info.fname,' ', tbl_auditor_info.lname) as Name") ;
		$this->db->from("tbl_api_logs");
		$this->db->join("tbl_auditor_info","tbl_api_logs.auditor_id = tbl_auditor_info.auditor_id", "left");
		$this->db->order_by("submission_date","desc");
		$this->db->where("tbl_api_logs.id",$id);
		$q = $this->db->get();
		return $q->result();
	}

	function get_data_query($table, $query)
	{
		$this->db->select("*");
		$this->db->where($query);
		$this->db->from($table);
		$q = $this->db->get();
		return $q->result();
	}

	function display_email($reportid, $logg_id)
	{
		$this->db->select("*");
		$this->db->where("report_id",$reportid);
		$this->db->like("lead_auditor",$logg_id);
		$this->db->or_like("co_auditor",$logg_id);
		$this->db->from("tbl_report_listing");
		$q = $this->db->get();
		return $q->num_rows();
	}

	function get_sub_menu($item_id)
	{
		$this->db->select("*");
		$this->db->where("menu_id",$item_id);
		$this->db->from("menu_sub_item");
		$q = $this->db->get();
		return $q->result();
	}

	function getlist_dm($table,$limit,$offset,$query= null)
	{
		$this->db->limit($limit,$offset);
		$this->db->select("*");
		$this->db->from($table);
		if($query != null){
			$this->db->where( "(" . $query . ") AND status <> -2");
		} else {
			$this->db->where("status <> -2");
		}
		

		$this->db->order_by("update_date","desc");
		$q = $this->db->get();
		return $q->result();
	}


	function get_list_location($table, $order, $query = null){
		if($query != null){
			$query = $this->db->query("SELECT * FROM $table WHERE " . $query . " ORDER BY " . $order);
		} else {
			$query = $this->db->query("SELECT * FROM $table ORDER BY " . $order);
		}
		
		$result = $query->result();
		return $result;
	}

    function get_list_with_order($table,  $query = null, $order){
        if($query != null){
            $query = $this->db->query("SELECT * FROM $table WHERE " . $query . " ORDER BY " . $order);
        } else {
            $query = $this->db->query("SELECT * FROM $table ORDER BY " . $order);
        }
        
        $result = $query->result();
        return $result;
	}
	
	public function get_audit_history($supplier_id) { 
		$q = $this->db->query("SELECT a.*, Max(b.Date) AS latest_audit_date FROM tbl_audit_history as a LEFT JOIN tbl_audit_history_dates as b ON a.id = b.history_id where a.supplier_id = ".$supplier_id." GROUP BY a.id ORDER BY a.update_date DESC, b.Date DESC");
		$result = $q->result();
        return $result;
}

}



