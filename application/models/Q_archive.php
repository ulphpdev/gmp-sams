<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Q_archive extends CI_Model{


	public function get_list($query = null, $limit, $offset, $group = null){

        $this->db->limit($limit,$offset);
        $this->db->select("*");
        $this->db->from("qv_audit_report_with_listing");

        if($query != null) {
            $this->db->where($query);
        }

        $this->db->where("Report_Status = -1");
        if($group != null) {
            $this->db->group_by($group);
        }

        $this->db->order_by("report_id", "desc");
        $q = $this->db->get();
        return $q->result();
	}

	public function get_pagination(){

		$this->db->select("*");
		$this->db->from("qv_audit_report_with_listing");
		$this->db->where("Report_Status = -1");
		$this->db->group_by("Report_No");
		$q = $this->db->get();
		return $q->num_rows();

	}

}