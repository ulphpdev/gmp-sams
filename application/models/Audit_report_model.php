<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Audit_report_model extends CI_Model{
	function __construct() {
        parent::__construct();
    }
    function read_report()
    {   
		    $this->db->select("a.report_no,a.report_id,b.fname,b.create_date");
		    $this->db->from("tbl_report_summary as a");
		    $this->db->join('tbl_inspector as b', 'a.report_id = b.report_id');
		    $query = $this->db->get();
		    $result = $query->result();
		    return $result;
    }
    function count_report()
    {   
   
        $this->db->select("*");
        $this->db->from('tbl_report_summary');
        return $this->db->get()->num_rows();
        // $query = $this->db->get();
        // return $query->result();
    }


    function get_report_list_model($limit,$offset){
        $query = $this->db->query("SELECT DISTINCT(a.report_id),a.template_id, a.report_no, a.status,CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date,b.name,
                                    CONCAT(c.fname,' ',c.lname) as auditor_name 
                                    FROM tbl_report_summary as a 
                                    LEFT JOIN tbl_company as b ON a.company_id = b.company_id 
                                    LEFT JOIN tbl_auditor_info as c ON a.auditor_id = c.auditor_id
                                    
                                    LEFT JOIN tbl_questions as d ON d.template_id = a.template_id
                                    -- LEFT JOIN tbl_elements as e ON e.element_id = d.element_id
                                    WHERE a.status != '-2' OR a.status != '-3'
                                    LIMIT ".$offset.", ".$limit." ");
        $result = $query->result();
        return $result;
    }
    function get_report($id){
        $query = $this->db->query('SELECT a.report_no,c.image,a.status,a.other_issues_executive,a.other_issues_audit,DATE_FORMAT(a.closure_date,"%e %M %Y") as closure_date,
(SELECT CONCAT(b.fname," ",b.lname) FROM tbl_reviewer as a LEFT JOIN tbl_reviewer_info as b ON b.reviewer_id = a.reviewer_id WHERE a.report_id = '.$id.') as reviewer,(SELECT b.designation FROM tbl_reviewer as a LEFT JOIN tbl_reviewer_info as b ON b.reviewer_id = a.reviewer_id WHERE a.report_id = '.$id.') as rev_pos,(SELECT b.department FROM tbl_reviewer as a LEFT JOIN tbl_reviewer_info as b ON b.reviewer_id = a.reviewer_id WHERE a.report_id = '.$id.') as rev_dep,(SELECT b.image FROM tbl_reviewer as a LEFT JOIN tbl_reviewer_info as b ON b.reviewer_id = a.reviewer_id WHERE a.report_id = '.$id.') as rev_image,
a.areas_to_consider,a.template_id,a.report_id, b.name, b.address1, b.address2, b.address3,b.country, CONCAT(c.fname," ", c.mname," ", c.lname) as auditor, c.department, c.company, c.designation,  CONCAT(DATE_FORMAT(a.audit_date_1,"%e")," & ",DATE_FORMAT(a.audit_date_2,"%e")," ",DATE_FORMAT(a.audit_date_1,"%M")," ",DATE_FORMAT(a.audit_date_2,"%Y")) as audit_date, a.other_activities FROM tbl_report_summary as a LEFT JOIN tbl_company as b ON a.company_id = b.company_id LEFT JOIN tbl_auditor_info as c ON a.auditor_id = c.auditor_id WHERE a.report_id = '.$id.'');
        $result = $query->result();
        return $result;
    }
    function get_report_table($id){
        $query = $this->db->query("SELECT fname, mname, lname, designation, company, department FROM tbl_reviewer_info
UNION
SELECT fname, mname, lname, designation, company, department FROM tbl_approver_info");
        $result = $query->result();
        return $result;
        // $this->db->select('fname, mname, lname, designation, company, department');
        // $this->db->from('tbl_reviewer_info');
        // $query1 = $this->db->get()->result();
        // $this->db->select('fname, mname, lname, designation, company, department');
        // $this->db->from('tbl_approver_info');
        // $query2 = $this->db->get()->result();
        // $query = $this->db->query($query1 . ' UNION ALL ' . $query2);
        // // $this->db->where('report_id', $id);
        // $query = $this->db->get()->result();
        // return $query;
        // $this->db->select("b.report_id, c.fname, c.mname, c.lname, c.designation, c.company, c.department, e.fname, e.mname, e.lname, e.designation, e.company, e.department");
        // $this->db->from("tbl_report_summary as a");
        // $this->db->join('tbl_approver as b', 'a.report_id = b.report_id');
        // $this->db->join('tbl_approver_info as c', 'b.approver_id = c.approver_id');
        // $this->db->join('tbl_reviewer as d', 'd.report_id = a.report_id');
        // $this->db->join('tbl_reviewer_info as e', 'e.reviewer_id = d.reviewer_id');
        // $this->db->where('a.report_id', $id);
        // $result = $this->db->get()->result();
        // return $result;
    }
    function get_supplier_history($id){
        $this->db->select("a.*, b.*");
        $this->db->from("tbl_report_summary as a");
        $this->db->join('tbl_company as b', 'a.company_id = b.company_id');
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_pdf($report_id){
        $this->db->select('*');
        $this->db->from('tbl_report_summary');
        $this->db->where('report_id', $report_id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_inspector($id){
        $this->db->select("a.*, b.*");
        $this->db->from("tbl_report_summary as a");
        $this->db->join('tbl_inspector as b', 'a.report_id = b.report_id');
        $this->db->join('tbl_inspection_changes as c', 'c.report_id = a.report_id');
        $this->db->where('a.report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_changes_report($id){
        $this->db->select("a.*, c.*");
        $this->db->from("tbl_report_summary as a");
        // $this->db->join('tbl_inspector as b', 'a.report_id = b.report_id');
        $this->db->join('tbl_inspection_changes as c', 'c.report_id = a.report_id');
        $this->db->where('a.report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_activity_name($id){
        $this->db->select("a.*, b.activity_name");
        $this->db->from("tbl_report_activities as a");
        $this->db->join('tbl_activities as b', 'a.activity_id = b.activity_id');
        // $this->db->join('tbl_activities as c', 'c.activity_id = b.activity_id');
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_scope_name($id){
        $this->db->select("a.*, b.scope_name");
        $this->db->from("tbl_audit_scope_detail as a");
        $this->db->join('tbl_audit_scope as b', 'a.scope_id = b.scope_id');
        // $this->db->join('tbl_activities as c', 'c.activity_id = b.activity_id');
        $this->db->where('a.report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_product_name($id){
        $this->db->select("a.*, b.*");
        $this->db->from("tbl_report_scope_product as a");
        $this->db->join('tbl_product as b', 'a.product_id = b.product_id');
        // $this->db->join('tbl_activities as c', 'c.activity_id = b.activity_id');
        $this->db->where('a.report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_audit_references($id){
        $this->db->select("*");
        $this->db->from("tbl_references");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
     function get_audit_document($id){
        $this->db->select("*");
        $this->db->from("tbl_pre_audit_documents");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
     function get_audit_area($id){
        $this->db->select("*");
        $this->db->from("tbl_report_summary");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_not_audit_area($id){
        $this->db->select("*");
        $this->db->from("tbl_report_summary");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_name_closeup_meeting($id){
        $this->db->select("a.*, b.*");
        $this->db->from("tbl_personnel as a");
        $this->db->join('tbl_personnel_met_detail as b', 'a.personnel_met_id = b.personnel_met_id');
        $this->db->where('report_id', $id);
        $this->db->where('a.personnel_met_id', 1);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_name_inspection($id){
        $this->db->select("a.*, b.*");
        $this->db->from("tbl_personnel as a");
        $this->db->join('tbl_personnel_met_detail as b', 'a.personnel_met_id = b.personnel_met_id');
        $this->db->where('report_id', $id);
        $this->db->where('a.personnel_met_id', 2);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_distribution($id){
        $this->db->select("a.*, b.*");
        $this->db->from("tbl_report_distribution as a");
        $this->db->join('tbl_distribution as b', 'a.distribution_id = b.distribution_id');
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_template_reference($id){
        $this->db->select("a.template_id, b.*, c.*, d.*");
        $this->db->from("tbl_report_summary as a");
        $this->db->join('tbl_template as b', 'a.template_id = b.template_id');
        $this->db->join('tbl_classification as c', 'c.classification_id = b.classification_id');
        $this->db->join('tbl_standard_reference as d', 'd.standard_id = b.standard_id');
        $this->db->where('a.report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_template_element($id){
        $query = $this->db->query("SELECT DISTINCT(a.element_id),b.element_name FROM tbl_questions as a
                                LEFT JOIN tbl_elements as b ON a.element_id = b.element_id
                                WHERE a.template_id = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_question($element_id, $template_id){
        $query = $this->db->query("SELECT DISTINCT(a.question_id), a.question, a.default_yes, a.mandatory, b.element_name, c.answer_details, c.category_id FROM tbl_questions as a
                                LEFT JOIN tbl_elements as b ON a.element_id = b.element_id
                                LEFT JOIN tbl_report_answers as c ON c.question_id = a.question_id
                                WHERE a.template_id = ".$template_id." AND a.element_id = ".$element_id."");
        $result = $query->result();
        return $result;
    }
    function get_element_noanswer($id){
        $query = $this->db->query("SELECT DISTINCT(a.element_id),b.element_name FROM tbl_questions as a
                                LEFT JOIN tbl_elements as b ON a.element_id = b.element_id
                                WHERE a.default_yes = ''");
        $result = $query->result();
        return $result;
    }
    function get_disposition($id){
        $this->db->distinct('a.audit_scope_id');
       $this->db->select("a.*, b.disposition_name, b.disposition_label, c.scope_detail");
        $this->db->from("tbl_report_scope_disposition as a");
        $this->db->join('tbl_disposition as b', 'a.disposition_id = b.disposition_id', 'LEFT');
         $this->db->join('tbl_report_audit_scope as c', 'c.audit_scope_id = a.audit_scope_id', 'LEFT');
        $this->db->where('a.report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_other_audit_report($id){
        $this->db->select("*");
        $this->db->from("tbl_report_summary");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_executive_summary($id){
        $this->db->select("*");
        $this->db->from("tbl_report_summary");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_remarks($id){
        $this->db->select("*");
        $this->db->from("tbl_report_summary");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }
    //pdf
    function get_data_by_id($id,$field,$table){
        $query = $this->db->query("SELECT DISTINCT(a.report_id),a.report_no,a.template_id,a.company_id,a.other_activities,a.audit_date_1,a.audit_date_2,a.p_inspection_date_1,a.p_inspection_date_2,a.auditor_id,a.closure_date,a.other_issues_audit,a.other_issues_executive,a.audited_areas,a.areas_to_consider,a.wrap_up_date,a.create_date,a.update_date,a.translator,a.status, b.* FROM ".$table." as a LEFT JOIN
            tbl_company as b ON a.company_id = b.company_id
         WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    // function get_data_by_activity($id,$field,$table){
    //     $query = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = ".$id."");
    //     $result = $query->result();
    //     return $result;
    // }
    function get_data_by_activity($id,$field,$table){
        $query = $this->db->query("SELECT b.activity_name, b.activity_id FROM ".$table." as a 
            LEFT JOIN tbl_activities as b ON b.activity_id = a.activity_id
            WHERE a.".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_sub($activity_id){
        $query = $this->db->query("SELECT a.*,b.sub_item_name FROM tbl_activities as a
                                LEFT JOIN tbl_sub_activities as b ON a.activity_id = b.activity_id
                                WHERE a.activity_id = $activity_id ");
        $result = $query->result();
        return $result;
    }
    function get_data_by_auditor($id,$field,$table){
        $query = $this->db->query("SELECT a.report_id, b.auditor_id, b.fname, b.lname, b.designation,b.department, b.company FROM ".$table." as a 
            LEFT JOIN tbl_auditor_info as b ON b.auditor_id = a.auditor_id
            WHERE a.".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_translator($id,$field,$table){
        $query = $this->db->query("SELECT translator FROM ".$table."
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_reference($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table."
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_document($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table."
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_standard_reference($id,$field,$table){
        $query = $this->db->query("SELECT a.*, b.standard_name FROM ".$table." as a
            LEFT JOIN tbl_standard_reference as b ON b.standard_id = a.standard_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_company($id,$field,$table){
        $query = $this->db->query("SELECT DISTINCT(a.report_id), a.p_inspection_date_1, b.name, b.background, c.fname,c.mname,c.lname, d.changes FROM ".$table." as a
            LEFT JOIN tbl_company as b ON b.company_id = a.company_id
            LEFT JOIN tbl_inspector as c ON c.report_id = a.report_id
            LEFT JOIN tbl_inspection_changes as d ON d.report_id = a.report_id
            WHERE a.".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_audit_scope($id,$field,$table){
        $query = $this->db->query("SELECT a.*, b.scope_name FROM ".$table." as a
            LEFT JOIN tbl_audit_scope as b ON b.scope_id = a.audit_scope_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_audit_product($id,$field,$table){
        $query = $this->db->query("SELECT DISTINCT(a.report_id), b.product_name FROM ".$table." as a
            LEFT JOIN tbl_product as b ON a.product_id = a.product_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_audit_audited_area($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table."
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_personnel_close_out($id,$field,$table){
        $query = $this->db->query("SELECT DISTINCT(a.report_id), a.fname, a.lname, a.designation, b.* FROM ".$table." as a
            LEFT JOIN tbl_personnel_met_detail as b ON b.personnel_met_id = a.personnel_met_id
            WHERE ".$field." = ".$id." AND a.personnel_met_id = '1'");
        $result = $query->result();
        return $result;
    }
    function get_data_by_personnel_during_inspection($id,$field,$table){
        $query = $this->db->query("SELECT DISTINCT(a.report_id), a.fname, a.lname, a.designation, b.* FROM ".$table." as a
            LEFT JOIN tbl_personnel_met_detail as b ON b.personnel_met_id = a.personnel_met_id
            WHERE ".$field." = ".$id." AND a.personnel_met_id = '2'");
        $result = $query->result();
        return $result;
    }
    function get_data_by_other_issue($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table."
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;;
    }
    //executive summary report pdf
    function get_data_by_executive_company($id,$field,$table){
        $query = $this->db->query("SELECT  b.name,b.address1,b.address2 FROM ".$table." as a
            LEFT JOIN tbl_company as b ON b.company_id = a.company_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_lead_auditor($id,$field,$table){
        $query = $this->db->query("SELECT a.audit_date_1, b.* FROM ".$table." as a
            LEFT JOIN tbl_auditor_info as b ON b.auditor_id = a.auditor_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_co_auditor($id,$field,$table){
        $query = $this->db->query("SELECT c.* FROM ".$table." as a
            LEFT JOIN tbl_co_auditors as b ON b.report_id = a.report_id
            LEFT JOIN tbl_auditor_info as c ON c.auditor_id = b.auditor_id
            WHERE a.".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_scope($id,$field,$table){
        $query = $this->db->query("SELECT b.* FROM ".$table." as a
            LEFT JOIN tbl_audit_scope as b ON b.scope_id = a.audit_scope_id
            WHERE a.".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_scope_product($id,$field,$table){
        $query = $this->db->query("SELECT b.* FROM ".$table." as a
            LEFT JOIN tbl_product as b ON b.product_id = a.product_id
            WHERE a.".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_element($id,$field,$table){
        $query = $this->db->query("SELECT element_name FROM ".$table." as a
            LEFT JOIN tbl_questions as b ON b.template_id = a.template_id
            LEFT JOIN tbl_elements as c ON c.element_id = b.element_id
            WHERE a.".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_not_audited($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table." 
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_standard_reference($id,$field,$table){
        $query = $this->db->query("SELECT b.* FROM ".$table." as a
            LEFT JOIN tbl_standard_reference as b ON b.standard_id = a.standard_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_disposition($id,$field,$table){
        $query = $this->db->query("SELECT c.* FROM ".$table." as a
            LEFT JOIN tbl_audit_scope as b ON a.audit_scope_id = b.scope_id
            LEFT JOIN tbl_disposition as c ON c.disposition_id = a.disposition_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_product_scope($id,$field,$table){
        $query = $this->db->query("SELECT a.audit_scope_id,b.* FROM ".$table." as a
            LEFT JOIN tbl_product as b ON a.product_id = b.product_id
            WHERE ".$field." = ".$id." AND a.product_id = b.product_id");
        $result = $query->result();
        return $result;
    }
    function get_data_by_executive_reviewer($id,$field,$table){
        $query = $this->db->query("SELECT b.* FROM ".$table." as a
            LEFT JOIN tbl_reviewer_info as b ON b.reviewer_id = a.reviewer_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_distribution($id,$field,$table){
        $query = $this->db->query("SELECT b.* FROM ".$table." as a
            LEFT JOIN tbl_distribution as b ON b.distribution_id = a.distribution_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    //annexure
    function get_data_by_annexure_company($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table." as a
             LEFT JOIN tbl_company as b ON b.company_id = a.company_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_approver($id,$field,$table){
        $query = $this->db->query("SELECT * FROM ".$table." as a
             LEFT JOIN tbl_approver_info as b ON b.approver_id = a.approver_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    function get_data_by_answers($id,$field,$table){
        $query = $this->db->query("SELECT a.answer_details,b.category_name FROM ".$table." as a
             LEFT JOIN tbl_question_category as b ON b.category_id = a.category_id
            WHERE ".$field." = ".$id."");
        $result = $query->result();
        return $result;
    }
    //start of auditor email
    function get_email_lead_auditor($id){
         $query = $this->db->query("SELECT a.status, a.remarks ,a.template_id,a.report_id, a.report_no, b.fname, b.lname, b.email, c.name FROM tbl_report_summary as a
             LEFT JOIN tbl_auditor_info as b ON b.auditor_id = a.auditor_id
             LEFT JOIN tbl_company as c ON c.company_id = a.company_id
            WHERE report_id = $id ");
        $result = $query->result();
        return $result;
    }
    function get_email_co_auditor($id){
         $query = $this->db->query("SELECT b.email, b.fname, b.lname FROM tbl_co_auditors as a
             LEFT JOIN tbl_auditor_info as b ON b.auditor_id = a.auditor_id
            WHERE report_id = $id ");
        $result = $query->result();
        return $result;
    }
    function get_email_department_head($id){
         $query = $this->db->query("SELECT b.email, b.fname, b.lname, a.reviewer_id FROM tbl_reviewer as a
             LEFT JOIN tbl_reviewer_info as b ON b.reviewer_id = a.reviewer_id
            WHERE report_id = $id ");
        $result = $query->result();
        return $result;
    }
    function get_email_division_head($id){
         $query = $this->db->query("SELECT b.email, b.fname, b.lname, a.approver_id FROM tbl_approver as a
             LEFT JOIN tbl_approver_info as b ON b.approver_id = a.approver_id
            WHERE report_id = $id ");
        $result = $query->result();
        return $result;
    }
// adding remarks 
     function add_status($report_id, $status, $report_submission)
   {    

        $this->db->set('status', $status);
        $this->db->set('report_submission', $report_submission);
        $this->db->where('report_id', $report_id);
        $this->db->update('tbl_report_summary');
        
    }
      function add_status1($report_id, $status)
   {    

        $this->db->set('status', $status);
        $this->db->where('report_id', $report_id);
        $this->db->update('tbl_report_summary');
        
    }
      function add_status2($report_id, $status)
   {    

        $this->db->set('status', $status);
        $this->db->where('report_id', $report_id);
        $this->db->update('tbl_report_summary');
        
    }
    function add_remarks_departmenthead($report_id, $status)
   {    
        $this->db->set('status', $status);
        $this->db->where('report_id', $report_id);
        $this->db->update('tbl_report_summary');
        
    }
 function add_remarks($report_id, $status, $report_issuance)
   {    
        $this->db->set('report_issuance', $report_issuance);
        $this->db->set('status', $status);
        $this->db->where('report_id', $report_id);
        $this->db->update('tbl_report_summary');
        
    }
    function add_new_remarks($report_id, $remarks, $create_date, $approver_id, $type)
   {    
        $this->db->set('remarks', $remarks);
        $this->db->set('report_id', $report_id);
        $this->db->set('create_date', $create_date);
        $this->db->set('id', $approver_id);
        $this->db->set('type', $type);
        $this->db->insert('tbl_report_remarks');
        
    }
      function add_new_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type)
   {    
        $this->db->set('remarks', $remarks);
        $this->db->set('report_id', $report_id);
        $this->db->set('create_date', $create_date);
        $this->db->set('id', $reviewer_id);
        $this->db->set('type', $type);
        $this->db->insert('tbl_report_remarks');
        
    }
    function reject_remarks($report_id, $remarks, $create_date, $approver_id, $type)
   {    
        $this->db->set('remarks', $remarks);
        $this->db->set('report_id', $report_id);
        $this->db->set('create_date', $create_date);
        $this->db->set('id', $approver_id);
        $this->db->set('type', $type);
        $this->db->insert('tbl_report_remarks');
        
    }
     function reject_remarks_departmenthead($report_id, $remarks, $create_date, $reviewer_id, $type)
   {    
        $this->db->set('remarks', $remarks);
        $this->db->set('report_id', $report_id);
        $this->db->set('create_date', $create_date);
        $this->db->set('id', $reviewer_id);
        $this->db->set('type', $type);
        $this->db->insert('tbl_report_remarks');
        
    }
     function check_status($report_id){
        $this->db->select("*");
        $this->db->from("tbl_report_summary");
        $this->db->where('report_id', $report_id);
        $result = $this->db->get()->result();
        return $result;
    }

     function count_co_auditor($report_id){
        $this->db->select("co_auditor");
        $this->db->from("tbl_report_listing");
        $this->db->where('report_id', $report_id);
        $result = $this->db->get()->result();
        return $result;
    }




    // tek
    function get_scope_product_by_scope($id,$scope_id){
        $query = $this->db->query("SELECT b.product_name FROM tbl_report_scope_product as a LEFT JOIN tbl_product as b ON a.product_id = b.product_id WHERE a.audit_scope_id = ".$scope_id." AND a.report_id = ".$id);
        $result = $query->result();
        return $result;
    }

    function get_element_recommendation($id){
        $query = $this->db->query("SELECT a.recommendation,b.element_name FROM tbl_report_element_recommendation as a LEFT JOIN tbl_elements as b ON a.element_id = b.element_id WHERE a.report_id = ".$id);
        $result = $query->result();
        return $result;
    }

    //nolie 9/8/17
    function audit_search($table){
        $this->db->select("*");
        $this->db->from($table);
        // $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }

    function audit_search_keyword($table, $field_name, $keyword){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field_name, $keyword);
        $result = $this->db->get()->result();
        return $result;
    }

    function audit_search_auditor($auditor_id, $limit, $offset){
         $query = $this->db->query("SELECT a.report_id, a.report_no, a.status,CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, e.name,
                                    CONCAT(d.fname,' ',d.lname) as auditor_name  FROM tbl_report_summary as a
            LEFT JOIN tbl_auditor_info as d ON d.auditor_id = a.auditor_id
            LEFT JOIN tbl_company as e ON e.company_id = a.company_id 
            WHERE a.auditor_id = $auditor_id 
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }
    function get_search_co_auditor($auditor_id, $limit, $offset){
         $query = $this->db->query("SELECT c.report_id, c.report_no, c.status,CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, d.name,
                                    CONCAT(b.fname,' ',b.lname) as auditor_name  FROM tbl_co_auditors as a
            LEFT JOIN tbl_auditor_info as b ON b.auditor_id = a.auditor_id
            LEFT JOIN tbl_report_summary as c ON c.auditor_id = a.auditor_id
            LEFT JOIN tbl_company as d ON d.company_id = c.company_id 
            WHERE c.auditor_id = $auditor_id 
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }

    function get_search_classification($classification_id, $offset, $limit){

       $query = $this->db->query("SELECT c.report_id, b.template_id, c.report_no, c.status,CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, e.name,
                                    CONCAT(d.fname,' ',d.lname) as auditor_name  FROM tbl_classification as a
            RIGHT JOIN tbl_template as b ON b.classification_id = a.classification_id
            RIGHT JOIN tbl_report_summary as c ON c.template_id = b.template_id
            LEFT JOIN tbl_auditor_info as d ON d.auditor_id = c.auditor_id
            LEFT JOIN tbl_company as e ON e.company_id = c.company_id 
            WHERE a.classification_id = $classification_id
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }
    function get_search_disposition($disposition_id, $offset, $limit){

       $query = $this->db->query("SELECT b.report_id, b.template_id, b.report_no, b.status, CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, e.name,
                                    CONCAT(d.fname,' ',d.lname) as auditor_name FROM tbl_report_scope_disposition as a
            RIGHT JOIN tbl_report_summary as b ON b.report_id = a.report_id
            LEFT JOIN tbl_auditor_info as d ON d.auditor_id = b.auditor_id
            LEFT JOIN tbl_company as e ON e.company_id = b.company_id 
            WHERE a.disposition_id = $disposition_id
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }

     //nolie 9/11/17
    function get_search_country($id, $offset, $limit){

       $query = $this->db->query("SELECT d.report_no, d.audited_areas, d.audit_date_1, d.status, e.lname, e.fname, CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, c.name,
                                    CONCAT(e.fname,' ',e.lname) as auditor_name FROM tbl_country as a
            LEFT JOIN tbl_province as b ON b.id = a.id                         
            LEFT JOIN tbl_company as c ON c.country = a.iso2_code
            RIGHT JOIN tbl_report_summary as d ON d.company_id = c.company_id
            LEFT JOIN tbl_auditor_info as e ON e.auditor_id = d.auditor_id

            WHERE a.id = $id 
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }

    function get_search_province($province_id, $offset, $limit){

       $query = $this->db->query("SELECT c.report_id, c.template_id, c.report_no, c.status, CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, e.name,
                                    CONCAT(d.fname,' ',d.lname) as auditor_name FROM tbl_province as a
            LEFT JOIN tbl_company as b ON a.province_id = b.address3
            RIGHT JOIN tbl_report_summary as c ON c.company_id = b.company_id
            LEFT JOIN tbl_auditor_info as d ON d.auditor_id = c.auditor_id
            LEFT JOIN tbl_company as e ON e.company_id = c.company_id 
            WHERE a.province_id = $province_id
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }

    function get_search_lead_auditor($auditor_id){

       $query = $this->db->query("SELECT a.report_no, a.audited_areas, a.audit_date_1, a.status, CONCAT(b.fname,' ',b.lname) as auditor_name FROM tbl_report_summary as a
            LEFT JOIN tbl_auditor_info as b ON a.auditor_id = b.auditor_id
            WHERE auditor_name = $auditor_id ");
        $result = $query->result();
        return $result;
    }

    function get_search_product_interest($product_id, $limit, $offset){

       $query = $this->db->query("SELECT c.report_id, c.template_id, c.report_no, c.status, CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, e.name,
                                    CONCAT(d.fname,' ',d.lname) as auditor_name FROM tbl_product as a
            LEFT JOIN tbl_company as b ON b.company_id = a.company_id
            RIGHT JOIN tbl_report_summary as c ON c.company_id = b.company_id
            LEFT JOIN tbl_auditor_info as d ON d.auditor_id = c.auditor_id
            LEFT JOIN tbl_company as e ON e.company_id = c.company_id 
            WHERE a.product_id = $product_id
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
    }

    function get_search_date_of_audit($date1, $date2, $limit, $offset)
   {    
     $query = $this->db->query("SELECT a.report_id, a.template_id, a.report_no, a.status, CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, b.name,
                                    CONCAT(c.fname,' ',c.lname) as auditor_name 
            FROM tbl_report_summary as a 
            LEFT JOIN tbl_company as b ON b.company_id = a.company_id
            LEFT JOIN tbl_auditor_info as c ON c.auditor_id = a.auditor_id
            WHERE a.audit_date_1  >= '$date1' AND a.audit_date_2 <= '$date2'
            LIMIT $offset, $limit");
        $result = $query->result();
        return $result;
        // $this->db->select('*');
        // $this->db->from($table);
        // $this->db->where('audit_date_1 >=', $date1);
        // $this->db->where('audit_date_2 <=', $date2);
        // $result = $this->db->get()->result();
        // return $result;
        
    }

    function date_of_report_submission($table, $create_date, $date1){
         $query = $this->db->query("SELECT a.report_id, a.template_id, a.report_no, a.status, CONCAT(DATE_FORMAT(audit_date_1,'%b'),'. ',DAY(audit_date_1),'-',DAY(audit_date_2),', ',YEAR(audit_date_2)) as audit_date, b.name,
                                    CONCAT(c.fname,' ',c.lname) as auditor_name 
            FROM tbl_report_summary as a 
            LEFT JOIN tbl_company as b ON b.company_id = a.company_id
            LEFT JOIN tbl_auditor_info as c ON c.auditor_id = a.auditor_id
            WHERE date(a.create_date)  = '$create_date' ");
        $result = $query->result();
        return $result;
    }



    function get_last_inspection_changes($id){
        $this->db->select("*");
        $this->db->from("tbl_inspection_changes");
        $this->db->where('report_id', $id);
        $this->db->order_by('update_date', "desc");
        $result = $this->db->get()->result();
        return $result;
    }

    function get_disposition2($id){
       $this->db->select("*");
        $this->db->from("qv_report_scope_product_disposition");
        $this->db->where('report_id', $id);
        $this->db->group_by('report_id');
        $this->db->group_by('disposition_id');
        $result = $this->db->get()->result();
        return $result;
    }

    function preaudit_documents($id){
       $this->db->select("*");
        $this->db->from("tbl_pre_audit_documents");
        $this->db->where('report_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }

    function stanard_reference($template_id){
       $this->db->select("*");
        $this->db->from("qv_standard_reference");
        $this->db->where('template_id', $template_id);
        $result = $this->db->get()->result();
        return $result;
    }
    function license_Accreditation($reportid){
       $this->db->select("*");
        $this->db->from("tbl_references");
        $this->db->where('report_id', $reportid);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_audit_dates($reportid){
       $this->db->select("*");
        $this->db->from("tbl_report_audit_date");
        $this->db->where('report_id', $reportid);
        $this->db->group_by('audit_date');
        $result = $this->db->get()->result();
        return $result;
    }
    function get_disposistion_product($reportid, $dispositionid){
       $this->db->select("product_name");
        $this->db->from("qv_report_scope_product_disposition");
        $this->db->where('report_id', $reportid);
        $this->db->where('disposition_id', $dispositionid);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_productname($product_id){
        $this->db->select("product_name");
        $this->db->from("tbl_product");
        $this->db->where('product_id', $product_id);
        $result = $this->db->get();
        $data = $result->result_array();

        return $data[0]['product_name'];
    }

    function get_sub_activities($activityid){
        $this->db->select("*");
        $this->db->from("tbl_sub_activities");
        $this->db->where('activity_id', $activityid);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_products($companyid){
        $this->db->select("*");
        $this->db->from("tbl_product");
        $this->db->where('company_id', $companyid);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_product_type($templateid){
        $this->db->select("*");
        $this->db->from("qv_classification_from_template");
        $this->db->where('template_id', $templateid);
        $result = $this->db->get()->result();
        return $result;
    }

      function get_reviewer($reportid){
        $this->db->select("*");
        $this->db->from("qv_reviewer");
        $this->db->where('report_id', $reportid);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_approver($reportid){
        $this->db->select("*");
        $this->db->from("qv_approver");
        $this->db->where('report_id', $reportid);
        $result = $this->db->get()->result();
        return $result;
    }


    function get_elements($reportid){
        $this->db->select("*");
        $this->db->from("qv_elements");
        $this->db->where('report_id', $reportid);
        $result = $this->db->get()->result();
        return $result;
    }
    function get_elements_q_a($reportid,$elementid){
        $this->db->select("*");
        $this->db->from("qv_questions_answers");
        $this->db->where('report_id', $reportid);
        $this->db->where('element_id', $elementid);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_audit_observation($reportid){
        $this->db->select("*");
        $this->db->from("qv_audit_observation");
        $this->db->where('report_id', $reportid);
        $this->db->group_by('category_name');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_audit_observation_yes($reportid){
        $this->db->select("*");
        $this->db->from("qv_observation_yes");
        $this->db->where('report_id', $reportid);
        $this->db->group_by('element_id');
        $result = $this->db->get()->result();
        return $result;
    }

    function add_report_submission_status($reportid,$remarks,$status,$position){
        $data = array(
                "report_id"=>$reportid, 
                "date"=> date("Y-m-d H:i:s"),
                "name"=>$this->session->userdata('name_approval'),
                "position"=>$position,
                "status"=>$status,
                "remarks"=>$remarks
            );
        $this->db->insert("tbl_report_submission_remarks",$data);
    }

     function add_report_submission_status_coauditor($reportid,$remarks,$status,$position, $name){
        $data = array(
                "report_id"=>$reportid, 
                "date"=> date("Y-m-d H:i:s"),
                "name"=>$name,
                "position"=>$position,
                "status"=>$status,
                "remarks"=>$remarks
            );
        $this->db->insert("tbl_report_submission_remarks",$data);
    }

    function add_report_submission_status_auditor($reportid,$remarks,$status,$position){
        $data = array(
                "report_id"=>$reportid, 
                "date"=> date("Y-m-d H:i:s"),
                "name"=>$this->session->userdata('name'),
                "position"=>$position,
                "status"=>$status,
                "remarks"=>$remarks
            );
        $this->db->insert("tbl_report_submission_remarks",$data);
    }

    function signature_stamp($data, $status)
    {   
         $this->db->insert("tbl_report_signature_stamp",$data);
    }
    function update_signature_stamp($reportid,$data)
    {
        $this->db->where("report_id",$reportid);
        $this->db->update("tbl_report_signature_stamp",$data);
    }

    function get_inspection_dates($companyid){
        $this->db->limit(2);
        $this->db->select("*");
        $this->db->from("tbl_audit_history");
        $this->db->where('supplier_id', $companyid);
        $this->db->order_by('id', 'desc');
        $result_history = $this->db->get()->result();
        
        $audit_dates = "";
        $inspectors = "";
        foreach ($result_history as $key => $value) {
            $this->db->select("Date");
            $this->db->from("tbl_audit_history_dates");
            $this->db->where("history_id",$value->id);
            $audit_dates = $this->db->get()->result();

            $this->db->select("inspector");
            $this->db->from("tbl_audit_history_inspectors");
            $this->db->where("history_id",$value->id);
            $inspectors = $this->db->get()->result();

            $result_array[] = array("audit_date"=>$audit_dates,"inspectors"=>$inspectors);
        }

        return $result_array;
    }

    function get_version($reportid){
        $this->db->select("version");
        $this->db->from("tbl_report_summary");
        $this->db->where('report_id', $reportid);
        $result = $this->db->get();
        $data = $result->result_array();

        return $data[0]['version'];
    }

    function update_version($reportid,$version)
    {
        $this->db->where("report_id",$reportid);
        $this->db->update("tbl_report_summary",array("version"=>$version));
    }

    function check_used_activity($activity_id,$report_id)
    {
        $this->db->select("*");
        $this->db->where("report_id",$report_id);
        $this->db->where("activity_id",$activity_id);
        $this->db->from("tbl_report_activities");
        $q = $this->db->get();

        if($q->num_rows() > 0 ){
            return "checkbox_block.jpg";
        } else {
            return "checkbox_open.jpg";
        }
    }

    function check_used_subactivity($subactivity_id,$report_id)
    {
        $this->db->select("*");
        $this->db->where("sub_item_id",$subactivity_id);
        $this->db->where("report_id",$report_id);
        $this->db->from("tbl_report_activities");
        $q = $this->db->get();

        if($q->num_rows() > 0 ){
            return "checkbox_block.jpg";
        } else {
            return "checkbox_open.jpg";
        }
    }

    public function get_co_auditor_stamp($report_id,$auditor_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_report_coauditor_submission");
        $this->db->where("report_id",$report_id);
        $this->db->where("auditor_id",$auditor_id);
        $q = $this->db->get();
        $data = $q->result_array();
        if($q->num_rows() > 0 ){
            return $data[0]['update_date'];
        } else {
            return null;
        }
        
    }


}