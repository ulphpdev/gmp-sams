<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Q_maintenance extends CI_Model{


	public function get_list($table,$query = null, $limit, $offset, $order = null){

		$this->db->limit($limit,$offset);
		$this->db->select("*");
		$this->db->from($table);

		if($query != null) {
			$this->db->where($query);
		}

		$this->db->where("status > -1");
		if($order != null) {
			$this->db->order_by($order, "desc");
		}
		$q = $this->db->get();
		return $q->result();

	}

	public function get_pagination($table,$query = null){

		$this->db->select("*");
		$this->db->from($table);
		if($query != null) {
			$this->db->where($query);
		}
		$this->db->where("status > -1");
		$q = $this->db->get();
		return $q->num_rows();

	}


	public function is_checking($table, $field, $item){
		$this->db->select("*");
		$this->db->where($field, $item);
		$this->db->where("status", "1");
		$this->db->from($table);
		$q = $this->db->get();
		return $q->num_rows();

	}

	public function content_insert($table, $data)
	{

		$this->db->insert($table,$data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}
	public function content_update($table, $query, $data)
	{

		$this->db->where($query);
		$this->db->update($table,$data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function update_status($where, $status, $field, $table) { 
        $sql = $this->db->query("UPDATE ".$table." SET status = '".$status."' WHERE ".$field." IN (".$where.")");
    }
	

}