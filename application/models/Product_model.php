<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Manila');

class Product_model extends CI_Model{ 



	function __construct() {

        parent::__construct();

        $this->load->helper('array');

    }



	public function getlist_product_model($limit,$offset,$sort){

		$this->db->select("p.*,c.name as company_name");

		$this->db->from('tbl_product as p');

		$this->db->join('tbl_company as c','c.company_id = p.company_id');

		$this->db->where("p.status","1")->or_where("p.status","0");

		$this->db->order_by($sort, 'DESC');

		$this->db->limit($limit,$offset);

		$result = $this->db->get()->result();

		return $result;

	}



	function get_product($id){

		$this->db->select("p.*,c.name as company_name");

		$this->db->from('tbl_product as p');

		$this->db->join('tbl_company as c','c.company_id = p.company_id');

		$this->db->where("product_id",$id);

		$result = $this->db->get()->result();

		return $result;

	}

}

?>