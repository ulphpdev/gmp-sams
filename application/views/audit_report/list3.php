
<style>
	
	.control-label{
		font-size: 12px;
	}

</style>
<ol class="breadcrumb" style="background-color: #fff;">
  	<li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
  	<li class="active">Audit Report</li>
</ol>


<div class="panel panel-default">
	<div class="panel-heading" style="background-color:  #fff">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#filter">Filter</a>
      </h4>
    </div>
  	<div class="panel-body"  style="padding-bottom: 17px;">
  		<div class="col-md-6 form-horizontal">
  			<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Audit Report No :</label>
				    <div class="col-sm-8">
				      	<input type="text" class="form-control filter-report-no input-sm" placeholder="">
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Audited Site :</label>
				    <div class="col-sm-8">
				      	<input type="text" class="form-control filter-audited-site input-sm" placeholder="">
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Report Status : </label>
				    <div class="col-sm-8">
				      	<select type="text" class="form-control filter-audit-status input-sm">
		            		<option selected value=""></option>
		            		<option value="0">Draft</option>
		            		<option value="1">Co-Auditor's Review</option>
		            		<option value="3">Submitted to Department Head</option>
		            		<option value="4">Submitted to Division Head</option>
		            		<option value="5">Approved by Division Head</option>
		            	</select>
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Auditor : </label>
				    <div class="col-sm-8">
				      	<select class="form-control auditor-filter input-sm">
							<option class="pull-right" value="" selected></option>
							<?php foreach ($auditors as $value) { ?>
								<option value="<?= $value->auditor_id;?>"><?= $value->fname . " " . $value->lname;?></option>
							<?php } ?>
						</select>
				    </div>
				</div>
	      	</span>
	      	<span id="report_no">
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Co-Auditor : </label>
				    <div class="col-sm-8">
				      	<select class="form-control co-auditor-filter input-sm">
							<option class="pull-right" style="max-width: 300px;" value="" selected></option>
							<?php foreach ($auditors as $value) { ?>
								<option value="<?= $value->auditor_id;?>"><?= $value->fname . " " . $value->lname;?></option>
							<?php } ?>
						</select> 
				    </div>
				</div>
	      	</span>

	      	<div class="form-group">
				    <label class="col-sm-4 control-label">Country :</label>
				    <div class="col-sm-8">
				      	<select class="form-control country-filter input-sm">
							<option value="" selected ></option>
							<?php foreach ($country_list as $value) { ?>
								<option value="<?= $value->country_code;?>"><?= $value->country_name;?></option>
							<?php } ?>
						</select> 
				    </div>
				</div>
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Product Type :</label>
				    <div class="col-sm-8">
				      	<select class="form-control type-of-product-filter input-sm">
							<option class="pull-right" value="" selected></option>
							<?php foreach ($type_of_products as $value) { ?>
								<option style="max-width: 300px;" value="<?= $value->classification_id;?>"><?= $value->classification_name;?></option>
							<?php } ?>
						</select> 
				    </div>
				</div>
	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Product of Interest :</label>
				    <div class="col-sm-8">
				      	<select class="form-control product-filter input-sm">
							<option class="pull-right" style="max-width: 300px;" value="" selected></option>
							<?php foreach ($products_list as $value) { ?>
								<option value="<?= $value->product_id;?>"><?=  $value->product_name;?></option>
							<?php } ?>
						</select>
				    </div>
				</div>

	          	<div class="form-group">
				    <label class="col-sm-4 control-label">Disposition :</label>
				    <div class="col-sm-8">
				      	<select class="form-control disposition-filter input-sm">
							<option class="pull-right" style="max-width: 300px;" value="" selected></option>
							<?php foreach ($disposition_list as $value) { ?>
								<option value="<?= $value->disposition_id;?>"><?=  $value->disposition_name;?></option>
							<?php } ?>
						</select>
				    </div>
				</div>
				

  				<div class="form-group">
				    <label class="col-sm-4 control-label">Date Modified :</label>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker3" class="form-control date-modified-start input-sm" placeholder="From" />
				    </div>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker4" class="form-control date-modified-end input-sm" placeholder="To" />
				    </div>
				</div>
		      	
		      	


  		</div>
  		<div class="col-md-6 form-horizontal">
  			<span id="report_no">


  				<div class="form-group">
				    <label class="col-sm-4 control-label">Audit Date :</label>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker1" class="form-control audit-date-start input-sm" placeholder="From" />
				    </div>
				    <div class="col-sm-4">
				    	<input type="text" id="datepicker2" class="form-control audit-date-end input-sm" placeholder="To" />
				    </div>
				</div>


				<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Preparation :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-preparation-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-preparation-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Date Reviewed :</label>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-reviewed-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-reviewed-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Review :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-review-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-review-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Date Approved :</label>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-approved-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="text" class="form-control date-approved-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Approval :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-approval-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-approval-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Lead Time Total :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-total-start input-sm" placeholder="Minimum">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control lead-time-total-end input-sm" placeholder="Maximum">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">GMP Score % :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control gmp-score-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control gmp-score-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>

		      	<span id="report_no">
		          	<div class="form-group">
					    <label class="col-sm-4 control-label">Coverage % :</label>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control coverage-from input-sm" placeholder="From">
					    </div>
					    <div class="col-sm-4">
					      	<input type="number" class="form-control coverage-to input-sm" placeholder="To">
					    </div>
					</div>
		      	</span>
	      	</span>
  		</div>
  	</div>
  	<div class="panel-footer" style="background-color: #fff;text-align: right;">
  		<button data-value="search" class="filter btn btn-success btn-sm">Search</button>

  		<iframe id="download_csv" hidden></iframe>
  		<button data-value="csv" class="filter filter-csv btn btn-info btn-sm">Export CSV</button>
  		<button class="filter_reset btn btn-default btn-sm">Reset</button>
  		<div class="clearfix"></div>
  	</div>
</div>


<div class="row" style="margin-top: -10px;padding-left: 17px;padding-right: 15px;">
	<div class="col-md-12 table-responsive">
		<table class = "table listdata table-striped" style="margin-bottom:0px; ">
          	<thead>
            	<tr>
           		 	<th>Audit Report No.</th>
            		<th>Audited Site</th>
            		<th>Auditor</th>
            		<th>Audit Date</th>
            		<th>Modified Date</th>
            		<th>Status</th>
            		<th style="width:10px;">Version</th>
            		<th style="width:150px; text-align: right;">Action</th>
          		</tr>  
          	</thead>
          	<tbody class="table_body"></tbody>
		</table>
	</div>

	<div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>

</div>



<div id="view-div-panel" class="rows div-view-audit-report " hidden style="margin-top: 30px;" >
	<div class="panel panel-default">
  		<div class="panel-body"  style="padding-bottom: 17px;">
			<div class="view_report_content"></div>
		</div>
	</div>
</div>


<div id="generate_PDF_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Generate Report</h4>
      		</div>
      		<div class="modal-body">
      			<input class="pdf-picker-id" hidden />
     			<input class="pdf-picker-no" hidden />
        		<select class="form-control pdf-picker">
        			<option value="generate_audit_report">AUDIT REPORT</option>
        			<option value="generate_executive_summary">EXECUTIVE SUMMARY</option>
        			<option value="generate_annexure">ANNEXURE - GMP AUDIT DETAILS [.PDF]</option>
        			<option value="generate_annexure_doc">ANNEXURE - GMP AUDIT DETAILS [.DOC]</option>
        			<option value="generate_raw_data_report">RAW DATA REPORT</option>
        		</select>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary btn-generate-pdf">Generate</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="export_PDF_modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Generate Report</h4>
      		</div>
      		<div class="modal-body">
      			<input class="pdf-picker-id" hidden />
     			<input class="pdf-picker-no" hidden />
        		<select class="form-control pdf-picker-export">
        			<option value="Audit_Report.pdf">AUDIT REPORT</option>
        			<option value="Executive_Report.pdf">EXECUTIVE SUMMARY</option>
        			<option value="Annexure.pdf">ANNEXURE - GMP AUDIT DETAILS [.PDF]</option>
        			<option value="Annexure.docx">ANNEXURE - GMP AUDIT DETAILS [.DOC]</option>
        			<option value="Raw_Data_Report.pdf">RAW DATA REPORT</option>
        		</select>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary btn-generate-export">Generate</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="approve_send_email" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Send Audit Report</h4>
      		</div>
      		<div class="modal-body"> 
        		<form action="" class="form-horizontal">   
		            <div class="form-group">   
		            	<div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2">To*: </div>
					        <div class="col-md-10">
					        	<textarea class="form-control inputs txthidden" id="sender_other_approved" hidden></textarea>
					        	<span class=""  style="font-size:10px;">Note: To add (+) more email, please separate with a comma (,)</span>
					        	<br>
					        	<span class="er-msg">Email should not be empty</span>
					        </div>
					    </div>  
					    <div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2" >Subject*: </div>
					        <div class="col-md-10">
					        	<input type="text"  name="" class="form-control inputs" id="subject_other_approved" >
					        	<span class="er-msg">Subject should not be empty</span>
					        </div>
					    </div>
					    <div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2">Message: </div>
					        <div class="col-md-10">
					        	<textarea class="form-control" id="message_other_approved" rows="5"></textarea>
					        </div>
				      	</div>
		            </div>
        		</form>
      		</div>

      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="send_audit_email_approved"  data-id="">Send</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<div id="audit_email-modal" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Send Audit Report</h4>
      		</div>
      		<div class="modal-body"> 
      			<div class="alert alert-warning email_alert">
				  	Unable to email. Answers for the template used are incomplete. Kindly log on to DART to answer them.
				</div>
        		<form action="" class="form-horizontal">
            		<input type="checkbox" id="co_auditor" class="dda ca cb" value="co_auditor"> Co-Auditor<br>
            		<input type="checkbox" id="department_head" class="dda dh cb" value="department_head"> Department Head<br>
            		<input type="checkbox" id="division_head" class="dda cb" value="division_head"> Division Head<br>
            		<input type="checkbox" id="other" class="other cb" id="other" value="other" disabled=""> Other<br>     
		            <div class="form-group">     
		              	<div class="col-md-12 pad-0 mb5">
					        <div class="col-md-2 txthidden" hidden>To*: </div><div class="col-md-10 txthidden" hidden><textarea class="form-control inputs txthidden" id="sender_other" hidden></textarea><span class="txthidden" hidden style="font-size:10px;">Note: To add (+) more email, please separate with a comma (,)</span><br><span class="er-msg">Email should not be empty</span></div>

					      </div>

					      <div class="col-md-12 pad-0 mb5">

					        <div class="col-md-2 txthidden" hidden>Subject*: </div><div class="col-md-10 txthidden" hidden><input type="text"  name="" class="form-control inputs txthidden" id="subject_other" hidden><span class="er-msg">Subject should not be empty</span></div>

					      </div>

					      <div class="col-md-12 pad-0 mb5">

					        <div class="col-md-2 txthidden" hidden>Message: </div><div class="col-md-10 txthidden" hidden><textarea class="form-control txthidden" id="message_other" height="100" hidden></textarea></div>

				      	</div>
        		</form>
      		</div>

      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="send_audit_email"  data-id="">Send</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>

<script	src="<?= base_url();?>asset/js/helper.js"></script>
<script>

	var limit = 10;
	var offset = 0;
	var query = "";


	var query_select = "a.*";
	var query_table = "ar_general a";
	var query_wheres = "";
	var query_join = "";
	var full_query = "";

	$(document).ready(function() {
		get_list();
		get_pagination();
		CKEDITOR.replace('message_other');
		CKEDITOR.replace('message_other_approved');
	})



	$(function(){
	    $(".filter-date-modified").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-reviewed-from").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-reviewed-to").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-approved-from").datepicker({ dateFormat: 'yy-mm-dd' });
	    $(".date-approved-to").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker1").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker3").datepicker({ dateFormat: 'yy-mm-dd' });
	    $("#datepicker4").datepicker({ dateFormat: 'yy-mm-dd' });
	});






	$(document).on('change','.pager_number', function() {
	    var page_number = parseInt($(this).val());
	    var page = (page_number - 1) * limit;
	    offset = page;
	    get_list();
	});

	$(document).on('click','.first-page', function() {
		var page_number = parseInt($('.page_number').val());
		if(page_number!=first()){
			var page = (first() - 1) * limit;
			offset = page;
	 	 	get_list();
	  		$('.pager_number').val($('.pager_number option:first').val());
		}
	});

	$(document).on('click','.prev-page', function() {
	    var page_number = parseInt($('.pager_number').val());
	    if(page_number!=first()){
		    var next = page_number - 2;
		    offset = next * limit;
		    get_list();
		    $('.pager_number').val(next + 1);
		}
	});

	$(document).on('click','.next-page', function() {
	    var page_number = parseInt($('.pager_number').val());
	    if(page_number!=last()){
		    var next = page_number + 1;
		    offset = (next - 1) * limit;
		    get_list();
		    $('.pager_number').val(next);
		}
	});


	$(document).on('click','.last-page', function() {
		var page_number = parseInt($('.page-number').val());
		if(page_number!=last()){
			var page = (last() - 1) * limit;
			offset = page;
	 	 	get_list();
	  		$('.pager_number').val($('.pager_number option:last').val());
		}
	});


	function first(){
	   return parseInt($('.pager_number option:first').val());
	}

	function last(){
	   return parseInt($('.pager_number option:last').val());
	}



	////////////////////////////////////////////FILTER//////////////////////////////////////////
	$(document).on('click','.filter_reset', function($e) {
		$e.preventDefault();
		query_wheres = "";
		query_join = "";

		$('.filter-report-no').val("");
		$('.filter-audited-site').val("");
		$('.date-modified-start').val("");
		$('.date-modified-end').val("");
		$('.audit-date-start').val("");
		$('.audit-date-end').val("");
		$('.date-reviewed').val("");
		$('.date-approved').val("");
		$('.lead-time-preparation-start').val("");
		$('.lead-time-preparation-end').val("");
		$('.lead-time-review-start').val("");
		$('.lead-time-review-end').val("");
		$('.lead-time-approval-start').val("");
		$('.lead-time-approval-end').val("");
		$('.lead-time-total-start').val("");
		$('.lead-time-total-end').val("");
		$('.gmp-score').val("");
		$('.coverage').val("");

		$('.filter-audit-status').val($(".filter-audit-status option:first").val());
		$('.auditor-filter').val($(".auditor-filter option:first").val());
		$('.type-of-product-filter').val($(".type-of-product-filter option:first").val());
		$('.co-auditor-filter').val($(".co-auditor-filter option:first").val());
		$('.country-filter').val($(".country-filter option:first").val());
		$('.product-filter').val($(".product-filter option:first").val());
		$('.disposition-filter').val($(".disposition-filter option:first").val());

		get_list();
		get_pagination();

		$('.filter-csv').hide();
	});


	$(document).on('click','.filter', function($e) {
		$e.preventDefault();

		query_wheres = "";
		query_join = "";
		limit = 10;
		offset = 0;

		if($('.filter-report-no').val() != ""){
			query_wheres += "a.Report_No = '"+ $('.filter-report-no').val() + "' ";
		}

		if($('.filter-audited-site').val() != ""){
			query_wheres += "AND a.Audited_Site like '%"+ $('.filter-audited-site').val() + "%' ";
		}

		if($('.filter-audit-status').val() != ""){
			if($('.filter-audit-status').val() == 1){
				query_wheres += "AND (a.status = 1 OR a.status = 2) ";
			} else {
				query_wheres += "AND a.status = '"+ $('.filter-audit-status').val() + "' ";
			}
			
		}

		if($('.auditor-filter').val() != ""){
			query_wheres += "AND a.auditor_id = '"+ $('.auditor-filter').val() + "' ";
		}

		if($('.date-modified-start').val() != "" && $('.date-modified-end').val() != ""){
			query_wheres += "AND DATE(a.modified_date) BETWEEN '" + $('.date-modified-start').val() + "' AND '" + $('.date-modified-end').val() + "'";
		}

		if($('.audit-date-start').val() != "" && $('.audit-date-end').val() != ""){
			query_join += "LEFT JOIN ar_audit_dates b ON a.report_id = b.report_id ";
			query_wheres += "AND b.First_Audit_Date >= '" + $('.audit-date-start').val() + "' AND b.Last_Audit_Date <='" + $('.audit-date-end').val() + "'";
		}

		if($('.type-of-product-filter').val() != ""){
			query_join += "LEFT JOIN ar_product_type c ON a.report_id = c.report_id  ";
			query_wheres += "AND c.Product_Type_ID ='" + $('.type-of-product-filter').val() + "'";
		}

		if($('.co-auditor-filter').val() != ""){
			query_join += "LEFT JOIN ar_coauditor d ON a.report_id = d.report_id ";
			query_wheres += "AND d.co_auditor_id ='" + $('.co-auditor-filter').val() + "'";
		}

		if($('.country-filter').val() != ""){
			query_join += "LEFT JOIN ar_company e ON a.report_id = e.report_id ";
			query_wheres += "AND e.country ='" + $('.country-filter').val() + "'";
		}

		if($('.product-filter').val() != ""){
			query_join += "LEFT JOIN ar_product_of_interest f ON a.report_id = f.report_id ";
			query_wheres += "AND f.product_of_interest_id ='" + $('.product-filter').val() + "'";
		}

		if($('.disposition-filter').val() != ""){
			query_join += "LEFT JOIN ar_disposition g ON a.report_id = g.report_id ";
			query_wheres += "AND g.disposition_id ='" + $('.disposition-filter').val() + "'";
		}

		if($('.date-reviewed-from').val() != "" && $('.date-reviewed-to').val() != ""){
			query_join += "LEFT JOIN tbl_report_signature_stamp h ON a.report_id = h.report_id ";
			query_wheres += "AND DATE(h.review_date) BETWEEN '" + $('.date-reviewed-from').val() + "' AND '" + $('.date-reviewed-to').val() + "'";
		}

		if($('.date-approved-from').val() != "" && $('.date-approved-to').val() != ""){
			query_join += "LEFT JOIN tbl_report_signature_stamp z ON a.report_id = z.report_id ";
			query_wheres += "AND DATE(z.approved_date) BETWEEN '" + $('.date-approved-from').val() + "' AND '" + $('.date-approved-to').val() + "'";
		}

		if($('.lead-time-preparation-start').val() != "" && $('.lead-time-preparation-end').val() != ""){
			query_join += "LEFT JOIN ar_lead_time_preparation i ON a.report_id = i.report_id ";
			query_wheres += "AND  i.Lead_Time_Preparation BETWEEN " + $('.lead-time-preparation-start').val() + " AND " + $('.lead-time-preparation-end').val();
		}

		if($('.lead-time-review-start').val() != "" && $('.lead-time-review-end').val() != ""){
			query_join += "LEFT JOIN ar_lead_time_review j ON a.report_id = j.report_id ";
			query_wheres += "AND  j.Lead_Time_Review BETWEEN " + $('.lead-time-review-start').val() + " AND " + $('.lead-time-review-end').val();
		}

		if($('.lead-time-approval-start').val() != "" && $('.lead-time-approval-end').val() != ""){
			query_join += "LEFT JOIN ar_lead_time_approval k ON a.report_id = k.report_id ";
			query_wheres += "AND  k.Lead_Time_Approval BETWEEN " + $('.lead-time-approval-start').val() + " AND " + $('.lead-time-approval-end').val() ;
		}

		if($('.lead-time-total-start').val() != "" && $('.lead-time-total-end').val() != ""){
			query_join += "LEFT JOIN ar_lead_time_total l ON a.report_id = l.report_id ";
			query_wheres += "AND  l.Lead_Time_Total BETWEEN " + $('.lead-time-total-start').val() + " AND " + $('.lead-time-total-end').val();
		}

		if($('.gmp-score-from').val() != "" && $('.gmp-score-to').val() != ""){
			query_join += "LEFT JOIN gmp_computation_rating m ON a.report_id = m.report_id ";
			query_wheres += "AND m.Rating BETWEEN " + $('.gmp-score-from').val() + " AND " + $('.gmp-score-to').val();
		}

		if($('.coverage-from').val() != "" && $('.coverage-to').val() != ""){
			query_join += "LEFT JOIN gmp_computation_coverage n ON a.report_id = n.report_id ";
			query_wheres += "AND n.Coverage BETWEEN " + $('.coverage-from').val() + " AND " + $('.coverage-to').val();
		}

		if(getFirstWord(query_wheres) == "AND"){
			query_wheres = query_wheres.substr(4,query_wheres.length);
		}

		if($(this).attr('data-value') == 'search'){
			get_list();
			get_pagination();
			$('.filter-csv').show();
		} 

		if($(this).attr('data-value') == 'csv'){
			get_csv();
		}

	});
	
	

	function get_list(){
		
		isLoading(true);
		console.log(query_select + " " + query_table + " " + query_join + " " + query_wheres); 
		aJax.post(
			"<?=base_url('c_auditreport/get_data');?>",
			{
				'limit' : limit,
				'offset' : offset,
				'select' : query_select,
				'table' : query_table,
				'join' : query_join,
				'where' : query_wheres
			},
			function(result){
				var obj = isJson(result);
				var table_body = new TBody();
				if(obj.length!=0){
					$.each(obj, function(key,value){
						table_body.td(value.Report_No);
						table_body.td(value.Audited_Site);
						table_body.td(value.Auditor_Name);
						table_body.td(gmp.audit_dates(value.Audit_Dates));
						table_body.td(moment(value.modified_date).format('LL'));
						table_body.td( gmp.status(value.Report_Status));
						table_body.td(value.version);

						var list_data = new UList();
						list_data.set_liclass("li-action");
						list_data.set_ulclass("ul-action");

						if(value.Report_Status == 5) {   

							<?php if($this->session->userdata('sess_role') == 1) { ?>
								list_data.li("<a href='#' id='"+value.Report_ID+"' id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='Archive' class='report-archive action'><span class='glyphicon glyphicon-briefcase'></span></a>");
							<?php } ?>
							
							if(value.display_email == "display") {
								list_data.li("<a href='#' id='"+value.Report_ID+"' id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='Email' class='export-send action'><span class='glyphicon glyphicon-envelope'></span></a>");
							}

							list_data.li("<a href='#' id='"+value.Report_ID+"' id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");

							list_data.li("<a href='#' id='"+value.Report_ID+"' id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='Generate Report' class='generate-export action'><span class='glyphicon glyphicon-save-file'></span></a>");

						}

						if(value.Report_Status != 5) { 
							list_data.li("<a href='#' id='"+value.Report_ID+"' id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='Generate Report' class='report-generate action'><span class='glyphicon glyphicon-save-file'></span></a>");
							
							if(value.display_email == "display") {
								list_data.li("<a href='#' id='"+value.Report_ID+"' id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='Email' class='report-send action'><span class='glyphicon glyphicon-envelope'></span></a>");
							}
							list_data.li("<a href='#' id='"+value.Report_ID+"' id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");
							list_data.li("<a href='#' id='"+value.Report_ID+"'  id-number='"+value.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='View' class='report-view action'><span class='glyphicon glyphicon-eye-open'></span></a>");
						}
						table_body.td(list_data.set());
						table_body.set();
					});
				} else {
					table_body.td_norecord(8);
                	table_body.set();
				}
				table_body.append(".table_body");
				
				isLoading(false);
			}

		);
	}

	function get_pagination()
	{
		var page_query = "";
		if(query_wheres != ""){
			if(query_join != ""){
				page_query = "SELECT " + query_select + " FROM " + query_table + " " + query_join + " WHERE " + query_wheres + " AND status >= 0 AND status <=5 GROUP BY a.Report_No ";
			} else {
				page_query = "SELECT " + query_select + " FROM " + query_table + " WHERE " + query_wheres + " AND status >= 0 AND status <=5 GROUP BY a.Report_No ";
			}
		} else {
			page_query = "SELECT " + query_select + " FROM " + query_table + " WHERE status >= 0 AND status <=5  GROUP BY a.Report_No";
		}
		aJax.post(
	        "<?=base_url("c_auditreport/get_data_pagination");?>",
	        {
	            'query':page_query
	        },
	        function(result){
	        	console.log(result);
	            var no_of_page = Math.ceil(result / limit);
	            var pagination = new Pagination(); //please check helper.js for this function
	            pagination.set_total_page(no_of_page);
	            pagination.set('.pager_div');
	        }
	    );
	}

	function getFirstWord(str) {
        var spacePosition = str.indexOf(' ');
        if (spacePosition === -1)
            return str;
        else
            return str.substr(0, spacePosition);
    };


	/////////////////////////////////////////////////////////////////


	$(document).on('click','.report-view', function($e) {
		$e.preventDefault();
		var id = $(this).attr('id');
		$.ajax({
		type: 'Post',
		url:'<?=base_url();?>c_auditreport/audit_report_view',
		data:{id:id, div : 'remarks-view'},
		}).done( function(data){
		  	$('.view_report_content').html(data);
		  	$('.div-view-audit-report').show();
		  	$('html, body').animate({
	            scrollTop: $("#view-div-panel").offset().top
	        }, 1000);
		})
	}) ;



	$(document).on('click','.report-generate', function($e) {
		$e.preventDefault();
		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		$('.pdf-picker-id').val(id);
		$('.pdf-picker-no').val(report_no);
		$("#generate_PDF_modal").modal({ show: true, backdrop: "static"});
	}) ;


	$(document).on('click','.generate-export', function($e) {
		$e.preventDefault();
		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		$('.pdf-picker-id').val(id);
		$('.pdf-picker-no').val(report_no);
		$('.btn-generate-export').attr('id-number',report_no);
		$("#export_PDF_modal").modal({ show: true, backdrop: "static"});
	}) ;

	$(document).on('click','.btn-generate-export', function($e) {
		$e.preventDefault();
		var report_no = $(this).attr('id-number');
		var id = $('.pdf-picker-id').val();
		var report_no = $('.pdf-picker-no').val();
		var link = $('.pdf-picker-export').val();
		var text = $('.pdf-picker-export option:selected').text()
		insert_audit_trail("Generate " + report_no + " - " + text);
		window.open("<?=base_url()?>json/export/approved/" + id + "/" + link);
	});

	$(document).on('click','.btn-generate-pdf', function($e) {
		$e.preventDefault();
		var id = $('.pdf-picker-id').val();
		var report_no = $('.pdf-picker-no').val();
		var link = $('.pdf-picker').val();
		var text = $('.pdf-picker option:selected').text()
		insert_audit_trail("Generate " + report_no + " - " + text);
		window.open("<?=base_url('Export_pdf')?>/" + link + '?report_id=' + id + "&action=view");
	});

	$(document).on('click','.report-trash', function($e) {
		$e.preventDefault();
		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		var dialog = bootbox.confirm({
			    message: "Are you sure you want to delete this record?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			    	if(result){
						$.ajax({
		                type: 'Post',
		                url:'<?=base_url();?>c_auditreport/audit_report_status_update',
		                data:{id:id, status:"-2"},
		                }).done( function(data){
		                    dialog.modal('hide');
		                    	update_audit_report_json();
		                    	insert_audit_trail("Delete Record " + report_no);
		                    	get_list(query, limit, offset, "Report_No");
								get_pagination(query, "Report_No");
		                });
					} 	       
			    }
			});
	}) ;



	$(document).on('click','.report-archive', function($e) {
		$e.preventDefault();
		
		var id = $(this).attr('id');
		var report_no = $(this).attr('id-number');
		var dialog = bootbox.confirm({
			message:"Are you sure you want to archive this record?",
		 	buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function(result){ 
			if(result){
				isLoading(true);
        		
				aJax.post(
					'<?=base_url();?>c_auditreport/audit_report_status_update',
					{
						id:id, status:"-1"
					},
					function(data){
						dialog.modal('hide');
	                    update_audit_report_json();
	                    insert_audit_trail("Archive Record " + report_no);
						isLoading(false);
						update_listing(id)
						bootbox.alert("successfully moved to archive folder.",function(){
							get_list();
							get_pagination();
						})
					}
				);

				}
			}	
		});
	}) ;

 

	$(document).on('click','#send_audit_email_approved', function() {
		var reportid = $(this).attr('data-id');
		var email = $('#sender_other_approved').val();
		var subject = $('#subject_other_approved').val();
		var message = CKEDITOR.instances.message_other_approved.getData();
		aJax.post(
			"<?=base_url("smtp/send/sendmail_other_approved");?>",
			{
				sender_other:email,
				subject_other:subject,
				message_other:message, 
				report_id:reportid
			},
			function(data){
				$('#approve_send_email').modal('hide');
				bootbox.alert("Annexure Word File & Rating Report are successfully sent!");
	        	insert_audit_trail("Email "+ reportid +" to " + email);
	        	$('#sender_other_approved').val("");
	        	$('#subject_other_approved').val("");
	        	$('#message_other_approved').html("");
			}
		);
	});


	$(document).on('click','.export-send', function($e) {

		$e.preventDefault();
		$('#approve_send_email').modal('show');
		var report_id = $(this).attr('id');
		var report_no= $(this).attr('id-number');
		$('#send_audit_email_approved').attr('data-id', report_id);
		$('#send_audit_email').attr('id-number', report_no);
	});

	$(document).on('click','.report-send', function($e) {

		$e.preventDefault();
		isLoading(true);

		$('.email_alert').hide();
		$('#co_auditor').attr('checked', false);
		$('#department_head').attr('checked', false);
		$('#division_head').attr('checked', false);
		$('#other').attr('checked', false);
	
	  	var report_id = $(this).attr('id');
	  	var report_no= $(this).attr('id-number');
		$('#send_audit_email').attr('data-id', report_id);
		$('#send_audit_email').attr('id-number', report_no);
		var template_id = $(this).attr('data-id');

		//check if answered all question 
		var total_unanswerd = 0;

		//start checking status
		aJax.post(
			"<?php echo base_url('Audit_report/check_status'); ?>",
			{
				'report_id':report_id
			},
			function(result){
				var obj = JSON.parse(result);
				$.each(obj, function(x,y){
					if(y.status == 5){
		          		$('.other').prop("disabled",false);
		          		$('.dda').prop("disabled", true);
		        	} else {      		

		        		aJax.post(
		        			"<?php echo base_url("c_auditreport/check_auditor_details"); ?>",
		        			{
		        				'report_id':report_id
		        			},
		        			function(result){
		        				if(result == 1){
			    					$('.other').prop("disabled",true);
			          				$('.dda').prop("disabled", false);
			          				$('.dh').prop("disabled", true);
			    				} else {
			    					$('.other').prop("disabled",true);
			          				$('.dda').prop("disabled", false);
			    				}

			    				aJax.post(
									"<?= base_url('c_auditreport/count_total_question');?>",
									{
										report_id: report_id
									},
									function(result){
										if(result > 0){
											$('.other').prop("disabled",true);
						      				$('.dda').prop("disabled", true);
						      				$('.dh').prop("disabled", true);
						      				$('.ca').prop("disabled", true);
						      				$('.email_alert').show();
						      				$('#send_audit_email').prop("disabled",true);
										} else {
											$('.email_alert').hide();
											$('#send_audit_email').prop("disabled",false);
										}
										$("#audit_email-modal").modal("show");
										isLoading(false);
									}
								);

		        			}

		        		);
		        	}

				});
			}
		);

	});



$(document).on('change', '#other', function(e){
	// alert('test');
   e.preventDefault();
     if($('#other').is(':checked') == true){
            $('.txthidden').removeAttr("hidden");
          }
      if($('#other').is(':checked') == false){
      	$('.txthidden').prop("hidden", true);
     
      }
 });



		


// email audit report
$(document).on('click', '#send_audit_email', function(e){

	isLoading(true);
	$("#audit_email-modal").modal("hide");

	var reportid = $(this).attr('data-id');
	var report_no = $(this).attr('id-number');

	console.log(reportid); 
	//co-auditors
	if($("#co_auditor").is(":checked")){

		$.ajax({
            type: 'Post',
            url:'<?=base_url("smtp/send/email_to_coauditor");?>',
            data:{report_id:reportid},
            }).done(function(data){
            	isLoading(false);
            		update_listing(reportid);
            		bootbox.alert("Report successfully sent to Co-Auditor(s).", function(){
            		console.log("Sent to co-auditor" + data);
	            	update_audit_report_json();
	            	insert_audit_trail("Email "+ report_no +" to Co-Auditors");
            		get_list();
					get_pagination();
            	});
            	
            });

        aJax.get(
        	"<?=base_url("api/generate_audit_report_json");?>/" + reportid,
        	function()
        	{
        		console.log("audit report updated");
        	}
        );
	}

	//reviewer
	if($("#department_head").is(":checked")){
		$.ajax({
            type: 'Post',
            url:'<?=base_url("smtp/send/email_to_department_head");?>',
            data:{report_id:reportid},
            }).done(function(data){
            	console.log("Sent to department head");
            	
            });

		$.ajax({
            type: 'Post',
            url:'<?=base_url("c_auditreport/update_date_submission");?>',
            data:{report_id:reportid},
            }).done(function(data){
            	update_listing(reportid);
            	update_audit_report_json();
            	insert_audit_trail("Email "+ report_no +" to Department Head");
            	bootbox.alert("Report successfully sent to Department Head.", function(){
            		get_list();
					get_pagination();
            	});
            });

        aJax.get(
        	"<?=base_url("api/generate_audit_report_json");?>/" + reportid,
        	function()
        	{
        		console.log("audit report updated");
        	}
        );
	}

	//approver
	if($("#division_head").is(":checked")){
		$.ajax({
            type: 'Post',
            url:'<?=base_url("smtp/send/email_to_division_head_from_leadauditor");?>',
            data:{report_id:reportid},
            }).done(function(data){
            	console.log("Sent to division head");

            });

		$.ajax({
            type: 'Post',
            url:'<?=base_url("c_auditreport/update_date_submission");?>',
            data:{report_id:reportid},
            }).done(function(data){
            	update_audit_report_json();
            	update_listing(reportid);
            	insert_audit_trail("Email "+ report_no +" to Division Head");
            	bootbox.alert("Report successfully sent to Division Head.", function(){
            		get_list();
					get_pagination();
            	});
            	
            });

        aJax.get(
        	"<?=base_url("api/generate_audit_report_json");?>/" + reportid,
        	function()
        	{
        		console.log("audit report updated");
        	}
        );
	}

	//approver
	if($("#other").is(":checked")){

		var email = $('#sender_other').val();
		var subject = $('#subject_other').val();
		var message = CKEDITOR.instances.message_other.getData();
		$.ajax({
            type: 'Post',
            url:'<?=base_url("smtp/send/sendmail_other");?>',
            data:{sender_other:email,subject_other:subject,message_other:message, report_id:reportid},
            }).done(function(data){
            	update_listing(reportid);
            	bootbox.alert("Annexure Word File & Rating Report are successfully sent!");
            	get_list();
				get_pagination();
            	insert_audit_trail("Email "+ report_no +" to " + email);
            	$('#sender_other').val("");
            	$('#subject_other').val("");
            	$('#message_other').html("");
            });
	}

	



});

function display_email(report_id){
	$.ajax({
    type: 'Post',
    url:'<?=base_url("c_auditreport/check_if_auditor");?>',
    data:{report_id:report_id},
    }).done(function(data){
    	return data;
    });
}


function get_csv(){
	isLoading(true);
	insert_audit_trail("Generate Filter CSV");
	window.location.href = "<?= base_url('c_auditreport/get_csv');?>?select="+ query_select + "&table=" + query_table + "&join=" + query_join + "&where="+ query_wheres;
	isLoading(false);
}

$('.cb').change(function() {
    $('.cb').prop("checked", false);
    $(this).prop("checked", true);     
});


</script>