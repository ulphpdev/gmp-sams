<style type="text/css">
    .tbl_format > thead > tr > th {
        padding: 5px;
        color: #000;
    }
</style>

<div class="col-md-12 bg-white">
	<div class="col-md-12">
		<div class="header"><h4>GMP AUDIT REPORT</h4><label class="gmp_report_no">Report Number</label></div>
	</div>

	<!-- Part I. Audit Information -->
	<div class="col-md-12"> 
		<div class="col-md-12 audit-header">Part I. Audit Information</div>
		<div class="col-md-12 sub-header"><label>A. General Information</label></div>
		<div class="col-md-12 pad-0 part1_content">
			<div class="col-md-12 pad-0">
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<label>Name of Manufacturer: </label> <span class="company_name"></span>
					</div>
					<div class="col-md-12 pad-0">
						<label>Site Address 1: </label> <span class="address1"></span>,<span class="address2"></span>
					</div>
					<div class="col-md-12 pad-0">
						<label>Site Address 2: </label> <span class="address3"></span>, <span class="country"></span>
					</div>
					<div class="col-md-12 pad-0">
						<label>Date of Audit: </label> <span class="audit_date"></span>
					</div>
				</div>
				<div class="col-md-6 pad-0">
					<div class="col-md-12 pad-0">
						<label>Name of Lead Auditor: </label> <span class="lead_auditor"></span>
					</div>
					<div class="col-md-12 pad-0">
						<label>Position: </label> <span class="position"></span>
					</div>
					<div class="col-md-12 pad-0">
						<label>Department/Division &amp; Company:  </label> <span class="auditor-dept"></span>
					</div>
				</div>
			</div>
			<div class="col-md-12 pad-0">
				<div class="table-responsive mb15">
					<table border="1" class="auditors_table tbl_format">
						<thead>
							<tr>
								<th>Name</th>
								<th>Position</th>
								<th>Department/Designation</th>
							</tr>
						</thead>
						<tbody class="auditors_tbody">
						
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-12 sub-header"><label>B. Company Background/Audit History</label></div>
		<div class="col-md-12 pad-0 part2_content">
			<div class="col-md-12 pad-0">
				<label>Supplier Background/History: </label><br>
				<span class="Supplier-history"></span><br><br>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<label>Date of Previous Inspection:</label> <span class="previous-inspection"></span>
		</div>
		<div class="col-md-12 pad-0">
			<label>Inspector(s) Involved:</label>
			<ol class="inspectors">
			</ol>
			<label>Major Changes Since Previous Inspection: </label>
			<ol class="changes">
			</ol>
		</div>
		<div class="col-md-12 sub-header"><label>C. Audit Information</label></div>
		<div class="col-md-12 pad-0">
			<label>Activities Carried out by the company: </label>
			<ol class="activities">
			</ol>
			<label>Other Activities Carried out by the company: </label> <span class="other-activities"></span><br>
			<label>Scope of Audit: </label>
			<ol class="scope_audit">
			</ol>
			<!-- <span>Products of Interest (for Insert Scope of Audit Name 1) </span>
			<ol class="product_interest">
				<li>Product Name</li>
				<li>Product Name</li>
			</ol> -->
		</div>
		<div class="col-md-12 pad-0">
			<div class="table-responsive mb15">
				<table border="1" class="license_table tbl_format">
					<thead>
						<tr>
							<th>Licenses/Certification</th>
							<th>Issuing Regulatory</th>
							<th>License/Certification Number</th>
							<th>Validity</th>
							<th>Issued Date</th>
						</tr>
					</thead>
					<tbody class="license_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<label>Pre-Audit Documents:</label>
			<ol class="pre-documents">
			</ol>
		</div>
		<div class="col-md-12 pad-0">
			<label>Audited areas:</label> <span class="audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<label>Not Audited areas:</label> <span class="not_audited_areas"></span>
		</div>
		<div class="col-md-12 pad-0">
			<label>Date of Wrap-up or Close out Meeting:</label> <span class="wrapup-date"></span>
		</div>
		<div class="col-md-12 pad-0">
			<label>Name of Present During Close-out Meeting</label>
			<div class="table-responsive mb15">
				<table border="1" class="present-meeting tbl_format">
					<thead>
						<tr>
							<th>Name</th>
							<th>Position/Department</th>
						</tr>
					</thead>
					<tbody class="present_met_tbody">
						<tr>
							<!-- <td>Juan Dela Cruz</td>
							<td>CIT</td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<label>Name of Personnel Met During Inspection </label>
			<div class="table-responsive mb15">
				<table border="1" class="personel-met tbl_format">
					<thead>
						<tr>
							<th>Name</th>
							<th>Position/Department</th>
						</tr>
					</thead>
					<tbody class="personel_met_tbody">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12 pad-0">
			<label>Distribution List (except Supplier and GMP Inspection)</label>
			<ol class="distribution-list">
			</ol>
		</div>
		<div class="col-md-12 sub-header"><label>D. Templates Answers</label></div>
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0">
				<label>Product Type: </label> <span class="report_producttype"></span>
			</div>
			<div class="col-md-12 pad-0">
				<label>Standard/Reference: </label> <span class="report_standardreference"></span>
			</div>
		</div>
		<!-- element tables -->
		<div class="col-md-12 pad-0">
			<div class="col-md-12 pad-0 element_con mb15"></div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="col-md-12 audit-header"><label>Part II. Audit Observation</label></div>
		<div class="col-md-12 pad-0">
			<label>Elements Requiring Recommendation: </label>
			<ul class="element-reco"></ul>
		</div>

		<div class="col-md-12 pad-0">
			<label>Audit Team's Findings and Observations Relevant to the Audit</label>
			<ol class="aud_findings"></ol>
		</div>

		<div class="col-md-12 pad-0">
			<label>Definition/Categorization of Audit Observations</label>
			<ol class="aud_definition"></ol>
		</div>

		<div class="col-md-12 pad-0">
			<label>Listing of Audit Observations and Concerns (in decreasing order of criticality)</label>
		</div>

		<div class="col-md-12 pad-0">
			<span><b>Critical Observation</b> - <i class="critical_observation">None</i></span><br>
			<span><b>Major Observation</b> - <i class="major_observation">None</i></span><br>
			<span><b>Minor Observation</b> - <i class="minor_observation">None</i></span>
		</div>
		<div class="clearfix"></div>
		<br>
		<div class="col-md-12 pad-0">
			<label>Disposition</label>
			<div class="table-responsive mb15">
				<table border="1" class="scope-disposition tbl_format">
					<thead>
						<tr>
							<th>Disposition</th>
							<th>Product</th>
						</tr>
					</thead>
					<tbody class="disposition_tbody">
						
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12 pad-0">
			<label>Other Issue for Audit Report: </label> <span class="other-issue-audit"></span>
		</div>	
		<div class="col-md-12 pad-0">
			<label>Other Issues for Executive Summary: </label> <span class="other-issue-executive"></span>
		</div>	
<!-- 
		<div class="col-md-12 pad-0">

			<span>Audit Closure Date:</span><label class="audit-close-date">02 September 2017 </label>

		</div>	 -->

	</div>

<hr noshade size=1 width="100%">
	<div class="col-md-12 remarks-view">
		<div class="col-md-12 pad-0">
			<label>Approval History</label>
			<div class="table-responsive mb15">
				<table border="1" class="report-remarks tbl_format">
					<thead>
						<tr>
							<th>Date &amp; Time</th>
							<th>Name</th>
							<th>Position</th>
							<th>Status</th>
							<th>Remarks</th>
						</tr>
					</thead>
					<tbody class="remarks_tbody">
					</tbody>
				</table>
			</div>
		</div>	
	</div>

	<div class="col-md-12 remarks_inputs remarks-reviewer">
		<div class="col-md-12 pad-0 input-remarks">
			<span><b>REMARKS<b></span>
			<div class="">

				<div class="col-md-12">
						<textarea class="form-control" id="txt_remarks" rows="4" cols="50"></textarea><br>
						
						<div class="col-md-6">
		  					<button name="subject" type="submit" class="btn-approval btn btn-warning reject_submit" value="reject">RESUBMIT TO LEAD-AUDITOR</button>
		  				</div>
						<div class="col-md-6">		
		  					<button name="subject" type="submit" class="btn-approval btn btn-success remarks_submit" value="approved">SUBMIT TO DIVISION HEAD</button>
		  				</div>
		  				
				</div>
				
			</div>
		</div>
	</div>

	<div class="col-md-12 remarks_inputs remarks-approver">
		<div class="col-md-12 pad-0 input-remarks">
			<span><b>REMARKS<b></span>
			
			<div class="">

				<div class="col-md-12">
						
						<textarea class="form-control" id="txt_remarks" rows="4" cols="50"></textarea><br>
						
						
						<div class="col-md-6">
  							<button name="subject" type="submit" class="btn-approval btn btn-warning reject_submit" value="reject">RESUBMIT TO LEAD-AUDITOR</button>
  						</div>
						
						<div class="col-md-6">
							<button name="subject" type="submit" class="btn-approval btn btn-success remarks_submit " value="approved">APPROVE</button>
						</div>
				</div>
				
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
var div_remarks = "<?= $div;?>"

$(document).ready(function(){	
	
	// console.log(div_remarks);
	if(div_remarks == "remarks-view"){
		$('.remarks-view').show();
		$('.remarks-reviewer').remove();
		$('.remarks-approver').remove();
	}

	if(div_remarks == "remarks-reviewer"){
		// console.log("reviewer");
		$('.remarks-view').show();
		$('.remarks-reviewer').show();
		$('.remarks-approver').remove();


	}

	if(div_remarks == "remarks-approver"){
		$('.remarks-view').show();
		$('.remarks-reviewer').remove();
		$('.remarks-approver').show();
	}

	


	aJax.get("<?= base_url() . 'listing/' . $_POST['id'];?>.json",function(data){
		var obj = isJson(data);

		//report summary
		$.each(obj.Report_Summary, function(x,y){
			check_stamp(y.report_id);
			remarks(y.report_id)
			$('.gmp_report_no').text(y.report_no + ' V' + y.version);
			$('.company_name').text(y.name);
			$('.address1').text(y.address1);
			$('.country').text(y.country);
	    	$('.address2').text(y.address2);
	    	$('.address3').text(y.address3);
			$('.lead_auditor').text(y.fname+' '+y.mname+' '+y.lname);
			if(y.designation != 0){
				$('.position').text(y.designation);
			}
			$('.auditor-dept').text(y.department+', '+y.company);
			$('.Supplier-history').html(nl2br(y.background));
            if(y.other_activities != ""){
                $('.other-activities').text(y.other_activities); 
            } else {
                $('.other-activities').text("None");
            }
			
			$('.audited_areas').text(y.audited_areas);
			if(y.areas_to_consider != "" || y.areas_to_consider != null){
				$('.not_audited_areas').text(y.areas_to_consider);
			} else {
				$('.not_audited_areas').text("None");
			}
			
			$('.wrapup-date').text(y.wrap_up_date);	
		});


		//audit dates
		$('.audit_date').html(obj.Audit_Dates_Formatted);


		//co auditors
		htm = "";
		$.each(obj.Co_Auditors, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.fname+' '+y.mname+' '+y.lname+'</td>';
			htm += '<td>Co-Auditor</td>';
			htm += '<td>'+y.department+', '+y.company+'</td>';
			htm += '</tr>';
		});
		$('.auditors_tbody').append(htm);

		//reviewer
		htm = "";
		$.each(obj.Reviewer, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.fname+' '+y.mname+' '+y.lname+'</td>';
			htm += '<td>Reviewer</td>';
			htm += '<td>'+y.department+', '+y.company+'</td>';
			htm += '</tr>';
		});
		$('.auditors_tbody').append(htm);

		//approver
		htm = "";
		$.each(obj.Approver, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.fname+' '+y.mname+' '+y.lname+'</td>';
			htm += '<td>Approver</td>';
			htm += '<td>'+y.department+', '+y.company+'</td>';
			htm += '</tr>';
		});
		$('.auditors_tbody').append(htm);

		//translator
		htm = "";
		$.each(obj.Translator, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.translator+'</td>';
			htm += '<td>Translator</td>';
			htm += '<td></td>';
			htm += '</tr>';
		});
		$('.auditors_tbody').append(htm);

		//Last Inspection Date
		if(obj.hasOwnProperty("Inspection_Audit_Dates_Formatted")){
			$('.previous-inspection').html(obj.Inspection_Audit_Dates_Formatted);
		} else {
			$('.previous-inspection').html("None");
		}
		
	 
		//Last Inspector
		htm = "";
		if(obj.hasOwnProperty("Inspection_Inspector")){
			$.each(obj.Inspection_Inspector, function(a,b){
				htm += '<li>' + b.inspector + '</li>';
			});
		} else {
			htm += 'None';
		}
		
		$('.inspectors').html(htm);

		//Last inspection Changes

		htm = "";
        if(obj.Inspection_Changes.length > 0){
            $.each(obj.Inspection_Changes, function(x,y){
                htm += '<li>'+y.changes+'</li>';
            }); 
        } else {
            htm += 'None';
        }
			

		$('.changes').append(htm);

		//Activities
		htm = '';
        if(obj.Activities.length > 0){
            $.each(obj.Activities, function(x,y){
                htm += '<li>'+y.activity_name;
                if(y.sub_activity.length > 0){
                    htm += '<ul style="list-style-type: square;">';
                    $.each(y.sub_activity, function(a, b){
                        htm += '<li>' + b.sub_item_name+ '</li>';
                    });
                    htm += '</ul>'
                }

                htm += '</li>';
            });     
        } else {
            htm += 'None';
        }
		
		$('.activities').append(htm);


		//scope
		htm = '';
		$.each(obj.Scope_Audit, function(x,y){
			if(y.scope != ""){
                if(y.scope_detail != ""){
                    htm += '<li>'+y.scope_name+' - ' +  y.scope_detail+  '</li>';
                } else {
                    htm += '<li>'+y.scope_name+' - None</li>';
                }
				
			} else {
                htm += 'None';
            }
		});		
		$('.scope_audit').append(htm);

		//scope prduct
		htm = '';
		$.each(obj.Scope_Product, function(x,y){	
			htm += '<label>Products of Interest ('+ y.scope +')</label>';
			htm += '<ol class="product_interest">';		
			$.each(y.products, function(a,b){
				htm += '<li>'+b.product_name+'</li>';	
			})		
			htm += '</ol>';	
		});
		$('.scope_audit').after(htm);


		//license
		htm = "";
		$.each(obj.License, function(x,y){
			htm += '<tr>';
			if(y.reference_name != ""){
				htm += '<td>'+y.reference_name+'</td>'; 
			} else {
				htm += '<td>None</td>'; 
			}
			
			if(y.issuer != ""){
				htm += '<td>'+y.issuer+'</td>';
			} else {
				htm += '<td>None</td>';
			}
			
			if(y.reference_no != ""){
				htm += '<td>'+y.reference_no+'</td>';
			} else {
				htm += '<td>None</td>';
			}
			
			if (Date.parse(y.validity)) {
			  htm += '<td>'+moment(y.validity).format("D MMMM YYYY")+'</td>';
			} else {
			  htm += '<td>None</td>';
			}

			if (Date.parse(y.issued)) {
			  htm += '<td>'+moment(y.issued).format("D MMMM YYYY")+'</td>';
			} else {
			  htm += '<td>None</td>';
			}
			
			htm += '</tr>';
		});
		$('.license_tbody').append(htm);


		//pre-documents
		htm = "";
		$.each(obj.Pre_Document, function(x,y){
			htm += '<li>'+y.document_name+'</li>';
		});		
		$('.pre-documents').append(htm);


		//name present
		htm = "";
		if(obj.Present_During_Meeting.length!=0){
			$.each(obj.Present_During_Meeting, function(x,y){
				htm += '<tr>';
				htm += '<td>'+y.name+'</td>';
				if(y.position != ""){
					htm += '<td>'+y.position+'</td>';
				} else {
					htm += '<td>None</td>';
				}
				
				htm += '</tr>';
			});
		}else{
			htm += '<tr><td colspan="2">N/A</td></tr>';
		}
		$('.present_met_tbody').append(htm);


		//met meeting
		htm = "";
		$.each(obj.Personel_Met, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.name+'</td>';
			if(y.designation != ""){
				htm += '<td>'+y.designation+'</td>';
			} else {
				htm += '<td>None</td>';
			}
			
			htm += '</tr>';
		});
		$('.personel_met_tbody').append(htm);

		//distribution
		htm = '';
		if(obj.Distribution.length == 0){
			htm += 'Not Applicable';
		}else{
			$.each(obj.Distribution, function(x,y){
				htm += '<li>'+y.distribution_name+'</li>';
			});	

			$.each(obj.Other_Distribution, function(x,y){
				htm += '<li>'+y.other_distribution+'</li>';
			});	
		}	
		$('.distribution-list').append(htm);


		//template
		var htm ="";
		$.each(obj.Template, function(x,y){
			$('.report_producttype').text(y.classification_name);
			$('.report_standardreference').text(y.standard_name);
		});


		//elements 
		var critical = "";
	    var major = "";
	    var minor = "";
		var index = 65; 
		var alpha = ["A","B","C","D","E","F","G","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		htm = "";
		$.each(obj.Template_Elements, function(x,y){
			var htm = '';
			htm += '<div class="col-md-12 pad-0 table-responsive element_item ei_'+y.element_id+'">';
			htm += '<span class="element_name"><b>ELEMENT '+alpha[y.order -1]+' - '+y.element_name.toUpperCase();+'</b></span>';
			htm += '<table border="1" class="element_table_'+y.element_id+' tbl_format">';
			htm += '<thead>';
			htm += '<tr>';
			htm += '<th>Questions</th>';
			htm += '<th>Answer(Yes/No/NA/NC)</th>';
			htm += '<th>Details</th>';
			htm += '<th>Item No.</th>';
			htm += '<th>Category</th>';
			htm += '</tr>';
			htm += '</thead>';
			htm += '<tbody>';
				$.each(y.questions, function(a,b){
					var category = '';
					htm += '<tr>';
					htm += '<td style="width:40%; vertical-align: top;">'+ nl2br(b.question)+'</td>';
					htm += '<td style="width:10%; vertical-align: top;" >'+b.answer_name+'</td>';
                    if(b.answer_name == "Yes" || b.answer_name == "No") {
                        htm += '<td style="width:30%; vertical-align: top;">'+b.answer_details+'</td>';
                    } else {
                        htm += '<td style="width:30%; vertical-align: top;"></td>';
                    }
					htm += '<td style="width:10%; vertical-align: top;">'+b.answer_no+'</td>';

					if(b.category != null){
						category = b.category;
					}

                    if(b.answer_name == "No") {
					   htm += '<td style="width:10%; vertical-align: top;">'+category+'</td>';
                    } else {
                        htm += '<td style="width:10%; vertical-align: top;"></td>';
                    }

					htm += '</tr>';

					switch(b.category) {
					    case 'Critical':
                            if(b.answer_no != ""){
                                critical += b.answer_no + ',';
                            }
					        break;
					    case 'Major':
                            if(b.answer_no != ""){
                                major += b.answer_no + ',';
                            }
					        break;
					    case 'Minor':
                            if(b.answer_no != ""){
                                minor += b.answer_no + ',';
                            }
					        
					        break;
					}
				});

            if(critical == ""){
                $('.critical_observation').html("None");
            } else {
                $('.critical_observation').html("Please refer to item no(s) " + critical.replace(/,\s*$/, ""));
            }

            if(major == ""){
                $('.major_observation').html("None");
            } else {
                $('.major_observation').html("Please refer to item no(s) " + major.replace(/,\s*$/, ""));
            }

            if(minor == ""){
                $('.minor_observation').html("None");
            } else {
                $('.minor_observation').html("Please refer to item no(s) " + minor.replace(/,\s*$/, ""));
            }
			htm += '</tbody>';
			htm += '</table>';
			htm += '</div>';

			$('.element_con').append(htm);
		});


		//element recommendation
		htm = '';
		var alpha = ["A","B","C","D","E","F","G","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		if(obj.Element_Reco.length == 0){
			htm += '<li>None</li>';
		}else{
			$.each(obj.Element_Reco, function(x,y){
				var order = y.order - 1;
                if(y.recommendation != ""){
                    htm += '<li>Element '+alpha[order]+' - '+y.recommendation+'</li>';
                } else {
                    htm += '<li>Element '+alpha[order]+' - None</li>';
                }
				
			});	
		}	
		$('.element-reco').html(htm);

		//audit findings
		htm = '';
		$.each(obj.Observation_Yes, function(x,y){
            if(y.Observation.trim().slice(0, -1) != ""){
                htm += '<li><b>' + y.Element + "</b> - " + y.Observation.trim().slice(0, -1) + "</li>";
            } else {
                htm += '<li><b>' + y.Element + "</b> - None</li>";
            }
			
		});
		$('.aud_findings').html(htm.replace(/,\s*$/, ""));




		//Definition/Categorization of Audit Observations
		htm = '';
		$.each(obj.Audit_Observation, function(x,y){
			htm += '<u>'+y.category_name+' Observation</u><br>';
			htm += y.description + '<br><br>';
		});
		$('.aud_definition').html(htm);

		//disposition
		var htm = '';
		$.each(obj.Disposition, function(x,y){
			htm += '<tr>';
			htm += '<td>'+y.Disposition+'</td>';
			htm += '<td>'+y.Products+'</td>';
			htm += '</tr>';
		});
		$('.disposition_tbody').append(htm);

		//other udit issue
		var issues = "<ul>";
		$.each(obj.other_issue_audit, function(x,y){
			issues += "<li>" + y.other_issues_audit + "</li>"
		});
		issues += "</ul>"; 
		$('.other-issue-audit').html(issues);

		//other issue exec
		var issues = "<ul>";
			$.each(obj.other_issue_exec, function(x,y){
				issues += "<li>"  + y.other_issues_executive + "</li>"
			});
			issues += "</ul>"; 
			$('.other-issue-executive').html(issues);


		//remarks
		htm = "";
		$.each(obj.Remarks, function(row,data2){
			htm += "<tr>";
			htm += "<td>" + moment(data2.date).format("LLL") + "</td>";
			htm += "<td>" + data2.name + "</td>";
			htm += "<td>" + data2.position + "</td>";
			htm += "<td>" + data2.status + "</td>";
			htm += "<td>" + data2.remarks + "</td>";
			htm += "</tr>";	
		});		

		$(".remarks_tbody").append(htm);

	});



});


function remarks(report_id){
	
	$.ajax({
		type: 'post',
		url: "<?=base_url()?>preview_report/get_remarks",
		data:{report_id:report_id}
	}).done(function(data){
		var obj = JSON.parse(data);
		var htm = '';
		var fname = ""
		
		if(obj.length > 0){
			$.each(obj, function(row,data2){
				htm += "<tr>";
				htm += "<td>" + moment(data2.date).format("LLL") + "</td>";
				htm += "<td>" + data2.name + "</td>";
				htm += "<td>" + data2.position + "</td>";
				htm += "<td>" + data2.status + "</td>";
				htm += "<td>" + data2.remarks + "</td>";
				htm += "</tr>";	
			});	
		} else {
			htm += "<tr>";	
			htm += "<td colspan='5' style='text-align:center;'>No records as of this time.</td>";	
			htm += "</tr>";	
		}
		$(".remarks_tbody").html(htm);
	});

	
}

function check_stamp(report_id)
{
	aJax.post(
		"<?= base_url("preview_report/check_stamp");?>",
		{
			'report_id' : report_id 
		},
		function(data){
			// console.log(data);
			var obj = JSON.parse(data);
			// console.log(obj.length);
			if(obj.length > 0){
				if(div_remarks == "remarks-approver"){
					if(obj[0].approved_date == null){
						$('.remarks_inputs').show();
					} else {
						$('.remarks_inputs').hide();
					}
				}

				if(div_remarks == "remarks-reviewer"){
					if(obj[0].review_date == null){
						$('.remarks_inputs').show();
					} else {
						$('.remarks_inputs').hide();
					}
				}
			} else {
				$('.remarks_inputs').show();
			}			
		}
	);
}

	function audit_date_formatter(date_array){
		console.log(date_array);
		var result = "";
		var month = "";
		var date = "";
		var year = "";
		var months = {};
		
		$.each(date_array, function(x,row){
			month = moment(row.Date).format('MMMM');
			date = moment(row.Date).format('D');
			year = moment(row.Date).format('Y');

			if(jQuery.inArray(month, months) < 0){
				months[month] = null;
			}
		});
		
		$.each(months, function(a,b){
			var dates = [];
			$.each(date_array, function(x,row){
				month = moment(row.Date).format('MMMM');
				date = moment(row.Date).format('D');
				if(month == a){
					dates.push(date);
				}	
			});
			months[a] = dates
		});
		
		var result = "";
		$.each(months, function(a,b){
			//dates
			audit_dates = "";
			last_date = b.pop();
			$.each(b, function(x,y){	
				audit_dates += y + ", ";
			});
			if(audit_dates.length > 0){
				result += audit_dates.replace(/,\s*$/, '') + " & " + last_date + " " + a + ", ";
			} else {
				result += last_date + " " + a + ", ";
			}
		});
		return result.replace(/,\s*$/, '') + " " + year;
		
	}

	function nl2br (str, is_xhtml) {
		if (typeof str === 'undefined' || str === null) {
			return '';
		}
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	}

</script>