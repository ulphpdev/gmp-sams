<?php 
  $table = "tbl_report_summary";
  $order_by = "report_id";
?>
<div>
<div class="col-md-12" id="form-content"></div>
<div class="col-md-12" id="form-table"></div>
<div class="col-md-12" id="inspector"></div>
<div class="col-md-12" id="changes"></div>
<div class="col-md-12" id="activity"></div>
<div class="col-md-12" id="scope"></div>
<div class="col-md-12" id="product"></div>
<div class="col-md-12" id="audit_document"></div>
<div class="col-md-12" id="audit_area"></div>
<div class="col-md-12" id="not_audit_area"></div>
<div class="col-md-12" id="closeup_meeting"></div>
<div class="col-md-12" id="inspection"></div>
<div class="col-md-12" id="distribution"></div>
<div class="col-md-12" id="template_reference"></div>
<div class="col-md-12" id="template_element"></div>
<div class="col-md-12" id="element_no"></div>
<div class="col-md-12" id="disposition"></div>
<div class="col-md-12" id="audit_issue"></div>
<div class="col-md-12" id="audit_executive"></div>
<div class="col-md-12" id="remarks"></div>
<!-- <button class=" btn-min btn btn-success"><span class = "glyphicon glyphicon-remove-cirlce"></span> Back to Listing <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none; style="float: right;"></span></button>  -->
</div>



<script type="text/javascript">
$(document).ready(function(){

// var table = "<?=$table;?>";
// var field = "<?=$order_by;?>";
var id = "<?= $id; ?>";
	on_load();
      function on_load(){
        var element_id = $('.view_report').attr('data-id');
        // var limit = 1;
        $.ajax({
        type: 'Post',
        url:'<?=base_url();?>audit_report/get_report',
        data:{id:id},
      }).done( function(data){
          var obj = JSON.parse(data);
          htm = '';
          $.each(obj, function(x,y){
              htm += '<div col-md-4><label> GMP Report </label></div>';
              htm += '<label> A. General Information </label>';
              htm += '<div class="col-md-12">';
              htm += '<div class="col-md-6"><label>Name of Manufacturer : '+y.name+'<label></div>';
              htm += '<div class="col-md-6"><label>Name of Lead Auditor: '+y.fname+' '+y.mname+' '+y.lname+'<label></div>';
              htm += '<div class="col-md-6"><label>Address 1 : '+y.address1+'<label></div>';
              htm += '<div class="col-md-6"><label>Position : '+y.designation+'<label></div>';
              htm += '<div class="col-md-6"><label>Address 2 : '+y.address2+'<label></div>';
              htm += '<div class="col-md-6"><label>Department / Division and Company : '+y.department+' / '+y.company+'<label></div>';
              htm += '</div>';
              get_report_table(y.report_id);
              get_supplier_history(y.report_id);
              get_inspector(y.report_id);
              get_changes_report(y.report_id);
              get_activity_name(y.report_id);
              get_scope_name(y.report_id);
              get_audit_references(y.report_id);
              get_audit_document(y.report_id);
              get_audit_area(y.report_id);
              get_not_audit_area(y.report_id);
              get_name_closeup_meeting(y.report_id);
              get_name_inspection(y.report_id);
              get_distribution(y.report_id);
              get_template_reference(y.report_id);
              get_template_element(y.report_id);
              get_element_noanswer();
              get_disposition(y.report_id);
              get_other_audit_report(y.report_id);
              get_executive_summary();
              get_remarks();
              htm += '</div>';
             
          });
          $('#form-content').append(htm);
     });      

}

function get_report_table(report_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_report_table",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        htm = '';
        htm += '<table style="border: 1px solid black;">';
        htm += '<thead  style="background-color: #7ec0ee;"">';
        htm += '<tr>';
        htm += '<td width="350"> Name </td>';
        htm += '<td width="350"> Position </td>';
        htm += '<td width="350"> Department/Designation </td>';
        htm += '</tr';
        htm += '</thead>';
        htm += '</table>'
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<table>';
        htm += '<tbody>';
        htm += '<tr>';
        htm += '<td width="350"> '+y.fname+''+y.mname+''+y.lname+'  </td>';
        htm += '<td width="350">'+y.designation+'</td>';
        htm += '<td width="350"> '+y.department+'/'+y.company+' </td>';
        htm += '</tr';
        htm += '</tbody>';
        htm += '</table>';
        htm += '</div>'
    })
      $('#form-table').append(htm);
});
}


function get_supplier_history(report_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_supplier_history",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        htm = '';
        htm += '<label> B. Company Background/History </label>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<textarea rows="4" cols="100">'+y.background+'</textarea>';
        htm += '</div>'
    })
      $('#form-table').after(htm);
});
}

function get_inspector(report_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_inspector",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm += '<label> Date of Preview Inspection </label>';
        htm +='<br>';
        htm += '<label>Inspector(s) Involved: </label>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'.'+y.fname+''+y.lname+''
        htm += '</div>'
        ctr++;
        
    })
      $('#inspector').after(htm);
});
}


function get_changes_report(report_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_changes_report",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm += '<label>Major Changes Since Previous Inspection</label>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.changes+''
        htm += '</div>'
        ctr++;
    })
      $('#changes').after(htm);
});
}

function get_activity_name(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_activity_name",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm += '<label>C. Audit Information</label>';
        htm +='<br>';
        htm +='<label>Activities Carried Out by the Company</label>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.activity_name+''
        htm += '</div>'
        ctr++;
    })
      $('#activity').after(htm);
});
}
function get_scope_name(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_scope_name",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        // htm += '<label>C. Audit Information</label>';
        // htm +='<br>';
        htm +='<label>Other Activities Carried Out by the Company</label>';
        htm +='<br>';
        htm += '<label>Scope of Audit</label>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.scope_name+'';
        htm += '</div>';
        ctr++;
        get_product_name(y.report_id,y.scope_name);
    })
      $('#scope').after(htm);
});
}

function get_product_name(report_id,scope_name){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_product_name",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        // htm += '<label>C. Audit Information</label>';
        // htm +='<br>';
        htm +='<label>Product of Interest ('+scope_name+')</label>';
        htm +='<br>';
        // htm += '<label>Scope of Audit</label>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.product_name+'';
        htm += '</div>';
        ctr++;
       
    })
      $('#product').after(htm);
});
}


function get_audit_references(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_audit_references",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        htm = '';
        htm += '<table style="border: 1px solid black;">';
        htm += '<thead  style="background-color: #7ec0ee;"">';
        htm += '<tr>';
        htm += '<td width="350"> License/Certification </td>';
        htm += '<td width="350"> Issuing Regulatory </td>';
        htm += '<td width="350"> License/Certification Number </td>';
         htm += '<td width="350"> Validity </td>';
        htm += '</tr';
        htm += '</thead>';
        htm += '</table>'
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<table>';
        htm += '<tbody>';
        htm += '<tr>';
        htm += '<td width="350">'+y.reference_name+'</td>';
        htm += '<td width="350">'+y.issuer+'</td>';
        htm += '<td width="350">'+y.reference_no+'</td>';
        htm += '<td width="350">'+y.validity+'</td>';
        htm += '</tr';
        htm += '</tbody>';
        htm += '</table>';
        htm += '</div>'

    })
      $('#product').after(htm);
});
}

function get_audit_document(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_audit_document",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Pre Audit Documents</label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.document_name+'';
        htm += '</div>';
        ctr++;
       
    })
      $('#audit_document').after(htm);
});
}

function get_audit_area(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_audit_area",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Audited Areas: </label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.audited_areas+'';
        htm += '</div>';
        ctr++;
       
    })
      $('#audit_area').after(htm);
});
}

function get_not_audit_area(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_not_audit_area",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Not Audited Areas: </label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.areas_to_consider+'';
        htm += '</div>';
        ctr++;
        htm +='<label>Date of Wrap or Close our Meeting:</label>';
        htm +='' +y.wrap_up_date+'';
        htm += '<br>';
       
    })
      $('#not_audit_area').after(htm);
});
}

function get_name_closeup_meeting(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_name_closeup_meeting",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Name of Present During Close-out Meeting </label>';
        htm += '<table style="border: 1px solid black;">';
        htm += '<thead  style="background-color: #7ec0ee;"">';
        htm += '<tr>';
        htm += '<td width="350"> Name </td>';
        htm += '<td width="350"> Position/Department </td>';
        htm += '</tr';
        htm += '</thead>';
        htm += '</table>'
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<table>';
        htm += '<tbody>';
        htm += '<tr>';
        htm += '<td width="350">'+y.fname+' '+y.lname+'</td>';
        htm += '<td width="350">'+y.designation+'</td>';
        htm += '</tr';
        htm += '</tbody>';
        htm += '</table>';
        htm += '</div>'
       
    })
      $('#closeup_meeting').after(htm);
});
}
function get_name_inspection(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_name_inspection",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Name of Personnel Met During Inspection</label>';
        htm += '<table style="border: 1px solid black;">';
        htm += '<thead  style="background-color: #7ec0ee;"">';
        htm += '<tr>';
        htm += '<td width="350"> Name </td>';
        htm += '<td width="350"> Position/Department </td>';
        htm += '</tr';
        htm += '</thead>';
        htm += '</table>'
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<table>';
        htm += '<tbody>';
        htm += '<tr>';
        htm += '<td width="350">'+y.fname+' '+y.lname+'</td>';
        htm += '<td width="350">'+y.designation+'</td>';
        htm += '</tr';
        htm += '</tbody>';
        htm += '</table>';
        htm += '</div>'
       
    })
      $('#inspection').after(htm);
});
}

function get_distribution(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_distribution",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Distribution List (except Supplier and GMP Inspection) </label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.distribution_name+'';
        htm += '</div>';
        ctr++;
       
    })
      $('#distribution').after(htm);
});
}

function get_template_reference(report_id){

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_template_reference",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        htm = '';
        htm +='<label>Template Answer </label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<label> Product Type: '+y.classification_name+' </label>';
        htm +='<br>';
        htm += '<label> Product Type: '+y.standard_name+' </label>';
        htm += '</div>';
       
    })
      $('#template_reference').after(htm);
});
}

function get_template_element(){
var id = $('.view_report').attr('data-id');

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_template_element",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        htm = '';
        $.each(obj, function(x,y){
        htm +='<label>'+y.element_name+'</label>';
        htm += '<table style="border: 1px solid black;">';
        htm += '<thead  style="background-color: #7ec0ee;"">';
        htm += '<tr>';
        htm += '<td width="350"> Questions </td>';
        htm += '<td width="350"> Answer (Yes/N/NA) </td>';
        htm += '<td width="350"> Details </td>';
        htm += '<td width="350"> Item No. </td>';
        htm += '<td width="350"> Category </td>';
        htm += '</tr';
        htm += '</thead>';
        htm += '</table>'
        htm +='<div class="que_'+y.element_id+'"></div>';
        get_question(y.element_id);
        htm += '</div>'
       
    })
      $('#template_element').after(htm);
});
}

function get_question(element_id){
var template_id = $('.view_report').attr('data-id');

    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_question",
        data:{element_id:element_id, template_id:template_id},
    }).done(function(data){
        var obj = JSON.parse(data);
        htm = '';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<table>';
        htm += '<tbody>';
        htm += '<tr>';
        htm += '<td width="350">'+y.question+'</td>';
        htm += '<td width="350">'+y.mandatory+'</td>';
        htm += '<td width="350">'+y.default_yes+' '+y.answer_details+'</td>';
        htm += '<td width="350">test</td>';
        htm += '<td width="350">'+y.category_id+'</td>';
        htm += '</tr';
        htm += '</tbody>';
        htm += '</table>';
        htm += '</div>'
        
    })
      $('.que_'+element_id).after(htm);
});
}

function get_element_noanswer(){
var id = $('.view_report').attr('data-id');
// alert(id);
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_element_noanswer",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Element Requiring Recomendation:  </label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.element_name+'';
        htm += '</div>';
        ctr++;
       
    })
      $('#element_no').after(htm);
});
}
function get_disposition(report_id){


    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_disposition",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Dispositions  </label>';
        htm += '<table style="border: 1px solid black;">';
        htm += '<thead  style="background-color: #7ec0ee;"">';
        htm += '<tr>';
        htm += '<td width="350"> Scope </td>';
        htm += '<td width="350"> Disposition </td>';
        htm += '</tr';
        htm += '</thead>';
        htm += '</table>'
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<table>';
        htm += '<tbody>';
        htm += '<tr>';
        htm += '<td width="350">'+y.scope_detail+'</td>';
        htm += '<td width="350">'+y.disposition_name+'</td>';
        htm += '</tr';
        htm += '</tbody>';
        htm += '</table>';
        htm += '</div>'
       
    })
      $('#disposition').after(htm);
});
}
function get_other_audit_report(){


    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_other_audit_report",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Other Issues for Audit Report:  </label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.other_issues_audit+'';
        htm += '</div>';
        ctr++;
       
    })
      $('#audit_issue').after(htm);
});
}
function get_executive_summary(){


    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_executive_summary",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Other Issues for Executive Summary:  </label>';
        htm +='<br>';
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += ''+ctr+'. '+y.other_issues_executive+'';
        htm += '</div>';
        ctr++;
       
    })
      $('#audit_executive').after(htm);
});
}
function get_remarks(report_id){


    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/audit_report/get_remarks",
        data:{id:id},
    }).done(function(data){
        var obj = JSON.parse(data);
        var ctr = 1;
        htm = '';
        htm +='<label>Remarks  </label>';
        htm += '<table style="border: 1px solid black;">';
        htm += '<thead  style="background-color: #7ec0ee;"">';
        htm += '<tr>';
        htm += '<td width="350"> Date </td>';
        htm += '<td width="350"> Name </td>';
        htm += '<td width="350"> Position </td>';
        htm += '<td width="350"> Status </td>';
        htm += '<td width="350"> Remarks </td>';
        htm += '</tr';
        htm += '</thead>';
        htm += '</table>'
        $.each(obj, function(x,y){
        htm += '<div col-md-12>';
        htm += '<table>';
        htm += '<tbody>';
        htm += '<tr>';
        htm += '<td width="350">test</td>';
        htm += '<td width="350">test</td>';
        htm += '<td width="350">test</td>';
        htm += '<td width="350">test</td>';
        htm += '<td width="350">test</td>';
        htm += '</tr';
        htm += '</tbody>';
        htm += '</table>';
        htm += '</div>'
       
    })
      $('#remarks').after(htm);
});
}


})

</script>