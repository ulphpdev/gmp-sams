<nav class="navbar navbar-expand-lg navbar-dark bg-dark " style="min-height: 0px; " id="mainNav">
    <div class="navbar-header" style="background-color: #fff;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <span class="navbar-brand small_user">
        <b> Hi  </b> <?php  echo ucwords($this->session->userdata('name')); ?>!  |  <a href="#" data-toggle="modal" data-target="#logout_modal" style="color:#000">Log-out</a>
      </span> 
    </div>

    <div class="collapse navbar-collapse navbar-collapse collapse sidebar-navbar-collapse" id="navbarResponsive">
      <ul class="nav navbar-nav main-menu test navbar-nav navbar-sidenav Sams_Menus" id="exampleAccordion" style="height: auto !important; margin-top: 0px !important;">
      </ul>
    </div>
  </nav>

  <style type="text/css">
    .nav_separator {
      border-bottom: 1px solid #fff; 
    }
  </style>

  <script type="text/javascript">
    get_menus();
    function get_menus() {
      aJax.post(
        "<?= base_url('global_controller/get_navigation');?>",
        {},
        function(data){
          var obj = JSON.parse(data);
          var htm = "";
          $.each(obj, function(x,y){
            if(y.sub_menu.length > 0){
              htm += '<li class="nav-item" data-toggle="tooltip" data-placement="right" title="'+y.name+'">';
              htm += '  <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#'+y.id+'" data-parent="#exampleAccordion">';
              htm += '    <img src="'+y.icon+'" width="25" />';
              htm += '    <span class="nav-link-text">'+y.name+'</span>';
              htm += '  </a>';

              htm += '<ul class="sidenav-second-level collapse" id="'+y.id+'">';
              var cnt = 0;
               $.each(y.sub_menu, function(a,b){
                cnt ++;
                if(cnt == 3){
                  htm += '<li class="nav_separator">';
                } else {
                  htm += '<li>';
                }
                htm += '    <a class="nav-sub-link" href="<?= base_url();?>'+b.url+'">';
                htm += '    <img src="<?= base_url();?>'+b.icon+'" width="25" /> ';
                htm += b.sub_menu+'</a>';
                htm += '</li>';
              });
              htm += '</ul>';
            } else {
              htm += '<li class="nav-item" data-toggle="tooltip" data-placement="right" title="'+y.name+'">';
              htm += '  <a class="nav-link" href="<?= base_url();?>'+y.url+'">';
              htm += '    <img src="'+y.icon+'" width="25" />';
              htm += '    <span class="nav-link-text">'+y.name+'</span>';
              htm += '  </a>';
            }
            htm += '</li>';
          });
          

          $('.Sams_Menus').html(htm);
        }
      );
    };

  </script>