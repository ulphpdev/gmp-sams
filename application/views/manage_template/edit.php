<style>
    .element_table, .activity_table {
        width : 100%;
        min-width: 500px;
    }
    
    table, tr, td { 
        vertical-align: top;
        /* border : 1px #000 solid; */
        padding: 10px;
    }

    .btn-arrange {
        border-color: #fff !important;
        width : 100%;
        height: 35px;
    }
    .btn-function {
        border-color: #fff !important;
        width : 100%;
        height: 35px;
    }

    .element_label, .activity_label {
        padding-top: 8px;
        color: #000;
    }

    .template_error {
        color: red;
        font-size: 12px;
    }

    .element-tbody, .activity-tbody {
        background : #ccc;
    }

    .element-tbody > tr > td {
        vertical-align: middle;
    }

    .activity-tbody {
        border-bottom: 1px #fff solid;
    }
    .element_add, .element_remove, .element_up, .element_down, .add_activity, .remove_activity, .add_subactivity {
        background: #ccc;
        display: inline-block;
        border: transparent;
    }

    .tbody_question > tr {
        border-bottom: 1px #ccc solid;
    }

    label  {
        font-size : 15px;
    }

    .sticky {
        position: fixed;
        top: 0;
        width: 100%;
    }
</style>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            Edit Template
        </h4>
    </div>
    <div class="panel-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">Product Type *: </label>
                <div class="col-sm-10">
                    <select id="classification" class = "required form-control" ></select>
                    <span class = "template_error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Standard/Reference *: </label>
                <div class="col-sm-10">
                    <select id="standard_reference" class = "required form-control" ></select>
                    <span class = "template_error"></span>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-body" style="padding: 0px !important;">
                <div class="template_elements table-responsive" style="margin-top: 0px !important;">
                    <table class="element_table"></table>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body" style="padding: 0px !important;">
                <div class="template_activity table-responsive" style="margin-top: 0px !important;">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button type="submit" class="btn btn-warning set_active update_status" data-status="1" data-id="<?= $id; ?>"> Activate</button>
        <button type="submit" class="btn btn-danger  set_inactive update_status" data-status="-1" data-id="<?= $id; ?>"> Deactivate</button>
        <button type="submit" class="btn btn-info save_final" data-version=1 data-sub-version=0 data-status="1" id=""><span class = "glyphicon glyphicon-paperclip"></span> Save as Final</button>
        <button type="submit" class="btn btn-primary savenewversion" data-version="" data-sub-version="0" id="" ><span class = "glyphicon glyphicon-paperclip"></span> Save as New Version</button>
        <button type="submit" class="btn btn-success update_template" data-version="" data-sub-version="" id=""><span class = "glyphicon glyphicon-floppy-saved"></span> Update</button>
        <button type="submit" class="btn btn-default" onclick="location.reload();" id=""><span class = "glyphicon glyphicon-remove-circle"></span> Close</button>
        <div class="clearfix"></div>
    </div>
</div>


<script type="text/javascript">

    var element_delete =  [];
    var question_delete =  [];
    var activity_delete =  [];
    var subactivity_delete =  [];
    var template_id = "<?= $id; ?>";
    var current_classification = 0;
    var current_standard = 0;

    $(document).ready(function() {
        var id = "<?= $id; ?>";
        load_template(id);
    });



  function load_template(id){
    aJax.postasync(
      "<?=base_url('Manage_template/view_template')?>",
      {
        id:id,
      },
      function(result){
        var obj = JSON.parse(result);
        current_classification = obj.classification_id;
        current_standard = obj.standard_id;

        product_type(obj.classification_id);
        standard(obj.standard_id);
        load_elements(obj.elements);
        load_actvity(obj.activities)

        checkinuse(obj.classification_id);

        

        sort();

        $('.savenewversion').attr("data-version",parseInt(obj.version) + 1);
        $('.savenewversion').attr("data-sub-version",0);

        $('.update_template').attr("data-version",parseInt(obj.version));
        $('.update_template').attr("data-sub-version",parseFloat(obj.sub_version) + 1);
        $('.update_template').attr("data-status",parseFloat(obj.status));


        if(obj.status == 1 || obj.status == -1){
          $('.savenewversion').show()
          $('.set_inactive').show()
          $('.set_active').show()
          $('.save_final').hide()
        }else{
          $('.savenewversion').hide()
          $('.set_inactive').hide()
          $('.set_active').hide()
          $('.save_final').show()
        }


      }
    );
  }

  function load_elements(obj){
    $.each(obj, function(x,y){
        var random_id = add_element_new(null,y.element_id,y.element_name);
        $.each(y.questions, function(a,b){
            add_question_new(".element_question_" + random_id, random_id, b.question_id, b.question, b.default_yes, b.required_remarks);
        });
    })
  }

  function load_actvity(obj){
    $.each(obj, function(x,y){
        var random_id = append_activity(y.activity_id, y.activity);
        $.each(y.sub_activities, function(a,b){
            add_subactivity(".subactivity_" + random_id,b.sub_activity_id, b.sub_activity);
        });
    })
  }

  function add_activity(element, dataid){
        var dataid = dataid || 0;
        var random_id = $.now() + dataid + template_id;
        var html = '';
        html += '                <thead class="activity-tbody activity_'+random_id+'">';
        html += '                    <td colspan="2" style="width: 100%;min-width: 500px;">';
        html += '                        <div class="form-group">';
        html += '                            <label class="col-md-2 col-xs-3 activity_label">Activity </label>';
        html += '                            <div class="col-md-10 col-xs-9">';
        html += '                                <input type="text" class="form-control activity_text" data-id='+dataid+'>';
        html += '                                <span class ="template_error"></span>';
        html += '                            </div>';
        html += '                        </div>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function add_activity" data-activity='+random_id+'>';
        html += '                            <i class="glyphicon glyphicon-plus"></i> Activity';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function remove_activity" data-activity='+random_id+'>';
        html += '                           <i class="glyphicon glyphicon-minus"></i> Activity';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function add_subactivity" data-activity='+random_id+'>';
        html += '                            <i class="glyphicon glyphicon-plus"></i> Sub-Activity';
        html += '                        </button>';
        html += '                    </td>';
        html += '                </thead>';
        html += '                <tbody class="sub-activity-tbody subactivity_'+random_id+'"></tbody>';

        $(html).insertAfter(element.replace(".activity_",".subactivity_"));

        if(dataid == 0){
            $('html, body').animate({
                scrollTop: $('.activity_'+random_id).offset().top
            }, 1000);
        }
        
        
    }

  function add_subactivity(element, dataid , value){
        var value = value || "";
        var dataid = dataid || 0;
        var random_id = $.now() + dataid + template_id;
        var html = '<tr class="tr-subactivity data_subactivity_'+random_id+'">';
        html += '                    <td colspan="3" style="width: 100%;">';
        html += '                        <div class="form-group">';
        html += '                            <label class="col-md-4 activity_label" style="text-align: right;">Sub-Activity </label>';
        html += '                            <div class="col-md-8">';
        html += '                                <input type="text" class="form-control sub_activity_text" data-id='+dataid+' value="'+value+'">';
        html += '                                <span class ="template_error"></span>';
        html += '                            </div>';
        html += '                        </div>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function insert_subactivity" data-subactivity='+random_id+'>';
        html += '                            <i class="glyphicon glyphicon-plus"></i> Add';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function remove_subactivity" data-subactivity='+random_id+'>';
        html += '                           <i class="glyphicon glyphicon-minus"></i> Remove';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    </tr>';

         

        if(dataid == 0){
            $(html).insertAfter(element);
            $('html, body').animate({
                scrollTop: $('.data_subactivity_'+random_id).offset().top
            }, 1000);

        } else {
            $(element).append(html);
        }
       

        
    }

  function append_activity(dataid, value, element){
        var element = element || null;
        var dataid = dataid || 0;
        var value = value || "";
        var random_id = $.now() + dataid  + template_id;
        var html = '';
        html += '                <thead class="activity-tbody activity_'+random_id+'">';
        html += '                    <td colspan="2" style="width: 100%; min-width:500px">';
        html += '                        <div class="form-group">';
        html += '                            <label class="col-md-2 col-xs-3 activity_label">Activity </label>';
        html += '                            <div class="col-md-10 col-xs-9">';
        html += '                                <input type="text" class="form-control activity_text" data-id='+dataid+' value="'+value+'">';
        html += '                                <span class ="template_error"></span>';
        html += '                            </div>';
        html += '                        </div>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function add_activity" data-activity='+random_id+'>';
        html += '                            <i class="glyphicon glyphicon-plus"></i> Activity';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function remove_activity" data-activity='+random_id+'>';
        html += '                           <i class="glyphicon glyphicon-minus"></i> Activity';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function add_subactivity" data-activity='+random_id+'>';
        html += '                            <i class="glyphicon glyphicon-plus"></i> Sub-Activity';
        html += '                        </button>';
        html += '                    </td>';
        html += '                </thead>';
        html += '                <tbody class="sub-activity-tbody subactivity_'+random_id+'"></tbody>';

        


        if(dataid != 0){
            $(".template_activity").append(html);
            $('html, body').animate({
                scrollTop: $('.activity_'+random_id).offset().top
            }, 1000);
            return random_id
        } else {
            $(html).insertAfter(element.replace(".activity_",".subactivity_"));
        }
        
    }

  function add_element_new(element, dataid, value){
        var element = element || null;
        var dataid = dataid || 0;
        var value = value || "";

        var random_id = $.now() + dataid + template_id;
        var html = '';
        html += '    <thead class="element-tbody element_body_'+random_id+'">';
        html += '       <tr>';
        html += '            <td style="width:20px">';
        html += '                    <button class="btn btn-default btn-arrange element_up" element-id='+random_id+'>';
        html += '                        <i class="glyphicon glyphicon-chevron-up"></i>';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width:20px">';
        html += '                    <button class="btn btn-default btn-arrange element_down" element-id='+random_id+'>';
        html += '                        <i class="glyphicon glyphicon-chevron-down"></i>';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td colspan="2" style="width: 100%;min-width: 300px;">';
        html += '                    <div class="form-group">';
        html += '                        <label class="col-md-3 element_label">Element A</label>';
        html += '                        <div class="col-md-9">';
        html += '                            <input type="text" class="form-control element_text" data-id='+dataid+' value="'+value+'">';
        html += '                            <span class ="template_error"></span>';
        html += '                        </div>';
        html += '                    </div>';
        html += '                </td>';
        html += '                <td style="width:50px">';
        html += '                    <button class="btn btn-default btn-function element_add"  element-id='+random_id+' element_question="element_question_'+random_id+'">';
        html += '                        <i class="glyphicon glyphicon-plus"></i> Element';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width:50px">';
        html += '                    <button class="btn btn-default btn-function element_remove"  element-id='+random_id+'>';
        html += '                        <i class="glyphicon glyphicon-minus"></i> Element';
        html += '                    </button>';
        html += '                </td>';
        html += '            </tr>';
        html += '        </thead>';
        html += '        <tbody class="tbody_question element_question_'+random_id+'"></tbody>';


        if(element == null){
            $(".element_table").append(html);
        } else {
            $(html).insertAfter(element);
            $('html, body').animate({
                scrollTop: $('.element_body_'+random_id).offset().top
            }, 1000);
        }

        if(dataid == 0){
            add_question_new(".element_question_" + random_id, random_id);
        } else {
            return random_id;
        }
        // add_question_new(".element_question_" + random_id, random_id);
        // add_question_new(element, elementid, dataid=0, value="", default="");
        
    }

    function add_question_new(element, elementid, dataid, value, yes, required){

        var dataid = dataid || 0;
        var value = value || "";
        var yes = yes || "";
        var required = required || 0;

        var random_id = $.now() + dataid + template_id;
        var html = '        <tr class="question_tr question_tr_'+random_id+'">';
        html += '                <td style="width:40px"></td>';
        html += '                <td style="width:20px">';
        html += '                    <button class="btn btn-default btn-arrange question_up" data-tr="question_tr_'+random_id+'">';
        html += '                        <i class="glyphicon glyphicon-chevron-up"></i>';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width:20px">';
        html += '                    <button class="btn btn-default btn-arrange question_down" data-tr="question_tr_'+random_id+'">';
        html += '                        <i class="glyphicon glyphicon-chevron-down"></i>';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width: 100%;">';
        html += '                <div class="form-group">';
        html += '                        <label class="question_label">Question A1:</label>';
        html += '                        <textarea type="text" data-id='+dataid+' class="form-control question_text unique_element_'+elementid+' required_element_'+elementid+'" rows="3">'+value+'</textarea>';
        html += '                        <span class ="template_error"></span>';
        html += '                    </div>';
        html += '                    <div class="form-group">';
        html += '                        <label>Default Answer for yes:</label>';
        html += '                        <input type="text" class="form-control question_default required_element_'+elementid+'" value="'+yes+'">';
        html += '                        <span class ="template_error"></span>';
        html += '                   </div>';
        html += '                    <div class="form-group">';
        html += '                        <label>Require Remarks:</label>';
    if(required == 1){
        html += '                        <input type="checkbox" value="" checked class="question_required">';
    } else {
        html += '                        <input type="checkbox" value="" class="question_required">';
    }
        html += '                    </div>';
        html += '                </td>';
        html += '                <td style="width:50px">';
        html += '                    <button class="btn btn-default btn-function question_add question_element_'+elementid+'" data-tr="question_tr_'+random_id+'" data-element-id='+elementid+'>';
        html += '                        <i class="glyphicon glyphicon-plus"></i> Question';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width:50px">';
        html += '                    <button class="btn btn-default btn-function question_remove"  data-tr="question_tr_'+random_id+'">';
        html += '                        <i class="glyphicon glyphicon-minus"></i> Question';
        html += '                    </button>';
        html += '                </td>';
        html += '            </tr>';

        $(element).append(html);
    }

    function append_question(element, elementid, dataid, value){
        var dataid = dataid || 0;
        var value = value || "";
        var random_id = $.now() + dataid + template_id;
        var html = '        <tr class="question_tr question_tr_'+random_id+'">';
        html += '                <td style="width:40px"></td>';
        html += '                <td style="width:20px">';
        html += '                    <button class="btn btn-default btn-arrange question_up question_up_'+elementid+'" data-tr="question_tr_'+random_id+'">';
        html += '                        <i class="glyphicon glyphicon-chevron-up"></i>';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width:20px">';
        html += '                    <button class="btn btn-default btn-arrange question_down question_down_'+elementid+'" data-tr="question_tr_'+random_id+'">';
        html += '                        <i class="glyphicon glyphicon-chevron-down"></i>';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width: 100%;">';
        html += '                <div class="form-group">';
        html += '                        <label class="question_label">Question A1:</label>';
        html += '                        <textarea type="text" data-id='+dataid+' class="form-control question_text unique_element_'+elementid+' required_element_'+elementid+'" rows="3"></textarea>';
        html += '                        <span class ="template_error"></span>';
        html += '                    </div>';
        html += '                    <div class="form-group">';
        html += '                        <label>Default Answer for yes:</label>';
        html += '                        <input type="text" class="form-control question_default required_element_'+elementid+'">';
        html += '                        <span class ="template_error"></span>';
        html += '                   </div>';
        html += '                    <div class="form-group">';
        html += '                        <label>Require Remarks:</label>';
        html += '                        <input type="checkbox" value="" class="question_required">';
        html += '                    </div>';
        html += '                </td>';
        html += '                <td style="width:50px">';
        html += '                    <button class="btn btn-default btn-function question_add question_element_'+elementid+'" data-tr="question_tr_'+random_id+'"  data-element-id='+elementid+'>';
        html += '                        <i class="glyphicon glyphicon-plus"></i> Question';
        html += '                    </button>';
        html += '                </td>';
        html += '                <td style="width:50px">';
        html += '                    <button class="btn btn-default btn-function question_remove"  data-tr="question_tr_'+random_id+'">';
        html += '                        <i class="glyphicon glyphicon-minus"></i> Question';
        html += '                    </button>';
        html += '                </td>';
        html += '            </tr>';

        $(html).insertAfter(element);

        $('html, body').animate({
            scrollTop: $('.question_tr_'+random_id).offset().top
        }, 1000);
    }

    function new_subactivity(element, dataid){
        var dataid = dataid || 0;
        var random_id = $.now() + dataid  + template_id;
        var html = '<tr class="tr-subactivity data_subactivity_'+random_id+'">';
        html += '                    <td colspan="3" style="width: 100%;">';
        html += '                        <div class="form-group">';
        html += '                            <label class="col-md-4 activity_label" style="text-align: right;">Sub-Activity </label>';
        html += '                            <div class="col-md-8">';
        html += '                                <input type="text" class="form-control sub_activity_text" data-id='+dataid+'>';
        html += '                                <span class ="template_error"></span>';
        html += '                            </div>';
        html += '                        </div>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function insert_subactivity" data-subactivity='+random_id+'>';
        html += '                            <i class="glyphicon glyphicon-plus"></i> Add';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    <td style="width:50px">';
        html += '                        <button class="btn btn-default btn-function remove_subactivity" data-subactivity='+random_id+'>';
        html += '                           <i class="glyphicon glyphicon-minus"></i> Remove';
        html += '                        </button>';
        html += '                    </td>';
        html += '                    </tr>';

        $(element).prepend(html);
        $('html, body').animate({
            scrollTop: $('.data_subactivity_'+random_id).offset().top
        }, 1000);
    }


  function product_type(id){
    aJax.postasync(
        "<?=base_url('Manage_template/get_product_type')?>",
        {},
        function(result){
            var obj = JSON.parse(result);
            var htm = '';
            $.each(obj, function(x,y){
                prodtypeid = id;
                if(id == y.classification_id){
                  htm += '<option selected value="'+y.classification_id+'">'+y.classification_name+'</option>';
                } else {
                  htm += '<option value="'+y.classification_id+'">'+y.classification_name+'</option>';
                }
                
            });
             $('#classification').append(htm);
        }
    );

    
  }

  function standard(id){
    aJax.postasync(
      "<?=base_url('Manage_template/get_standard_reference')?>",
      {},
      function(result){
          var obj = JSON.parse(result);
          var htm = '';
          $.each(obj, function(x,y){
            if(id == y.standard_id){
              htm += '<option selected class="sr sr-'+y.standard_id+'" value="'+y.standard_id+'">'+y.standard_name+'</option>';
            } else {
              htm += '<option class="sr sr-'+y.standard_id+'" value="'+y.standard_id+'">'+y.standard_name+'</option>';
            }
          });
          $('#standard_reference').append(htm);
      }
    );

  }

    function checkinuse(id){

       var standrd_id_current=id;
        aJax.postasync(
            "<?=base_url('Manage_template/check_in_use1')?>",
            {
                classification_id : standrd_id_current
            },
            function(result){
                var obj = JSON.parse(result);
                $.each(obj, function(x,y){
                    if(current_standard != y.standard_id){
                        $('.sr-'+y.standard_id).attr("disabled","disabled");
                    }
                });
            }
        );
      }

  $(document).on('change', '#classification', function() {
        
        $('#standard_reference')
            .empty()
            .append('<option selected="selected" value="" selected disabled>Loading Standard and Reference</option>')
        ;
        var class_id_current= $(this).val();
        setTimeout(function(){
            aJax.postasync(
                "<?=base_url('Manage_template/get_standard_reference')?>",
                {},
                function(result){
                    var obj = JSON.parse(result);
                    var htm = '';
                    htm += '<option value="" selected hidden>Select..</option>';
                    $.each(obj, function(x,y){
                        htm += '<option class="sr sr-'+y.standard_id+'" value="'+y.standard_id+'">'+y.standard_name+'</option>';
                    });
                    $('#standard_reference').html(htm);


                }
            );

            
            aJax.postasync(
                "<?=base_url('Manage_template/check_in_use1')?>",
                {
                    classification_id : class_id_current
                },
                function(result){
                    var obj = JSON.parse(result);
                    $.each(obj, function(x,y){
                        $('.sr-'+y.standard_id).attr("disabled","disabled");
                    });
                }
            );

            if(current_classification == class_id_current){
                $('.sr-'+current_standard).removeAttr("disabled");
            }
        }, 1000);
    });

  function sort(){
        var order = 0;
        var max_element = 25; //25
        var max_question = 70; //70
        $('.element_down').show();
        //element question
        $('.element-tbody').each(function(x,y) { 
            order++ ;
            //remove delete button if first
            if(order == 1){

                $(this).find('.element_up').hide();
                if($('.element-tbody').size() > 1){
                    $(this).find('.element_remove').show();
                } else {
                    $(this).find('.element_remove').hide();
                }
            } else {
                $(this).find('.element_remove').show();
                $(this).find('.element_up').show();
            }


            //check max
            if(max_element <= order){
                $('.element_add').prop("disabled",true);
            } else {
                $('.element_add').prop("disabled",false);
            }

            //add order in text element
            $(this).find('.element_text').attr("orders", order);

            //add order in label element
            $(this).find('.element_label').html("Element " + toLetters(order));


            //each question
            var question_order = 0;
            
            $(this).next().find('.question_tr').each(function(a,b){
                question_order++;
                var element_id = $(this).find('.question_add').attr("data-element-id");
                $(this).find('.question_label').html("Question " + toLetters(order) + question_order);
                $(this).find('.question_down').show();

                $(this).find('.question_text').attr("orders", question_order);

                //check max
                if(max_question <= question_order){
                    $('.question_element_' + element_id).prop("disabled",true);
                } else {
                    $('.question_element_' + element_id).prop("disabled",false);
                }

                if(question_order == 1){
                    $(this).find('.question_up').hide();
                    if($(this).find('.question_tr').size() > 1){
                        $(this).find('.question_remove').show();
                    } else {
                        $(this).find('.question_remove').hide();
                    }
                } else {
                    $(this).find('.question_remove').show();
                    $(this).find('.question_up').show();
                }
            });
            $(this).next().find('.question_tr').last().find('.question_down').hide();
        });

        $('.element-tbody').last().find('.element_down').hide();



        //activities
        var activity_order = 0;
        $('.activity-tbody').each(function(x,y) { 
            activity_order++;
            //add order in text activity
            $(this).find('.activity_text').attr("orders", activity_order);

            //add order in label activity
            $(this).find('.activity_label').html("Activity " + activity_order);


            if(order == 1){

                if($('.activity-tbody').size() > 1){
                    $(this).find('.remove_activity').show();
                } else {
                    $(this).find('.remove_activity').hide();
                }
            } else {
                $(this).find('.remove_activity').show();
            }

            //get subactivity group id
            var sub_activities = ".subactivity_" + $(this).attr("class").replace("activity-tbody activity_","");

            var sub_order = 0;
            $(sub_activities).find('.tr-subactivity').each(function(a,b){
                sub_order++;
                $(this).find('.activity_label').html("Sub-Activity " + sub_order);
                $(this).find('.sub_activity_text').attr("orders", sub_order);
            });

        });
        
    }

    function toLetters(num) {
        var mod = num % 26;
        var pow = num / 26 | 0;
        var out = mod ? String.fromCharCode(64 + mod) : (pow--, 'Z');
        return pow ? toLetters(pow) + out : out;
    }

    //validation
    var validate = {
        emailaddress : function(email){
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(email);
        },
        required: function(element){
            var counter = 0;
            $(element).each(function(){
                if($(this).val() != null){
                    var input = $(this).val().trim();
                    if (input.length == 0) {
                        $(this).css('border-color','red');
                        $(this).next().html("This field is required!");
                        counter++;
                    }else{
                        if(input == 0 || input == "0"){
                            $(this).css('border-color','red');
                            $(this).next().html("Invalid Input.");
                            counter++;
                        } else {
                        $(this).css('border-color','#ccc');
                        $(this).next().html("");
                        }           
                    }
                } else {
                    counter++;
                    $(this).css('border-color','red');
                    $(this).next().html("This field is required!");
                }
                
            });
            return counter;
        },
        unique: function(element) {
            var values = {};
            var count = 0;
            var checks = $(element);
    
            checks.each(function(i, elem)
            {
                if(elem.value in values) {
                    count++ ;
                    $(elem).css('border-color','red');
                    $(elem).next().html("Please enter a Unique Value.");
                    $(values[elem.value]).css('border-color','red');
                    $(values[elem.value]).next().html("Please enter a Unique Value.");
                    $(values[elem.value]).next().show();
                } else {
                    values[elem.value] = elem;
                }
            });
            return count;
        }
    }

    //modals
    var modal = {
        confirm : function(message,cb){
            bootbox.confirm({
                message: message,
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-default'
                    }
                },
                callback: cb
            });
        },
        alert : function(message, cb){
            bootbox.alert({
                message: message,
                callback: cb
            });
        },
        show : function(message, size, cb){
            bootbox.alert({
                message: message,
                size: size,
                callback: cb
            });
        },
        input : function(message,type, cb){
            bootbox.prompt({
                title: message,
                inputType: type,
                callback: cb
            });
        },
        custom : function(modal, action){
            $(modal).modal(action);
        }
    }

 



    //save template - new version
    $(document).on('click', '.savenewversion', function(){ 
        var new_version = $(this).attr('data-version');
        var sub_version = $(this).attr('data-sub-version');

        modal.confirm("Are you sure you want to save this template?",function(result){
            if(result){
                $('.alert-danger').remove();
                var error_message = "";
                var error = 0;
                
                //validate product type required
                if(validate.required("#classification") != 0){
                    error++;
                    error_message += "<li><b>Product Type</b> is required.</li>";
                }
                
                //validate product type required
                if(validate.required("#standard_reference") != 0){
                    error++;
                    error_message += "<li><b>Standard/Reference</b> is required.</li>";
                }

                //validate required element
                if(validate.required(".element_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Element</b> is blank.</li>";
                }
                
                //validate unique element
                if(validate.unique(".element_text") != 0){
                    error++;
                    error_message += "<li>Each <b>Elements</b> must be unique.</li>";
                }
                
                //validate required question
                if(validate.required(".question_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Question</b> is blank.</li>";
                }
                
                //validate required activity
                if(validate.required(".activity_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Activity</b> is blank.</li>";
                }
                //validate required sub activity
                if(validate.required(".sub_activity_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Sub-Activity</b> is blank.</li>";
                }

                //validate unique activity
                if(validate.unique(".activity_text") != 0){
                    error++;
                    error_message += "<li>Each <b>Activities</b> must be unique.</li>";
                }

                //validate unique question per element
                $('.element-tbody').each(function(x,y) { 
                    var element_id = $(this).attr("class").trim().replace("element-tbody element_body_","");
                    var element_name = $('.element_body_'+ element_id).find('.element_label').html();
                    if(validate.unique(".unique_element_" + element_id) != 0){
                        error++;
                        error_message += "<li>Questions in <b>"+ element_name +"</b> must be unique.</li>";
                    }
                });
                
                if(error > 0){
                    //display error message
                    var html_error = '';
                    html_error += '<div class="alert alert-danger" style="text-align: left;">';
                    html_error += '  <strong>There are some error saving template, please correct the item(s) below:</strong>';
                    html_error += '     <ol>';
                    html_error +=           error_message;
                    html_error += '     </ol>';
                    html_error += '</div>';

                    $(".panel-footer").prepend(html_error);
                    $('html, body').animate({
                        scrollTop: $('.alert-danger').offset().top
                    }, 1000);
                } else {
                    //save template
                    isLoading(true);
                    var obj = template_object(1,new_version,sub_version);
                    insert_audit_trail("Save Template " + template_id + " as new version from " + (parseFloat(new_version) - 1) + '.' + sub_version +  " to " + new_version + '');

                    save_template(obj);
                }
            }
        });
    });  

    //update template
    $(document).on('click', '.update_template', function(){ 
        var new_version = $(this).attr('data-version');
        var sub_version = $(this).attr('data-sub-version');
        var sub_status = $(this).attr('data-status');

        modal.confirm("Are you sure you want to save this template?",function(result){
            if(result){
                $('.alert-danger').remove();
                var error_message = "";
                var error = 0;
                
                //validate product type required
                if(validate.required("#classification") != 0){
                    error++;
                    error_message += "<li><b>Product Type</b> is required.</li>";
                }
                
                //validate product type required
                if(validate.required("#standard_reference") != 0){
                    error++;
                    error_message += "<li><b>Standard/Reference</b> is required.</li>";
                }

                //validate required element
                if(validate.required(".element_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Element</b> is blank.</li>";
                }
                
                //validate unique element
                if(validate.unique(".element_text") != 0){
                    error++;
                    error_message += "<li>Each <b>Elements</b> must be unique.</li>";
                }
                
                //validate required question
                if(validate.required(".question_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Question</b> is blank.</li>";
                }
                
                //validate required activity
                if(validate.required(".activity_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Activity</b> is blank.</li>";
                }
                //validate required sub activity
                if(validate.required(".sub_activity_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Sub-Activity</b> is blank.</li>";
                }

                //validate unique activity
                if(validate.unique(".activity_text") != 0){
                    error++;
                    error_message += "<li>Each <b>Activities</b> must be unique.</li>";
                }

                //validate unique question per element
                $('.element-tbody').each(function(x,y) { 
                    var element_id = $(this).attr("class").trim().replace("element-tbody element_body_","");
                    var element_name = $('.element_body_'+ element_id).find('.element_label').html();
                    if(validate.unique(".unique_element_" + element_id) != 0){
                        error++;
                        error_message += "<li>Questions in <b>"+ element_name +"</b> must be unique.</li>";
                    }
                });
                
                if(error > 0){
                    //display error message
                    var html_error = '';
                    html_error += '<div class="alert alert-danger" style="text-align: left;">';
                    html_error += '  <strong>There are some error saving template, please correct the item(s) below:</strong>';
                    html_error += '     <ol>';
                    html_error +=           error_message;
                    html_error += '     </ol>';
                    html_error += '</div>';

                    $(".panel-footer").prepend(html_error);
                    $('html, body').animate({
                        scrollTop: $('.alert-danger').offset().top
                    }, 1000);
                } else {
                    //save template
                    isLoading(true);
                    var obj = template_object(sub_status,new_version,sub_version);
                    insert_audit_trail("Update Template " + template_id + " version " + new_version + '.' + (parseFloat(sub_version) - 1) +  " to " + new_version + '.' + sub_version);
                    save_template(obj);
                }
            }
        });
    });

    //update template
    $(document).on('click', '.save_final', function(){ 

        modal.confirm("Are you sure you want to save this template?",function(result){
            if(result){
                $('.alert-danger').remove();
                var error_message = "";
                var error = 0;
                
                //validate product type required
                if(validate.required("#classification") != 0){
                    error++;
                    error_message += "<li><b>Product Type</b> is required.</li>";
                }
                
                //validate product type required
                if(validate.required("#standard_reference") != 0){
                    error++;
                    error_message += "<li><b>Standard/Reference</b> is required.</li>";
                }

                //validate required element
                if(validate.required(".element_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Element</b> is blank.</li>";
                }
                
                //validate unique element
                if(validate.unique(".element_text") != 0){
                    error++;
                    error_message += "<li>Each <b>Elements</b> must be unique.</li>";
                }
                
                //validate required question
                if(validate.required(".question_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Question</b> is blank.</li>";
                }
                
                //validate required activity
                if(validate.required(".activity_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Activity</b> is blank.</li>";
                }
                //validate required sub activity
                if(validate.required(".sub_activity_text") != 0){
                    error++;
                    error_message += "<li>One of the <b>Sub-Activity</b> is blank.</li>";
                }

                //validate unique activity
                if(validate.unique(".activity_text") != 0){
                    error++;
                    error_message += "<li>Each <b>Activities</b> must be unique.</li>";
                }

                //validate unique question per element
                $('.element-tbody').each(function(x,y) { 
                    var element_id = $(this).attr("class").trim().replace("element-tbody element_body_","");
                    var element_name = $('.element_body_'+ element_id).find('.element_label').html();
                    if(validate.unique(".unique_element_" + element_id) != 0){
                        error++;
                        error_message += "<li>Questions in <b>"+ element_name +"</b> must be unique.</li>";
                    }
                });
                
                if(error > 0){
                    //display error message
                    var html_error = '';
                    html_error += '<div class="alert alert-danger" style="text-align: left;">';
                    html_error += '  <strong>There are some error saving template, please correct the item(s) below:</strong>';
                    html_error += '     <ol>';
                    html_error +=           error_message;
                    html_error += '     </ol>';
                    html_error += '</div>';

                    $(".panel-footer").prepend(html_error);
                    $('html, body').animate({
                        scrollTop: $('.alert-danger').offset().top
                    }, 1000);
                } else {
                    //save template
                    isLoading(true);
                    var obj = template_object(1,1,0);
                    insert_audit_trail("Update Template Status "+ template_id +" into Final");
                    save_template(obj);
                }
            }
        });
    }); 

    function save_template(obj)
    {
        // console.log(obj);
        aJax.post(
            "<?=base_url('Manage_template/generate_template')?>",
            obj,
            function(result){
                update_config();
                isLoading(false);
                modal.alert("Template is successfully saved.",function(){
                    location.reload();
                });
            }
        );
    }


    function template_object(status, new_version, sub_version)
    {
        var elements = [];
        $('.element-tbody').each(function(x,y) { 
            var element_name = $(this).find('.element_text').val();
            var element_order = $(this).find('.element_text').attr("orders");
            var element_id = $(this).find('.element_text').attr("data-id");

            var questions = [];
            $(this).next().find('.question_tr').each(function(a,b){
                var question = $(this).find('.question_text').val();
                var question_order = $(this).find('.question_text').attr('orders');
                var question_id = $(this).find('.question_text').attr('data-id');

                var default_yes = $(this).find('.question_default').val();
                if ($(this).find('.question_required').is(':checked')) {
                    var required = 1;
                } else {
                    var required = 0;
                }
                
                //build question object
                var question_details = {
                    "data-id" : question_id,
                    "question" : question,
                    "default_yes" : default_yes,
                    "required" : required,
                    "order" : question_order
                }
                questions.push(question_details);
            });

            //build element object
            var element_details = {
                "element" : element_name,
                "order" : element_order,
                "dataid" : element_id,
                "questions" : questions,
            }

            elements.push(element_details);
        });


        var activities = [];
        $('.activity-tbody').each(function(x,y) { 
            var subactivities = [];
            $(this).next().find(".tr-subactivity").each(function(b,c){
                var subactivity = $(this).find('.sub_activity_text').val();
                var order = $(this).find('.sub_activity_text').attr("orders");
                var data_id = $(this).find('.sub_activity_text').attr("data-id");
                var sub_array = {
                    "sub_activity" : subactivity,
                    "order" : order,
                    "data_id" : data_id,
                }
                subactivities.push(sub_array);
            });
            var activity = {
                "activity" : $(this).find(".activity_text").val(),
                "order" : $(this).find(".activity_text").attr("orders"),
                "data_id" : $(this).find(".activity_text").attr("data-id"),
                "subactivities" : subactivities
            }
            activities.push(activity);
        });


        //complete json 
        var template = {
            "classification": $('#classification').val(),
            "standard": $('#standard_reference').val(),
            "status": status,
            "elements" : elements,
            "activity" : activities,
            "action" : "update",
            "del_elements" : element_delete,
            "del_questions" : question_delete,
            "del_activities" : activity_delete,
            "del_subactivities" : subactivity_delete,
            "template_id" : template_id,
            "new_version" : new_version,
            "sub_version" : sub_version
        }

        return template;
    }

    $(document).on('click', '.update_status', function() {
        var status = $(this).attr('data-status');
        if(status == 1){
            var message = "Are you sure you want set this template as Active?";
        } else {
            var message = "Are you sure you want set this template as Inactive?";
        }

        modal.confirm(message,function(result){
            if(result){
                isLoading(true);
                aJax.post(
                    "<?=base_url('global_controller/inactive_global_update');?>",
                    {
                        id:template_id,
                        type:status, 
                        table:'tbl_template', 
                        order_by: 'template_id'
                    },
                    function(data){
                        if(status == 1){
                            insert_audit_trail("Set as Active - " + template_id);
                        } else {
                            insert_audit_trail("Set as Inactive - " + template_id);
                        }
                        generate_json(template_id);
                        update_config();
                        modal.alert("Template is successfully saved.",function(){
                            location.reload();
                        });
                    }
                );
            }
        });
    });


    function generate_json(template_id){

        aJax.get(
            "<?=base_url()?>/api/template_list",
            function(data){

            }
        );

        aJax.get(
            "<?=base_url()?>/api/template_info/"+template_id,
            function(data){

            }
        );
    }
</script>