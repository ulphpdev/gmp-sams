<style type="text/css">

.disabled{

    pointer-events: none;

    cursor: not-allowed;

}

</style>



<input type="hidden" name="success" id="success" value="0">

<input type="hidden" name="version" id="version" value="0">

<div class="col-md-12 col-sm-12 container-template_add">

   <div class="col-md-12 add-template-body">

      <div class="col-md-12 pad-5">

         <div class="col-md-2">Product Type</div>

         <div class = "col-md-4">

            <select id="classification" class = "inputcss form-control fullwidth inputs">

            </select>

            <span class = "er-msg">Classification should not be empty *</span>

         </div>

      </div>

      <div class="col-md-12 pad-5">

         <div class="col-md-2">Standard/Reference</div>

         <div class = "col-md-4">

            <select id="standard_reference" class = "inputcss form-control fullwidth inputs">

            </select>

            <span class = "er-msg">Standard/Reference should not be empty *</span>

         </div>

      </div>

   </div>

   <div class="col-md-12 add-template-body-bottom">

      <div class="col-md-12">

          <button type="submit" class="btn btn-primary button-template" id="add_element" data-id='1' data-cnt="1"><i class="glyphicon glyphicon-plus"></i> Add Element</button>

      </div>

      <div class="col-md-12" id="main-content" style="padding:0px 15px;">

         

    </div>

    <div class="col-md-12">

        <button type="submit" class="btn btn-primary button-template" data-id="1" id="add_activity" data-cnt="1"><i class="glyphicon glyphicon-plus"></i> ADD ACTIVITY</button>

     </div>

    <div class="col-md-12 activity_container"></div>

   </div>

   <div class="col-md-12">

      <div class="col-md-12 template_action">

          <button  class="btn btn-danger button-template cancel"><span class = "glyphicon glyphicon-remove-circle"></span> Cancel</button>

          <button type="submit" class="btn btn-info button-template save_template savenewversion" data-status="1" id=""><span class = "glyphicon glyphicon-floppy-saved"></span> Save as New Version</button>

          <button type="submit" class="btn btn-primary button-template save_template submitfinal" data-status="0" id=""><span class = "glyphicon glyphicon-floppy-saved"></span> Update</button>


          <button class="save_final button-template btn btn-info"> <span class = "glyphicon glyphicon-check"></span> Save as Final</button>

      </div>

   </div>

</div>



<script type="text/javascript">


$(document).ready(function(){

var id = "<?= $id; ?>";



get_template(id);



function get_template(id){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/Manage_template/get_data_by_id",

        data:{id:id,table:'tbl_template',field:'template_id'}

    }).done(function(data){

        var obj = JSON.parse(data);

        $.each(obj, function(x,y){

            productType(y.classification_id);

            standard_reference(y.standard_id);

            get_elements(y.template_id);

            get_activity(y.template_id);

            $('#version').val(y.version);

            if(y.status == 1){

                $('.savedraft').hide();

                $('.savenewversion').show()

                $('.save_final').hide()

            }else{

                $('.submitfinal').text('Save');

                $('.savedraft').show();

                $('.savenewversion').hide()

                $('.save_final').show()

            }

        });

    });

}



function productType(selected){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/Manage_template/get_product_type"

    }).done(function(data){

        var obj = JSON.parse(data);

        var htm = '';

        // htm += '<option value="" selected hidden>--Select--</option>';

        $.each(obj, function(x,y){

            htm += '<option value="'+y.classification_id+'">'+y.classification_name+'</option>';

        });

        $('#classification').append(htm);

        $('#classification option[value="'+selected+'"]').attr('selected','selected');

    });

}



function standard_reference(selected){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/Manage_template/get_standard_reference"

    }).done(function(data){

        var obj = JSON.parse(data);

        var htm = '';

        // htm += '<option value="" hidden>--Select--</option>';

        $.each(obj, function(x,y){

            htm += '<option value="'+y.standard_id+'">'+y.standard_name+'</option>';

        });

        $('#standard_reference').append(htm);

        $('#standard_reference option[value="'+selected+'"]').attr('selected','selected');

    });

}



function get_elements(template_id){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/Manage_template/get_elements",

        data:{template_id:template_id}

    }).done(function(data){

        var obj = JSON.parse(data);

        var htm = '';

        var que = '';

        var el_ctr = 1;

        $.each(obj, function(x,y){

            htm += '<div class="element_item col-md-12 pad-0">';

            if(el_ctr == 1){

                var active = 'hidden';  

            }else{

                var active = '';

            }

            htm += '<div class="col-md-1">';

            htm += '<button class="btn btn-default up '+active+'" order="'+el_ctr+'"><span class="glyphicon glyphicon-chevron-up"></span></button>';

            htm += '<button class="btn btn-default down" order="'+el_ctr+'"><span class="glyphicon glyphicon-chevron-down"></span></button>';

            htm += '</div>';



            htm += '<div class="col-md-11 pad-0">';

            

            htm += '<div class="col-md-12 element_' + y.element_id + ' update_element elem_div pad-5">';

            htm += '<div class="col-md-2 el_text">Element ' + toLetters(el_ctr) + " " + el_ctr + '</div>';

            htm += '<div class="col-md-8">';

                htm += '<input type="text" id="elem_'+y.element_id+'" data-elem="'+y.element_id+'" data-id="'+y.element_id+'" class="elem_'+y.element_id+' elem_val form-control" value="'+y.element_name+'">';

                htm += '<span class = "er-msg">This should not be empty *</span>';

            htm += '</div>'; 



            htm += '<div class="col-md-1 pad-0">';

            htm += '<button class="btn btn-default '+active+'"type="submit" data-id="'+y.element_id+'" data-cnt="'+el_ctr+'" id="remove_element" title="Remove Element"><i class="glyphicon glyphicon-minus "></i></button>';

            htm += '</div>';

            

            htm += '</div>';

            htm += '<div class="question_div_parent update_question" id="element_question_'+y.element_id+'"></div>';

            htm += '</div>';

            htm += '</div>';

            $('#add_element').attr('data-id', el_ctr);

            $('#add_element').attr('data-cnt', el_ctr);

            get_questions(y.element_id,template_id);

            el_ctr++;

        });

        $('#main-content').append(htm);

        checkorder('');

    });

}





function get_questions(element_id,template_id){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/Manage_template/get_questions",

        data:{element_id:element_id,template_id:template_id}

    }).done(function(data){

        var obj = JSON.parse(data);

        var que = ''

        var que_ctr = 1;

        $.each(obj, function(x,y){

            if(que_ctr == 1){

                var hidden = 'hidden';

            }else{

                var hidden = '';

             }

            que += '<div class="col-md-12 question_div  update_question question_'+y.question_id+' pad-5">';

            que += '<div class="col-md-2 pad-0 quest-title-ctr">';

            que += '<div class="col-md-8 pad-0">';

            que += '<button class="btn btn-default up_q '+hidden+'" order="'+que_ctr+'">';

            que += '<span class="glyphicon glyphicon-chevron-up" ></span>';

            que += '</button>';

            que += '<button class="btn btn-default down_q" order="'+que_ctr+'">';

            que += '<span class="glyphicon glyphicon-chevron-down"></span>';

            que += '</button>';

            que += '</div>';

            que += '<div class="col-md-8 pad-0"><span class="qtext">Question '+que_ctr+'</span></div>';

            que += '</div>';

            que += '<div class="col-md-8">';

            que += '<textarea id="ques_'+y.question_id+'" data-id="'+y.question_id+'"  data-elem= "'+y.element_id+'" class="ques_'+y.question_id+' ques_val form-control">'+y.question+'</textarea>';

            que += '<span class = "er-msg">This should not be empty *</span>';

            que += '<div>Default Answer for yes:    <input id="default_yes_'+y.question_id+'"  data-elem="'+y.element_id+'" class="inputcss default_yes_'+y.question_id+' default_yes_val form-control df_element_'+y.element_id+'" type="text" value="'+y.default_yes+'" name="fname" ><span class = "er-msg">This should not be empty *</span>';                  

            que += '</div>';

            // que += '<form>';

            

            // if(y.mandatory == 'yes'){

            // que += '<input class="mandatory" id="mandatory_'+y.question_id+'"  type="radio" name="mandatory" value="yes" checked> Yes'; 

            // que += '<input id="mandatory_'+y.question_id+'" type="radio" name="mandatory" value="no"> No <br>';

            // }else{

            // que += '<input class="mandatory" id="mandatory_'+y.question_id+'"  type="radio" name="mandatory" value="yes"> Yes'; 

            // que += '<input id="mandatory_'+y.question_id+'" type="radio" name="mandatory" value="no" checked> No <br>';    

            // }



            // que += '</form>';

            que += '</div>';

            if(que_ctr == 1){

            que += '<div class="col-md-1 qbtn pad-0"><button type="submit" class="btn btn-default parent_elem_'+element_id+'" data-id= "'+element_id+'" data-cnt= "'+obj.length+'" id="add_question" element="'+element_id+'" class="btn btn-default parent_elem_'+element_id+'"><i class="glyphicon glyphicon-plus " ></i></button></div>';    

            }else{

            que += '<div class="col-md-1 qbtn pad-0"><button type="submit" class="remove  btn btn-default" data-elem="'+element_id+'"><i class="glyphicon glyphicon-minus "></i></button></div>';     

            que += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default parent_elem_'+element_id+'" data-id="' + element_id + '" data-cnt="1" id="add_question" element='+element_id+' class="parent_elem_'+element_id+'"><i class="glyphicon glyphicon-plus " ></i></button></div>';

            }

            que += '</div>';

            que_ctr++;

            

        });



        $('#element_question_'+element_id).append(que);

        checkorder_q('',element_id);

    });

}







$(document).on('click', '#add_question', function() {

    var htm =''

    var id = $(this).attr('data-id');

    var fcnt = $(this).attr('data-cnt');

    var cnt = parseInt($(this).attr('data-cnt'))+1;

    // alert(cnt);

    var error = 1;

    var ctr =parseInt($(this).attr('element'));

    console.log("id : " + id);

   

    $('#element_question_'+ctr+' .ques_val').each(function() {

        if ($(this).val() == 0) {

            $(this).css('border-color', 'red');

            $(this).next().show();

            error = 1;

        }else {

            if($(this).val()  == 0 || $(this).val() == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                error++;
            } else {
            if($(this).val()  == 0 || $(this).val() == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                error++;
            } else {
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
              error = 0;
            }  
            }  
        }

    });

    if(error == 0){

        // alert(ctr);

        htm += '<div class="4remove pad-5 col-md-12 insert_question question_div question_'+cnt+'">';

        htm += '<div class="col-md-2 pad-0 quest-title-ctr">';

        htm += '<div class="col-md-8 pad-0">';

        htm += '<button class="btn btn-default up_q" order="'+cnt+'">';

        htm += '<span class="glyphicon glyphicon-chevron-up" ></span>';

        htm += '</button>';

        htm += '<button class="btn btn-default down_q" order="'+cnt+'">';

        htm += '<span class="glyphicon glyphicon-chevron-down"></span>';

        htm += '</button>';

        htm += '</div>';

        htm += '<div class="col-md-8 pad-0"><span class="qtext">Question '+cnt+'</span></div>';

        htm += '</div>';

        htm += '<div class="col-md-8 boom"><textarea id="ques_'+cnt+'" data-id="0" data-elem="'+ctr+'" class="form-control ques_val ques_' + cnt + '"></textarea><span class = "er-msg">This should not be empty *</span>';

        htm += '<div>Default Answer for yes:<input type="text" value="" name="fname" id="default_yes_' + cnt + '" data-elem="' + ctr + '" class="inputcss default_yes_val default_yes_' + cnt + ' form-control df_element_'+id+'"><span class = "er-msg">This should not be empty *</span></div>';

        // htm += '<form > ';

        // htm += '<input class="mandatory_1"   type="radio" name="mandatory" value="yes"  id="mandatory_' + cnt + '" data-elem="' + ctr + '" checked> Yes ';

        // htm += '<input class="mandatory_1" type="radio" name="mandatory" value="no" id="mandatory_' + cnt + '" data-elem="' + ctr + '"> No <br>';

        // htm += '</form>';

        htm += '</div>';

        htm += '<div class="col-md-1 qbtn pad-0">'

        htm += '<button type="submit " class="remove btn btn-default" data-elem="'+id+'"><i class="glyphicon glyphicon-minus "></i></button>';

        htm += '</div>';

        htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default parent_elem_'+id+'" data-id="' + id + '" data-cnt="1" id="add_question" element='+id+' class="parent_elem_'+id+'"><i class="glyphicon glyphicon-plus " ></i></button></div>';


        htm += '</div>';

        htm += '</div>';

        $('#element_question_'+ctr).append(htm);

        $(this).attr('data-cnt',cnt);

        checkorder_q('',ctr);

    } 

}); 



//remove question

$(document).on('click', '.remove', function() {

    if($(this).parent('div').parent('.question_div').hasClass('insert_question')){

        $(this).parent('div').parent('.question_div').remove();

    }else{

        $(this).parent('div').parent('.question_div').removeClass('update_question').addClass('removed');

    }

    var element = $(this).attr('data-elem');

    var cnt = parseInt($('#add_question.parent_elem_'+element).attr('data-cnt'))-1;

    var htm ="";

    var ctr = 1;

    // var element_ctr =1;

    // alert(element_ctr);

    $('#add_question.parent_elem_'+element).attr('data-cnt',cnt);

    

    $('#element_question_'+element+ ' .question_div .quest-title-ctr .qtext').each(function(){

        if($(this).parents('.question_div_parent').hasClass('removed') == false){

            $(this).text('Question '+ctr);

            ctr++;

        }       

    });

});





function get_activity(template_id){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/Manage_template/get_activity",

        data:{template_id:template_id}

    }).done(function(data){

        var obj = JSON.parse(data);

        var htm = '';

        var act_ctr = 1;

        $.each(obj, function(x,y){



            htm += '<div class="col-md-12 activity_' + y.activity_id + ' update_ activity_div pad-5">';

            htm += '<div class="col-md-2">Activity ' + act_ctr + '</div>';

            htm += '<div class="col-md-4"><input type="text" id="activity_' + y.activity_id + '" data-activity="' + y.activity_id + '" data-id="' + y.activity_id + '" class="act_'+y.activity_id+' act_val form-control" value="'+y.activity_name+'"><span class = "er-msg">This should not be empty *</span></div>';

            htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default" data-id="' + y.activity_id + '" data-cnt="1" data-ctr="1" id="add_sub_activity" title="Add Sub-activity"><i class="glyphicon glyphicon-plus " ></i></button></div>';

            htm += '</div>';       

            act_ctr++; 

            $('#add_activity').attr('data-id', act_ctr);

            get_subactivity(y.activity_id);

        });

         $('.activity_container').append(htm);



    });

}



function get_subactivity(activity_id){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/Manage_template/get_data_by_id",

        data:{id:activity_id,table:'tbl_sub_activities',field:'activity_id'}

    }).done(function(data){

        var obj = JSON.parse(data);

        var htm = '';

        var sub_ctr = 1;

        if(obj.length == 0){

            $('.activity_'+activity_id+' #add_sub_activity').attr('data-cnt','0');

        }else{

            $.each(obj, function(x,y){



                htm += '<div class="col-md-12 subact_div subActivity_' + y.sub_item_id + ' update_  pad-0" style="margin-top:5px;">';

                htm += '<div class="col-md-2 subtxt" style="text-align:right;">Sub-activity ' + sub_ctr + '</div>';

                htm += '<div class="col-md-4"><input type="text" value="'+y.sub_item_name+'" id="subactivity_' + y.sub_item_id + '" data-subactivity="' + sub_ctr + '" data-id="' + y.sub_item_id + '" class="subact_'+y.sub_item_id+' subact_val form-control"><span class = "er-msg">This should not be empty *</span></div>';

                htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default" data-id="' + y.sub_item_id + '"  data-cnt="'+sub_ctr+'" data-activity="'+activity_id+'" id="remove_sub_activity" title="Remove Sub-activity"><i class="glyphicon glyphicon-minus" ></i></button></div>';

                htm += '</div>';

                $('.activity_'+activity_id+' #add_sub_activity').attr('data-cnt',sub_ctr);

                $('.activity_'+activity_id+' #add_sub_activity').attr('data-ctr',sub_ctr);

                sub_ctr++;      


                

            });

        }



        $('.activity_'+activity_id).append(htm);



    });

}



$(document).on('click','#remove_sub_activity', function(){

    var act_id = $(this).attr('data-activity');

    var sub_id = $(this).attr('data-id');

    var cnt = parseInt($('.activity_'+act_id+' #add_sub_activity').attr('data-cnt'))-1;

    // alert(cnt);

    var ctr1 = 1;

    if($(this).parent('div').parent('.subActivity_'+sub_id).hasClass('insert_') == true){

        $(this).parent('div').parent('.subActivity_'+sub_id).remove();

    }else{

        $(this).parent('div').parent('.subActivity_'+sub_id).removeClass('update_').addClass('removed');

    }

    

    $('.activity_'+act_id+' #add_sub_activity').attr('data-cnt',cnt);



    $('.activity_'+act_id+ ' .subact_div .subtxt').each(function(){

        if($(this).parent('.subact_div').hasClass('removed') == false){

            $(this).text('Sub-activity '+ctr1);

            ctr1++;

        }

    });



});





$(document).on('click', '#add_element', function() {



    var htm = '';

    var id = parseInt($(this).attr('data-id'))+1;

    var cnt = parseInt($(this).attr('data-cnt'))+1;

    $('.ques_val').css('border-color', '#ccc');

    $('.elem_val').css('border-color', '#ccc');

    $('.er-msg').hide();

    var error = 0;

    var ctr = 1;

    $('.elem_val').each(function() {

        if ($(this).val() == 0) {

            $(this).css('border-color', 'red');

            $(this).next().show();

            error++;

        } else {

            if($(this).val()  == 0 || $(this).val() == "0"){
                $(this).css('border-color','red');
                $(this).next().html("Invalid Input.");
                $(this).next().show();
                error++;
            } else {
              $(this).css('border-color', '#ccc');
              $(this).next().hide();
            }  

        }

    })

    if (error == 0) {

        htm += '<div class="element_item col-md-12 pad-0">';

        htm += '<div class="col-md-1"><button class="btn btn-default up" order="'+id+'"><span class="glyphicon glyphicon-chevron-up"></span></button><button class="btn btn-default down" order="'+id+'"><span class="glyphicon glyphicon-chevron-down"></span></button></div>';

        htm += '<div class="col-md-11 pad-0">';

        htm += '<div class="col-md-12 element_' + cnt + ' insert_ elem_div pad-5">';

        htm += '<div class="col-md-2 el_text">Element ' + id + '</div>';

        htm += '<div class="col-md-4">   <input type="text" id="elem_' + cnt + '" data-elem="' + cnt + '" data-id="' + cnt + '" class="elem_1 elem_val form-control"><span class = "er-msg">This should not be empty *</span></div>';

        htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default" data-id="' + cnt + '" data-cnt="'+cnt+'" id="remove_element" title="Remove Element"><i class="glyphicon glyphicon-minus " ></i></button></div>';

        htm += '</div><div class="question_div_parent insert_question" id="element_question_'+cnt+'">';

        htm += '<div class="col-md-12 question_div insert_question question_' + cnt + '">';

        htm += '<div class="col-md-2 pad-0 quest-title-ctr">';

        htm += '<div class="col-md-8 pad-0">';

        htm += '<button class="btn btn-default up_q hidden" order="'+ctr+'">';

        htm += '<span class="glyphicon glyphicon-chevron-up" ></span>';

        htm += '</button>';

        htm += '<button class="btn btn-default down_q" order="'+ctr+'">';

        htm += '<span class="glyphicon glyphicon-chevron-down"></span>';

        htm += '</button>';

        htm += '</div>';

        htm += '<div class="col-md-8 pad-0"><span class="qtext">Question '+ctr+'</span></div>';

        htm += '</div>';

        htm += '<div class="col-md-8"><textarea id="ques_' + cnt + '" data-elem="' + cnt + '" class="ques_val ques_' + cnt + ' form-control"></textarea><span class = "er-msg">This should not be empty *</span>';

        htm += '<div>Default Answer for yes:<input type="text" value="test" name="fname" id="default_yes_' + cnt + '" data-elem="' + cnt + '" class="inputcss default_yes_val default_yes_' + cnt + ' form-control"><span class = "er-msg">This should not be empty *</span></div>';

        // htm += '<form>';

        // htm += '<input  class="mandatory' + cnt + '"  type="radio" name="mandatory" value="yes"  id="mandatory_' + cnt + '" data-elem="' + cnt + '" checked> Yes ';

        // htm += '<input class="mandatory' + cnt + '" type="radio" name="mandatory" value="no" id="mandatory_' + cnt + '" data-elem="' + cnt + '"> No <br>';

        // htm += '</form>';

       

        htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default parent_elem_'+cnt+'" data-id="' + cnt + '" data-cnt="1" id="add_question" element='+cnt+' class="parent_elem_'+cnt+'"><i class="glyphicon glyphicon-plus " ></i></button>';
        htm += '</div>';



        htm += '</div></div></div>';

        $('#main-content').append(htm);

        $(this).attr('data-id', id);

        $(this).attr('data-cnt', cnt);



        $('.element_item').each(function(){

            $(this).find('.down').removeClass('disabled');

        });



        $('.element_item:last-child').each(function(){

            $(this).find('.down').addClass('disabled');

        });





        ctr++;

    } else {

        // bootbox.alert('<b>Fill out required fields.</b>', function() {}).off("shown.bs.modal");

    }

});





$(document).on('click','#add_sub_activity', function(){

    // alert('a')

    var ctr = $(this).attr('data-cnt');

    var id = $(this).attr('data-id');

    var fcnt = parseInt($(this).attr('data-cnt'))+1;

    var fctr = parseInt($(this).attr('data-ctr'))+1;

    var error = 0;

    if(fcnt != 1){

        $('.subact_'+id).each(function(){

            if($(this).val() == ''){

                $(this).css('border-color', 'red');

                $(this).next().show();

                error++;

            }else{

                $(this).css('border-color', '#ccc');

                $(this).next().hide();

            }

        });

    }

    if(error == 0){

        htm = '';

        htm += '<div class="col-md-12 subact_div subActivity_' + fctr+ ' insert_ pad-0" style="margin-top:5px;">';

        htm += '<div class="col-md-2 subtxt" style="text-align:right;">Sub-activity ' + fcnt + '</div>';

        htm += '<div class="col-md-4"><input type="text" id="subactivity_' + fctr + '" data-subactivity="' + fctr + '" data-id="' + fctr + '" class="subact_'+fctr+' subact_val form-control"><span class = "er-msg">This should not be empty *</span></div>';

        htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default" data-id="' + fctr + '" data-cnt="'+fcnt+'" data-activity="'+id+'" id="remove_sub_activity" title="Remove Sub-activity"><i class="glyphicon glyphicon-minus" ></i></button></div>';

        htm += '</div>';

        $('.activity_'+id).append(htm);

        $(this).attr('data-cnt', parseInt(ctr)+1);

        $(this).attr('data-ctr', fctr);

    }

});





$(document).on('click', '#add_activity', function() {


    var htm = '';

    var id = $(this).attr('data-id');

    var ctr = $(this).attr('data-cnt');

    var error = 0;

    if(ctr != 1){

        $('.act_val').each(function(){

            if($(this).val() == ''){

                $(this).css('border-color', 'red');

                $(this).next().show();

                error++;

            }else{

                $(this).css('border-color', '#ccc');

                $(this).next().hide();

            }

        });

    }

   

    if (error == 0) {

        htm += '<div class="col-md-12 activity_' + id + ' insert_ activity_div pad-5">';

        htm += '<div class="col-md-2">Activity ' + id + '</div>';

        htm += '<div class="col-md-4"><input type="text" id="activity_' + id + '" data-activity="' + id + '" data-id="' + id + '" class="act_1 act_val form-control"><span class = "er-msg">This should not be empty *</span></div>';

        htm += '<div class="col-md-1 pad-0"><button type="submit" class="btn btn-default" data-id="' + id + '" data-cnt="0" id="add_sub_activity" title="Add Sub-activity"><i class="glyphicon glyphicon-plus " ></i></button></div>';

        htm += '</div>';

        $('.activity_container').append(htm);

        $(this).attr('data-cnt', parseInt(ctr)+1);

        $(this).attr('data-id', parseInt(id)+1);

    } 

});



$('.save_template').click(function(){

    var status = $(this).attr('data-status');

    var error = 0;

    $('.inputcss').each(function(){

        if($(this).val() == ''){

            $(this).css('border-color', 'red');

            $(this).next().show();

            error++;

        }else{

            $(this).css('border-color', '#ccc');

            $(this).next().hide();

        }

    });



    $('.elem_val').each(function(){

        var elem_id = $(this).attr('data-elem');

        if($(this).val() != ''){

            $('.ques_'+elem_id).each(function(){

                if($(this).val() == ''){

                    $(this).css('border-color', 'red');

                    $(this).next('.er-msg').show();

                    error++;

                }else{

                    $(this).css('border-color', '#ccc');

                    $(this).next('.er-msg').hide();

                }

            });

            $(this).css('border-color', '#ccc');

            $(this).next().hide();

        }else{

            $(this).css('border-color', 'red');

            $(this).next().show();

            error++;

        }

    });



     $('.act_val').each(function(){

        var act_id = $(this).attr('data-id');

        if($(this).val() != ''){

            $('.subact_'+act_id).each(function(){

                if($(this).val() == ''){

                    $(this).css('border-color', 'red');

                    $(this).next('.er-msg').show();

                    error++;

                }else{

                    $(this).css('border-color', '#ccc');

                    $(this).next('.er-msg').hide();

                }

            });

            $(this).css('border-color', '#ccc');

            $(this).next().hide();

        }else{

            $(this).css('border-color', 'red');

            $(this).next().show();

            error++;

        }

    });



    if(error == 0){

        // UPDATE Template name

        var classification_id = $('#classification').val();

        var standard_id = $('#standard_reference').val();

        var version = $('#version').val();

        $.ajax({

            type: 'post',

            url: "<?=base_url()?>/Manage_template/update_template",

            data:{version:version,id:id,classification_id:classification_id,standard_id:standard_id,status:status,table:'tbl_template',field:'template_id'}

        }).done(function(data){

        });



        // Update Element

        $('.elem_val').each(function(){

            if($(this).parents('.elem_div').hasClass('update_element') == true){

               var element_id = $(this).attr('data-id');

               var element_name = $(this).val();

               var order = $(this).parents('.element_item').find('.up').attr('order');

               var template_id = id;

               var elem_ctr = $(this).attr('data-elem');

               $.ajax({

                    type: 'post',

                    url: "<?=base_url()?>/Manage_template/update_element",

                    data:{element_id:element_id,element_name:element_name,table:'tbl_elements',field:'element_id',order:order}

               }).done(function(data){

                    generate_json(template_id);

                    save_questions(element_id,template_id,elem_ctr);

               });

            }else 

            if($(this).parents('.elem_div').hasClass('insert_') == true){

                var element_name = $(this).val();

                var elem_ctr = $(this).attr('data-elem');

                var template_id = id;

                var order = $(this).parents('.element_item').find('.up').attr('order');



                $.ajax({

                    type: 'post',

                    url: "<?=base_url()?>/Manage_template/save_element",

                    data:{element_name:element_name,order:order}

                }).done(function(element_id1){

                    generate_json(template_id);

                    save_questions(element_id1,template_id,elem_ctr);

                });

            }else 

            if($(this).parents('.elem_div').hasClass('removed') == true){

                var element_name = $(this).val();

                var elem_ctr = $(this).attr('data-elem');

                var template_id = id;



                $.ajax({

                    type: 'post',

                    url: "<?=base_url()?>/Manage_template/delete_data",

                    data:{id:elem_ctr,field:'element_id',table:'tbl_questions'}

                }).done(function(element_id){

                    generate_json(template_id);

                    // save_questions(element_id,template_id,elem_ctr);

                });

            }

        });



        function save_questions(element_id1,template_id,elem_ctr){

            $('#element_question_'+elem_ctr+' .question_div').each(function(){

                if($(this).hasClass('insert_question') == true){

                    var question = $(this).find('.ques_val').val();

                    var default_yes = $(this).find('.default_yes_val').val();

                    // var mandatory = $(this).find('input[name="mandatory"]:checked').val();

                    var order =  $(this).find('.up_q').attr('order');

                    var template_id = id;

                    $.ajax({

                        type: 'post',

                        url: "<?=base_url()?>/Manage_template/save_questions",

                        // data:{order:order,element_id:element_id1,template_id:template_id,question:question,default_yes:default_yes,mandatory:mandatory}
                         data:{order:order,element_id:element_id1,template_id:template_id,question:question,default_yes:default_yes}

                    }).done(function(data){

                        generate_json(template_id);

                        $('#success').val('success');

                        update_config();

                    });

                }else 

                if($(this).hasClass('update_question') == true){

                    var question_id = $(this).find('.ques_val').attr('data-id');

                    var element_id = $(this).find('.ques_val').attr('data-elem');

                    var question = $(this).find('.ques_val').val();

                    var default_yes = $(this).find('.default_yes_val').val();

                    var mandatory = $(this).find('input[name="mandatory"]:checked').val();

                    var template_id = id;

                    var order =  $(this).find('.up_q').attr('order');

                    $.ajax({

                        type: 'post',

                        url: "<?=base_url()?>/Manage_template/update_question",

                        // data:{order:order,question_id:question_id,question:question,default_yes:default_yes,mandatory:mandatory,table:'tbl_questions',field:'question_id'}
                        data:{order:order,question_id:question_id,question:question,default_yes:default_yes,table:'tbl_questions',field:'question_id'}

                    }).done(function(data){

                        generate_json(template_id);

                    });

                }else 

                if($(this).hasClass('removed') == true){

                    var question_id = $(this).find('.ques_val').attr('data-id');

                    var template_id = id;

                    $.ajax({

                        type: 'post',

                        url: "<?=base_url()?>/Manage_template/delete_data",

                        data:{id:question_id,table:'tbl_questions',field:'question_id'}

                    }).done(function(data){

                        generate_json(template_id);

                        $('#success').val('success');

                    });

                }

            });

        }

        

        // Update Activity  

        $('.activity_div').each(function(){

            if($(this).hasClass('update_') == true){

                var activity_id = $(this).find('.act_val').attr('data-id');

                var activity_name = $(this).find('.act_val').val();

                var act_ctr = $(this).find('.act_val').attr('data-id');

                var template_id = id;

                $.ajax({

                    type: 'post',

                    url: "<?=base_url()?>/Manage_template/update_activity",

                    data:{activity_id:activity_id,activity_name:activity_name,table:'tbl_activities',field:'activity_id'}

                }).done(function(data){

                    generate_json(template_id);

                    save_subactivity(activity_id,act_ctr);

                });

            }else 

            if($(this).hasClass('insert_') == true){

                var activity_id = $(this).find('.act_val').attr('data-id');

                var activity_name = $(this).find('.act_val').val();

                var act_ctr = $(this).find('.act_val').attr('data-id');

                var template_id = id;

                $.ajax({

                    type: 'post',

                    url: "<?=base_url()?>/Manage_template/save_activity",

                    data:{activity_name:activity_name,template_id:template_id}

                }).done(function(data){

                    generate_json(template_id);

                    save_subactivity(activity_id,act_ctr);

                });

            }

        });



        function save_subactivity(activity_id,act_ctr){



            $('.activity_'+activity_id+' .subact_div').each(function(x,index){



                if($(this).hasClass('update_') == true){

                    var sub_item_id = $(this).find('.subact_'+act_ctr).attr('data-id');

                    var sub_item_name = $(this).find('.subact_'+act_ctr).val();

                    

                    $.ajax({

                        type: 'post',

                        url: "<?=base_url()?>/Manage_template/update_subactivity",

                        data:{sub_item_id:sub_item_id,sub_item_name:sub_item_name,table:'tbl_sub_activities',field:'sub_item_id'}

                    }).done(function(data){ 

                    generate_json(template_id);                      

                    });

                }else 

                if($(this).hasClass('insert_') == true){

                    var sub_item_name = $(this).find('.subact_val').val();

                    $.ajax({

                        type: 'post',

                        url: "<?=base_url()?>/Manage_template/save_subactivity",

                        data:{activity_id:activity_id,subActivity:sub_item_name}

                    }).done(function(data){

                        generate_json(template_id);

                    });

                }else 

                if($(this).hasClass('removed') == true){

                    var sub_item_id = $(this).find('.subact_val').attr('data-id');

                    // alert(sub_item_id)                  

                    $.ajax({

                        type: 'post',

                        url: "<?=base_url()?>/Manage_template/delete_data",

                        data:{id:sub_item_id,table:'tbl_sub_activities',field:'sub_item_id'}

                    }).done(function(data){

                    generate_json(template_id);                       

                    });

                }



            });                

        }



        bootbox.alert("Template was successfully updated", function(){

            location.reload();

        });

    }

});



$(document).on('click','#remove_element', function(){

    var id = $(this).attr('data-id');

    var ctr = 1;

    var dialog = bootbox.confirm({
          message: "Are you sure you want to remove this element?<br> Note: All items under it will also be erased.",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
            if(result == true){

                    $('#add_element').attr('data-id',parseInt($('#add_element').attr('data-id'))-1);

                    

                    if($('.element_'+id).hasClass('update_element') == false){

                        $('.element_'+id).parents('.element_item').remove();

                    }else{

                        $('.element_'+id).removeClass('update_element').addClass('removed');

                        $('.element_'+id).parents('.element_item').addClass('hidden');

                    }

                    // $('#element_question_'+id).remove();



                    $('.elem_div').each(function(){

                        if($(this).hasClass('removed') == false){

                            $(this).find('.el_text').text('Element '+ctr);

                            ctr++;

                        }

                    });

                }
          }
        });



});





$(document).on('click','.down', function(){

    var $parent = $(this).parents(".element_item");

    if($parent.hasClass('hidden') == false){

        var current_order = $(this).attr('order');

        var prev_order = $parent.next().find('.down').attr('order');

        $(this).attr('order',prev_order);

        $(this).siblings('.up').attr('order',prev_order);



        $parent.next().find('.down').attr('order',current_order);

        $parent.next().find('.up').attr('order',current_order);



        // $parent.find('.el_text').text('Element ' + prev_order);

        // $parent.prev('.el_text').text('Element ' + prev_order);

        $parent.insertAfter($parent.next()); 



        checkorder(current_order);

    }

    

});



$(document).on('click','.up', function(){  

    $('.element_item').each(function(){

        $(this).find('.down').removeClass('disabled');

    });



    var $parent = $(this).parents(".element_item");



        var current_order = $(this).attr('order');

        var prev_order = $parent.prev().find('.up').attr('order');



        $(this).attr('order',prev_order);

        $(this).siblings('.down').attr('order',prev_order);



        $parent.prev().find('.down').attr('order',current_order);

        $parent.prev().find('.up').attr('order',current_order);



        $parent.insertBefore($parent.prev()); 



        checkorder(current_order);

});



function checkorder(current_order){

    $('.up').each(function(x,index){

        if($(this).attr('order') == 1){

            $(this).addClass('hidden');

            $(this).parents('.element_item').find('#remove_element').addClass('hidden');

        }else{

            $(this).removeClass('hidden');

            $(this).parents('.element_item').find('#remove_element').removeClass('hidden');

        }

        

    });

    var ctr = 1;

 

    $('.element_item').each(function(){

        $(this).find('.el_text').text('Element '+ toLetters(ctr) +  ctr);

        ctr++;

         

    }); 

    // alert(ctr);

     $('.element_item').each(function(){

            $(this).find('.down').removeClass('disabled');

        });

    

   

    $('.element_item:last-child').each(function(){

            $(this).find('.down').addClass('disabled');

        });

    

}



$(document).on('click','.down_q', function(){

    var $parent = $(this).parents(".question_div");

    if($parent.hasClass('hidden') == false){

        var current_order = $(this).attr('order');

        var prev_order = $parent.next().find('.down_q').attr('order');

        var elem = $(this).parents('.question_div').find('.ques_val').attr('data-elem');

        $(this).attr('order',prev_order);

        $(this).siblings('.up_q').attr('order',prev_order);



        $parent.next().find('.down_q').attr('order',current_order);

        $parent.next().find('.up_q').attr('order',current_order);



        // $parent.find('.el_text').text('Element ' + prev_order);

        // $parent.prev('.el_text').text('Element ' + prev_order);

        $parent.insertAfter($parent.next()); 



        checkorder_q(current_order,elem);

    }

    

});



$(document).on('click','.up_q', function(){  

    $('.question_div').each(function(){

        $(this).find('.down_q').removeClass('disabled');

    });



    var $parent = $(this).parents(".question_div");

        var elem = $(this).parents('.question_div').find('.ques_val').attr('data-elem');

        var current_order = $(this).attr('order');

        var prev_order = $parent.prev().find('.up_q').attr('order');



        $(this).attr('order',prev_order);

        $(this).siblings('.down_q').attr('order',prev_order);



        $parent.prev().find('.down_q').attr('order',current_order);

        $parent.prev().find('.up_q').attr('order',current_order);



        $parent.insertBefore($parent.prev()); 



        checkorder_q(current_order,elem);

});



function checkorder_q(current_order,element){

    $('#element_question_'+element+' .up_q').each(function(x,index){

        if($(this).attr('order') == 1){

            $(this).addClass('hidden');

            var elem = $(this).parents('.question_div_parent').find('#add_question').attr('data-id');

            var ctr = $(this).parents('.question_div_parent').find('#add_question').attr('data-cnt');

            // alert(elem +'-'+ ctr);

            $(this).parents('.question_div').find('.qbtn').html('<button type="submit" data-id= "'+elem+'" data-cnt= "'+ctr+'" id="add_question" element="'+elem+'" class="parent_elem_'+elem+' btn btn-default"><i class="glyphicon glyphicon-plus " ></i></button>');



            // $(this).parents('.question_div').find('.remove').addClass('hidden');

        }else{

            $(this).removeClass('hidden');

            $(this).parents('.question_div').find('.qbtn').html('<button type="submit" class="remove btn btn-default" data-elem="'+element+'"><i class="glyphicon glyphicon-minus "></i></button>');

            // $(this).parents('.question_div').find('.remove').removeClass('hidden');

        }

        

    });

    var ctr = 1;

 

    $('#element_question_'+element+' .question_div').each(function(){

        $(this).find('.qtext').text('Question ' +  ctr);

        ctr++;

         

    }); 

    // alert(ctr);

     $('#element_question_'+element+' .question_div').each(function(){

            $(this).find('.down_q').removeClass('disabled');

        });

    

   

    $('#element_question_'+element+' .question_div:last-child').each(function(){

            $(this).find('.down_q').addClass('disabled');

        });

    

}





function generate_json(template_id){

    $.ajax({

        type: 'post',

        url: "<?=base_url()?>/api/template_list",

    }).done(function(data){

        $.ajax({

            type: 'post',

            url: "<?=base_url()?>/api/template_info/"+template_id,

        }).done(function(data){

          

        });  

    });

}






});


function toLetters(num) {
    var mod = num % 26;
    var pow = num / 26 | 0;
    var out = mod ? String.fromCharCode(64 + mod) : (pow--, 'Z');
    return pow ? toLetters(pow) + out : out;
}


$(document).on('click','.save_final', function(){  

    var table = "tbl_template";
    var order_by = "template_id";
    var id = <?= $id; ?>;

    $.ajax({
      type: 'Post',
      url:'<?=base_url();?>global_controller/inactive_global_update',
      data:{id:id,type:"1", table:table, order_by:order_by},
    }).done( function(data){
        console.log(data);
        $.ajax({
            type: 'post',
            url: "<?=base_url()?>/api/template_info/"+id,
        }).done(function(data){
          
        }); 
                    
        bootbox.alert('<b>Successfully save as fnal. </b>', function() {
            location.reload();
        }).off("shown.bs.modal");
          
    });

});



</script>