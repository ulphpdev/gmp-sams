<?php 
  $table = "tbl_classification";
  $table1 = "tbl_standard_reference";
  $order_by = "template_id";
  $role = $this->session->userdata('sess_role');
?>
<ol class="breadcrumb" style="background-color: #fff;">

    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
    <li class="active">Manage template</li>

</ol>
<div class="panel panel-default">
    <div class="input-group">
      <input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="fname">
      <div class="input-group-btn">
        <button class="btn btn-primary btn-sm" id="btn_search" type="submit">
            <i class="glyphicon glyphicon-search"> Search</i>
        </button>
      </div>
    </div>

   <!--  <div class="panel-body"  style="padding-bottom: 15px;">
      <div class="col-md-3 pad-0" style="margin-bottom: 10px;"><input class="form-control" type="text" id="txt_search" name="fname" placeholder="SEARCH"></div>
      <button class="btn btn-primary"  id="btn_search"><span class = "glyphicon glyphicon-search"></span> SEARCH</button>
    </div> -->

</div>
<div class="btn-navigation menu-btn  col-md-12 pad-0" >
	<button data-type = "Trash" class="inactive btn-min btn btn-default bold" style="display: none;"><span class = "glyphicon glyphicon-trash"></span> TRASH</button>
  <button data-type = "Active" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-ok"></span>Publish</button>
  <button data-type = "Inactive" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-remove-circle"></span>Unpublish</button>
	<button class="add btn-min btn btn-success bold"><span class = "glyphicon glyphicon-plus"></span> ADD TEMPLATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button>
</div>
<div class="row">dsadas
	<div class="col-md-12" id="list_data">
		<div class="table-responsive">
			<table class = "table listdata" style="margin-bottom:0px;">
          <thead>
            <tr>
            <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th>
            <th>Product Type</th>
            <th>Standard/Reference</th>
            <th>Version</th>
            <th>Modified Date</th>
             <th>Status</th>
            <th>Action</th>
          </tr>  
          </thead>
          <tbody>
            
          </tbody>
        
			</table>
		</div>
		<div class="pagination col-md-12"></div>
	</div>
	<div class="col-md-12 content-container" id="form_data">
	</div>
<div class="btn-navigation menu-btn col-md-12" >
	<button class="update btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button> 
	<button class="save btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list cancel btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>
</div>
<script>
$(document).ready(function() {
/* Start of list */
var role = "<?=$role;?>";
  var limit = '10';
  var offset = '1';
  get_list(limit,offset);
  get_pagination(offset,limit);
  $(document).on('click', '.btn-close2', function(){
      get_list(limit,offset);
        get_pagination(offset,limit);
      $('.inactive').hide();
      $('.selectall').attr('checked', false);
        $('#success-modal').modal('hide');    
     });
  $(document).on('change','.page-number', function() {
    var page_number = parseInt($(this).val());
      $('.listdata tbody').html('');
      get_list(limit,page_number);
  });
  $(document).on('click','.next-page', function() {
    var page_number = parseInt($('.page-number').val());
    var next = page_number +1;
    if(page_number!=last()){
      get_list(limit,next);
      $('.page-number').val(next);
    }
  });
  $(document).on('click','.last-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=last()){
      get_list(limit,last());
      $('.page-number').val($('.page-number option:last').val());
    }
  });
  $(document).on('click','.prev-page', function() {
    var page_number = parseInt($('.page-number').val());
    var prev = page_number -1;
    if(page_number!=first()){
      get_list(limit,prev);
      $('.page-number').val(prev);
    }
  });
  $(document).on('click','.first-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=first()){
      get_list(limit,first());
      $('.page-number').val($('.page-number option:first').val());
    }
  });
  function first(){
    return parseInt($('.page-number option:first').val());
  }
  function last(){
    return parseInt($('.page-number option:last').val());
  }
	function get_list(limit,offset){
    $('.delete').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    var table1 = "tbl_standard_reference";
    var txt_search  =  $('#txt_search').val();
    // var status = '';
    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>global_controller/getlist_manage_template',
        data:{limit:limit, offset:offset, table:table, order_by:order_by, table1:table1, txt_search:txt_search},
      }).done( function(data){
      	 
              var obj = JSON.parse(data);
              var htm = '';
          if(obj.length != 0){
            $.each(obj, function(index, row){
                if(row.status == 1){
                var status = "Active";
              } else {
                var status = "Draft";
              }
              var standard = row.standard_name.substr(0,50);
              if(standard.length == 50){
                var standard1 = standard+'...';
              }else{
                var standard1 = standard;
              }
              if(row.status == 3){}
                htm+="<tr>";
                htm+="<td><input class = 'select' data-id = '"+row.template_id+"' type ='checkbox'></td>";
                htm+="<td><d>"+row.classification_name+"</d></td>";
                htm+="<td><d>"+standard1+"</d></td>";
                htm+="<td><d>"+parseFloat(row.version)+"."+row.sub_version+"</d></td>";
                htm+="<td><d>"+row.update_date+"</d></td>";
                htm+="<td><d>"+status+"</d></td>";
                // htm+="<td><d style='color:#3071a9'>"+row.reference_no+"</d></td>";
                // htm+="<td><d style='color:#3071a9'>"+row.validity+"</d></td>";
                htm+="<td><a class='view_template action_list action' data-status='' id='"+row.template_id+"' data-id='"+row.template_id+"' title='Preview Template'><span class='glyphicon glyphicon-eye-open'></span></a>";
                htm+="<a class='email_data action' data-status='' id='"+row.template_id+"'  title='email'> <span class='glyphicon glyphicon-envelope'></span></a>";
                htm+="<a class='edit action_list action' data-status='' id='"+row.template_id+"' data-elem='"+row.element_id+"' title='edit'><span class='glyphicon glyphicon-pencil'></span></a>";
                if(role == '1'){
                  if(row.status > 0){
                    htm+='<a class="archive action_list action" data-status="" id="'+row.template_id+'" title="Archive Template"> <span class="glyphicon glyphicon-briefcase"></span></a>';
                  }
                }
                htm+="<a class='delete_data action_list action' data-status='' id='"+row.template_id+"' title='delete'> <span class='glyphicon glyphicon-trash'></span></a></td>";
                htm+="</tr>";
              }
            });
          }else{
            htm+="<tr><td></td><td colspan='6'>No record found</td></tr>";
          }
          $('.listdata tbody').html('');
          $('.listdata tbody').html(htm);
     }); 
  }
  function get_pagination(page_num,limit){
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    var table1 = "tbl_standard_reference";
    var txt_search  =  $('#txt_search').val();
    $.ajax({
        type: 'Post',
        url:'<?=base_url();?>global_controller/getlist_manage_template_count',
        data:{limit:limit, offset:offset, table:table, order_by:order_by, table1:table, txt_search:txt_search},
      }).done( function(data){
          var cnt = 0;
          var htm = '';
          htm += '<span class = "glyphicon  first-page"> &#60;&#60;First </span> | <span class = "glyphicon  prev-page"> &#60;Previous </span> | ';
          htm += 'Page <select class="page-number"  style="" disabled> ';
          for(var x =1; x<=data; x++){
            htm += "<option value='"+x+"'>"+x+"</option>";
            cnt++;
          }
          htm += '</select> of '+data+' | ';
          htm += '<span class = "glyphicon  next-page"> &#62;Next</span> | <span class = "glyphicon last-page"> Last&#62;&#62; </span>';
          $('.pagination').html('');

          if(data > 1){
          $('.pagination').html(htm);
          }
     }); 
  }
/* End of list */
/* Start of Add */
$(document).on('click', '.add', function(){
$(this).hide();
// $('.save').show();
$('.update').hide();
$('.article_list').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:'', module:'manage_template', type:'add'},
}).done( function(data){
	$('.content-container').html(data);
        $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
        // get_list(limit,offset);
        // get_pagination('1',limit);
});
})
$(document).on('click','.view_template', function(){
var id = $(this).attr('id');
window.open("<?=base_url()?>Export_pdf/preview_template?template_id="+id);
});
$(document).on('click', '.article_list', function(){
$('.article_list').hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
$('#form_data').html('');
get_list(limit,offset);
get_pagination('1',limit);
})
/* End of Add */
/* Start of update */
$(document).on('click', '.edit', function(){
var id = $(this).attr('id');
// $('.update').show();
// $('.add').show();
// $('.save').hide();
// $('.cancel').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:id, module:'manage_template', type:'edit'},
}).done( function(data){
	$('.content-container').html(data);
        $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
        // get_list(limit,offset);
        // get_pagination('1',limit);
});
});
$(document).on('click', '.article_list', function(){
$(this).hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
})
/* End of update */
/* start of delete */
$('.selectall').click(function(){
     if(this.checked) { 
            $('.select').each(function() { 
                this.checked = true;  
                $('.delete').show();
                $('.inactive').show();           
            });
        }else{
            $('.select').each(function() { 
              $('.delete').hide();
                $('.inactive').hide();
                this.checked = false;                 
            });         
        }
});
$(document).on('click', '.select', function(){
    var x = 0;
        $('.select').each(function() {                
                if (this.checked==true) { x++; } 
                if (x > 0 ) {
                   $('.delete').show();
                   $('.inactive').show(); 
                } else {
                $('.delete').hide();
                $('.inactive').hide();
                $('.selectall').attr('checked', true);
              	}
        });
});
$(document).on('click', '.inactive', function(){
    var x = 0;
    var data_type = $(this).attr('data-type');
    $('.select').each(function() {                
          if (this.checked==true) {  x++;   } 
          if (x > 0 ) {
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to "'+data_type+'" selected record? <button data-type = "'+data_type+'" class = "btn-remove"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
          }
      });
});
$(document).on('click', '.delete_data', function(){
    var x = 0;
    var data_type = $(this).attr('id');
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to delete this record? <button data-type = "'+data_type+'" class = "btn-remove-data"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
});
$(document).on('click', '.archive', function(){
    var x = 0;
    var data_type = $(this).attr('id');
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to move this template to Archive? <button data-type = "'+data_type+'" class = "btn-archive-data"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
});
$(document).on('click', '.email_data', function(){
    // var x = 0;
    var template_id = $(this).attr('id');
            // $('.modal-footer .btn-close').hide();
            // $('.msg').html('<p>TEST</p>');
            $('#send_email_template').attr('data-id',template_id);
            $('#email-modal').modal('show');
});
$(document).on('click', '.btn-archive-data', function(){
    var x = 0;
    var table = "tbl_template";
    var order_by = "<?=$order_by;?>";
    var id = $(this).attr('data-type');
    // alert(table+"-----"+order_by+"---"+id);
    var type = '3';     
      $.ajax({
          type: 'Post',
          url:'<?=base_url();?>global_controller/inactive_global_update',
          data:{id:id,type:type, table:table, order_by: order_by},
        }).done( function(data){
          $('#success-modal').modal('hide');
            generate_json(id);
            bootbox.alert('<b>Successfully moved to archive folder.</b>', function() {
              location.reload();
            }).off("shown.bs.modal");
            
          });
     x++;
});
$(document).on('click', '.btn-remove-data', function(){
    var x = 0;
    var table = "tbl_template";
    var order_by = "<?=$order_by;?>";
    var id = $(this).attr('data-type');
    // alert(table+"-----"+order_by+"---"+id);
    var type = '-2';     
      $.ajax({
          type: 'Post',
          url:'<?=base_url();?>global_controller/inactive_global_update',
          data:{id:id,type:type, table:table, order_by: order_by},
        }).done( function(data){
          $('#success-modal').modal('hide');
            generate_json(id);
            get_list(limit,offset);
            get_pagination(offset,limit);
          	// bootbox.alert('<b>Successfully updated.</b>', function() {
           //    location.reload();
           //  }).off("shown.bs.modal");
            
          });
     x++;
});
$(document).on('click', '.btn-remove', function(){
    var x = 0;
    var type = $(this).attr('data-type');
    if (type == 'Trash') {
      var type = '-2';
    }
    if (type == 'Inactive') {
      var type = '0';
    }
    if (type == 'Active') {
      var type = '1';
    }
  
      $('.select').each(function() {                
          if (this.checked==true) {
            
            var table = "tbl_template";
            // var table1 = "<?=$table;?>";
            var order_by = "template_id"//"<?=$order_by;?>";
            var id = $(this).attr('data-id');
                        
              $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/inactive_global_update',
                  data:{id:id,type:type, table:table, order_by: order_by},
                }).done( function(data){
             
                  $('#success-modal').modal('hide');                 
                    generate_json(id);
                    get_list(limit,offset);
                    get_pagination(offset,limit);
                  //   bootbox.alert('<b>Successfully updated.</b>', function() {
                  //   location.reload();
                  // }).off("shown.bs.modal");
                  });
             x++;
          } 
        
      });
});
/* End of delete */
$(document).on('click', '#btn_search', function(){
var txt_search = $('#txt_search').val();
 get_list(limit,offset);
  get_pagination(offset,limit);
    // $.ajax({
    // url: '<?php echo base_url(); ?>Template_add/search',
    // type: 'POST',
    //   data: {
    //   'txt_search': txt_search,
    //   },
    // success: function(data) {
    // console.log(data);
    // }
    // });
})
function generate_json(template_id){
        $.ajax({
            type: 'post',
            url: "<?=base_url()?>/api/template_list",
        }).done(function(data){
            // $.ajax({
            //     type: 'post',
            //     url: "<?=base_url()?>/api/template_info/"+template_id,
            // }).done(function(data){
              
            // });  
        });
    }
});
// SEARCH
</script>