  <ol class="breadcrumb" style="background-color: #fff;">
      <li><a href="http://sams.ecomqa.com/"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>
      <li class="active">Archive</li>
      <li class="active">Audit Report</li>
  </ol>
  <div class="row">
    <div class="col-md-12" id="list_data">
        <div class="table-responsive">

            <!-- LIST -->
            <table class = "table listdata table-striped" style="margin-bottom:0px;">
                <thead>
                    <tr>
                        <th>Audit Report Number</th>
                        <th>Audited Site</th>
                        <th>Auditor</th>
                        <th>Audit Date</th>
                        <th>Version</th>
                        <th style="text-align: right;">Actions</th>
                    </tr>  
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <!-- PAGINATION -->
        <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
    </div>
</div>

  <div id="view-div-panel" class="rows div-view-audit-report" hidden style="margin-top: 30px;" >
    <div class="panel panel-default">
      <div class="panel-body"  style="padding-bottom: 17px;">
        <div class="view_report_content"></div>
      </div>
    </div>
  </div>
<div id="generate_PDF_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Generate Report</h4>
          </div>
          <div class="modal-body">
            <input class="pdf-picker-id" hidden />
            <input class="pdf-picker-no" hidden />
            <select class="form-control pdf-picker">
              <option value="audit_report">AUDIT REPORT</option>
              <option value="executive_summary">EXECUTIVE SUMMARY</option>
              <option value="annexure_pdf">ANNEXURE - GMP AUDIT DETAILS [.PDF]</option>
              <option value="annexure_doc">ANNEXURE - GMP AUDIT DETAILS [.DOC]</option>
              <option value="raw_data">RAW DATA REPORT</option>
              <option value="approval_history">APPROVAL HISTORY</option>
            </select>
          </div>
          <div class="modal-footer"> 
            <button type="button" class="btn btn-primary btn-generate-pdf">Generate</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>
    </div>
</div>
<script>
  var limit = 10;
  var offset = 0;
  var query = "tbl_report_listing.status = -1";
  $(document).ready(function() {
    get_list("tbl_report_listing.status = -1", limit, offset);
    get_pagination("tbl_report_listing.status = -1");
  });
  function get_list(query, limit, offset){
    var audit_date = "";
    var audit_month = "";
    var audit_year = "";
    $('.div-view-audit-report').hide();
    isLoading(true);
    var data = {'limit':limit, 'offset':offset, 'query':query};
    aJax.post(
       "<?=base_url('global_controller/getlist_global_archive');?>",
        {
            'limit':limit, 
            'offset':offset, 
            'query':query, 
        },
      function(result){
          var data = isJson(result);
          var table_body = new TBody();
          if(data.length!=0){
            $.each(data, function(index, row){
              table_body.td(row.report_no);
              table_body.td(row.site_name);
              table_body.td(row.auditor_name);
              table_body.td(row.audit_date_formatted);
              table_body.td(row.version);
              var list_data = new UList();
              list_data.set_liclass("li-action");
              list_data.set_ulclass("ul-action");
              list_data.li("<a href='#' id='"+row.report_id+"' id-number='"+row.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Generate Report' class='report-generate action'><span class='glyphicon glyphicon-save-file'></span></a>");
              // list_data.li("<a href='#' id='"+row.Report_ID+"'  id-number='"+row.Report_No+"' data-toggle='tooltip' data-placement='bottom' title='View' class='report-view action'><span class='glyphicon glyphicon-eye-open'></span></a>");
              list_data.li("<a href='#' id='"+row.report_id+"' id-number='"+row.report_no+"' data-toggle='tooltip' data-placement='bottom' title='Trash' class='report-trash action'><span class='glyphicon glyphicon-trash'></span></a>");
              // list_data.li("<a href='<?= base_url("generate/approval_history");?>?report_id="+row.Report_ID+"' target='_blank' data-toggle='tooltip' data-placement='bottom' title='Generate Approval History' class='action'><span class='glyphicon glyphicon-eye-open'></span></a>"); 
              table_body.td(list_data.set());
              table_body.set();
            });
          } else {
            table_body.td_norecord(5);
            table_body.set();
          }
          table_body.append(".listdata tbody");
          isLoading(false);
      }
    );
  }
  function get_pagination(query){
    aJax.post(
      "<?=base_url('c_archive/get_pagination');?>",
      {
        'query':query
      },
      function(data){
        var no_of_page = Math.ceil(data / limit);
        var htm = "";
        for(var x =0; x<=no_of_page; x++){
          htm += "<option value='" + (x + 1) + "'>" + (x + 1) + "</option>";
        }
        $('.pager_number').html('');
        $('.pager_number').html(htm);
        if(no_of_page < 2){
          $('.pager_div').hide();
        } else {
          $('.pager_div').show();
        }
      }
    );
  }
  $(document).on('change','.pager_number', function() {
      var page_number = parseInt($(this).val());
      var page = (page_number - 1) * limit;
      get_list(query, limit, page);
  });
  $(document).on('click','.first-page', function() {
    var page_number = parseInt($('.page_number').val());
    if(page_number!=first()){
      var page = (first() - 1) * limit;
      get_list(query, limit, page);
        $('.pager_number').val($('.pager_number option:first').val());
    }
  });
  $(document).on('click','.prev-page', function() {
      var page_number = parseInt($('.pager_number').val());
      if(page_number!=first()){
        var next = page_number - 2;
        console.log(next * limit);
        get_list(query, limit, next * limit);
        $('.pager_number').val(next + 1);
    }
  });
  $(document).on('click','.next-page', function() {
      var page_number = parseInt($('.pager_number').val());
      if(page_number!=last()){
        var next = page_number + 1;
        get_list(query, limit, (next - 1) * limit);
        $('.pager_number').val(next);
    }
  });
  $(document).on('click','.last-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=last()){
      var page = (last() - 1) * limit;
      get_list(query, limit, page);
        $('.pager_number').val($('.pager_number option:last').val());
    }
  });
  function first(){
     return parseInt($('.pager_number option:first').val());
  }
  function last(){
     return parseInt($('.pager_number option:last').val());
  }
  ////////////////////////////////////////
  $(document).on('click','.report-view', function($e) {
    $e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
    type: 'Post',
    url:'<?=base_url();?>c_auditreport/audit_report_view',
    data:{id:id},
    }).done( function(data){
        $('.view_report_content').html(data);
        $('.div-view-audit-report').show();
        $('html, body').animate({
            scrollTop: $("#view-div-panel").offset().top
        }, 1000);
    })
  }) ;
  $(document).on('click','.report-generate', function($e) {
    $e.preventDefault();
    var id = $(this).attr('id');
    var report_no = $(this).attr('id-number');
    $('.pdf-picker-id').val(id);
    $('.pdf-picker-no').val(report_no);
    $("#generate_PDF_modal").modal({ show: true, backdrop: "static"});
  }) ;
  
  $(document).on('click','.btn-generate-pdf', function($e) {
    $e.preventDefault();
    var id = $('.pdf-picker-id').val();
    var report_no = $('.pdf-picker-no').val();
    var link = $('.pdf-picker').val();
    var text = $('.pdf-picker option:selected').text()
    insert_audit_trail("Generate " + report_no + " - " + text);
    window.open("<?=base_url('generate')?>/" + link + '?report_id=' + id + "&action=I");
  });

$(document).on('click','.report-trash', function($e) {
    $e.preventDefault();
    var id = $(this).attr('id');
    var report_no = $(this).attr('id-number');
    var dialog = bootbox.confirm({
          message: "Are you sure you want to delete this record?",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
            if(result){
            $.ajax({
                    type: 'Post',
                    url:'<?=base_url();?>c_auditreport/audit_report_status_update',
                    data:{id:id, status:"-2"},
                    }).done( function(data){
                        insert_audit_trail("Delete Record " + report_no);
                        dialog.modal('hide');
                        location.reload();
                    });
          }          
          }
      });
  }) ;
</script>