<div class="report_view">
</div>
<script type="text/javascript">
$(document).ready(function(){
	var id = <?= $_GET['report_id'];?>;
	$.ajax({
	type: 'Post',
	url:'<?=base_url();?>c_auditreport/audit_report_view',
	data:{id:id, div : 'remarks-approver'},
	}).done( function(data){
	  	$('.report_view').html(data);
	})
});


//submit remarks
$(document).on('click','.remarks_submit', function(e){
 	e.preventDefault();
 	isLoading(true);
   	var remarks = $('#txt_remarks').val();
   	var id = "<?=$_GET['report_id']?>"
   	var reviewer_id = "<?=  $this->session->userdata('userid') ?>";
   	var type = 'approver';
 	status = 5;
 	var newdate = new Date();
    var report_issuance = moment(newdate).format('YYYY-MM-DD hh:mm:ss');
	$.ajax({
	    url:'<?php echo base_url(); ?>Audit_report_link/add_remarks',
	    type: 'POST',
	    data: {
	        'id':id,
	        'status':status,
	        'report_issuance':report_issuance
	        },
		}).done(function(data){

			$.ajax({
	            type: 'Post',
	            url:'<?=base_url("Audit_report_link/add_report_submission_status");?>',
	            data:{report_id:id, remarks : remarks, status:"Approved",position:"Division Head"},
	            }).done(function(data){

	            	$.ajax({
				      	type: 'Post',
				      	url: "http://sams.ecomqa.com/api/audit_report_list",
				      	}).done( function(data){});

	            });

			var data =  {'id':id,'remarks':remarks,'create_date':report_issuance,'reviewer_id':reviewer_id,'type':type};
			$.ajax({
			    url:'<?php echo base_url("Audit_report_link/add_new_remarks_departmenthead"); ?>',
			    type: 'POST',
			    data: data,
			}).done(function(data){});

			$.ajax({
	            type: 'GET',
	            url:'<?=base_url("export_pdf/generate_audit_report");?>?report_id='+id+'&action=export'
	            }).done(function(data){
	            	check_done();
	            });

	        $.ajax({
	            type: 'GET',
	            url:'<?=base_url("export_pdf/generate_annexure");?>?report_id='+id+'&action=export'
	            }).done(function(data){
	            	check_done();
	            });


	        $.ajax({
	            type: 'GET',
	            url:'<?=base_url("export_pdf/generate_annexure_doc");?>?report_id='+id+'&action=export'
	            }).done(function(data){
	            	check_done();
	            });
	            
	        $.ajax({
	            type: 'GET',
	            url:'<?=base_url("export_pdf/generate_raw_data_report");?>?report_id='+id+'&action=export'
	            }).done(function(data){
	            	check_done();
	            });

	        $.ajax({
	            type: 'GET',
	            url:'<?=base_url("export_pdf/generate_executive_summary");?>?report_id='+id+'&action=export'
	            }).done(function(data){
	            	check_done();
	            });


			$.ajax({
	            type: 'Post',
	            url:'<?=base_url("smtp/send/email_to_co_auditor_approve_from_approver");?>',
	            data:{report_id:id, remarks : "Approved : " + remarks},
	            }).done(function(data){
	            	check_done();
	            });

	        aJax.get(
	        	"<?=base_url("api/generate_audit_report_json");?>/" + id,
	        	function()
	        	{
	        		check_done();
	        	}
	        );

	        aJax.post(
	    	"<?=base_url("Audit_report_link/signature_stamp_approved");?>",
	    	{
	    		'report_id' : id,
	    		'field' : "approved_date"
	    	},
	    	function(data){
	    		check_done();
	    	}
	    );
	        

		});
});

var count = 0;
function check_done()
{
	count ++;
	if(count == 7){
		bootbox.alert("Successfully approved!", function(){
			isLoading(false);
			location.reload();
		});
	}

}

$(document).on('click','.reject_submit', function(e){
   e.preventDefault();
   	var remarks = $('#txt_remarks').val();
   	if(remarks == ""){
   		bootbox.alert("Remarks field is required", function(){});
   	} else {						
	   	var id = "<?=$_GET['report_id']?>"
	   	var newdate = new Date();
	   	var create_date = moment(newdate).format('YYYY-MM-DD hh:mm:ss');
	   	var approver_id = "<?=  $this->session->userdata('userid') ?>"
	   	var type = 'approver';
		$.ajax({
		    url:'<?php echo base_url(); ?>Audit_report_link/reject_remarks',
		    type: 'POST',
		    data: {
		        'id':id,
		        'remarks':remarks,
		        'create_date':create_date,
		        'approver_id':approver_id,
		        'type':type
		        },
		    }).done(function(data){


		    	aJax.post(
		    		"<?= base_url("preview_report/reset_stamp");?>",
					{
						'report_id' : id
					},
					function(data){
						console.log("reset stamp");
					}
		    	);

				$.ajax({
		            type: 'Post',
		            url:'<?=base_url("Audit_report_link/add_report_submission_status");?>',
		            data:{report_id:id, remarks : remarks, status:"",position:"Division Head"},
		            }).done(function(data){


	            });

		    	$.ajax({
		            type: 'Post',
		            url:'<?=base_url("smtp/send/email_to_co_auditor_rejected_from_approver");?>',
		            data:{report_id:id, remarks : remarks},
		            }).done(function(data){
		            	console.log(data);
		            	bootbox.alert("Successfully sent an email to Lead Auditor", function(){
							location.reload();
						});
		            });
		    });
		}

});

</script>