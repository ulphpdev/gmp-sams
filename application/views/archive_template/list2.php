<?php 
  $table = "tbl_classification";
  $table1 = "tbl_standard_reference";
  $order_by = "template_id";
  $role = $this->session->userdata('sess_role');
?>
<ol class="breadcrumb" style="background-color: #fff;">

    <li><a href="<?= base_url();?>"><span class="glyphicon glyphicon-home" style="font-size: 16px;"></span></a></li>

    <li>Archive</li>

    <li class="active">Template</li>

</ol>

<div class="input-group col-md-5">
    <input type="text" class="form-control" placeholder="Search" id="txt_search" name="fname">
    <div class="input-group-btn">
        <button class="btn btn-primary" id="btn_search" type="submit">
            <i class="glyphicon glyphicon-search"> </i> Search
        </button>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
	<div class="col-md-12" id="list_data">
		<div class="table-responsive">
			<table class = "table listdata table-striped" style="margin-bottom:0px;">
          <thead>
            <tr>
            <!-- <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th> -->
            <th style="width: 200px;">Product Type</th>
            <th>Standard/Reference</th>
            <th>Version</th>
            <th style="width: 200px;">Date Created</th>
            <th style="width: 200px;">Auditor Name</th>
             <!-- <th>Status</th> -->
            <th style="text-align: right; width: 100px">Actions</th>
          </tr>  
          </thead>
          <tbody>
            
          </tbody>
        
			</table>
      
      </div>
      <div style="text-align: center; margin-top: 20px;" class="form-inline pager_div"></div>
		</div>
	</div>
	<div class="col-md-12 content-container" id="form_data">
	</div>
<div class="btn-navigation menu-btn col-md-12" >
	<button class="update btn-min btn btn-success btn-sm bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button> 
	<button class="save btn-min btn btn-success btn-sm bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list cancel btn-min btn-sm btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>
</div>
<script>
$(document).ready(function() {
/* Start of list */
var role = "<?=$role;?>";
  var limit = '10';
  var offset = '0';
  var query = "";
  get_list("",limit,offset);
  get_pagination("");

  $(document).on('click', '.btn-close2', function(){
      get_list(limit,offset);
      get_pagination("");
      $('.inactive').hide();
      $('.selectall').attr('checked', false);
      $('#success-modal').modal('hide');    
  });

  $(document).on('change','.pager_number', function() {
      var page_number = parseInt($(this).val());
      var page = (page_number - 1) * limit;
      get_list(query, limit, page);
  });


  $(document).on('click','.first-page', function() {
    var page_number = parseInt($('.page_number').val());
    if(page_number!=first()){
      var page = (first() - 1) * limit;
      get_list(query, limit, page);
        $('.pager_number').val($('.pager_number option:first').val());
    }
  });


  $(document).on('click','.prev-page', function() {
      var page_number = parseInt($('.pager_number').val());
      if(page_number!=first()){
        var next = page_number - 2;
        console.log(next * limit);
        get_list(query, limit, next * limit);
        $('.pager_number').val(next + 1);
    }
  });


  $(document).on('click','.next-page', function() {
      var page_number = parseInt($('.pager_number').val());
      if(page_number!=last()){
        var next = page_number + 1;
        get_list(query, limit, (next - 1) * limit);
        $('.pager_number').val(next);
    }
  });


  $(document).on('click','.last-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=last()){
      var page = (last() - 1) * limit;
      get_list(query, limit, page);
        $('.pager_number').val($('.pager_number option:last').val());
    }
  });


  function first(){
     return parseInt($('.pager_number option:first').val());
  }

  function last(){
     return parseInt($('.pager_number option:last').val());
  }

  $(document).on('click', '#btn_search', function(){
    var txt_search = $('#txt_search').val();
    query = "status = 3 AND (classification_name LIKE '%" + txt_search + "%' OR standard_name LIKE '%" + txt_search + "%') ";
    get_list(query,limit,offset)
    get_pagination(offset,limit);
  });

	function get_list(query,limit,offset){
    isLoading(true);
    $('.delete').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);   
    aJax.post(
        "<?=base_url('Archive2/getlist_archive_template2');?>",
        {
            'limit':limit, 
            'offset':offset, 
            'query':query
        },
        function(result){
            var obj = JSON.parse(result);
            var table_body = new TBody();

            console.log(obj.length);
            if(obj.length!=0){
                $.each(obj, function(index, row){
                  table_body.td(row.classification_name);
                  table_body.td(row.standard_name);
                  table_body.td(row.version);
                  table_body.td(moment(row.create_date).format("LLL"));
                  table_body.td(row.auditor_name);

                  var list_data = new UList();
                  list_data.set_liclass("li-action");
                  list_data.set_ulclass("ul-action");

                  list_data.li("<a class='trash_template action_list action' data-status='' id='"+row.template_id+"' data-id='"+row.template_id+"' title='Preview Template'><span class='glyphicon glyphicon-trash'></span></a>");
                  list_data.li("<a class='view_template action_list action' data-status='' id='"+row.template_id+"' data-id='"+row.template_id+"' title='Preview Template'><span class='glyphicon glyphicon-save-file'></span></a>");

                  table_body.td(list_data.set());
                  table_body.set();
                });
            } else {
              table_body.td_norecord(7);
              table_body.set();
            }
            table_body.append(".listdata tbody");
            isLoading(false);
        }
    );
  }


  function get_pagination(query){
    aJax.post(
        "<?=base_url("global_controller/getlist_pagination");?>",
        {
            'query':query, 
            'table': 'tbl_template', 
            'status':'status = -1'
        },
        function(result){
            var no_of_page = Math.ceil(result / limit);
            var pagination = new Pagination(); //please check helper.js for this function
            pagination.set_total_page(no_of_page);
            pagination.set('.pager_div');
        }
    );
  }


})

$(document).on('click','.view_template', function(){
  var id = $(this).attr('id');
  insert_audit_trail("Generate Template " + id);
  window.open("<?=base_url()?>generate/preview_template?template_id="+id);
});




function generate_json(template_id){
    $.ajax({
        type: 'post',
        url: "<?=base_url()?>/api/template_list",
    }).done(function(data){
    });
}



$(document).on('click', '.trash_template', function(){
  var id = $(this).attr('data-id');
  console.log(id);
    var dialog = bootbox.confirm({
          message: "Are you sure you want to delete this record?",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
            if(result){
                var table = "tbl_template";
                var order_by = "template_id"
                

                aJax.post(
                    "<?=base_url('manage_template/update_status');?>",
                    {
                        template_id:id,
                        status:'-2'
                    },
                    function(result){       
                        bootbox.alert('<b>Successfully Deleted.</b>', function() {
                          insert_audit_trail("Delete Record" + id);
                          location.reload();
                        }).off("shown.bs.modal");
                    }
                );
            }          
          }
      });
});

// SEARCH
</script>