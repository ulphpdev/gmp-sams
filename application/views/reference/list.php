<?php 
  $table = "tbl_references";
  $order_by = "create_date";
?>

<div class="col-md-12 sub_content">
<!--   <span class="glyphicon glyphicon-home"></span> <span class="sub_content_title">Data Maintenance > Standard/Reference </span> -->
  <span class="glyphicon glyphicon-home"></span> <span class="sub_content_title">Data Maintenance > License(s)/Accreditation(s)/Certification(s) </span>
</div>

<div class="col-md-4">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="Search" id="txt_search" placeholder="">
    <span class="er-msg file-err">This field is required</span>
    <div class="input-group-btn">
      <button class="btn btn-primary" id="btn_search" type="submit">
          <i class="glyphicon glyphicon-search"> </i> 
          Search
      </button>
    </div>
  </div>
</div>


<div class="btn-navigation menu-btn col-md-12 pad-0" >
	<button data-type = "Trash" class="inactive btn-min btn btn-default bold" style="display: none;"><span class = "glyphicon glyphicon-trash"></span> TRASH</button> 
  <button data-type = "Active" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-ok"></span>Publish</button>
  <button data-type = "Inactive" class="inactive btn-min btn btn-default bold"><span class = "glyphicon glyphicon-remove-circle"></span>Unpublish</button>
	<button class="add btn-min btn btn-success bold"><span class = "glyphicon glyphicon-plus"></span> ADD REFERENCE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CLOSE</button>
</div>
<div class="row">
	<div class="col-md-12" id="list_data">
		<div class="table-responsive">
			<table class = "table listdata" style="margin-bottom:0px;">
          <thead>
            <tr>
            <th style = "width:10px;"><input class = "selectall" type = "checkbox"></th>
            <th>Name of Site</th>
            <th>Licenses & Certification</th>
            <th>Issues</th>
           <th>Certificate Number</th>
            <th>Validity</th>
             <th>Edit</th>
 
          </tr>  
          </thead>
          <tbody>
            
          </tbody>
        
			</table>
		</div>
		<div class="pagination col-md-12"></div>
	</div>
	<div class="col-md-12 content-container" id="form_data">
	</div>

<div class="btn-navigation menu-btn col-md-12" >
	<button class="update btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button> 
	<button class="save btn-min btn btn-success bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
	<button class="article_list cancel btn-min btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

</div>
<script>

$(document).ready(function() {

/* Start of list */

  var limit = '5';
  var offset = '1';
  get_list(limit,offset);
  get_pagination(offset,limit);

  $(document).on('click', '.btn-close2', function(){
      get_list(limit,offset);
        get_pagination(offset,limit);
      $('.inactive').hide();
      $('.selectall').attr('checked', false);
        $('#success-modal').modal('hide');    
     });
  $(document).on('change','.page-number', function() {
    var page_number = parseInt($(this).val());
      $('.listdata tbody').html('');
      get_list(limit,page_number);
  });
  $(document).on('click','.next-page', function() {
    var page_number = parseInt($('.page-number').val());
    var next = page_number +1;
    if(page_number!=last()){
      get_list(limit,next);
      $('.page-number').val(next);
    }
  });
  $(document).on('click','.last-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=last()){
      get_list(limit,last());
      $('.page-number').val($('.page-number option:last').val());
    }
  });
  $(document).on('click','.prev-page', function() {
    var page_number = parseInt($('.page-number').val());
    var prev = page_number -1;
    if(page_number!=first()){
      get_list(limit,prev);
      $('.page-number').val(prev);
    }
  });
  $(document).on('click','.first-page', function() {
    var page_number = parseInt($('.page-number').val());
    if(page_number!=first()){
      get_list(limit,first());
      $('.page-number').val($('.page-number option:first').val());
    }
  });
  function first(){
    return parseInt($('.page-number option:first').val());
  }
  function last(){
    return parseInt($('.page-number option:last').val());
  }
	function get_list(limit,offset){
    $('.delete').hide();
    $('.inactive').hide();
    $('.selectall').attr('checked', false);
    var table = "<?=$table;?>";
    var order_by = "<?=$order_by;?>";
    var table2 = "tbl_company";

    aJax.post(
          "<?=base_url('global_controller/getlist_reference');?>",
          {limit:limit, offset:offset, table:table, order_by:order_by, table2:table2},
          function(result){
              var obj = JSON.parse(result);
              var table_body = new TBody(); //please check helper.js for this function

              if(obj.length!=0){  

                $.each(obj,function(key, row){

                    table_body.td("<input class = 'select' data-id = '"+row.reference_id+"' type ='checkbox'>");
                    table_body.td(row.name);
                    table_body.td(row.reference_name);
                    table_body.td(row.issuer);
                    table_body.td(row.reference_no);
                    table_body.td(row.validity);

                    var action_button = new UList(); //please check helper.js for this function
                    action_button.set_liclass("li-action");
                    action_button.set_ulclass("ul-action");
                    action_button.li("<a class='edit action_list' data-status='' id='"+row.reference_id+"' title='Edit'><span class='glyphicon glyphicon-pencil'></span></a>");
                    action_button.li("<a class='delete_data action_list' data-status='' id='"+row.reference_id+"' title='Delete'> <span class='glyphicon glyphicon-trash'></span></a>");

                    table_body.td(action_button.set());
                    table_body.set();
                })
                  
              } else {
                table_body.td_norecord(7);
                table_body.set();
              }
              table_body.append('.listdata tbody');
              isLoading(false);
          }
      ); 
  }
  
  function get_pagination(query){
      aJax.post(
          "<?=base_url("global_controller/getlist_pagination");?>",
          {
              'query':query, 
              'table': 'tbl_references', 
              'status':'status = 1 or status = 0'
          },
          function(result){
              var no_of_page = Math.ceil(result / limit);
              var pagination = new Pagination(); //please check helper.js for this function
              pagination.set_total_page(no_of_page);
              pagination.set('.pager_div');
          }
      );
  }

/* End of list */

/* Start of Add */

$(document).on('click', '.add', function(){
$(this).hide();
$('.save').show();
$('.update').hide();
$('.article_list').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:'', module:'reference', type:'add'},
}).done( function(data){
	$('.content-container').html(data);
        $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
        // get_list(limit,offset);
        // get_pagination('1',limit);
});
})

$(document).on('click', '.article_list', function(){
$('.article_list').hide();
$('.save').hide();
$('.update').hide();
$('.add').show();
$('#form_data').html('');
get_list(limit,offset);
get_pagination('1',limit);
})

/* End of Add */

/* Start of update */

$(document).on('click', '.edit', function(){
var id = $(this).attr('id');
$('.update').show();
$('.add').show();
$('.save').hide();
$('.cancel').show();
$.ajax({
    type: 'Post',
    url:'<?=base_url();?>global_controller/action_global',
    data:{id:id, module:'auditor', type:'edit'},
}).done( function(data){
	$('.content-container').html(data);
        $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
        // get_list(limit,offset);
        // get_pagination('1',limit);
});


});

$(document).on('click', '.article_list', function(){
$(this).hide();
$('.save').hide();
$('.update').hide();
$('.add').show();

})

/* End of update */

/* start of delete */

$('.selectall').click(function(){
     if(this.checked) { 
            $('.select').each(function() { 
                this.checked = true;  
                $('.delete').show();
                $('.inactive').show();           
            });
        }else{
            $('.select').each(function() { 
              $('.delete').hide();
                $('.inactive').hide();
                this.checked = false;                 
            });         
        }
});

$(document).on('click', '.select', function(){
    var x = 0;
        $('.select').each(function() {                
                if (this.checked==true) { x++; } 
                if (x > 0 ) {
                   $('.delete').show();
                   $('.inactive').show(); 
                } else {
                $('.delete').hide();
                $('.inactive').hide();
                $('.selectall').attr('checked', true);
              	}
        });
});

$(document).on('click', '.inactive', function(){
    var x = 0;
    var data_type = $(this).attr('data-type');
    $('.select').each(function() {                
          if (this.checked==true) {  x++;   } 
          if (x > 0 ) {
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to "'+data_type+'" selected record? <button data-type = "'+data_type+'" class = "btn-remove"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
          }
      });
});

$(document).on('click', '.delete_data', function(){
    var x = 0;
    var data_type = $(this).attr('id');
            $('.modal-footer .btn-close').hide();
            $('.msg').html('<p>Are you sure you want to delete this record? <button data-type = "'+data_type+'" class = "btn-remove-data"> Yes </button><button class = "btn-close2"> No </button></p>');
            $('#success-modal').modal('show');
});

$(document).on('click', '.btn-remove-data', function(){
    var x = 0;
            var table = "<?=$table;?>";
            var order_by = "<?=$order_by;?>";
            var id = $(this).attr('data-type');
            var type = '-2';     
              $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/inactive_global_update',
                  data:{id:id,type:type, table:table, order_by: order_by},
                }).done( function(data){
                  $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
                    get_list(limit,offset);
                    get_pagination('1',limit);
                  });
             x++;

});

$(document).on('click', '.btn-remove', function(){
    var x = 0;
    var type = $(this).attr('data-type');
    if (type == 'Trash') {
      var type = '-2';
    }
    if (type == 'Inactive') {
      var type = '0';
    }
    if (type == 'Active') {
      var type = '1';
    }
  
      $('.select').each(function() {                
          if (this.checked==true) {
            
            var table = "<?=$table;?>";
            var order_by = "<?=$order_by;?>";

            var id = $(this).attr('data-id');
                        
              $.ajax({
                  type: 'Post',
                  url:'<?=base_url();?>global_controller/inactive_global_update',
                  data:{id:id,type:type, table:table, order_by: order_by},
                }).done( function(data){
             
                  $('#success-modal').modal('hide');
                  	// bootbox.alert('<b>Successfully Deleted.</b>', function() {}).off("shown.bs.modal");
                    get_list(limit,offset);
                    get_pagination('1',limit);
                  });
             x++;
          } 
        
      });
});

/* End of delete */

});
</script>