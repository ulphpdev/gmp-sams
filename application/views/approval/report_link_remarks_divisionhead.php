<style>

	@media screen and (max-width: 768px) {
		.btn-approval {
			width: 100%;
			/* margin-bottom: 5px; */
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 10px;
		}

		.bg-white {
			background: #fff;
			padding: 15px;
		}
	}
</style>

<div class="report_view">
</div>
<script type="text/javascript">

$(document).ready(function(){
	load_details();
});

function load_details()
{
	modal.loading(true,"Loading report...");
	var id = <?= $_GET['report_id'];?>;
	aJax.post(
		"<?=base_url();?>c_auditreport/audit_report_view",
		{
			id:id, 
			div : 'remarks-approver'	
		},
		function(data){
			$('.report_view').html(data);
			modal.loading(false);
		}
	);
}

//submit remarks
$(document).on('click','.remarks_submit', function(e){
	e.preventDefault();
	var no_click = 0;
	confirm("Are you sure you want to approve this report?",function(result){
        if(result){
			no_click ++;
			$(".bootbox ").modal("hide");
			if(no_click <= 1){
				modal.loading(true,"Please wait.. Report is being tagged as approved.");
				setTimeout(approved, 2000);
			}
        }
    });
});


function approved()
{
	var remarks = $('#txt_remarks').val();
   	var id = "<?=$_GET['report_id']?>"
   	var create_date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
   	var approver_id = "<?=  $this->session->userdata('userid_approval') ?>"
   	var type = 'approver';
   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'approver_id':approver_id,'type':type};
	
	aJax.post(
		"<?php echo base_url("approval/division_head_approved"); ?>",
		data,
		function(data){
			bootbox.alert("Successfully approved!", function(){
				isLoading(false);
				load_details();
				location.reload();
			});
		}
	);

}


var count = 0;
function check_done(id)
{

	aJax.get("<?= base_url('api/update_listing');?>?report_id=" + id,function(data){

	});

	count ++;
	if(count == 1){
		
    	console.log("update listing");
    	bootbox.alert("Successfully approved!", function(){
			isLoading(false);
			load_details();
		});
	}

}

$(document).on('click','.reject_submit', function(e){
	e.preventDefault();
	var no_click = 0;
	confirm("Are you sure you want to resubmit this report to Lead Auditor?",function(result){
        if(result){
			no_click ++;
			$(".bootbox ").modal("hide");
			if(no_click <= 1){
            	modal.loading(true,"Please wait.. Report is being submitted back to Lead Auditor and/or Co-Auditor(s).");
            	setTimeout(send_back, 2000);
			}
        }
    });
});

function send_back(){
   	var remarks = $('#txt_remarks').val();
   	if(remarks == ""){
   		bootbox.alert("Remarks field is required", function(){
			location.reload();
		});
   	} else {						
	   	var id = "<?=$_GET['report_id']?>"
	   	var create_date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
	   	var approver_id = "<?=  $this->session->userdata('userid_approval') ?>"
	   	var type = 'approver';
	   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'approver_id':approver_id,'type':type};
		
	   	aJax.post(
	   		"<?php echo base_url("approval/division_head_reject"); ?>",
	   		data,
	   		function(data){
	   			bootbox.alert("Remarks is successfully sent to Lead and Co-Auditor(s).", function(){
					location.reload();
				});
	   		}
	   	);
	}
};


var modal = {
	loading : function(isloading, message){
		if(isloading){
			bootbox.dialog({ 
				message: message, 
				closeButton: false 
			});
		} else {
			$('.bootbox').modal('hide');
		}
	}
}

</script>