<style>

	@media screen and (max-width: 768px) {
		.btn-approval {
			width: 100%;
			/* margin-bottom: 5px; */
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 10px;
		}

		.bg-white {
			background: #fff;
			padding: 15px;
		}
	}
</style>

<div class="report_view">
</div>
<script type="text/javascript">
$(document).ready(function(){
	load_details();
});

function load_details()
{
	modal.loading(true,"Loading report...");
	var id = <?= $_GET['report_id'];?>;
	aJax.post(
		"<?=base_url('export/view');?>",
		{
			id:id,
			div : 'remarks-reviewer'
		},
		function(data){
			$('.report_view').html(data);
			modal.loading(false);
		}
	);
}

//submit remarks
$(document).on('click','.remarks_submit', function(e){
	e.preventDefault();
	var no_click = 0;
	confirm("Are you sure you want to submit this report to Division Head?",function(result){
        if(result){
			no_click ++;
			$(".bootbox ").modal("hide");
			if(no_click <= 1){
				modal.loading(true, "Please wait.. Report is being tagged as reviewed and being submitted to Division Head ");
            	setTimeout(send_div, 2000);
			}

        }
    });
});

function send_div(){
   	var remarks = $('#txt_remarks').val();
   	var id = "<?=$_GET['report_id']?>"
   	var create_date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
   	var reviewer_id = "<?=  $this->session->userdata('userid_approval') ?>"
   	var type = 'reviewer';
   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'reviewer_id':reviewer_id,'type':type};

	aJax.post(
		"<?php echo base_url("approval/department_head_approved"); ?>",
		data,
		function(data){
			modal.loading(false);
			bootbox.alert("Successfully sent to Division Head!", function(){
    			update_listing(id);
				load_details();
				update_audit_report_json();
				location.reload();
			});
		}
	);
};

$(document).on('click','.reject_submit', function(e){
	e.preventDefault();
	var no_click = 0;
	confirm("Are you sure you want to resubmit this report to Lead Auditor?",function(result){
        if(result){
			no_click ++;
			$(".bootbox").modal("hide");
			if(no_click <= 1){
				modal.loading(true, "Please wait.. Report is being submitted back to Lead Auditor and/or Co-Auditor(s). ");
            	setTimeout(send_back, 2000);
			}

        }
    });
});

function send_back(){
   	var remarks = $('#txt_remarks').val();

   	if(remarks == ""){
   		bootbox.alert("Remarks field is required", function(){
			location.reload();
		});
   	} else {
	   	var id = "<?=$_GET['report_id']?>"
	   	var create_date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
	   	var reviewer_id = "<?=  $this->session->userdata('userid_approval') ?>"
	   	var type = 'reviewer';
	   	var data =  {'id':id,'remarks':remarks,'create_date':create_date,'reviewer_id':reviewer_id,'type':type};

	   	aJax.post(
	   		"<?php echo base_url("approval/department_head_reject"); ?>",
	   		data,
	   		function(data){
				modal.loading(false);
	   			bootbox.alert("Remarks is successfully sent to Lead and Co-Auditor(s).", function(){
					location.reload();
				});
	   		}
	   	);
	}
};


var modal = {
	loading : function(isloading, message){
		if(isloading){
			bootbox.dialog({
				message: message,
				closeButton: false
			});
		} else {
			$('.bootbox').modal('hide');
		}
	}
}

</script>