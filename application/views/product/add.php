<?php
  $table = "tbl_product";
  $order_by = "product_id";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Add Product
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Name of Site *: </label>
            <div class="col-sm-6">
                <select id="name_site" class="inputcss form-control fullwidth inputs_product"></select>
                <span class = "er-msg">Name of site should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="name" class = "inputcss form-control fullwidth inputs_product" >
                <span class = "er-msg">Name should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">


  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";

  $(document).ready(function(){
    get_company();
  });

  $('.save').off('click').on('click', function() {

    var name_site = $('#name_site').val();
    var name = $('#name').val();
    var type = $('#type').val();

    if(validateFields('.inputs_product') == 0){
      var no_click = 0;
      confirm("Are you sure you want to save this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
              isduplicate("tbl_product", "company_id ="+name_site+" AND product_name = '" + name + "' AND status >= 0", function(result){
                if(result == "true"){
                  $("#name").css('border-color','red');
                  $("#name").next().html("This field should contain a unique value.");
                  $("#name").next().show();
                } else {
                  isLoading(true);
                  $(this).prop('disabled',true);
                  aJax.post(
                    "<?=base_url('product/save_product');?>",
                    {
                      table:"tbl_product",
                      field:field,
                      name_site:name_site,
                      name:name
                    },
                    function(data){
                      isLoading(false);
                      $('.update').prop('disabled',false);
                      updateAPI("product");
                      update_config();
                      insert_audit_trail("Create " + name);
                      bootbox.alert('<b>Record is successfully saved!</b>', function() {
                        location.reload();
                      });
                    }
                  );
                }
              });
          }
        }
      });
    }
  });

  function get_company(){
    $.ajax({
      type: 'Post',
      url:'<?=base_url();?>global_controller/getlist_global_dm',
      data:{table:'tbl_company'},
    }).done( function(data){
      var obj = JSON.parse(data);
      var htm = '';
      htm += '<option value="">--Select--</option>';
      $.each(obj, function(index, row){
          htm += '<option value="'+row.company_id+'">'+row.name+'</option>';
      });
      $('#name_site').html(htm);
    });
  }
</script>