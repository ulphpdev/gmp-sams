<div class="panel panel-default" style="margin-top: 25px;">
    <div class="panel-heading">
        <h4 class="panel-title">
            Report Details
        </h4>
    </div>
    <div class="panel-body analysis_tbody">
    </div>
</div>

<script type="text/javascript">

function checkNAN(data)
{
    if(isNaN(data)){
        return 0;
    } else {
        return data
    }  
}

$(document).ready(function(){
    load_details();
});

function load_details()
{
    var id = "<?= $id; ?>";
    aJax.get("<?= base_url() . 'listing/' . $id . '_analysis';?>.json",function(data){
    var obj = isJson(data);
    var html = "";
    html += "<table class='table table-bordered'>";
    html += "   <tbody>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Audit Report Number</td>";
    html += "           <td>"+obj.report_no+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Site Name</td>";
    html += "           <td>"+obj.name+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Audit Date</td>";
    html += "           <td>"+obj.audit_date+"</td>";
    html += "       </tr>";
    html += "   </tbody>";
    html += "</table>";
    html += "<table  class='table table-bordered'>";
    html += "   <thead>";
    html += "       <tr>";
    html += "           <th colspan='2'>Answers</th>";
    html += "       </tr>";
    html += "   <thead>";
    html += "   <tbody>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Number of Yes</td>";
    html += "           <td>"+obj.yes+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Number of No</td>";
    html += "           <td>"+obj.no+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Number of Not Applicable</td>";
    html += "           <td>"+obj.NA+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Number of Not Covered</td>";
    html += "           <td>"+obj.NC+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <th style='width: 300px;'>Total No. Questions</th>";
    html += "           <th>"+checkNAN(obj.no_question)+"</th>";
    html += "       </tr>";
    html += "   </tbody>";
    html += "</table>";
    html += "<table  class='table table-bordered'>";
    html += "   <thead>";
    html += "       <tr>";
    html += "           <th colspan='2'>Categories</th>";
    html += "       </tr>";
    html += "   <thead>";
    html += "   <tbody>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Number of Minor Observations</td>";
    html += "           <td>"+checkNAN(obj.no_minor)+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Number of Major Observations</td>";
    html += "           <td>"+checkNAN(obj.no_major)+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Number of Critical Observations</td>";
    html += "           <td>"+checkNAN(obj.no_critical)+"</td>";
    html += "       </tr>";
    html += "   </tbody>";
    html += "</table>";

    html += "<table  class='table table-bordered'>";
    html += "   <thead>";
    html += "       <tr>";
    html += "           <th colspan='2'>Product Type</th>";
    html += "       </tr>";
    html += "   <thead>";
    html += "   <tbody>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Limit</td>";
    html += "           <td>"+obj.limit+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Major Unacceptable Disposition</td>";
    html += "           <td>"+checkNAN(obj.major_unacceptable)+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Critical Unacceptable Disposition</td>";
    html += "           <td>"+checkNAN(obj.critical_unacceptable)+"</td>";
    html += "       </tr>";
    html += "   </tbody>";
    html += "</table>";


    html += "<table  class='table table-bordered'>";
    html += "   <thead>";
    html += "       <tr>";
    html += "           <th colspan='2'>Computation</th>";
    html += "       </tr>";
    html += "   <thead>";
    html += "   <tbody>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>No. of Applicable Questions</td>";
    html += "           <td>"+obj.Total_No_of_Applicable_Questions+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>No. of Covered Questions</td>";
    html += "           <td>"+obj.Total_No_of_Covered_Questions+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Factor for Major</td>";
    html += "           <td>"+obj.Factor_for_Major.toFixed(2);+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Factor for Critical</td>";
    html += "           <td>"+obj.Factor_for_Critical.toFixed(2);+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Weight for Major</td>";
    html += "           <td>"+obj.Weight_for_Major.toFixed(2);+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Weight for Critical</td>";
    html += "           <td>"+obj.Weight_for_Critical.toFixed(2);+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <td style='width: 300px;'>Weighted Denominator</td>";
    html += "           <td>"+obj.Weighted_Denominator.toFixed(2);+"</td>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <th style='width: 300px;'>GMP Rating</th>";
    html += "           <th>"+checkNAN(obj.rating) + "%"+"</th>";
    html += "       </tr>";
    html += "       <tr>";
    html += "           <th style='width: 300px;'>% Coverage</th>";
    html += "           <th>"+checkNAN(obj.coverage) + "%"+"</th>";
    html += "       </tr>";
    html += "   </tbody>";
    html += "</table>";

    $('.analysis_tbody').html(html);


        }
    );
}

</script>