

<span class="audit_date"></span>


<script type="text/javascript">
	
	$(document).ready(function() {
		var obj = isJson(<?= $audit_dates; ?>);
		console.log(obj);
		var auditdate = test.audit_date(obj);
		$('.audit_date').html(auditdate);
	});


	var test = {
		audit_date : function(audit_dates){
			var month_format = ["", "January","february","March","April","May","June","July","August","September","October","November","December"];
			var current_month = 0;
			var month = 0;
			var date = "";
			var month_text = "";
			var year = ""
			var month_array = [];
		    $.each(audit_dates, function(index, row){
		    	month = moment(row.audit_date).format('M');
		    	year = moment(row.Date).format('Y');
		    	console.log(current_month + "=" + month);
		    	if(current_month != month){
		    		month_array.push(month);
		    		date = date.trim().substring(0,date.trim().length - 1) + " " + month_format[current_month] + " & " + moment(row.audit_date).format('DD') + ", ";
		    		current_month = month;
		    	} else {
		    		date += moment(row.audit_date).format('DD') + ", ";
		    	}
		    });
		    return date.trim().substring(1,date.trim().length - 1) + " " + month_format[current_month] + " " + year;
		   
		}
	}

</script> 