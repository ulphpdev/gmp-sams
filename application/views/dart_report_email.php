<style type="text/css">
	
	table {
	    border-collapse: collapse;
	    width: 100%;
	    font-size: 14px;
        margin-bottom : 20px;
	}

	table, th, td {
	    border: 1px solid black;
	    padding: 3px;
	}

    .label-head {
        background-color: #4b4b4b;
        color: #fff;
    }

</style>

<?php foreach ($logs as $key => $value) {
    $values = json_decode($value->json);
    $action = $value->action;
    $submission_date = $value->submission_date;
    $report_id = $value->report_id;
}

?>
<div style="width: 100%; min-height: 700px; padding: 25px;" class="bg-white">

	<table>
        <tbody>
            <tr>
                <td style="width: 40%;" class="label-head">Action</td>
                <td><?= $action;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Date</td>
                <td><?= $submission_date;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Report ID</td>
                <td><?= $report_id;?></td>
            </tr>
            <tr>
                <td style="width:40%;" class="label-head">User</td>
                <td><?= $values->co_auditor_name;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Token</td>
                <td><?= $values->token;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Event</td>
                <td><?= $values->cmdEvent;?></td>
            </tr>
            <tr>
                <td style="width: 40%;" class="label-head">Email Address</td>
                <td><?= $values->co_auditor_email;?></td>
            </tr>
        </tbody>
	</table>

</div>
