<?php
  $table = "tbl_company";
  $order_by = "company_id";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Edit Supplier
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Type *: </label>
            <div class="col-sm-6">
                <select id="type" class = "inputcss form-control fullwidth inputs_edit_site">
                    <option value="">--Select--</option>
                    <option value="1">Supplier</option>
                    <option value="2">Manufacturer</option>
                </select>
                <span class = "er-msg">Type should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Company Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="company_name" class = "inputcss form-control fullwidth inputs_edit_site" >
                <span class = "er-msg">Company name should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Country *: </label>
            <div class="col-sm-6">
                <select id="country" class = "inputcss form-control fullwidth inputs_edit_site"></select>
                <span class = "er-msg">Country should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Province *: </label>
            <div class="col-sm-6 province">
                <input class="form-control inputcss inputs_edit_site" name="" placeholder="Province.." disabled="" />
                <span class = "er-msg">Province should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">City *: </label>
            <div class="col-sm-6 city">
                <input class="form-control inputcss inputs_edit_site" name="" placeholder="City.." disabled="">
                <span class = "er-msg">City should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Street *: </label>
            <div class="col-sm-6">
                <input type = "text" id="street" class = "inputcss form-control fullwidth inputs_edit_site" >
                <span class = "er-msg">Street should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Company Background *: </label>
            <div class="col-sm-8">
                <textarea id="background" class="inputcss form-control fullwidth inputs_edit_site" rows="10"></textarea>
                <span class = "er-msg er-msg-email">Company background should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">
var selected_city = "";
var selected_country = "";
var current_name = "";

$(document).ready(function(){
  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";
  var id = "<?= $id; ?>";
  on_load_details();

  function on_load_details(){
    var limit = 1;
    aJax.post(
      "<?=base_url('global_controller/edit_global');?>",
      {
        id:id,
        limit:limit,
        table:table,
        field:field
      },
      function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(index, row){
            current_name = row.name;
            $('#type option[value="'+row.type+'"]').attr('selected','selected');
            $('#company_name').val(row.name);
            $('#street').val(row.address1);
            $('#background').val(row.background);
            get_country(row.country);
            load_province(row.country, row.address3)
            load_city(row.address3, row.address2)
        });
      }
    );
  }

  $('.update').off('click').on('click', function() {
    var type = $('#type').val();
    var name = $('#company_name').val();
    var country = $('#country').val();
    if($('#province option:selected').text() != ""){
      var province = $('#province option:selected').text()
      var province_id = $('#province').val()
    } else {
      var province = $('#province').val()
      var province_id = "0"
    }

    if($('#city option:selected').text() != ""){
      var city = $('#city option:selected').text()
      var city_id = $('#city').val()
    } else {
      var city = $('#city').val()
      var city_id = "0"
    }
    var street = $('#street').val();
    var background = $('#background').val();

    if(validateFields('.inputs_edit_site') == 0){
      var no_click = 0;
      confirm("Are you sure you want to update this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_company", "name = '" + name + "' AND name <> '"+current_name+"' AND status >= 0", function(result){
              if(result == "true"){
                $("#company_name").css('border-color','red');
                $("#company_name").next().html("This field should contain a unique value.");
                $("#company_name").next().show();
              } else {
                isLoading(true);
                aJax.post(
                  "<?=base_url('data_maintenance/update_company');?>",
                  {
                    type:type,
                    name:name,
                    country:country,
                    province: province,
                    province_id: province_id,
                    city:city,
                    city_id:city_id,
                    street:street,
                    background:background,
                    id:id,
                    table:table,
                    field:field
                  },
                  function(data){
                    isLoading(false);
                    $('.update').prop('disabled',false);
                    updateAPI("site");
                    insert_audit_trail("Update " + name);
                    update_config();
                    bootbox.alert('<b>Record is successfully updated!</b>', function() {
                      location.reload();
                    }).off("shown.bs.modal");
                  }
                );
              }
            });
          }
        }
      });
    }
  });

  $(document).on('change', '#country', function() {
    get_province($(this).val());
    get_cities("");
  });

  $(document).on('change', '#province', function() {
    console.log($(this).val());
    if($(this).val() == "xx"){
      var html = "";
        html += '<input class = "inputcss form-control fullwidth inputs_edit_site" id="province" placeholder="Enter Province/Region Name". />';
        html += '<span class = "er-msg">Province should not be empty *</span></div>';
        $('.province').html(html);
        $('#province').focus();
      var html = "";
        html += '<input class = "inputcss form-control fullwidth inputs_edit_site" id="province" placeholder="Enter City Name". />';
        html += '<span class = "er-msg">Province should not be empty *</span></div>';
        $('.city').html(html);
    }
    get_cities($(this).val());
  });

  $(document).on('change', '#city', function() {
    console.log($(this).val());
    if($(this).val() == "xx"){
      var html = "";
        html += '<input class = "inputcss form-control fullwidth inputs_edit_site" id="province" placeholder="Enter City Name". />';
        html += '<span class = "er-msg">Province should not be empty *</span></div>';
        $('.city').html(html);
    }
  });

});


function load_province(country_id, province_name){
  aJax.post(
    "<?= base_url('data_maintenance/load_province');?>",
    {
      province_name: province_name,
      country_id: country_id
    },
    function(data){
      $('.province').html(data);
    }
  );
}

function load_city(province_name, city_name){
  aJax.post(
    "<?= base_url('data_maintenance/load_city');?>",
    {
      province_name: province_name,
      city_name: city_name
    },
    function(data){
      $('.city').html(data);
    }
  );
}


function get_country(selected){
  aJax.post(
    "<?= base_url('data_maintenance/get_country');?>",
    {
      country_id: selected
    },
    function(data){
      $('#country').html(data);
    }
  );
}
function get_province(country_id){
    aJax.post(
      "<?= base_url('data_maintenance/get_province');?>",
      {
        country_id: country_id
      },
      function(data){
        $('.province').html(data);
      }
    );
  }
  function get_cities(province_id){
    aJax.post(
      "<?= base_url('data_maintenance/get_city');?>",
      {
        province_id: province_id
      },
      function(data){
        $('.city').html(data);
      }
    );
  }
</script>