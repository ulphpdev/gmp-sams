<?php
  $table = "tbl_company";
  $order_by = "company_id";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Add Supplier
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Type *: </label>
            <div class="col-sm-6">
                <select id="type" class = "inputcss form-control fullwidth inputs_site">
                    <option value="">--Select--</option>
                    <option value="1">Supplier</option>
                    <option value="2">Manufacturer</option>
                </select>
                <span class = "er-msg">Type should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Company Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="company_name" class = "inputcss form-control fullwidth inputs_site" >
                <span class = "er-msg">Company name should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Country *: </label>
            <div class="col-sm-6">
                <select id="country" class = "inputcss form-control fullwidth inputs_site"></select>
                <span class = "er-msg">Country should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Province *: </label>
            <div class="col-sm-6 province">
                <input class="form-control inputcss inputs_site" name="" placeholder="Province.." disabled="" />
                <span class = "er-msg">Province should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">City *: </label>
            <div class="col-sm-6 city">
                <input class="form-control inputcss inputs_site" name="" placeholder="City.." disabled="">
                <span class = "er-msg">City should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Street *: </label>
            <div class="col-sm-6">
                <input type = "text" id="street" class = "inputcss form-control fullwidth inputs_site" >
                <span class = "er-msg">Street should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Company Background *: </label>
            <div class="col-sm-8">
                <textarea id="background" class="inputcss form-control fullwidth inputs_site" rows="10"></textarea>
                <span class = "er-msg er-msg-email">Company background should not be empty *</span>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="products">
            <div class="form-group">
                <label class="control-label col-sm-2">Product *: </label>
                <div class="col-sm-6">
                    <input class="form-control site_product inputcss" placeholder="Product Name">
                    <span class = "er-msg er-msg-email">This field should not be empty *</span>
                </div>
                <div class="col-sm-2">
                    <div class="btn-group">
                        <button type="submit" style="border: none; padding: 5px;" class="btn btn-default product-add">
                            <i class="glyphicon glyphicon-plus "></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>


<script type="text/javascript">

$(document).ready(function(){
  get_country();
  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";
  var product_cnt = 0;


  $('.save').off('click').on('click', function() {

    if(validateFields('.inputcss') == 0){

      var type = $('#type').val();
      var name = $('#company_name').val();
      var country = $('#country').val();
      if($('#province option:selected').text() != ""){
        var province = $('#province option:selected').text()
        var province_id = $('#province').val()
      } else {
        var province = $('#province').val()
        var province_id = "0"
      }

      if($('#city option:selected').text() != ""){
        var city = $('#city option:selected').text()
        var city_id = $('#city').val()
      } else {
        var city = $('#city').val()
        var city_id = "0"
      }

      var street = $('#street').val();
      var background = $('#background').val();

      var no_click = 0;
      confirm("Are you sure you want to save this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_company", "name = '" + name + "' AND status >= 0", function(result){
              if(result == "true"){
                $("#company_name").css('border-color','red');
                $("#company_name").next().html("This field should contain a unique value.");
                $("#company_name").next().show();
              } else {
                if(check_unique()  == 0){
                  isLoading(true);
                  aJax.post(
                    "<?=base_url('data_maintenance/save_company');?>",
                    {
                      type:type,
                      name:name,
                      country:country,
                      province: province,
                      province_id: province_id,
                      city:city,
                      city_id:city_id,
                      street:street,
                      background:background,
                      table:table,
                      field:field
                    },
                    function(data){
                      isLoading(false);
                      updateAPI("site");
                      insert_audit_trail("Create " + name);
                      update_config();
                      save_site_products(data);
                      bootbox.alert('<b>Record is successfully saved!</b>', function() {
                        location.reload();
                      }).off("shown.bs.modal");
                    }
                  );
                }
              }
            });
          }
        }
      });
    }
  });

  $(document).on('change', '#country', function() {
    get_province($(this).val());
    get_cities("");
  });

  $(document).on('change', '#province', function() {
    console.log($(this).val());
    if($(this).val() == "xx"){
      var html = "";
        html += '<input class = "inputcss form-control fullwidth inputs_site" id="province" placeholder="Enter Province/Region Name". />';
        html += '<span class = "er-msg">Province should not be empty *</span></div>';
        $('.province').html(html);
        $('#province').focus();
      var html = "";
        html += '<input class = "inputcss form-control fullwidth inputs_site" id="province" placeholder="Enter City Name". />';
        html += '<span class = "er-msg">Province should not be empty *</span></div>';
        $('.city').html(html);
    }
    get_cities($(this).val());
  });

  $(document).on('change', '#city', function() {
    console.log($(this).val());
    if($(this).val() == "xx"){
      var html = "";
        html += '<input class = "inputcss form-control fullwidth inputs_site" id="city" placeholder="Enter City Name". />';
        html += '<span class = "er-msg">Province should not be empty *</span></div>';
        $('.city').html(html);
    }
  });

  function get_country(){
    aJax.post(
      "<?= base_url('data_maintenance/get_country');?>",
      {
        country_id: ""
      },
      function(data){
        $('#country').html(data);
      }
    );
  }
  function get_province(country_id){
    aJax.post(
      "<?= base_url('data_maintenance/get_province');?>",
      {
        country_id: country_id
      },
      function(data){
        $('.province').html(data);
      }
    );
  }
  function get_cities(province_id){
    aJax.post(
      "<?= base_url('data_maintenance/get_city');?>",
      {
        province_id: province_id
      },
      function(data){
        $('.city').html(data);
      }
    );
  }

  $(document).on('click', '.product-add', function() {
    product_cnt++;
      var html = "";
      html += '<div class="form-group product_'+product_cnt+'">';
      html += '          <label class="control-label col-sm-2"></label>';
      html += '          <div class="col-sm-6">';
      html += '              <input class="form-control site_product inputcss" placeholder="Product Name">';
      html += '              <span class = "er-msg er-msg-email">This field should not be empty *</span>';
      html += '          </div>';
      html += '          <div class="col-sm-2">';
      html += '              <div class="btn-group">';
      html += '                  <button type="submit" style="border: none; padding: 5px;" class="btn btn-default product-add">';
      html += '                       <i class="glyphicon glyphicon-plus "></i>';
      html += '                  </button>';
      html += '                 <button type="submit" style="border: none; padding: 5px;" data-div="product_'+product_cnt+'" class="btn btn-default product-remove">';
      html += '                     <i class="glyphicon glyphicon-minus "></i>';
      html += '                 </button>';
      html += '              </div>';
      html += '          </div>';
      html += '      </div>';
      $('.products').append(html);
  });

  $(document).on('click', '.product-remove', function() {
    var div = $(this).attr("data-div");
    $("." + div).remove();
  });

  function save_site_products(site_id)
  {
    $('.site_product').each(function(){
      var product = $(this).val();
      aJax.post(
        "<?=base_url('product/save_product');?>",
        {
          table:"tbl_product",
          field:"product_id",
          name_site:site_id,
          name:product
        },
        function(data){
          insert_audit_trail("Create " + product);
        }
      );
    });
  }


  function check_unique()
  {
    var values = {};
    var countUnique = 0;
    var checks = $(".site_product");
    checks.removeClass("error");

    checks.each(function(i, elem)
    {
      if(elem.value in values) {
        $(elem).css('border-color','red');
        $(elem).next().html("Please enter a Unique Value.");
        $(elem).next().show();
        $(values[elem.value]).css('border-color','red');
        $(values[elem.value]).next().html("Please enter a Unique Value.");
        $(values[elem.value]).next().show();
      } else {
        values[elem.value] = elem;
        ++countUnique;
      }
    });

    if(countUnique == checks.size()) {
      return 0;
    } else {
      return 1;
    }
  }


});


</script>