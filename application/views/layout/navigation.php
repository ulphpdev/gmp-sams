<div class="col-md-2 col-sm-12">
  <a href="<?= base_url();?>">
    <img src="<?= base_url();?>asset/img/sams3.png" />
  </a>
</div>
<div class="col-md-10 col-sm-12 ">
	<div class="user_large dropdown pull-right"  style="margin-top: 15px;">
	  <span class="dropdown-toggle btn-user" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
	   <label> Hi  </label> <?php  echo ucwords($this->session->userdata('name')); ?>!  |  
     <a href="#" data-toggle="modal" data-target="#logout_modal" style="color:#fff">Log-out</a>
	  </span>
	</div>
</div>


<!-- Logout Modal-->
<div class="modal fade" id="logout_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary btn-logout" href="#">Logout</a>
      </div>
    </div>
  </div>
</div>