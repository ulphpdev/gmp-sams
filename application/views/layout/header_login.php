<!DOCTYPE>
<html>
  <title>SAMS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <head>
    <style>
    .loader{
      position:relative;
      display:none;
      background-color:#fff;
      opacity:0.5;
      z-index:99999;
    }
    .modal-content{
      position: center !important;
    }
    </style>

    <script type="text/javascript">
      var base_url = "<?= base_url();?>";
    </script>
    <link rel="icon" type="image/png" href="<?= base_url();?>asset/img/logo_image.png" />
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/sb-admin.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootbox.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/bootstrap-table.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/knockout-3.4.2.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/helper.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/js/custom.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/bootstrap-table.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/sb-admin.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/loading.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/vendor/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.ico"> -->
    <script type="text/javascript">
      $(document).ready(function() {
        
        update_config();
        $('.btn-logout').click(function(e) {
          e.preventDefault();
          $('#btn-logout').modal('hide');
          isLoading(true);
          
          var el = document.createElement('a');
          el.href = window.location.href;
          aJax.post(
            "<?=base_url('global_controller/insert_audit_trail')?>",
            {
              action:'Log-out',
              uri:el.pathname
            },
            function(data){
              aJax.get(
                "<?=base_url('login/unsetSession');?>",
                function(result){
                  window.location = '<?=base_url();?>';
                }
              );
            }
          );
          
        })
        
      });

      function update_listing(reportid){
        aJax.get(
          "<?= base_url('api/update_listing'); ?>?report_id="+reportid,
          function(data){ 
            // console.log("Listing Updated"); 
          }
        );
      }

      function isLoading(visible){
        if(visible == true){
          $('.loading').show();
        } else {
          $('.loading').hide();
        }
      }
      function updateAPI(api){
        update_config();
        aJax.get(
          "<?= base_url('api'); ?>/" + api,
          function(){
            // console.log("API Update");
          }
        );
      }
      function insert_audit_trail(action){
        var el = document.createElement('a');
        el.href = window.location.href;
        aJax.post(
          "<?=base_url('global_controller/insert_audit_trail')?>",
          {
            action:action,
            uri:el.pathname
          },
          function(data){
            return 'success';
          }
        );
      }
      function update_config(){
        aJax.get(
          "<?=base_url("api/generate_config_json")?>",
          function(data){
             // console.log("new generate_config_json saved!");
          }
        );
        aJax.get(
          "<?=base_url("api/audit_report_list")?>",
          function(data){
             // console.log("new audit_report_list saved!");
          }
        );
        aJax.get(
          "<?=base_url("api/template_list")?>",
          function(data){
             // console.log("new template_list saved!");
          }
        );
        aJax.get(
          "<?=base_url("api/update_dm")?>",
          function(data){
             // console.log("DM JSON Updated");
          }
        );
      }

      function isduplicate(table, query, cb){
        aJax.post(
          "<?= base_url('global_controller/check_if_exist');?>",
          {
            "table" : table,
            "query" : query
          },
          function(result){
            jQuery.globalEval(result);
            cb(result);
          }
        );
      }


      function check_inuse(query, cb){
        aJax.post(
          "<?=base_url('global_controller/getlist_in_use');?>",
          {
            'query': query
          },
          function(data){
            cb(data)
          }
        );
      } 

      function isIE() {
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/") > -1 ){ 
          return true;
        } else {
          return false
        }
      }

    </script>
  </head>
<body>
<?php $this->load->view('layout/browser'); ?>
<div class="loading" hidden>Loading&#8230;</div>

<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style = "overflow:hidden">
      <div class="modal-header modal-head">
        <button type="button" class="close modal-close btn-close" style = "color:#fff !important; opacity:10" data-dismiss="modal" aria-label="Close"><span style = "color:#fff !important; opacity:10"  aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Message</h4>
      </div>
      <div class="modal-body"> 
       <d class = "msg"> Successfully saved... </d>   
       <d class = "file_msg" style="display:none;">File(s) cannot be deleted/inactive because it is in use:<br></d>
       <d class = "file_account" style="display:none;"></d> 
      </div>
      <div class="modal-footer" style = "border:0px">
        <!--   <button class = "btn btn-primary btc2 btn-close" data-dismiss="modal" style = "padding:2px 10px;" >Close</button> -->
      </div>
    </div>
  </div>
</div>
  