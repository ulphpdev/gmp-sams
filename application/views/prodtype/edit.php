<?php
  $table = "tbl_classification";
  $order_by = "create_date";
?>

<div class="panel-heading">
    <h4 class="panel-title">
        Edit Product Type
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Product Type *: </label>
            <div class="col-sm-6">
                <input type = "text" id="txt-prod" class = "inputcss form-control fullwidth inputs_edit_classification" >
                <span class = "er-msg"> Product type should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Limit *: </label>
            <div class="col-sm-2">
                <input type = "number" id="limit" class = " input_number inputcss form-control fullwidth inputs_edit_classification" >
                <span class = "er-msg"> Limit should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Category : </label>
            <div class="col-sm-6">
                <select class="form-control" id="ques_category">
                    <option class="hidden selected" disabld selected value="">Select Category</option>
                </select>
                <span class = "er-msg"> Question category should not be empty *</span>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" id="add_cat" data-id="" data-text="">ADD</button>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2"></label>
            <div class="col-sm-10">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 150px;">Category Name</th>
                                <th style="width: 700px !important;">Description</th>
                                <th style="width: 100px;"></th>
                            </tr>
                        </thead>
                        <tbody class="cat_list">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4">No. of major to have unacceptable disposition *: </label>
            <div class="col-sm-2">
                <input type = "number" id="no_major" class = " input_number inputcss form-control fullwidth inputs_edit_classification" >
                <span class = "er-msg"> No. of major should not be empty *</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4">No. of critical to have unacceptable disposition *: </label>
            <div class="col-sm-2">
                <input type = "number" id="no_critical" class = "input_number inputcss form-control fullwidth inputs_edit_classification" >
                <span class = "er-msg"> No. of critical should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">
$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "classification_id";
  var id = "<?= $id; ?>";
  var current_classification = "";
	on_load();

  $("body").delegate('.input_number', 'focusout', function(){
      if($(this).val() < 0){
          $(this).val('0');
      }
  });


  function on_load(){
    isLoading(true);
    var limit = 1;
    var classification_id;
    aJax.post(
      "<?=base_url('global_controller/edit_global');?>",
      {
        id:id,
        limit:limit,
        table:table,
        field:field
      },
      function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(index, row){
          current_classification = row.classification_name;
          $('#txt-prod').val(row.classification_name);
          $('#limit').val(row.limit);
          $('#no_major').val(row.no_major);
          $('#no_critical').val(row.no_critical);
          $('#no_critical').val(row.no_critical);
          classification_id = row.classification_id;
        });
          get_question_category();
          get_classifications(classification_id);
      }
    );
  }

  $('.update').off('click').on('click', function() {
    var catname = $('#txt-prod').val();
    var limit = $('#limit').val();
    var major = $('#no_major').val();
    var critical = $('#no_critical').val();
     var get_time = func_time();
    if(validateFields('.inputs_edit_classification') == 0){
      var no_click = 0;
      confirm("Are you sure you want to update this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_classification", "classification_name = '" + catname + "' AND classification_name <> '"+current_classification+"' AND status >= 0", function(result){
              if(result == "true"){
                $("#txt-prod").css('border-color','red');
                $("#txt-prod").next().html("This field should contain a unique value.");
                $("#txt-prod").next().show();
              } else {
                $(this).prop('disabled',true);
                isLoading(true);
                aJax.post(
                  "<?=base_url('global_controller/save_classif');?>",
                  {
                    id:id,
                    table:table,
                    field:'classification_id',
                    catname:catname,
                    get_time: get_time,
                    limit:limit,
                    major:major,
                    critical:critical,
                    action: 'update'
                  },
                  function(data){
                    console.log("result data : " + data);
                    isLoading(false);
                    var class_id = data;
                    $('.update').prop('disabled',false);


                    $('.cat_item').each(function(x,y){
                      var catid = $(this).attr('data-id');
                      var data_add = $(this).attr('data-add');
                      var catdesc = $(this).find('textarea').val();

                      if(data_add != "0"){
                        aJax.postasync(
                          "<?=base_url('global_controller/update_classification_category')?>",
                          {
                            'catclass':class_id,
                            'cat_id':catid,
                            'description':catdesc
                          },
                          function(data){

                          }
                        );
                      } else {
                        aJax.postasync(
                          "<?=base_url()?>global_controller/save_classification_category?create_date="+get_time+"&update_date="+get_time,
                          {
                            'classification_id':class_id,
                            'category_id':catid,
                            'description':catdesc
                          },
                          function(data){
                            // (get_alertx);
                          }
                        );
                      }
                    });

                    updateAPI("classification");
                    update_config();
                    insert_audit_trail("Update " + catname);
                    bootbox.alert('<b>Record is successfully updated!</b>', function() {
                        location.reload();
                    });
                  }
                );
              }
            });
          }
        }
      });
    }
  });

  function func_time(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var get_time = date+' '+time;
    return get_time;
  }

})



function get_question_category(){
  $.ajax({
    type: 'post',
    url: "<?=base_url()?>global_controller/get_active_list",
    data:{table:'tbl_question_category'}
  }).done(function(data){
    var obj = JSON.parse(data);
    var htm = '';
    htm += '<option class="selected" value="">--Select question category--</option>';
    $.each(obj, function(x,y){
      if(y.category_id != 3 && y.category_id != 2){
        htm += '<option value="'+y.category_id+'">'+y.category_name+'</option>';
      }
    });
    $('#ques_category').html(htm);
  });
}

$('#ques_category').on('change',function(){
  var cat_id = $(this).val();
  var cat_name = $('#ques_category option:selected').text();
  $('#add_cat').attr('data-id',cat_id).attr('data-text',cat_name);
});

$('#add_cat').click(function(){
  var cat_id = $(this).attr('data-id');
  var cat_name = $(this).attr('data-text');
  var htm = '';
  if(cat_id == '' && cat_name == ''){
    bootbox.alert("Select question category to proceed!",function(e){});
  }else{
    if($('.ct_'+cat_id).length == 0){
      get_question_category();

        htm += '<tr class="cat_item ct_'+cat_id+'" data-id="'+cat_id+'" data-add=0>';
        htm += '   <td>'+cat_name+'</td>';
        htm += '   <td>';
        htm += '       <textarea class="form-control prodtype inputs_classification inputs_edit_classification" placeholder="Input description here. "></textarea>';
        htm += '       <span class = "er-msg"> Description should not be empty *</span>';
        htm += '   </td>';
        htm += '   <td>';
        htm += '        <a href="#" class="remove_cat" style="text-align:center;font-size: 10pt;padding-top: 3px;cursor:pointer;">';
        htm += '            <span class="glyphicon glyphicon-remove" style="font-size:18pt;color:#de0000;"></span>';
        htm += '        </a>';
        htm += '   </td>';
        htm += '</tr>';
        $('.cat_list').append(htm);

    }else{
      bootbox.alert("Question category is already in the list, select other category!",function(e){});
    }

  }
  $(this).attr('data-id','').attr('data-text','');
});

function get_classifications(id)
{
    $.ajax({
      type: 'post',
      url:'<?=base_url();?>global_controller/get_question_classification',
        data:{id:id},
      }).done(function(data){
          var obj = JSON.parse(data);
          var htm = '';
          $.each(obj, function(x,y){
            htm += '<tr class="cat_item cat_item ct_'+y.category_id+' mb5" data-id="'+y.category_id+'" data-class="'+y.classification_id+'">';
            htm += '   <td>'+y.category_name+'</td>';
            htm += '   <td>';
            htm += '       <textarea class="form-control prodtype inputs_classification inputs_edit_classification" placeholder="Input description here. ">'+y.description+'</textarea>';
            htm += '       <span class = "er-msg"> Description should not be empty *</span>';
            htm += '   </td>';
            htm += '   <td>';
            if(y.category_name != "Major"){
                if(y.category_name != "Critical"){
                    htm += '        <a href="#" data-id="'+y.category_id+'" data-class="'+y.classification_id+'" class="remove_cat" style="text-align:center;font-size: 10pt;padding-top: 3px;cursor:pointer;">';
                    htm += '            <span class="glyphicon glyphicon-remove" style="font-size:18pt;color:#de0000;"></span>';
                    htm += '        </a>';
                }
            }
            htm += '   </td>';
            htm += '</tr>';
          });
          $('.cat_list').append(htm);
          isLoading(false);
      });
}

$(document).on('click','.remove_cat', function(e){
    e.preventDefault();
  var cat_id = $(this).attr('data-id');
  var cat_class = $(this).attr('data-class');
  var dialog = bootbox.confirm({
          message: "Are you sure you want to delete this record?",
          buttons: {
              confirm: {
                  label: 'Yes',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
              if(result){
                  $.ajax({
                    type: 'post',
                    url:'<?=base_url();?>global_controller/remove_question_classification',
                    data:{cat_id:cat_id, cat_class:cat_class},
                    }).done(function(data){
                      $('.ct_' + cat_id).remove();
                  });
            }
        }
    });

});

</script>