<?php
  $table = "tbl_question_category";
  $order_by = "create_date";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Add Category
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Category Name *: </label>
            <div class="col-sm-6">
                <input type = "text" id="txt-name" class = "inputcss form-control fullwidth inputs_category" >
                <span class = "er-msg">Category should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">

$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";

  $('.save').off('click').on('click', function() {
    var catname = $('#txt-name').val();
    if(validateFields('.inputs_category') == 0){
      var no_click = 0;
      confirm("Are you sure you want to save this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_question_category", "category_name = '" + catname + "' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputs_category").css('border-color','red');
                $(".inputs_category").next().html("This field should contain a unique value.");
                $(".inputs_category").next().show();
              } else {
                $(this).prop('disabled',true);
                isLoading(true);

                aJax.post(
                  "<?=base_url('global_controller/save_category');?>",
                  {
                    table:table,
                    catname:catname,
                    action: 'save'
                  },
                  function(data){
                    isLoading(false);
                    $('.update').prop('disabled',false);
                    insert_audit_trail("Create " + catname);
                    updateAPI("question_category");
                    bootbox.alert('<b>Record is successfully saved!</b>', function() {
                     location.reload();
                    });
                  }
                );
              }
            });
          }
        }
      });
    }
  });

});



</script>