<?php
  $table = "tbl_audit_scope";
  $order_by = "scope_id";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Add Type of Audit
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Type of Audit *: </label>
            <div class="col-sm-6">
                <input type = "text" id="type" class = "inputcss form-control fullwidth inputs_type_of_audit" >
                <span class = "er-msg">Type of audit should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">
$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";

  $('.save').off('click').on('click', function() {
    var type = $('#type').val();
    if(validateFields('.inputs_type_of_audit') == 0){
      var no_click = 0;
      confirm("Are you sure you want to save this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_audit_scope", "scope_name = '" + type + "' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputs_type_of_audit").css('border-color','red');
                $(".inputs_type_of_audit").next().html("This field should contain a unique value.");
                $(".inputs_type_of_audit").next().show();
              } else {
                isLoading(true);
                aJax.post(
                  "<?=base_url('global_controller/type_audit_array');?>",
                  {
                    type:type,
                    table:table,
                    field:field,
                    action: 'save'
                  },
                  function(data){
                    isLoading(false);
                    $('.update').prop('disabled',false);
                    updateAPI("type_audit");
                    update_config();
                    insert_audit_trail("Create " + type);
                    bootbox.alert('<b>Record is successfully saved!</b>', function() {
                      location.reload();
                    }).off("shown.bs.modal");
                  }
                );
              }
            });
          }
        }
      });
    }
  });
})
</script>