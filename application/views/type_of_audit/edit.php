<?php
  $table = "tbl_audit_scope";
  $order_by = "scope_id";
?>
<div class="panel-heading">
    <h4 class="panel-title">
        Edit Type of Audit
    </h4>
</div>
<div class="panel-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Type of Audit *: </label>
            <div class="col-sm-6">
                <input type = "text" id="type" class = "inputcss form-control fullwidth inputs_edit_type_of_audit" >
                <span class = "er-msg">Type of audit should not be empty *</span>
            </div>
        </div>
    </div>
</div>
<div class="panel-footer">
    <button class="update btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> UPDATE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="save btn-min btn btn-success  bold"><span class = "glyphicon glyphicon-floppy-saved"></span> SAVE <span><img src="../images/assets/loader.gif" class="loader_gif" style="display: none;"></span></button>
    <button class="cancel btn-min  btn btn-default bold" ><span class = "glyphicon glyphicon-remove-circle"></span> CANCEL</button>
</div>

<script type="text/javascript">
$(document).ready(function(){

  var table = "<?=$table;?>";
  var field = "<?=$order_by;?>";
  var id = "<?= $id; ?>";
  var current_scope = "";
  on_load();

  function on_load(){
    var limit = 1;
    aJax.post(
      "<?=base_url('global_controller/edit_global');?>",
      {
        id:id,
        limit:limit,
        table:table,
        field:field
      },
      function(data){
        var obj = JSON.parse(data);
        $.each(obj, function(index, row){
          current_scope = row.scope_name;
          $('#type').val(row.scope_name);
        });
      }
    );
  }

  $('.update').off('click').on('click', function() {
    var type = $('#type').val();
    if(validateFields('.inputs_edit_type_of_audit') == 0){
      var no_click = 0;
      confirm("Are you sure you want to update this record?",function(result){
        if(result){
          no_click ++;
          if(no_click <= 1){
            isduplicate("tbl_audit_scope", "scope_name = '" + type + "' AND scope_name <> '"+current_scope+"' AND status >= 0", function(result){
              if(result == "true"){
                $(".inputs_edit_type_of_audit").css('border-color','red');
                $(".inputs_edit_type_of_audit").next().html("This field should contain a unique value.");
                $(".inputs_edit_type_of_audit").next().show();
              } else {
                isLoading(true);
                aJax.post(
                  "<?=base_url('global_controller/type_audit_array');?>",
                  {
                    id:id,
                    type:type,
                    table:table,
                    field:field,
                    action: 'update'
                  },
                  function(data){
                    isLoading(false);
                    $('.update').prop('disabled',false);
                    updateAPI("type_audit");
                    update_config();
                    insert_audit_trail("Update " + type);
                    bootbox.alert('<b>Record is successfully updated!</b>', function() {
                      location.reload();
                    });
                  }
                );
              }
            });
          }
        }
      });
    }
  });
})
</script>