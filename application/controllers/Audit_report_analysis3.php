<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');
class Audit_report_analysis3 extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Audit_report_analysis_model');
		$this->load->model('Audit_report_model');
		$this->load->model('q_auditreport');
		$this->load->helper('url');

		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}
		
	}
	public function index()
	{	
		$data['content'] = 'audit_report_analysis/list';
		$this->load->view('layout/layout',$data);
		
	}
	public function get_list_report(){
		$limit = $_POST['limit'];
		$offset = ($_POST['offset']-1)* $limit;
		$data = $this->Audit_report_analysis_model->get_report_analysis_list($limit,$offset);
		echo json_encode($data);
	}
	public function get_list_report_count() {
		$table = $_POST['table'];
		$order_by = $_POST['order_by'];
		$per_page = $_POST['limit'];
		$limit = '9999999';
		$offset = '0';
		$data = count($this->Audit_report_analysis_model->get_report_analysis_list($limit,$offset));
		$page_count = ceil($data/$per_page);
		echo $page_count;
	}
	public function get_report_analysis_view(){

		header('Content-type: Application/JSON');
 
		$report_id = $_POST['id'];
		$arrayobj = $this->Audit_report_analysis_model->get_report_analysis_view2($report_id);

		$audit_dates = $this->Audit_report_model->get_audit_dates($report_id);

		$len = count($audit_dates);
        $auddate = "";
        $audyear = "";
        $audmonth = "";
        foreach ($audit_dates as $key => $value) {
            $audmonth = date_format(date_create($value->audit_date),"F");
            $audyear = date_format(date_create($value->audit_date),"Y");
            if ($key == $len - 1) {
               $auddate .= ' & ' . date_format(date_create($value->audit_date),"d");
            } else {
               $auddate .= ', ' . date_format(date_create($value->audit_date),"d");
            }
        }

        $auditdate_format = $audmonth . " " . substr($auddate,2) . " " . $audyear;

		if(count($arrayobj) > 0 ){
			$obj = array(
				"NA"=>$arrayobj[0]->answer_na,
				"NC"=>$arrayobj[0]->answer_nc,
				"name"=>$arrayobj[0]->name,
				"no"=>$arrayobj[0]->answer_no,
				"no_critical"=>$arrayobj[0]->category_critical,
				"no_major"=>$arrayobj[0]->category_major,
				"no_minor"=>$arrayobj[0]->category_minor,
				"question_count"=>count($arrayobj),
				"report_id"=>$arrayobj[0]->report_id,
				"report_no"=>$arrayobj[0]->report_no,
				"yes"=>$arrayobj[0]->answer_yes,
				"rating"=>number_format($arrayobj[0]->Rating,2),
				"coverage"=>number_format($arrayobj[0]->Coverage,2),
				"audit_date"=>$auditdate_format
			);
		} else {
			$query = "report_id = " . $report_id;
        	$report = $this->q_auditreport->get_list($query,1,0, "");

			$obj = array(
				"NA"=>0,
				"NC"=>0,
				"name"=>$report[0]->name,
				"no"=>0,
				"no_critical"=>0,
				"no_major"=>0,
				"no_minor"=>0,
				"question_count"=>0,
				"report_id"=>$report_id,
				"report_no"=>$report[0]->Report_No,
				"yes"=>0,
				"rating"=>0,
				"coverage"=>0,
				"audit_date"=>$auditdate_format
			);
		}

		


		echo json_encode($obj);
	}
	//audito_report_analysis
	// public function get_pagination(){

	// 	header('Content-type: Application/JSON');

	// 	$query = $_POST['query'];

	// 	echo $this->q_auditreport->get_pagination($query);

	// }

	
    //pagination audit_report_analysis
    public function get_pagination($query = null){



        $this->db->select("*");

        $this->db->from("audit_report_analysis_view");

        if($query != null) {

            $this->db->where($query);

        }

        $q = $this->db->get();

        return $q->num_rows();



    }
}
