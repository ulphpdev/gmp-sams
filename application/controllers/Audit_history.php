<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Audit_history extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->model('global_model');
		$this->load->model('template_model');
		$this->load->helper('url');

		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}

	}


	public function index()
	{	
		$data['content' ]= 'audit_history/list';
		$this->load->view('layout/layout',$data);
	}

	public function get_list()
	{
		$result = $this->global_model->getlist_global_model("qv_audit_history",$_POST['limit'],$_POST['offset'],$_POST['query'],"id","status = 1 ");
		$result_array = array();
		foreach ($result as $key => $value) {
        	$audit_dates = $this->template_model->get_data_by_id($value->id,'history_id','tbl_audit_history_dates');
        	$inspectors = $this->template_model->get_data_by_id($value->id,'history_id','tbl_audit_history_inspectors');
        	$changes = $this->template_model->get_data_by_id_order($value->id,'history_id','tbl_audit_history_changes','orders');

			$result_array[] = array(
				"id"=>$value->id,
				"name"=>$value->name,
				"major_changes"=>$changes,
				"audit_dates"=>$this->format_audit_date($audit_dates),
				"inspectors"=>$inspectors,
				"update_date"=>$value->update_date,
				"report_no"=>$value->report_no,
				"from_API"=>$value->API,
			);
		}
		
		echo json_encode($result_array);
	}

	public function insert_history()
	{
		
		if($_POST['datatype'] == 0){
			$data = array(
				"supplier_id"=>$_POST['supplier_id'],
				"report_no"=>strip_tags($_POST['report_no']),
				"create_date"=>date("Y-m-d H:i:s"),
				"update_date"=>date("Y-m-d H:i:s"),
				"status"=>1,
				"API"=>0
			);

			echo $this->global_model->save_data('tbl_audit_history', $data);
		} else {
			$data = array(
				"supplier_id"=>$_POST['supplier_id'],
				"report_no"=>strip_tags($_POST['report_no']),
				"update_date"=>date("Y-m-d H:i:s")
			);
			$this->global_model->update_data('id', $_POST['datatype'], 'tbl_audit_history', $data);
			echo $_POST['datatype'];
		}

	}

	public function insert_history_changes()
	{
		
		if($_POST['datatype'] == 0){
			$data = array(
				"history_id"=>$_POST['history_id'],
				"changes"=>strip_tags($_POST['changes']),
				"orders"=>$_POST['orders']
			);
			$this->global_model->save_data('tbl_audit_history_changes', $data);
		} else {
			$data = array(
				"changes"=>strip_tags($_POST['changes']),
				"orders"=>$_POST['orders']
			);
			$this->global_model->update_data('id', $_POST['datatype'], 'tbl_audit_history_changes', $data);
		}
		
	}

	public function insert_history_dates()
	{
		

		if($_POST['datatype'] == 0){
			$data = array(
				"history_id"=>$_POST['history_id'],
				"date"=>$_POST['date']
			);
			$this->global_model->save_data('tbl_audit_history_dates', $data);
		} else {
			$data = array(
				"date"=>$_POST['date']
			);
			$this->global_model->update_data('id', $_POST['datatype'], 'tbl_audit_history_dates', $data);
		}
		
	}

	public function insert_history_inspector()
	{
		
		if($_POST['datatype'] == 0){
			$data = array(
				"history_id"=>$_POST['history_id'],
				"inspector"=>strip_tags($_POST['inspector'])
			);
			echo $this->global_model->save_data('tbl_audit_history_inspectors', $data);
		} else {
			$data = array(
				"inspector"=>strip_tags($_POST['inspector'])
			);
			$this->global_model->update_data('id', $_POST['datatype'], 'tbl_audit_history_inspectors', $data);
		}
	}

	public function get_details()
	{
		$result = $this->global_model->get_where('tbl_audit_history', 'id', $_POST['id']);
		foreach ($result as $key => $value) {
			$audit_dates = $this->global_model->get_where('tbl_audit_history_dates', 'history_id', $_POST['id']);
			$inspectors = $this->global_model->get_where('tbl_audit_history_inspectors', 'history_id', $_POST['id']);
			$changes = $this->global_model->get_where('tbl_audit_history_changes', 'history_id', $_POST['id']);

			$array[] = array(
				"supplier_id"=>$value->supplier_id,
				"report_no"=>$value->report_no,
				"audit_dates"=>$audit_dates,
				"inspectors"=>$inspectors,
				"changes"=>$changes
			);
		}

		echo json_encode($array);
	}

	public function remove_changes()
	{
		$query = "id = " . $_POST['id'];
		$this->global_model->trash_data($query, 'tbl_audit_history_changes');
	}
	public function remove_inspector()
	{
		$query = "id = " . $_POST['id'];
		$this->global_model->trash_data($query, 'tbl_audit_history_inspectors');
	}

    public function remove_audit_date()
    {
        $query = "id = " . $_POST['id'];
        $this->global_model->trash_data($query, 'tbl_audit_history_dates');
    }

	public function remove_audit_history()
	{
		$query = "report_id = " . $_POST['id'];
		$this->global_model->trash_data($query, 'tbl_audit_history');
	} 

	public function view_details()
	{
		$result = $this->global_model->get_audit_history($_POST['id']);	
		$array = array();
		foreach ($result as $key => $value) {
			$audit_dates = $this->global_model->get_where('tbl_audit_history_dates', 'history_id', $value->id, null, "Date");
			$inspectors = $this->global_model->get_where('tbl_audit_history_inspectors', 'history_id', $value->id);
			$major_changes = $this->global_model->get_where('tbl_audit_history_changes', 'history_id', $value->id);
			
			$array[] = array(
				"supplier_id"=>$value->supplier_id,
				"report_no"=>$value->report_no,
				"major_changes"=>$this->format_changes($major_changes),
				"modified_date"=>$value->update_date,
				"audit_dates"=>$this->format_audit_date($audit_dates),
				"inspectors"=>$this->format_inspector($inspectors)
			);
		}

		echo json_encode($array);
	}

	function format_changes($data){

        if(count($data) > 0){
            $str = array_pop($data);
            $result = "";
            if(count($data) > 0 ){
                $first = "";
                foreach ($data as $key => $value) {
                    $first .= $value->changes . ", ";
                }   
                $result = rtrim(trim($first), ',') . " & " . $str->changes;
            } else {
                $result = $str->changes;  
            }
        } else {
            $result = "None";
        }
		
			
		return $result;
	}

	function format_inspector($data){
        if(count($data) > 0){
    		$str = array_pop($data);
    		$result = "";
    		if(count($data) > 0 ){
    			$first = "";
    			foreach ($data as $key => $value) {
    				$first .= $value->inspector . ", ";
    			}	
    			$result = rtrim(trim($first), ',') . " & " . $str->inspector;
    		} else {
    			$result = $str->inspector;
    		}
        } else {
            $result = "None";
        }
			
		return $result;
	}
	
	function format_audit_date($data){
        
		$dates = array();
		foreach($data as $day){
			$timestamp = strtotime($day->Date);
			$dates[date('Y', $timestamp)][date('F', $timestamp)][] = date('d', $timestamp);
		}
		$result_final = "";

		$year_i = 0;
		$year_len = count($dates);
		$year_sep = "";

		foreach ($dates as $year => $months) {
			$result_year = $year;
			$result = "";
			$result_with_last_month = "";
			$dates_first = "";
			$str = "";
			$month_i = 0;
			$month_len = count($months);
			$month_sep = "";
			foreach ($months as $month => $date) {
				$str = array_pop($date);
				$dates_first = "";

				if ($month_i == $month_len - 1) {
					// last
					if($month_len > 1){
						$month_sep = " & ";
					} else {
						$month_sep = "";
					}
					
				} else {
					$month_sep = ", ";
				}

				if(count($date) > 0 ){
					foreach ($date as $value) {
						$dates_first .= (int)$value . ", ";
					}
					$result .=  $month_sep . rtrim(trim($dates_first), ',') . ", " . (int) $str . " " . $month   .", ";
				} else {
					$result .=  $month_sep . (int) $str . " " . $month ;
				}

				$month_i++;
			}

			if ($year_i == $year_len - 2) {
				if($year_len > 2){
					$result_final .= ltrim(rtrim(trim($result), ','), ',') . " " . $result_year . " & ";
				} else {
					$result_final .= ltrim(rtrim(trim($result), ','), ',') . " " . $result_year . ", ";
				}
			} else {
				$result_final .= ltrim(rtrim(trim($result), ','), ',') . " " . $result_year . ", ";
			}

			$year_i++;

			
		}

		return rtrim(trim($result_final), ',');
    }

}
