<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Generate extends CI_Controller {

		function __construct(){
			parent::__construct();
	        $this->load->helper('url');
	        $this->load->library('Phpword');
	        $this->load->model('Template_model');
	        $this->load->model('Audit_report_model');
	        $this->load->model('Preview_report_model');
	        $this->load->model('global_model');
		}	

		function is_date( $str ) {
		    try {
		        $dt = new DateTime( trim($str) );
		    }
		    catch( Exception $e ) {
		        return false;
		    }
		    $month = $dt->format('m');
		    $day = $dt->format('d');
		    $year = $dt->format('Y');
		    if( checkdate($month, $day, $year) ) {
		        return true;
		    }
		    else {
		        return false;
		    }
        }
        
        function htmlformatter($string){
            return nl2br(htmlentities($string));
        }

		function index()
		{
			$id = $_GET['report_id'];
			$string = file_get_contents("./listing/".$id.".json");
			$details = json_decode($string, false);
			echo "<pre>";

			print_r($details);

			echo "</pre>";
		}


		public function approval_history(){  
	        $report_id = $_GET['report_id'];
            $action = $_GET['action'];
            $string = file_get_contents("./listing/".$report_id.".json");
            $details = json_decode($string, false);
            $report = $details->Report_Summary;

             foreach ($this->global_model->get_report_details($report_id) as $value) {
                $report_no = $value->Report_No;
                $company = $value->name;
            }
	       
	        $this->load->library("Pdf_approval_history");
	        $orientation = 'L';
	        $unit = 'mm';
	        $format = 'LETTER';
	        $unicode = true;
	        $encoding = 'UTF-8';
	        $pdf = new Pdf_approval_history($orientation, $unit, $format, $unicode, $encoding);
	        $pdf->SetTitle("Approval History : " . $report_no );
	        $pdf->SetFont('Helvetica', '', 10);
            $pdf->setFooterRight("CONFIDENTIAL");
	        $pdf->SetPrintHeader(false);
	        $pdf->AddPage("L", "Legal"); 
	        $html = '';
	        $x = 'A';

	       

	       
	       	$html .= '<div>';
	       	$html .= '	<div>';
	       	$html .= '		<b>AUDIT REPORT NO: </b><span style="color:blue;text-transform:uppercase;">'.$report_no."</span><br>";
	       	$html .= '		<b>SITE: </b><span style="color:blue;text-transform:uppercase;">'.$company."</span><br>";
	       	$html .= '	</div>';
	       	$html .= '</div>';

	      	$html .= '<b>APPROVAL HISTORY</b><br>';
	       	$html .= '<table border="1" cellpadding="3" style="font-size: 9px;">';
	       	$html .= '	<thead>';
	       	$html .= '		<tr style="background-color:#5a5a5a;color:#fff;">';
	       	$html .= '			<th>Date & Time</th>';
	       	$html .= '			<th>Name</th>';
	       	$html .= '			<th>Position</th>';
	       	$html .= '			<th>Status</th>';
	       	$html .= '			<th>Remarks</th>';
	       	$html .= '		</tr>';
	       	$html .= '	</thead>';
	       	$html .= '	<tbody>';

            if(count($this->Preview_report_model->get_remarks($report_id)) > 0 ){
                foreach ($this->Preview_report_model->get_remarks($report_id) as $key => $value) {
                    $html .= '      <tr>';
                    $html .= '          <td>'.date("d F Y H:i",strtotime($value->date)).'H</td>';
                    $html .= '          <td>'.$this->htmlformatter($value->name).'</td>';
                    $html .= '          <td>'.$this->htmlformatter($value->position).'</td>';
                    $html .= '          <td>'.$this->htmlformatter($value->status).'</td>';
                    $html .= '          <td>'.$this->htmlformatter($value->remarks).'</td>';
                    $html .= '      </tr>';
                }
            } else {
                $html .= '      <tr>';
                $html .= '          <td colspan="5" style="text-align:center;">No records as of this time.</td>';
                $html .= '      </tr>';
            }

	       	

	       	$html .= '	</tbody>';
	       	$html .= '</table>';
            $pdf->writeHTML($html,true,false,true,false,'');   
            switch ($action) {
                case 'I':
                    $pdf->Output($report[0]->report_no.'-Approval History.pdf', 'I');
                    break;
                
                case 'F':
                    if (!file_exists('./json/export/approved/' . $id)) {
                        mkdir('./json/export/approved/' . $id, 0777, true);
                    }

                    if (!file_exists('./json/export/archive/' . $id)) {
                        mkdir('./json/export/archive/' . $id, 0777, true);
                    }

                    $pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Approval History.pdf', 'F');
                    $pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Approval History.pdf', 'F');
                    break;

                case 'D':
                    $pdf->Output('Approval History '.$report[0]->report_no.'.pdf', 'D');
                    break;
                
            }
	    } 

		public function preview_template(){  
	        $template_id = $_GET['template_id'];
	        $template = $this->Template_model->get_data_by_id($template_id,'template_id','tbl_template');
	        $classification = $this->Template_model->get_data_by_id($template[0]->classification_id,'classification_id','tbl_classification');
	        $standard = $this->Template_model->get_data_by_id($template[0]->standard_id,'standard_id','tbl_standard_reference');
	        // foreach element
	        $element = $this->Template_model->get_elements($template_id);
	       
	        $this->load->library("Pdf_template");
	        $orientation = 'P';
	        $unit = 'mm';
	        $format = 'LETTER';
	        $unicode = true;
	        $encoding = 'UTF-8';
	        $pdf = new Pdf_template($orientation, $unit, $format, $unicode, $encoding);
	        $pdf->SetTitle("Template " . $template_id );
	        $pdf->SetFont('Helvetica', '', 10);
            $pdf->setFooterRight("CONFIDENTIAL");
	        $pdf->SetPrintHeader(false);
	        $pdf->AddPage(); 
	        $html = '';
	        $x = 'A';
	       
	        if($template[0]->sub_version == 0){
	        	$version = $template[0]->version;
	        } else {
	        	$version = $template[0]->version . "." . $template[0]->sub_version;
	        }
	        $html .= '<div><div><label>PRODUCT TYPE: <span style="color:blue;text-transform:uppercase;">'.$classification[0]->classification_name.'</span></label><br/>
	                <label>STANDARD / REFERENCE: <span style="color:blue;text-transform:uppercase;">'.$standard[0]->standard_name.'</span></label><br/>
	                <label>VERSION: <span style="color:blue;text-transform:uppercase;">'.$version.'</span></label></div></div>';
	        $html .= '<div style="font-size12px;">';
	        foreach($element as $value){
	        $ctr = 1;                   
	            $html .= '<h4>ELEMENT '.$x.' - '. $this->htmlformatter($value->element_name).'</h4>';
	            $html .= '<table border="1" cellpadding="3">';
	            $html .= '<tr style="background-color:#5a5a5a;color:#fff;">
	                    <td width="10%"></td>
	                    <td width="45%">Questions</td>
	                    <td width="45%">Input for Template <br/> (Default answer for Yes)</td>
	                    </tr>';
	            $questions = $this->Template_model->get_questions($template_id,$value->element_id);
	        
	            foreach ($questions as $key => $value) {
	                $html .= '<tr><td>'.$x.$ctr.'</td><td>'.$this->htmlformatter($value->question).'</td><td>'.$this->htmlformatter($value->default_yes).'</td></tr>';
	                $ctr++;
	            } 
	            $html .= '</table>';
	            $x++;
	        }  
	        $html .= '</div>';  
	        $pdf->writeHTML($html,true,false,true,false,'');   
	        $pdf->Output('Template '.$template_id.'.pdf', 'I');
	        $pdf->Output(__DIR__.'./../../email_attach/Template-' . $template_id . '.pdf', 'F');
	    } 


		function audit_report2()
		{
			$id = $_GET['report_id'];
			$action = $_GET['action'];
			$string = file_get_contents("./listing/".$id.".json");
			$details = json_decode($string, false);

			$anastring = file_get_contents("./listing/".$id."_analysis.json");
			$analysis = json_decode($anastring, false);

			$report = $details->Report_Summary;
			$activities = $details->Report_activities;
			$audit_dates = $details->Audit_Dates;
			$lead_auditor = $details->Lead_Auditor;
			$co_auditor = $details->Co_Auditors;
			$translators = $details->Translator;
			$license_Accreditation = $details->License;
			$preaudit_documents = $details->Pre_Document;
			$stanard_reference = $details->Template;
            if(isset($details->Inspection_Audit_Dates_Formatted)){
                $inspection_date = $details->Inspection_Audit_Dates_Formatted;
            } else {
                $inspection_date = "None";
            }

            if(isset($details->Inspection_Inspector)){
                $inspection_inspector = $details->Inspection_Inspector;
            } else {
                $inspection_inspector = "";
            }

            $inspection_changes = $details->Inspection_Changes;
			
			$scope = $details->Scope_Product;
			$present_during_meeting = $details->Present_During_Meeting;
			$get_personel_met = $details->Personel_Met;
			$observation_findings = $details->Observation_Yes;
			$observation = $details->Audit_Observation;

			$signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
			$co_auditor = $this->Audit_report_model->get_data_by_auditor($report[0]->report_id, 'report_id', 'tbl_co_auditors');
			$reviewer_info = $this->Audit_report_model->get_reviewer($report[0]->report_id);
        	$approver = $this->Audit_report_model->get_approver($report[0]->report_id);
        	$country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');
            $lead_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_summary');

			$params = array("report_no"=>$report[0]->report_no);

			$this->load->library("Pdf",$params);
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->setReportNo($report[0]->report_no);
	        $pdf->setFooterRight("CONFIDENTIAL");
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '27', '20');
	        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $timesitalic = $pdf->AddFont('timesitalic');
	        $gothic = $pdf->AddFont('gothic');

	        $cgi = $pdf->AddFont('cgi');
	        $pdf->setListIndentWidth(20);
	        $pdf->AddPage();
	        $html = '';
	        $x = 'A';  
	        $html .= '<style> 
	                    .gothic {
	                        font-size: 12px;
	                        font-family: gothic;
	                    }

                        .gothic-title {
                            font-size: 13.5px;
                            font-family: gothic;
                        }

                        .italic_font {
                            font-weight: 700;
                            font-size: 11px;
                            font-family: timesitalic;
                        } 
	                </style>';
	        $html .='<div style="text-align:center"><center><label><h2 class="gothic">GMP AUDIT REPORT</h2><span style="margin-top:-80px;"><b>'.$report[0]->report_no.'</b></span></label></center></div>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b class="gothic">AUDITED SITE:</b> </td>';
	        $html .='<td width="30"></td>';
	        $html .='<td width="500" class="gothic"> '. $report[0]->name.' </td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td><td class="gothic"> '.$report[0]->address1. ', ' .$report[0]->address2. '</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td><td class="gothic"> '.$report[0]->address3.', '. strtoupper($country[0]->country_name) .' </td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

////////////////////////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b class="gothic">ACTIVITIES CARRIED OUT BY THE COMPANY:</b></td>';
	        $html .='<td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<table style="margin-top: 20px;">';
	        foreach ($activities as $key => $value) { 
	        	$box = str_replace(base_url(),"",$value->activity_box);
	        	if($box == "asset/img/checkbox_block.jpg"){
		            $html .='<tr>';
		            $html .='<td style="width: 75%;" class="gothic">';
		            $html .= $value->activity_name;
		            $html .='</td>';
		            // $html .='<td style="width: 5%;">';
		            // $html .='<img style="position: fixed; right: 0;" src="'.$box.'" width="15">';
		            // $html .='</td>';
		            $html .='</tr>';
		            foreach ($value->sub_activity as $k => $v) {
		            	$sub_box = str_replace(base_url(),"",$v->sub_box);
		            	if($sub_box == "asset/img/checkbox_block.jpg") {
			                $html .='<tr>';
			                $html .='<td style="width: 75%;" class="gothic">';
			                $html .= ' &nbsp; &nbsp; &nbsp; ' . $v->sub_name;
			                $html .='</td>';
			                // $html .='<td style="width: 5%;">';
			                // $html .='<img style="position: fixed; right: 0;" src="'. $sub_box.'" width="15">';
			                // $html .='</td>';
			                $html .='</tr>';
			            }
		            }
		        }
	            
	        }
	        if($report[0]->other_activities != "" || $report[0]->other_activities != null){
	            $html .='<tr>';
	            $html .='<td style="width: 75%;" class="gothic">Others: ' . $report[0]->other_activities . '</td>';
	            // $html .='<td style="width: 5%;">';
	            //     $html .='<img style="position: fixed; right: 0;" src="asset/img/checkbox_block.jpg" width="15">';
	            //     $html .='</td>';
	            $html .='</tr>';
	        }
	        
	        $html .='</table>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

/////////////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b class="gothic">AUDIT DATE:</b></td><td width="30"></td>';
	        $html .='<td width="500" class="gothic">';

	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->Date),"F");
	            $audyear = date_format(date_create($value->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->Date),"d");
	            }
	        }

	        $html .= substr($auddate,2) . " " . $audmonth . " " . $audyear;
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


//////////////////////////////////////
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b class="gothic">AUDITOR/S:</b></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<table>';
	        $html .='<tbody>';
	        $html .='<tr>';
	        $html .='<td class="gothic">'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'</td><td class="gothic">' .$lead_auditor[0]->company. '</td>';
	        $html .='</tr>';
	        foreach ($co_auditor as $key => $value) {
	            $html .='<tr>';
	            $html .='<td class="gothic">'.$value->fname. ' ' .$value->lname.'</td><td class="gothic">' .$value->company. '</td>';
	            $html .='</tr>';
	        }
	        $html .='<tr><td></td></tr>';
	        $html .='</tbody>';
	        $html .='</table>';
            $html .= '<br/><b class="gothic">Translator Usage </b><span class="gothic"> (Name of employee, or approved translator, participating in this audit as determined by the Lead Auditor): ';
	        $translators_name ="";
	        foreach ($translators as $key => $value) {
	            $translators_name .= ", " . $value->translator;
	        }
	        $html .= substr($translators_name, 1);
            $html .='</span>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

/////////////////////////////////////////////

	        $html .='<div nobr="true">';
	        $html .='<table>';
	        $html .='<tr>';
	        $html .='<td width="100"><b class="gothic">REFERENCE:</b></td><td width="30"></td>';
	        $html .='<td width="500" class="gothic">';
	        $html .='<b class="gothic">License(s) / Accreditation(s) / Certification(s)</b> held by supplier and verified during the audit';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td width="130"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square" class="gothic">';
	        foreach ($license_Accreditation as $key => $value) {
	            $html .= '<li class="gothic">';
	            if($value->reference_name != ""){
	            	$html .= $value->reference_name . '<br />';
	            } else {
	            	$html .= 'None<br />';
	            }

	            if($value->issuer != ""){
	            	$html .= $value->issuer . '<br />';
	            } else {
	            	$html .= 'None<br />';
	            }

	            if($value->reference_no != ""){
	            	$html .= 'License / Certificate No . ' . $value->reference_no . '<br />';
	            } else {
	            	$html .= 'License / Certificate No: None<br />';
	            }

	            if($this->is_date($value->validity)){
	            	$html .= 'Validity: ' . date_format(date_create($value->validity),"d F Y") . '<br />';
	            } else {
	            	$html .= 'Validity: None <br />';
	            }

	            if($this->is_date($value->validity)){
	            	$html .= 'Issue Date: ' . date_format(date_create($value->issued),"d F Y") . '<br />';
	            } else {
	            	$html .= 'Issued Date: None <br />';
	            }
	            
	            
	            $html .= '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500" class="gothic">';
	        $html .='<b>Pre-audit documents</b> provided and reviewed';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500" class="gothic">';
	        $html .='<ul type="square">';
	        foreach ($preaudit_documents as $key => $value) {
	            $html .= '<li>' . $value->document_name . '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500" class="gothic">';
	        $html .='<b>Regulatory and UNILAB Standards Used</b>';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='<tr>';
	        $html .='<td width="100"></td><td width="30"></td>';
	        $html .='<td width="500">';
	        $html .='<ul type="square" class="gothic">';
	        foreach ($stanard_reference as $key => $value) {
	            $html .= '<li>' . $value->standard_name . '</li>';
	        }
	        $html .='</ul>';
	        $html .='</td>';
	        $html .='</tr>';

	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

////////////////////////////////////////////////////

	        $html .='<div nobr="true">';
	        $html .='<table nobr="true" class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2" class="gothic-title"><b>SUPPLIER BACKGROUND / HISTORY</b></td>';
	        $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2"><span style="text-align:justify;">' . $report[0]->background . '</span></td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td width="500"><b>Date of previous inspection </b><span class="italic_font">(if applicable)</span></td>';
	        $html .='</tr>';
           
    	    $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><ul type="square"><li>';
            $html .= $inspection_date;
	        $html .= '</li></ul></td>';
	        $html .='</tr>';

	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><b>Names of Inspectors invloved in previous inspection</b> <span class="italic_font">(if applicable)</span>';

	        $html .='<ul type="square">';
            if($inspection_inspector != ""){
                if(count($inspection_inspector) > 0){
                    foreach ($inspection_inspector as $key => $value) {
                        if(trim($value->inspector) != ""){
                            $html .='<li>'.$value->inspector.'</li>';
                        }
                    }
                } else {
                   $html .='<li>None</li>'; 
                }
    	        
            } else {
                $html .='<li>None</li>';
            }
	        $html .='</ul>';

	        $html .='</td>';
	        $html .='</tr>';
        
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100"></td>';
	        $html .='   <td><b>Major change/s since the previous inspection</b> <span class="italic_font">(if applicable)</span></td>';
	        $html .='</tr>';
            if($inspection_changes != ""){
    	        if(count($inspection_changes) > 0 ){
    	        	foreach ($inspection_changes as $key => $value) {
    		            $html .='<tr>';
    		            $html .='   <td width="100"></td>';
    		            $html .='   <td><ul type="square"><li>' . $value->changes . '</li></ul></td>';
    		            $html .='</tr>';
    		        }
    	        } else {
    	        	$html .='<tr>';
    	            $html .='   <td width="100"></td>';
    	            $html .='   <td><ul type="square"><li>None</li></ul></td>';
    	            $html .='</tr>';
    	        }
            } else {
                $html .='<tr>';
                $html .='   <td width="100"></td>';
                $html .='   <td><ul type="square"><li>None</li></ul></td>';
                $html .='</tr>';
            }
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


///////////////////////////////////////////////////////////////


	        $html .='<div nobr="true">';
	        $html .='<table class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2" class="gothic-title"><b>BRIEF REPORT OF THE AUDIT ACTIVITIES UNDERTAKEN:</b></td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>SCOPE: </b></td>';
	        $html .='</tr>';
	        
	        foreach ($scope as $key => $value) {
	            $html .='<tr nobr="true">';
	            $html .='   <td colspan="2">';
	            $html .='<u>'.$value->scope.'</u> '.$value->scope_details.' for:';
	            $html .='   <ul type="square">';
	            foreach ($value->products as $key => $value) {
	               $html .='    <li> '.$value->product_name.' </li>';
	            }
	            $html .='   </ul>';
	            $html .='</td>';
	            $html .='</tr>';
	            $html .='<tr><td colspan="2"></td></tr>';
	        }
	        
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"><b>AUDITED AREA(S): </b></td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">' . $report[0]->audited_areas . '</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">The audit did not cover the following areas, to be considered during the next audit:</td>';
	        $html .='</tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">' . $report[0]->areas_to_consider . '</td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';


///////////////////////////////////////////////////////////////

	        $html .='<div>';
	        $html .='<table class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"  class="gothic-title"><b>PERSONNEL MET DURING THE AUDIT:</b></td>';
	        $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">Issues and audit observations were discussed during the wrap-up meeting held on ' . $report[0]->wrap_up_date;
	        $html .='. The audit report will focus on the observations that were discussed during the audit.</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2">Present during the close-out meeting:</td>';
	        $html .='</tr>';
	        
	        $present_count = 0;
	        foreach ($present_during_meeting as $key => $value) {
	            $present_count++;
	            $html .='<tr>';
	            $html .='   <td colspan="1">';
	            $html .='&nbsp; &nbsp; &nbsp;'. $present_count . ') &nbsp; &nbsp; &nbsp; ' . $value->name;
	            $html .='   </td>';
	            $html .='   <td colspan="1">';
	            if($value->position != ""){
	            	$html .=        $value->position;
	            } else {
	            	$html .=        "None";
	            }
	            
	            $html .='   </td>';
	            $html .='</tr>';
	        }


	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"  class="gothic-title" >Other personnel met during the inspection:</td>';
	        $html .='</tr>';

	        if(count($get_personel_met) > 0){
	            $present_count = 0;
	            foreach ($get_personel_met as $key => $value) {
	                $present_count++;
	                $html .='<tr>';
	                $html .='   <td colspan="1">';
	                $html .='&nbsp; &nbsp; &nbsp;'. $present_count . ') &nbsp; &nbsp; &nbsp; ' . $value->name;
	                $html .='   </td>';
	                $html .='   <td colspan="1">';
	                if($value->designation != ""){
		            	$html .=        $value->designation;
		            } else {
		            	$html .=        "None";
		            }
	                $html .='   </td>';
	                $html .='</tr>';
	            }
	        } else {
	            $html .='<tr>';
	            $html .='   <td colspan="1">None';
	            $html .='   </td>';
	            $html .='   <td colspan="1">';
	            $html .='   </td>';
	            $html .='</tr>';
	        }
	        
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';


///////////////////////////////////////////////////////////

	        $html .='<div nobr="true">';
	        $html .='<table nobr="true" class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'>".'<b class="gothic-title">'."AUDIT TEAM'S FINDINGS AND OBSERVATIONS RELEVANT TO THE AUDIT</b></td>";
	        $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'>The audit consisted of an in-depth review of quality and GMP elements including, but not limited to the following:</td>";
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .="   <td colspan='2'>";
	        $html .='       <ul type="square">';
	        if(count($observation_findings) > 0){
	            foreach ($observation_findings as $key => $value) {
	            	$observation_x = substr(trim($value->Observation), 0, -1);
	            	if($observation_x != ""){
	               	 	$html .= "<li><b>" . $value->Element. "</b> - " . $observation_x. "</li>";
	            	} else {
	            		$html .= "<li><b>" . $value->Element. "</b> - None</li>";
	            	}
	            }
	        }
	        $html .="       </ul>";
	        $html .="   </td>";
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='<hr><br/>';
	        $html .='</div>';

	        $html .='<div nobr="true">';
	        $html .='<table nobr="true" class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'>". '<b class="gothic-title">' ."DEFINITION / CATEGORIZATION OF AUDIT OBSERVATIONS</b></td>";
	        $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
	        // $html .='<tr><td colspan="2"></td></tr>';

	        foreach ($observation as $key => $value) {
        		$html .='<tr nobr="true">';
                $html .="   <td colspan='2'><u>" . $value->category_name . "</u></td>";
                $html .='</tr>';
                $html .='<tr>';
                $html .="   <td colspan='2'>". '<i class="italic_font">' . $value->description . "</i></td>";
                $html .='</tr>';
                $html .="<tr><td colspan='2'></td></tr>";
	        }
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

//////////////////////////////

	        $html .='<div>';
	        $html .='<table class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .="   <td colspan='2'>". '<b  class="gothic-title">' . "LISTING OF AUDIT OBSERVATIONS AND CONCERNS</b>( in decreasing order of criticality )</td>";
	        $html .='</tr>';
	        $html .="<tr><td colspan='2'></td></tr>";

	        $ctrx = 1;
	        $critical = null;
	        $major = null;
	        $minor = null;
	        foreach ($details->Template_Elements as $key => $value) {
	            foreach ($value->questions as $k => $v) {
	            	switch ($v->category) {
	            		case 'Critical':
                            $critical .= $v->answer_no . ",";
                            break;
                        
                        case 'Major':
                            $major .= $v->answer_no . ",";
                            break;
                        
                        case 'Minor':
                            $minor .= $v->answer_no . ",";
                            break;
	            	}
	            }
	        }
	        $html .= '<style> 
	                    .italic {
	                        font-size: 12px;
	                        font-family: cgi;
	                    } 
	                </style>';
	        $html .="<tr nobr='true'><td><b>Critical Observations - </b>";
	        if($critical == null) {
	            $html .= '<span class="italic_font">None</span>';
	        } else {
	            $html .= '<span class="italic_font" style="text-align:justify;">Please refer to item no(s) ' . substr(trim($critical), 0, -1)  . '</span>';
	        }
	        $html .="</td></tr>";

	        $html .="<tr nobr='true'><td><b>Major Observations - </b>";
	        if($major == null) {
	            $html .= '<i class="italic_font">None</i>';
	        } else {
	            $html .= '<span class="italic_font" style="text-align:justify;">Please refer to item no(s) ' . substr(trim($major), 0, -1) . '</span>';
	        }
	        $html .="</td></tr>"; 

	        $html .="<tr nobr='true'><td><b>Minor Observations - </b>";
	        if($minor == null) {
	            $html .= '<i class="italic_font">None</i>';
	        } else {
	            $html .= '<span class="italic_font" style="text-align:justify;">Please refer to item no(s) ' . substr(trim($minor), 0, -1) . '</span>';
	        }
	        $html .="</td></tr>";
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .="<hr>";
	        $html .="<tr><td colspan='2'></td></tr>";
	        $html .='</table><br/>';
	        $html .='<br/>';
	        $html .='</div>';

//////////////////////////////////////////////

	        $html .='<div>';
	        $html .='<table class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"  class="gothic-title"><b>SUMMARY AND RECOMMENDATION</b></td>';
	        $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">There were';
	        if($analysis->no_critical < 1) { 
	            $html .=' <b><u>no</u></b> Critical, ';
	        } else { 
	            $html .= ' <b><u>' . $analysis->no_critical . '</u></b> Critical';
	        }
	        if($analysis->no_major < 1) { 
	            $html .=' <b><u>no</u></b> Major, ';
	        } else { 
	            $html .= ' <b><u>' .  $analysis->no_major . '</u></b> Major';
	        }
	        if($analysis->no_minor < 1) { 
	            $html .=' and <b><u>no</u></b> Minor, ';
	        } else { 
	            $html .= ' and <b><u>' . $analysis->no_minor . '</u></b> Minor ';
	        }
	        $html .= 'observations noted at this facility during the inspection.</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        if(count($details->Recommendation) > 0){
	            $html .='<tr nobr="true">';
	            $html .='   <td colspan="2">We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] observations which can be grouped into the following Quality System and/or GMP elements:</td>';
	            $html .='</tr>';
	        }
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">';
	        $html .='       <ul type="square">';
	        foreach ($details->Recommendation as $key => $value) {
	        	if($value->recommendation != ""){
	        		$html .='   <li><b>' . $value->element_name .'</b> - ' . $value->recommendation. '</li>';
	        	} else {
	        		$html .='   <li><b>' . $value->element_name .'</b> - None</li>';
	        	}
	        }
	        $html .='       </ul>';
	        $html .='   </td>';
	        $html .='</tr>';
	        $html .='</table><br/>';
	        $html .='</div>';

///////////////////////////////////////

	        $html .='<div>';
	        $html .='<table class="gothic">';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2"  class="gothic-title"><b>CONCLUSION</b></td>';
	        $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td colspan="2">Based on the results of this assessment, ' . $report[0]->name . ' is at this time considered:</td>';
	        $html .='</tr>';
	        $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr nobr="true">';
	        $html .='   <td width="100">Disposition: </td>';
	        $html .='   <td width="500">';
	        foreach ($details->Disposition_Product as $key => $value) {
	            $html .='<span><b>'.$value->disposition_name.'</b> ' . $value->disposition_label . ':</span><br>';
	            $html .='       <div>';
	            foreach ($value->disposition_products as $key => $value) {
	                $html .='        <span><img src="asset/img/bullet_black.png"> &nbsp; &nbsp;'.$value->product_name.' </span><br>';
	             }             
	             $html .='       </div>';
	        }
	        $html .='</td>';
	        $html .='</tr>';
	        $html .='</table class="gothic"><br/>';
	        $html .='</div>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='<tr nobr="true">';
	        $html .='   <td colspan="2" class="gothic-title"><b>OTHER ISSUES:</b></td>';
	        $html .='</tr>';
            $html .='<tr><td colspan="2"></td></tr>';
	        $html .='<tr>';
	        $html .='   <td>';
	        $html .='<ul type="square" class="gothic">';
	        foreach ($details->other_issue_audit as $key => $value){
	            $html .='<li> '.$value->other_issues_audit.' </li>';
	        }
	        $html .='</ul>';
	        $html .='   </td>';
	        $html .='</tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='</table><br/>';
	        

	        /////////////////////////////////
        	$html .='<br/>';
	        $html .='</div style="margin-bottom: 50px;">';
	        $html .='<br />';
	        $html .='<div nobr="true">';
	        $html .='<table style="border: 1px solid black;" class="gothic">';
	        $html .='   <tr style="border: 1px solid black;">'; 
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Name</b></td>';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Department / Designation</b></td>';
	        $html .='       <td style="text-align: center; border: 1px solid black;"><b>Date / Time</b></td>';
	        $html .='   </tr>';
	        $html .='   <tr><td style="border: 1px solid black;" colspan="3"><b>Prepared By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        if(isset($signature_stamp[0]->approved_date)) {
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'*<br></span></td>';
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($lead_stamp[0]->report_submission)) . '<br>' . date("H:i",strtotime($lead_stamp[0]->report_submission)) .'H<br></span></td>';
	        } else {
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br></span></td>';
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }

	        $html .='   </tr>';
	        foreach ($co_auditor as $key => $value) {
                //co auditor is reviewer
                $coauditor_name = $value->fname. ' ' .$value->lname;
                $reviewer_name = $reviewer_info[0]->fname. ' ' .$reviewer_info[0]->lname;
                $approver_name = $approver[0]->fname. ' ' .$approver[0]->lname;
                if($coauditor_name != $reviewer_name){
                    if($coauditor_name != $approver_name){
                        //check if submitted chanbges
                        $coauditorchanges = $this->global_model->get_data_query("tbl_report_coauditor_submission", "report_id = " . $id . " AND auditor_id = " . $value->auditor_id);
                        if(count($coauditorchanges) > 0){
                            $coauditor_stamp = $this->Audit_report_model->get_co_auditor_stamp($report[0]->report_id,$value->auditor_id);
                            if(isset($signature_stamp[0]->approved_date)) {
                                $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' .$value->fname. ' ' .$value->lname.'*<br></span></td>';
                                $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' . $value->designation . ' / Co-Auditor <br>' . $value->department . ", " .$value->company . '<br></span></td>';
                                if($coauditor_stamp != null){
                                    $html .='   <td style="border: 1px solid black; text-align: center; "><span><br>'. date("d F Y",strtotime($coauditor_stamp)) . '<br>' . date("H:i",strtotime($coauditor_stamp)) .'H<br></span></td>';
                                } else {
                                    $html .='   <td style="border: 1px solid black; text-align: center; "><span><br></span></td>';
                                }
                            } else {
                                $html .='<tr style="padding: 20px;border: 1px solid black;">';
                                $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' .$value->fname. ' ' .$value->lname.'<br></span></td>';
                                $html .='   <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>' . $value->designation . ' / Co-Auditor <br>' . $value->department . ", " .$value->company . '<br></span></td>';
                                $html .='   <td style="border: 1px solid black; text-align: center; "><span><br></span></td>';
                            }
                            $html .='</tr>';
                        }
                    }
                }
	        }
	        $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Reviewed By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        if(isset($signature_stamp[0]->approved_date)) { 
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '*<br></span></td>';
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$reviewer_info[0]->designation. '<br>' .$reviewer_info[0]->department. ', ' . $reviewer_info[0]->company . '<br></span></td>';
	        	if($signature_stamp[0]->review_date != null){
                    $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->review_date)) . '<br>' . date("H:i",strtotime($signature_stamp[0]->review_date)) .'H<br></span></td>';
	        	} else {
                    $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        	}
	        } else {
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '<br></span></td>';
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$reviewer_info[0]->designation. '<br>' .$reviewer_info[0]->department. ', ' . $reviewer_info[0]->company . '<br></span></td>'; 
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }
	        $html .='   </tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Approved By</b></td></tr>';
	        $html .='   <tr style="padding: 20px;border: 1px solid black;">';
	        if(isset($signature_stamp[0]->approved_date)) { 
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '*<br></span></td>';
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$reviewer_info[0]->designation. '<br>' .$approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->approved_date)) .'H<br></span></td>';
	        } else {
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
                $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$reviewer_info[0]->designation. '<br>' .$approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
	            $html .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
	        }
	        $html .='   </tr>';
	        $html .='</table>';
	        $html .='<b class="gothic">*This document was signed electronically and this is the manifestation of the electronic signature.</b>';
	        $html .='</div>';

	        $pdf->writeHTML($html,true,false,true,false,'');

	        switch ($action) {
	        	case 'I':
	        		$pdf->Output($report[0]->report_no.'-Audit_Report.pdf', 'I');
	        		break;
	        	
	        	case 'F':
	        		if (!file_exists('./json/export/approved/' . $id)) {
					    mkdir('./json/export/approved/' . $id, 0777, true);
					}

					if (!file_exists('./json/export/archive/' . $id)) {
					    mkdir('./json/export/archive/' . $id, 0777, true);
					}

	        		$pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Audit_Report.pdf', 'F');
	        		$pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Audit_Report.pdf', 'F');
	        		break;

	        	case 'D':
	        		$pdf->Output('Audit_Report '.$report[0]->report_no.'.pdf', 'D');
	        		break;
	        	
	        }
        	

		}



		function convertNumberToWord($num = false)
	    {
	        $num = str_replace(array(',', ' '), '' , trim($num));
	        if(! $num) {
	            return false;
	        }
	        $num = (int) $num;
	        $words = array();
	        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
	            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
	        );
	        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
	        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
	            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
	            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
	        );
	        $num_length = strlen($num);
	        $levels = (int) (($num_length + 2) / 3);
	        $max_length = $levels * 3;
	        $num = substr('00' . $num, -$max_length);
	        $num_levels = str_split($num, 3);
	        for ($i = 0; $i < count($num_levels); $i++) {
	            $levels--;
	            $hundreds = (int) ($num_levels[$i] / 100);
	            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ': '');
	            $tens = (int) ($num_levels[$i] % 100);
	            $singles = '';
	            if ( $tens < 20 ) {
	                $tens = ($tens ? ' ' . $list1[$tens] . ' ': '' );
	            } else {
	                $tens = (int)($tens / 10);
	                $tens = ' ' . $list2[$tens] . ' ';
	                $singles = (int) ($num_levels[$i] % 10);
	                $singles = ' ' . $list1[$singles] . ' ';
	            }
	            $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ': '' );
	        } //end for loop
	        $commas = count($words);
	        if ($commas > 1) {
	            $commas = $commas - 1;
	        }
	        return implode(' ', $words) ;
	    }


	    function executive_summary2()
	    {
	    	$id = $_GET['report_id'];
	    	$action = $_GET['action'];
			$string = file_get_contents("./listing/".$id.".json");
			$details = json_decode($string, false);

			$anastring = file_get_contents("./listing/".$id."_analysis.json");
			$analysis = json_decode($anastring, false);

			$report = $details->Report_Summary;
			$audit_dates = $details->Audit_Dates;
			$co_auditor = $details->Co_Auditors;
			$lead_auditor = $details->Lead_Auditor;
			$reviewer = $details->Reviewer;
			$approver = $details->Approver;
			$signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
			$country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');
            $lead_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_submission_date');


			$this->load->library("Pdf");
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->setReportNo($report[0]->report_no);
	        $pdf->setFooterRight("CONFIDENTIAL");
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '32', '20');
	        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $gothic = $pdf->AddFont('gothic');
	        $timesitalic = $pdf->AddFont('timesitalic');
	        $italic_font = $pdf->AddFont('helveticaBI');
	        $pdf->setListIndentWidth(30);
	        $pdf->AddPage();
	        $html = '';
	        $x = 'A';
	        $html .= '<style> 
	                    .gothic {
	                        font-size: 12px;
	                        font-family: gothic;
	                    } 
                        .gothic-title {
                            font-size: 13.5px !important;
                        }
                        .gothic-head {
                            font-size: 16px !important;
                        }
                        .tr-name {
                            padding : 0px;
                        }
	                </style>';
	        $html .='<div style="text-align:center"><center><label><h3 class="gothic-size">GMP AUDIT REPORT EXECUTIVE SUMMARY REPORT</h3></label></center></div>';
	        $html .='<div>';
	        $html .='<table class="gothic">';
            $html .='<tr>';
            $html .='  <td width="150">';
            $html .='Company Name';
            $html .='  </td>';
            $html .='  <td>';
            $html .=        $report[0]->name;
            $html .='  </td>';
            $html .='</tr>';
            $html .='<tr>';
            $html .='  <td width="150">';
            $html .='Site Address';
            $html .='  </td>';
            $html .='  <td>';
            $html .=        $report[0]->address1.', '.$report[0]->address2.' <br> '.$report[0]->address3.', '. strtoupper($country[0]->country_name);
            $html .='  </td>';
            $html .='</tr>';
	        $html .='</table>';
	        $html .='</div>';

////////////////////////////////////


	        $html .='<div  nobr="true">';
	        $html .='<label><b class="gothic-title">Summary Statement: </b></label>';
	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->Date),"F");
	            $audyear = date_format(date_create($value->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->Date),"d");
	            }
	        }

	        // $html .= substr($auddate,2) . " " . $audmonth . " " . $audyear;
	        $html .='<p class="gothic" style="text-align:justify;">An audit was conducted at the above facility on ' . substr($auddate,2) . " " .$audmonth . " " . $audyear.'. This document serves to attest that the original audit documentation has been reviewed and that this report is an accurate summary of the original audit documentation.</p><br><br>';
	        $html .='<table class="gothic">';
            $html .='<tr>';
            $html .='  <td width="150">';
            $html .='Lead Auditor';
            $html .='  </td>';
            $html .='  <td>';
            $html .=        $lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname;
            $html .='  </td>';
            $html .='</tr>';
            $auditteamcount = 1;
            foreach ($co_auditor as $key => $value){
                $html .='<tr>';
                if($auditteamcount == 1){
                    $html .='<td width="150">Audit Team Member/s</td>';
                } else {
                    $html .='<td width="150"></td>';
                }
                
                $html .='<td>'.$value->fname.' '.$value->lname.'</td>';
                $html .='</tr>';
                $auditteamcount++;
            }
	        $html .='</table>';
	        $html .='</div>';

////////////////////////////////////////

	        $html .='<div class="gothic">';
	        $html .='<label><b class="gothic-title">Scope: </b></label>';
	        foreach ($details->Scope_Product as $key => $value) {
	            $html .='<p><u>'.$value->scope.'</u> '.$value->scope_details.' for the following products:</p>';
	           $html .='<ul type="square">';
	            foreach ($value->products as $key => $value) {
	               $html .='<li style="margin-left: 200px;"> '.$value->product_name.' </li>';
	            }
	            $html .='</ul>';
	        }
	        $html .='</div>';

///////////////////////////////////////////////////

	        $html .='<div class="gothic">';
	        $html .='<p>The audit consisted of an in-depth review of their quality systems including, but not limited to the following: </p>';
	        $html .='<table>';
	        foreach ($details->Template_Elements as $key => $value) {
	            $html .='<tr>';
	            $html .='<td width="15%;"></td>';
	            $html .='<td width="85%;">';
	            $html .='<img src="asset/img/bullet.png" width="8"> &nbsp; '.$value->element_name;
	            $html .='</td>';
	            $html .='</tr>';
	        }
	        $html .='<tr>';
	        $html .='<td width="15%;"></td>';
	        $html .='<td width="85%;">';
	        $html .='<img src="asset/img/bullet.png" width="8"> &nbsp; License(s) Accreditation(s)/ Certification(s) held by supplier and verified during the audit. ';
	        $html .='</td>';
	        $html .='</tr>';
	        
	        $html .='</table>';
	        $html .='</div>';

//////////////////////////////////////


	        $html .='<div class="gothic">';
	        $html .='<p>The audit did not cover the following areas, to be covered during the next audit: </p>';
	        $html .='<ul type="square"><li>'.$report[0]->areas_to_consider.'</li></ul>';
	        $html .='<hr><br>';
	        $html .='</div>';

	        $html .='<div  class="gothic">';
	        $html .='<p>The standard(s) used during the audit include:</p>';
	        $html .='<ul type="square">';
	        foreach ($details->Template as $key => $value){
	            $html .='<li> '.$value->standard_name.' </li>';
	        }
	        $html .='</ul>';
	        $html .='</div>';

////////////////////////////////////////////

	if($analysis->no_critical != ""){
		$critical = $analysis->no_critical;
	} else {
		$critical = 0;
	}

	if($analysis->no_major != ""){
		$no_major = $analysis->no_major;
	} else {
		$no_major = 0;
	}

	if($analysis->no_minor != ""){
		$no_minor = $analysis->no_minor;
	} else {
		$no_minor = 0;
	}


        $html .='<div  class="gothic" nobr="true">';
        $html .='<label><b class="gothic-title"> Audit Results: </b></label>';
        $html .='<p>The following are the observations, categorized based on criticality, noted during '. substr($auddate,2) . " " . $audmonth . " " . $audyear.' inspection.</p>';
        $html .='<table border="1" style="100%" cellpadding="3">';
        $html .='<tr style="background-color:#ccc;">';
        $html .='<td style="font-weight:bold;text-align:center;">No. of Critical <br/> Observations</td>';
        $html .='<td style="font-weight:bold;text-align:center;">No. of Major <br/> Observations </td>';
        $html .='<td style="font-weight:bold;text-align:center;">No. of Minor <br/> Observations</td>';
        $html .='</tr>';
        $html .='<tr>';
        $html .='<td style="text-align:center;">'.$critical.'</td>';
        $html .='<td style="text-align:center;">'.$no_major.'</td>';
        $html .='<td style="text-align:center;">'.$no_minor.'</td>';
        $html .='</tr>';
        $html .='</table>';
        $html .='</div>';
        $html .='<div>';
        $html .='<label><b class="gothic-title">Observations: </b></label>';
        $html .='<p style="text-align:justify;" class="gothic">We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] Observations which can be grouped into the following Quality System and/or GMP elements:</p>';
        $html .='<ul type="square" class="gothic">';
        if(count($details->Recommendation) > 0){
        	foreach ($details->Recommendation as $k => $v) {
	            $html .='<li ><b >'.$v->element_name.'</b> - '.$v->recommendation.'</li>';
	        }
        } else {
        	$html .='<li>None</li>';
        }
        
        $html .='</ul>';
        $html .='</div>';

/////////////////////////////////////


        $html .='<div class="gothic">';
        $html .='<p>Based on the results of this asessement, <u>'.$report[0]->name.'</u> is at this time considered: </p>';
        $html .='<table>';
        $ctr1 = 1;
        foreach ($details->Disposition_Product as $key => $value) {
            $html .='<tr>';
            $html .='<td width="10%"></td>';
            if($ctr1 == 1){
                $html .='<td width="20%"><b>Disposition: </b></td>';
            } else {
                $html .='<td width="20%"></td>';
            }
            
            $html .='<td width="70%">';
            $html .='<p><b>'.$value->disposition_name.' ' . $value->disposition_label . '</b></p><br>';
            $html .='<div>';
            foreach ($value->disposition_products as $k => $v) {
                $html .='        <span>&nbsp;&nbsp;&nbsp;&nbsp;<img src="asset/img/bullet_black.png"> &nbsp; &nbsp; '. $v->product_name.' </span><br>';
             }             
            $html .='</div>';
            $html .='<div>&nbsp;</div>';
            $html .='</td>';
            $html .='</tr>';
            $ctr1++;
            
        }
        $html .='</table>';
        $html .='</div>';
        $html .='<div class="gothic">';
        // if($report[0]->closure_date == '' || $report[0]->closure_date == null){
        //     $close_date = 'N/A';
        // }else{
        //     $close_date = $report[0]->closure_date;
        // }
        // $html .='<p><b>Audit Closure Date, if applicable: '.$close_date.'</b></p>';
        $html .='<p><b>OTHER ISSUES:</b></p>';
        $html .='<ul type="square">';
        foreach ($details->other_issue_exec as $key => $value){
            $html .='<li> '.$value->other_issues_executive.' </li>';
        }
        $html .='</ul>';
        $html .='<hr><br>';
        $html .='</div>';
        $pdf->writeHTML($html,true,false,true,false,'');


/////////////////////////////////////
        
        $pdf->AddPage();
        $pdf->SetXY(25,170);
        $html2 ='';        
        $html2 .= '<style> 
                    b {
                        font-size: 12px;
                        font-family: gothic;
                    } 
                    
                    td {
                        font-size: 12px;
                        font-family: gothic;
                    } 

                    li {
                        font-size: 12px;
                        font-family: gothic;
                    } 

                    .italic_font {
                        font-weight: 700;
                        font-size: 12px;
                        font-family: timesitalic;
                    } 
                </style>';

    	$html2 .='<div nobr="true">';
        $html2 .='<table style="border: 1px solid black;">';
        $html2 .='   <tr style="border: 1px solid black;">';
        $html2 .='       <td style="text-align: center; border: 1px solid black;"><b>Name</b></td>';
        $html2 .='       <td style="text-align: center; border: 1px solid black;"><b>Department / Designation</b></td>';
        $html2 .='       <td style="text-align: center; border: 1px solid black;"><b>Date / Time</b></td>';
        $html2 .='   </tr>';
        $html2 .='   <tr><td style="border: 1px solid black;" colspan="3"><b>Prepared By</b></td></tr>';
        $html2 .='   <tr style="padding: 20px;border: 1px solid black;">';
        if(isset($signature_stamp[0]->approved_date)) {
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'*<br></span></td>';
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($lead_stamp[0]->reviewer)) . '<br>' . date("H:i",strtotime($lead_stamp[0]->reviewer)) .'H<br></span></td>';
        } else {
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br></span></td>';
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
             $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
        }

        $html2 .='   </tr>';
        $html2 .='   <tr style="padding: 20px;border: 1px solid black;"><td style="border: 1px solid black;" colspan="3"><b>Approved By</b></td></tr>';
        $html2 .='   <tr style="padding: 20px;border: 1px solid black;">';
        if(isset($signature_stamp[0]->approved_date)) { 
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '*<br></span></td>';
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$approver[0]->designation. '<br>' .  $approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->approved_date)) .'H<br></span></td>';
        } else {
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br>'.$approver[0]->designation. '<br>' .  $approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
            $html2 .='       <td style="padding: 20px;text-align: center; border: 1px solid black;"><span><br><br></span></td>';
        }
        $html2 .='   </tr>';
        $html2 .='</table>';
        $html2 .='<b>*This document was signed electronically and this is the manifestation of the electronic signature.</b>';
        $html2 .='</div>';
        $html2 .='</div>';

        $html2 .='<div>';
        $html2 .='<b class="italic_font">Distribution List (include the Supplier Quality representative(s) from ALL locations of use associated with this supplier)</b>';
        $html2 .='<ul type="square">';
        foreach ($details->Distribution as $key => $value){
            $html2 .='<li>'.$value->distribution_name.'</li>';
        }
        if(!empty($details->Other_Distribution)){
            foreach ($details->Other_Distribution as $key => $value) {
                $html2 .='<li>'.$value->other_distribution.'</li>';
            }
        }
        $html2 .='<li>Corporate GMP Department, UNILAB</li>';
        $html2 .='</ul>';
        $html2 .='</div>';
        $pdf->writeHTML($html2,true,false,true,false,'');     

	        switch ($action) {
		    	case 'I':
		    		 $pdf->Output($report[0]->report_no.'-Executive_Report.pdf', 'I');
		    		break;
		    	
		    	case 'F':
		    		if (!file_exists('./json/export/approved/' . $id)) {
					    mkdir('./json/export/approved/' . $id, 0777, true);
					}

					if (!file_exists('./json/export/archive/' . $id)) {
					    mkdir('./json/export/archive/' . $id, 0777, true);
					}

	        		$pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Executive_Report.pdf', 'F');
	        		$pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Executive_Report.pdf', 'F');
					break;

		    	case 'D':
		    		$pdf->Output('Executive_Report '.$report[0]->report_no.'.pdf', 'D');
		    		break;
		    	
		    }
	    }


	    function annexure_pdf()
	    {
	    	$id = $_GET['report_id'];
	    	$action = $_GET['action'];
			$string = file_get_contents("./listing/".$id.".json");
			$details = json_decode($string, false);

			$anastring = file_get_contents("./listing/".$id."_analysis.json");
			$analysis = json_decode($anastring, false);

			$report = $details->Report_Summary;
			$audit_dates = $details->Audit_Dates;
			$lead_auditor = $details->Lead_Auditor;
			$reviewer = $details->Reviewer;
			$approver = $details->Approver;
			$signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
			$country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');


			$this->load->library("Pdf");
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	        $pdf->setReportNo($report[0]->report_no);
            $pdf->SetTitle("Annexure : " . $report[0]->report_no );
            $pdf->setStatus((int)$report[0]->status );
	        $pdf->setFooterRight("CONFIDENTIAL");
	        $pdf->setCenter(180);
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '32', '20');
	        $pdf->SetMargins( 10, 10, 10, 10);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $pdf->SetFont('gothic', '', 10);
	        $pdf->SetPrintHeader(false);    
	        $pdf->AddPage('L', 'LETTER');
	        $html = '';
	        $x = 'A';
	        
	        $html .='<div style="text-align:center">
	        <center><label><h2>ANNEXURE - GMP AUDIT DETAILS</h2></label></center>
	        </div>';
	        $html .='<table style="100%" border="1" cellpadding="">';
	        $html .='<tr>';
	        $html .='<td width="10%"><label><b> Audited Site: </b></label></td>';
	        $html .='<td width="50%"><label> '.$report[0]->name.'</label></td>';
	        $html .='<td width="15%"><label><b> Audit Report No: </b></label></td>';
	        $html .='<td width="25%"><label> '.$report[0]->report_no.'</label></td>';
	        $html .='</tr>';
	        $html .='<tr>';
	        $html .='<td><label><b> Site Address: </b></label></td>';
	        $html .='<td><label> '.$report[0]->address1.", ".$report[0]->address2. " " .$report[0]->address3. ", ".strtoupper($country[0]->country_name).'</label></td>';
	        $html .='<td><label><b> Audit Date(s): </b></label></td>';
	       
           //AUDIT DATE

            $audit_date_html = $details->Audit_Dates_Formatted;
	        $html .='<td><label> '. $audit_date_html.'</label></td>';
	        $html .='</tr>';
	        $html .='</table>';

///////////////////////

	        $html .='<table><tr><td></td></tr></table>';
	        $html .='<table height="30px" style="100%" border="1" cellpadding="3px" >';
	        $html .='<tr>';
	        $html .='<td width="5%" align="center">';
	        $html .='	<table height="1">';
	        $html .='	<tr><td>';
	        $html .='		<h4><b>No. </b></h4>';
	        $html .='	</td></tr>';
	        $html .='	</table>';
	        $html .='</td>';
	        $html .='<td width="10%" align="center">';
	        $html .='	<table height="1">';
	        $html .='	<tr><td>';
	        $html .='		<h4><b>CATEGORY</b></h4>';
	        $html .='	</td></tr>';
	        $html .='	</table>';
	        $html .='</td>';
	        $html .='<td width="20%" align="center">';
	        $html .='	<table height="1">';
	        $html .='	<tr><td>';
	        $html .='		<h4><b>AUDIT OBSERVATIONS</b></h4>';
	        $html .='	</td></tr>';
	        $html .='	</table>';
	        $html .='</td>';
	        $html .='<td width="20%" align="center">';
	        $html .='	<table height="1">';
	        $html .='	<tr><td>';
	        $html .='		<h4><b>ROOT CAUSE</b></h4>';
	        $html .='	</td></tr>';
	        $html .='	</table>';
	        $html .='</td>';
	        $html .='<td width="20%" align="center">';
	        $html .='	<table height="1">';
	        $html .='	<tr><td>';
	        $html .='		<h4><b>CORRECTIVE ACTION/ PREVENTIVE ACTION</b></h4>';
	        $html .='	</td></tr>';
	        $html .='	</table>';
	        $html .='</td>';
	        $html .='<td width="10%" align="center">';
	        $html .='	<table height="1">';
	        $html .='	<tr><td>';
	        $html .='		<h4><b>TARGET DATE</b></h4>';
	        $html .='	</td></tr>';
	        $html .='	</table>';
	        $html .='</td>';
	        $html .='<td width="15%" align="center">';
	        $html .='	<table height="1">';
	        $html .='	<tr><td>';
	        $html .='		<h4><b>REMARKS</b></h4>';
	        $html .='	</td></tr>';
	        $html .='	</table>';
	        $html .='</td>';
	        // $html .='<td width="10%" align="center"><label><b><br>CATEGORY</b></label></td>';
	        // $html .='<td width="22%" align="center"><label><b><br>AUDIT OBSERVATIONS</b></label></td>';
	        // $html .='<td width="20%" align="center"><label><b><br>ROOT CAUSE</b></label></td>';
	        // $html .='<td width="20%" align="center"><label><b>CORRECTIVE ACTION/ PREVENTIVE ACTION</b></label></td>';
	        // $html .='<td width="10%" align="center"><label><b><br>TARGET DATE</b></label></td>';
	        // $html .='<td width="15%" align="center"><label><b><br>REMARKS</b></label></td>';
	        $html .='</tr>';
	        $html .='</table>';

/////////////////////////////////////////

	        $html .='<table><tr><td></td></tr></table>';
	        foreach ($details->Annexure_Observation as $key => $value) {
	            $html .='<table border="1" cellpadding="3px">';
	            $html .='<tr  style="background-color:#ccc;"><td colspan="7" ><b>' . $value->Element .  '</b></td></tr>';
	            if(count($value->Observation) > 0){
	            	foreach ($value->Observation as $a => $b) {
		                $html .='<tr>';
		                $html .='<td width="5%" align="center">'.$b->no.'</td>';
		                $html .='<td width="10%" align="center">'.$b->category.'</td>';
		                $html .='<td width="20%">'.$b->observation.'</td>';
                        if($b->category != "-"){
                            $html .='<td width="20%" align="center"></td>';
                            $html .='<td width="20%" align="center"></td>';
                            $html .='<td width="10%" align="center"></td>';
                            $html .='<td width="15%" align="center"></td>';
                        } else {
                            $html .='<td width="20%" align="center">-</td>';
                            $html .='<td width="20%" align="center">-</td>';
                            $html .='<td width="10%" align="center">-</td>';
                            $html .='<td width="15%" align="center">-</td>';
                        }
		                
		                $html .='</tr>';
		            }
	            } else {
	                $html .='<tr>';
	                $html .='<td width="5%" align="center">-</td>';
	                $html .='<td width="10%" align="center">-</td>';
	                $html .='<td width="20%">None Observed</td>';
	                $html .='<td width="20%" align="center">-</td>';
	                $html .='<td width="20%" align="center">-</td>';
	                $html .='<td width="10%" align="center">-</td>';
	                $html .='<td width="15%" align="center">-</td>';
	                $html .='</tr>';
	            }
	            
	            $html .='</table>';
	        }

	        $html .='<table><tr><td></td></tr></table>';

/////////////////////////////////////////

			// $html .='<div>';
	  //       $html .='<table>';
	  //       $html .='<tr><td></td><td></td></tr>';
	  //       $html .='<tr>';
	  //       $html .='<td><label>Prepared and Reviewed by: </label></td>';
	  //       $html .='<td><label>Approved by: </label></td>';
	  //       $html .='</tr>';
	  //       $html .='<tr><td></td><td></td></tr>';
	  //       $html .='<tr><td></td><td></td></tr>';
	  //       $html .='<tr>';
	  //       if(isset($signature_stamp[0]->approved_date)) { 
	  //           $html .='<td style="text-align:center;">'. date("d F Y",strtotime($report[0]->create_date)) . ' ' . date("H:i",strtotime($report[0]->create_date)) . 'H<br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br>'.$lead_auditor[0]->designation.'<br>'.$lead_auditor[0]->department.'</td>';
	  //       } else {
	  //           $html .='<td style="text-align:center;">'. $lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br>'.$lead_auditor[0]->designation.'<br>'.$lead_auditor[0]->department.'</td>';
	  //       }
	  //       if(isset($signature_stamp[0]->approved_date)) { 
	  //           $html .='<td style="text-align:center;">'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). ' ' . date("H:i",strtotime($signature_stamp[0]->approved_date)) . 'H<br>'.$approver[0]->fname . ' ' . $approver[0]->lname.'<br>'.$approver[0]->designation.'<br>'.$approver[0]->department.'</td>';
	  //       } else {
	  //           $html .='<td style="text-align:center;">'.$approver[0]->fname . ' ' . $approver[0]->lname .'<br>'.$approver[0]->designation.'<br>'.$approver[0]->department.'</td>';
	  //       }

	         
	  //       $html .='</tr>';
	  //       $html .='</table>';
	  //       $html .='</div>';

	        $pdf->writeHTML($html,true,false,true,false,'');     

	        switch ($action) {
		    	case 'I':
		    		 $pdf->Output($report[0]->report_no.'-Annexure.pdf', 'I');
		    		break;
		    	
		    	case 'F':
		    		if (!file_exists('./json/export/approved/' . $id)) {
					    mkdir('./json/export/approved/' . $id, 0777, true);
					}
					if (!file_exists('./json/export/archive/' . $id)) {
					    mkdir('./json/export/archive/' . $id, 0777, true);
					}
	        		$pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Annexure.pdf', 'F');
	        		$pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Annexure.pdf', 'F');
		    		break;

		    	case 'D':
		    		$pdf->Output('Annexure '.$report[0]->report_no.'.pdf', 'D');
		    		break;
		    	
		    }

       	 	
		}

		function raw_data2()
		{
			$id = $_GET['report_id'];
			$action = $_GET['action'];
			$string = file_get_contents("./listing/".$id.".json");
			$details = json_decode($string, false);

			$anastring = file_get_contents("./listing/".$id."_analysis.json");
			$analysis = json_decode($anastring, false);

			$report = $details->Report_Summary;
			$audit_dates = $details->Audit_Dates;
			$co_auditor = $details->Co_Auditors;
			$lead_auditor = $details->Lead_Auditor;
			$template = $details->Template;
			$reviewer = $details->Reviewer;
			$signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
			$country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');

			$this->load->library("Pdf");
	        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->setStatus((int)$report[0]->status);
	        $pdf->setFooterRight("CONFIDENTIAL");
	        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
	        $pdf->setFooterData(array(0,64,0), array(0,64,128));
	        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	        $pdf->SetMargins('20', '32', '20');
	        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	        $pdf->SetFont('gothic', '', 10);
	        $pdf->AddPage();

	        $html = '';
	        $html .='<style>.odd{ background-color:#ccc; } .even { background-color: #bbb;} .bordered { border: .5px solid black;} .bordered-white {border: 1px solid #fff;} .spacer { margin-top: 5px; margin-bottom: 5px; }</style>';
	        $html .='<div>';
	        $html .='<table>';
	        $html .='   <tr>';
	        $html .='       <td width="80%" style="text-align: center;">';
	        $html .='           <label style="vertical-align:middle;"><h3>GMP RAW DATA REPORT</h3></label>';
	        $html .='       </td>';
	        $html .='       <td width="20%" class="bordered" style="text-align: center;">';
	        $html .='           <center>';
	        $html .='               <label><h2>Rating:<br />';
	        $html .='               ' . (number_format($analysis->rating,2) + 0) . '%</h2></label>';
	        $html .='               <label>% Coverage:' . (number_format($analysis->coverage,2) + 0) . '</label><br>';
	        $html .='           </center>';
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='</table>';
	        $html .='</div>';

/////////////////////////////////////////
	        $html .='<table>';
	        $html .='   <tr>';
	        $html .='       <td width="150">Company Name</td>';
	        $html .='       <td width="200">: ' . $report[0]->name . '</td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="100">Date of Audit</td>';
	        $html .='       <td width="200">: ';
	        
	        $len = count($audit_dates);
	        $auddate = "";
	        $audyear = "";
	        $audmonth = "";
	        foreach ($audit_dates as $key => $value) {
	            $audmonth = date_format(date_create($value->Date),"F");
	            $audyear = date_format(date_create($value->Date),"Y");
	            if ($key == $len - 1) {
	               $auddate .= ' & ' . date_format(date_create($value->Date),"d");
	            } else {
	               $auddate .= ', ' . date_format(date_create($value->Date),"d");
	            }
	        }
	        $html .= $audmonth . " " . substr($auddate,2) . " " . $audyear;

	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='   <tr>';
	        $html .='       <td width="150">Site Address</td>';
	        $html .='       <td width="500">: '.$report[0]->address1. ', ' .$report[0]->address2.' '.$report[0]->address3.', '.strtoupper($country[0]->country_name).'</td>';
	        $html .='       <td width="1"></td>';
	        $html .='       <td width="1"></td>';
	        $html .='       <td width="1"></td>';
	        $html .='   </tr>';
	        $html .='   <tr>';
	        $html .='       <td width="150">Lead Auditor</td>';
	        $html .='       <td width="300">: ' . $lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname . '</td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="10"></td>';
	        $html .='   </tr>';
	        $html .='   <tr>';
	        $html .='       <td width="150">Co-Auditor</td>';
	        $html .='      	<td width="300">:';
	        foreach ($co_auditor as $key => $value) {
	            $html .= '  <span>' . $value->fname. ' ' .$value->lname.'</span><br>';
	        }
	        $html .='      	</td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="10"></td>';
	        $html .='       <td width="10"></td>';
	        $html .='   </tr>';
	        $html .='</table>';

	/////////////////////////////

	        $html .='<div>';
	        $html .='<table>';
	        $html .='   <tr>';
	        $html .='       <td>';
	        $html .='           <b>Product type: </b>' . $template[0]->classification_name;
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='   <tr>';
	        $html .='       <td>';
	        $html .='           <b>Products: </b>';
	        $html .='           <ol>';
	        $array = array();
	        foreach ($details->Scope_Product as $key => $value) {
	            foreach ($value->products as $k => $v) {
	             	$html .='    <li> '.$v->product_name.' </li>';
	        	}  
	        }
	         
	        $html .='           </ol>';
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='<tr><td></td></tr>';
	        $html .='   <tr>';
	        $html .='       <td>';
	        $html .='           <b>Standard/Reference: </b>'. $template[0]->standard_name;
	        $html .='       </td>';
	        $html .='   </tr>';
	        $html .='</table>';
	        $html .='</div>';

///////////////////////////////////////////

	        $letters = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

	        foreach ($details->Template_Elements as $key => $value) {
	            $html .='<label><h3>Element ' . $letters[$value->order - 1] . ' - ' . $value->element_name . '</h3></label>';  
	            $html .='<table cellpadding="10">';
	            $html .='   <tr style="background-color: #4594cd;" > ';
	            $html .='       <td class="bordered" style="text-align: center;" width="10%"></td>';
	            $html .='       <td class="bordered" style="text-align: center;" width="30%"><h4 style="color: #fff;">Questions</h4></td>';
	            $html .='       <td class="bordered" style="text-align: center;" width="15%"><h4 style="color: #fff">Answer</h4></td>';
	            $html .='       <td class="bordered" style="text-align: center;" width="30%"><h4 style="color: #fff">Remarks</h4></td>';
	            $html .='       <td class="bordered" style="text-align: center;" width="15%"><h4 style="color: #fff">Category</h4></td>';
	            $html .='   </tr>';
	            $question_count = 0;
	            foreach ($value->questions as $a => $b) {
	                $question_count++;
	                $html .='   <tr> ';
	                $html .='       <td class="bordered" style="text-align: center;">'. $letters[$value->order - 1]. $question_count . '</td>';
	                $html .='       <td class="bordered">' . $b->question . '</td>';
	                $html .='       <td class="bordered" style="text-align: center;">' . $b->answer_name . '</td>';
	                $html .='       <td class="bordered">' . $b->answer_details . '</td>';
	                $html .='       <td class="bordered" style="text-align: center;">' . $b->category . '</td>';
	                $html .='   </tr>';
	            }
	            $html .='</table>';
	        }
	        $pdf->writeHTML($html,true,false,true,false,'');     

	        switch ($action) {
		    	case 'I':
		    		 $pdf->Output($report[0]->report_no.'-Raw_Data.pdf', 'I');
		    		break;
		    	
		    	case 'F':
		    		if (!file_exists('./json/export/approved/' . $id)) {
					    mkdir('./json/export/approved/' . $id, 0777, true);
					}
					if (!file_exists('./json/export/archive/' . $id)) {
					    mkdir('./json/export/archive/' . $id, 0777, true);
					}

	        		$pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Raw_Data.pdf', 'F');
	        		$pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Raw_Data.pdf', 'F');
		    		break;

		    	case 'D':
		    		$pdf->Output('Raw_Data '.$report[0]->report_no.'.pdf', 'D');
		    		break;
		    	
		    }


		}


		function annexure_doc()
		{
			$id = $_GET['report_id'];
			$action = $_GET['action'];
			$string = file_get_contents("./listing/".$id.".json");
			$details = json_decode($string, false);

			$anastring = file_get_contents("./listing/".$id."_analysis.json");
			$analysis = json_decode($anastring, false);

			$report = $details->Report_Summary;
			$audit_dates = $details->Audit_Dates;
			$lead_auditor = $details->Lead_Auditor;
			$reviewer = $details->Reviewer;
			$approver = $details->Approver;
			$signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
			$country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');

            //AUDIT DATE
            $audit_date_formatted = $details->Audit_Dates_Formatted;

			$phpWord = new \PhpOffice\PhpWord\PhpWord();

            $phpWord->getCompatibility()->setOoxmlVersion(14);
            $phpWord->getCompatibility()->setOoxmlVersion(15);

            
            $section = $phpWord->createSection(array('orientation'=>'landscape','marginLeft' => 500, 'marginRight' => 500, 'marginTop' => 600, 'marginBottom' => 600));
            
            // Create WAtermark
            // if((int)$report[0]->status == 0){
            //     $header = $section->createHeader();
            //     $header->addWatermark( __DIR__.'./../../asset/img/draft.png', array(
            //         'width' => 600,
            //         'height' => 600,
            //         'positioning' => 'absolute',
            //         'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
            //         'posHorizontalRel' => 'margin',
            //         'posVerticalRel' => 'line',
            //     ));
            // }
            

            $targetFile = "./assets/word/";
            $filename = 'Annexure_report ' . $report[0]->report_no  .  '.docx';

            $tableStyle = array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  );
            $styleCell = array('borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black' );
            $noSpace = array('textBottomSpacing' => -1);        
            
            $headcell = [ 'bgColor'=>'#6086B8','gridSpan' => 7, 'borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black'];
            $headcell_blank = [ 'gridSpan' => 7];

            $HeaderFont = array('bold'=>true, 'italic'=> false, 'size'=>16, 'name' => 'Century Gothic', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
            $BoldFont = array('bold'=>true, 'italic'=> false, 'size'=>12, 'name' => 'Century Gothic', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
            $Font = array('bold'=>false, 'italic'=> false, 'size'=>12, 'name' => 'Century Gothic', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
            $CapsFont = array('bold'=>true, 'allCaps'=>true,'italic'=> false, 'size'=>12, 'name' => 'Century Gothic', 'afterSpacing' => 0, 'Spacing'=> 2, 'cellMargin'=>0);
            $Center_text = ['align' => \PhpOffice\PhpWord\Style\Alignment::ALIGN_CENTER];
            $spacing = array('align'=>'center','alignment'=>'center','spaceAfter' => 20, 'spaceBefore' => 20);

            $section->addText('ANNEXURE - GMP AUDIT REPORT',$HeaderFont, $Center_text);

            $table = $section->addTable('Header-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(2500,$styleCell)->addText('Audited Site:',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->name),$Font,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(2500,$styleCell)->addText('Audit Report No.:',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->report_no),$Font,array('align' => 'left', 'spaceAfter' => 5));

            $table->addRow();
            $table->addCell(2500,$styleCell)->addText('Site Address:',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($report[0]->address1. ', ' .$report[0]->address2 . ' ' . $report[0]->address3.', '.strtoupper($country[0]->country_name)),$Font,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(2500,$styleCell)->addText('Audit Date(s):',$BoldFont,array('align' => 'left', 'spaceAfter' => 5));
            $table->addCell(6000,$styleCell)->addText($this->text_formatter($audit_date_formatted),$Font,array('align' => 'left', 'spaceAfter' => 5));
 
            $section->addTextBreak();
            $table = $section->addTable('2nd-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(500,$styleCell)->addText('NO.',$BoldFont,$spacing,$Center_text);
            $table->addCell(2500,$styleCell)->addText('CATEGORY',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('AUDIT OBSERVATIONS',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('ROOT CAUSE',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('CORRECTIVE ACTION \ PREVENTIVE ACTION',$BoldFont,$spacing,$Center_text);
            $table->addCell(2000,$styleCell)->addText('TARGET DATE',$BoldFont,$spacing,$Center_text);
            $table->addCell(4000,$styleCell)->addText('REMARKS',$BoldFont,$spacing,$Center_text);
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(null, $headcell_blank);


                foreach ($details->Annexure_Observation as $key => $value) {
                    $table->addRow(-0.5, array('exactHeight' => -5));
                    $table->addCell(null, $headcell)->addText($this->text_formatter($value->Element),$CapsFont,array('align' => 'left', 'spaceAfter' => 5));
                    if(count($value->Observation) > 0){
                    	foreach ($value->Observation as $a => $b) {
	                        $table->addRow(-0.5, array('exactHeight' => -5));
	                        $table->addCell(500,$styleCell)->addText($b->no,$Font,$Center_text);
	                        $table->addCell(2500,$styleCell)->addText($this->text_formatter($b->category),$Font,$Center_text);
	                        $table->addCell(4000,$styleCell)->addText($this->text_formatter($b->observation),$Font);
                            if($b->category != "-"){
    	                        $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
    	                        $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
    	                        $table->addCell(2000,$styleCell)->addText('',$Font,$Center_text);
    	                        $table->addCell(4000,$styleCell)->addText('',$Font,$Center_text);
                            } else {
                                $table->addCell(4000,$styleCell)->addText('-',$Font,$Center_text);
                                $table->addCell(4000,$styleCell)->addText('-',$Font,$Center_text);
                                $table->addCell(2000,$styleCell)->addText('-',$Font,$Center_text);
                                $table->addCell(4000,$styleCell)->addText('-',$Font,$Center_text);
                            }
	                    }
                    } else {
                        $table->addRow(-0.5, array('exactHeight' => -5));
                        $table->addCell(500,$styleCell)->addText("-",$Font,$Center_text);
                        $table->addCell(2500,$styleCell)->addText($this->text_formatter("-"),$Font,$Center_text);
                        $table->addCell(4000,$styleCell)->addText($this->text_formatter("None Observed"),$Font);
                        $table->addCell(4000,$styleCell)->addText('-',$Font,$Center_text);
                        $table->addCell(4000,$styleCell)->addText('-',$Font,$Center_text);
                        $table->addCell(2000,$styleCell)->addText('-',$Font,$Center_text);
                        $table->addCell(4000,$styleCell)->addText('-',$Font,$Center_text);
                    }
                    
                }

                $section->addTextBreak();
                // $table = $section->addTable('2nd-Table',array('borderSize' => 1, 'borderColor' => '999999', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0  ));
                
                // $table->addRow(-0.5, array('exactHeight' => -5));
                // $table->addCell(10000,$headcell_blank)->addText('Prepared and reviewed by:',$Font);
                // $table->addCell(10000,$headcell_blank)->addText('Approved by:',$Font);

                // $table->addRow();
                // if(isset($signature_stamp[0]->approved_date)) {
                //     $table->addCell(10000,$headcell_blank)->addText($this->text_formatter('<w:br />' . date("d F Y",strtotime($report[0]->create_date)) . ' ' . date("H:i",strtotime($report[0]->create_date))) . 'H<w:br />' . $this->text_formatter($lead_auditor[0]->fname . " " . $lead_auditor[0]->lname) . '<w:br />' . $this->text_formatter($lead_auditor[0]->designation) . '<w:br />' . $this->text_formatter($lead_auditor[0]->department),$Font,$Center_text,array('spaceAfter' => 5));
                // } else {
                //     $table->addCell(10000,$headcell_blank)->addText('<w:br /><w:br />' . $this->text_formatter($lead_auditor[0]->fname . " " . $lead_auditor[0]->lname) . '<w:br />' . $this->text_formatter($lead_auditor[0]->designation) . '<w:br />' . $this->text_formatter($lead_auditor[0]->department),$Font,$Center_text,array('spaceAfter' => 5));
                // }

                // if(isset($signature_stamp[0]->approved_date)) { 
                //     $table->addCell(10000,$headcell_blank)->addText('<w:br />' . $this->text_formatter(date("d F Y",strtotime($signature_stamp[0]->approved_date)). ' ' . date("H:i",strtotime($signature_stamp[0]->approved_date))) . 'H<w:br />' . $this->text_formatter($approver[0]->fname . " " . $approver[0]->lname) . '<w:br />' . $this->text_formatter($lead_auditor[0]->designation) . '<w:br />' . $this->text_formatter($lead_auditor[0]->department),$Font,$Center_text);
                // } else {
                //    $table->addCell(10000,$headcell_blank)->addText('<w:br /><w:br />' . $this->text_formatter($approver[0]->fname . " " . $approver[0]->lname) . '<w:br />' . $this->text_formatter($approver[0]->designation) . '<w:br />' . $this->text_formatter($approver[0]->department),$Font,$Center_text);
                // }

                $footer = $section->createFooter();

                $footer->addPreserveText( "\t\t\t\t\t\t\t\t\t\t" . $report[0]->report_no, null, array('align'=>'left'));
                $footer->addLine(['weight' => 1, 'width' => 1000, 'height' => 0]);
                $footer->addPreserveText('Page {PAGE} of {NUMPAGES}' . "\t\t\t\t\t\t\t\t" . date("d F Y H:i") . "H" . "\t\t\t\t\t\t\t" . "CONFIDENTIAL", null, array('align'=>'left'));

                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
                


                switch ($action) {
			    	case 'I':
			    		$objWriter->save($filename);
			    		header('Content-Description: File Transfer');
		                header('Content-Type: application/octet-stream');
		                header('Content-Disposition: attachment; filename='.$filename);
		                header('Content-Transfer-Encoding: binary');
		                header('Expires: 0');
		                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		                header('Pragma: public');
		                header('Content-Length: ' . filesize($filename));
		                flush();
		                readfile($filename);
		                unlink($filename); // deletes the temporary file
		                exit;
			    		break;
			    	
			    	case 'F':
				    	if (!file_exists('./json/export/approved/' . $id)) {
						    mkdir('./json/export/approved/' . $id, 0777, true);
						}
						if (!file_exists('./json/export/archive/' . $id)) {
						    mkdir('./json/export/archive/' . $id, 0777, true);
						}

                    	$objWriter->save(__DIR__.'./../../json/export/approved/' . $id .'/Annexure.docx');
                    	$objWriter->save(__DIR__.'./../../json/export/archive/' . $id .'/Annexure.docx');
			    		exit;

			    		break;

			    	case 'D':
			    		$objWriter->save($filename);
			    		header('Content-Description: File Transfer');
		                header('Content-Type: application/octet-stream');
		                header('Content-Disposition: attachment; filename='.$filename);
		                header('Content-Transfer-Encoding: binary');
		                header('Expires: 0');
		                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		                header('Pragma: public');
		                header('Content-Length: ' . filesize($filename));
		                flush();
		                readfile($filename);
		                unlink($filename); // deletes the temporary file
		                exit;
			    		break;
			    	
			    }

                
		}

		function text_formatter($text){
                return str_replace('&', '&amp;', $text);
        }

        function format_audit_date($data){

            $result = "";

            $months = array();
            foreach ($data as $date_str) {
              $timestamp = strtotime($date_str->Date);
              $month = date('F', $timestamp);
              $date = date('d', $timestamp);
              $year = date('Y', $timestamp);

              if (empty($months[$month])) {
                  $months[$month] = array();
              }

              $months[$month][] = $date;
            }
            
            foreach ($months as $month => $dates) {
                $str = array_pop($dates);
                if(count($dates) > 0 ){
                    $result .= implode(', ', $dates) . " & " . $str . " " . $month  . ", ";
                } else {
                    $result .= $str . " " . $month . ", ";
                }
                
            }

            $result = rtrim(trim($result), ',');
            return $result . " " . $year;
        }

        function audit_report()
        {
            $id = $_GET['report_id'];
            $action = $_GET['action'];
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $activities = $details->Report_activities;
            $audit_dates = $details->Audit_Dates;
            $lead_auditor = $details->Lead_Auditor;
            $co_auditor = $details->Co_Auditors;
            $translators = $details->Translator;
            $license_Accreditation = $details->License;
            $preaudit_documents = $details->Pre_Document;
            $stanard_reference = $details->Template;
            if(isset($details->Inspection_Audit_Dates)){
                $inspection_date = $details->Inspection_Audit_Dates;
            } else {
                $inspection_date = "";
            }

            if(isset($details->Inspection_Inspector)){
                $inspection_inspector = $details->Inspection_Inspector;
            } else {
                $inspection_inspector = "";
            }

            $inspection_changes = $details->Inspection_Changes;
            
            $scope = $details->Scope_Product;
            $present_during_meeting = $details->Present_During_Meeting;
            $get_personel_met = $details->Personel_Met;
            $observation_findings = $details->Observation_Yes;
            $observation = $details->Audit_Observation;

            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $co_auditor = $this->Audit_report_model->get_data_by_auditor($report[0]->report_id, 'report_id', 'tbl_co_auditors');
            $reviewer_info = $this->Audit_report_model->get_reviewer($report[0]->report_id);
            $approver = $this->Audit_report_model->get_approver($report[0]->report_id);
            $country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');
            $lead_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_summary');

            $params = array("report_no"=>$report[0]->report_no);

            $this->load->library("Pdf",$params);
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->SetTitle("Audit Report : " . $report[0]->report_no );
            $pdf->setStatus((int)$report[0]->status);
            $pdf->setFooterRight("CONFIDENTIAL");
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins('20', '35', '20');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $timesitalic = $pdf->AddFont('timesitalic');
            $gothic = $pdf->AddFont('gothic');
            $cgi = $pdf->AddFont('cgbi');
            $pdf->setListIndentWidth(8);
            $pdf->AddPage();
            $html = '';
            $x = 'A';  


            //ACTIVITIES CARRIED OUT BY COMPANY
            $activities_carried_out = "";
            $activities_carried_out_count = 0;
            foreach ($activities as $key => $value) { 
                $box = str_replace(base_url(),"",$value->activity_box);
                if($box == "asset/img/checkbox_block.jpg"){
                    $activities_carried_out_count++;
                    $activities_carried_out .=   '<tr>';
                    if($activities_carried_out_count == 1){
                        $activities_carried_out .=   '      <td colspan="3" rowspan="2"><b>ACTIVITIES CARRIED OUT BY COMPANY:</b></td>';
                        $activities_carried_out .=   '      <td colspan="9">'.$this->htmlformatter($value->activity_name).'</td>';
                    }

                    if($activities_carried_out_count == 2){
                        $activities_carried_out .=   '      <td colspan="9">'.$this->htmlformatter($value->activity_name).'</td>';
                    }

                    if($activities_carried_out_count > 2){
                        $activities_carried_out .=   '      <td colspan="3"></td>';
                        $activities_carried_out .=   '      <td colspan="9">'.$this->htmlformatter($value->activity_name).'</td>';
                    }
                    
                    
                    $activities_carried_out .=   '</tr>';
                    if(count($value->sub_activity) > 0){
                        foreach ($value->sub_activity as $k => $v) {
                            $sub_box = str_replace(base_url(),"",$v->sub_box);
                            if($sub_box == "asset/img/checkbox_block.jpg") {
                                $activities_carried_out_count++;
                                $activities_carried_out .=   '<tr>';                    
                                if($activities_carried_out_count <= 2){
                                    $activities_carried_out .=   '      <td colspan="1"></td>';
                                    $activities_carried_out .=   '      <td colspan="9">'.$this->htmlformatter($v->sub_name).'</td>';
                                }

                                if($activities_carried_out_count > 2){
                                    $activities_carried_out .=   '     <td colspan="4"></td>';
                                    $activities_carried_out .=   '     <td colspan="8">'.$this->htmlformatter($v->sub_name).'</td>';
                                }
                               
                                $activities_carried_out .=   '</tr>';
                            }
                        }
                    }
                }
            }

            if($activities_carried_out_count == 0){
               

                if($report[0]->other_activities != "" || $report[0]->other_activities != null){
                    $activities_carried_out .=   '<tr>';
                    $activities_carried_out .=   '      <td colspan="3" rowspan="2"><b>ACTIVITIES CARRIED OUT BY COMPANY:</b></td>';
                    $activities_carried_out .=   '      <td colspan="9">Others: '.$this->htmlformatter($report[0]->other_activities).'</td>';
                    $activities_carried_out .=   '</tr>';
                } else {
                    $activities_carried_out .=   '<tr>';
                    $activities_carried_out .=   '      <td colspan="3" rowspan="2"><b>ACTIVITIES CARRIED OUT BY COMPANY:</b></td>';
                    $activities_carried_out .=   '      <td colspan="9">None</td>';
                    $activities_carried_out .=   '</tr>';
                }

                
            } else {
                if($report[0]->other_activities != "" || $report[0]->other_activities != null){
                    if($activities_carried_out_count <= 2){
                        $activities_carried_out .=   '<tr>';
                        $activities_carried_out .=   '      <td colspan="12">Others: '.$this->htmlformatter($report[0]->other_activities).'</td>';
                        $activities_carried_out .=   '</tr>';
                    } else {
                        $activities_carried_out .=   '<tr>';
                        $activities_carried_out .=   '      <td colspan="3"></td>';
                        $activities_carried_out .=   '      <td colspan="9">Others: '.$this->htmlformatter($report[0]->other_activities).'</td>';
                        $activities_carried_out .=   '</tr>';
                    }
                    
                }
            }

            


            //AUDIT DATE
            $audit_date =  $details->Audit_Dates_Formatted;

            // $len = count($audit_dates);
            // $auddate = "";
            // $audyear = "";
            // $audmonth = "";
            // foreach ($audit_dates as $key => $value) {
            //     $audmonth = date_format(date_create($value->Date),"F");
            //     $audyear = date_format(date_create($value->Date),"Y");
            //     if ($key == $len - 1) {
            //        $auddate .= ' & ' . date_format(date_create($value->Date),"d");
            //     } else {
            //        $auddate .= ', ' . date_format(date_create($value->Date),"d");
            //     }
            // }

            // $audit_date = substr($auddate,2) . " " . $audmonth . " " . $audyear;


            //CO AUDITOR
            foreach ($co_auditor as $key => $value) {
                $co_auditor_table   =   '<tr>
                                            <td colspan="3"></td>
                                            <td colspan="4">'.$this->htmlformatter($value->fname) . ' ' . $this->htmlformatter($value->lname) .'</td>
                                            <td colspan="5">'. strtoupper($this->htmlformatter($value->designation)).'</td>
                                        </tr> ';
            }


            //TRANSLATOR
            $translators_name ="";
            foreach ($translators as $key => $value) {
                $translators_name .= ", " . $this->htmlformatter($value->translator);
            }
            $translator = substr($translators_name, 1);


            //LICENSE
            $license = '';
            foreach ($license_Accreditation as $key => $value) {
                $license .= '<li>';
                if($value->reference_name != ""){
                    $license .= $this->htmlformatter($value->reference_name) . '<br />';
                } else {
                    $license .= 'None<br />';
                }

                if($value->issuer != ""){
                    $license .= $this->htmlformatter($value->issuer) . '<br />';
                } else {
                    $license .= 'None<br />';
                }

                if($value->reference_no != ""){
                    $license .= 'License / Certificate No . ' . $this->htmlformatter($value->reference_no) . '<br />';
                } else {
                    $license .= 'License / Certificate No: None<br />';
                }

                if($this->is_date($value->validity)){
                    $license .= 'Validity: ' . date_format(date_create($value->validity),"d F Y") . '<br />';
                } else {
                    $license .= 'Validity: None <br />';
                }

                if($this->is_date($value->validity)){
                    $license .= 'Issue Date: ' . date_format(date_create($value->issued),"d F Y") . '<br />';
                } else {
                    $license .= 'Issued Date: None <br />';
                }
                
                
                $license .= '</li>';
            }


            //PREAUDIT_DOCUMENT
            $preaudit_document = '';
            foreach ($preaudit_documents as $key => $value) {
                $preaudit_document .= '<li>' . $this->htmlformatter($value->document_name) . '</li>';
            }

            //STANDARD
            $standard = '';
            foreach ($stanard_reference as $key => $value) {
                $standard .= '<li>' . $this->htmlformatter($value->standard_name) . '</li>';
            }

            //LAST INSPECTION DATE
            $last_inspection_date = '';
            if(isset($details->Inspection_Audit_Dates_Formatted)){
                if($details->Inspection_Audit_Dates_Formatted != ""){
                    $last_inspection_date = $details->Inspection_Audit_Dates_Formatted;
                } else {
                    $last_inspection_date .= "None";
                }
            } else {
                $last_inspection_date .= "None";
            }
            

            //LAST INSPECTORS INVLOVE
            $inspector_invloved= '';
            if($inspection_inspector != ""){
                if(count($inspection_inspector) > 0){
                    foreach ($inspection_inspector as $key => $value) {
                        if(trim($value->inspector) != ""){
                            $inspector_invloved .='<li>'.$this->htmlformatter($value->inspector).'</li>';
                        }
                    }
                } else {
                   $inspector_invloved .='<li>None</li>'; 
                }
                
            } else {
                $inspector_invloved .='<li>None</li>';
            }


            //MAJOR CHANGES
            $major_changes = "";
            if($inspection_changes != ""){
                if(count($inspection_changes) > 0 ){
                    foreach ($inspection_changes as $key => $value) {
                        $major_changes .='<li>' . $this->htmlformatter($value->changes) . '</li>';
                    }
                } else {
                    $major_changes .='<li>None</li>';
                }
            } else {
                $major_changes .='<li>None</li>';
            }

            //SCOPE REPORT
            $scope_report = '';
            foreach ($scope as $key => $value) {
                $scope_report .='<tr>';
                $scope_report .='   <td colspan="12"><u>'.$this->htmlformatter($value->scope).'</u> '.$this->htmlformatter($value->scope_details).':</td>';
                $scope_report .='</tr>';
                $scope_report .='<tr>';
                $scope_report .='   <td colspan="3"></td>';
                $scope_report .='   <td colspan="9">';
                $scope_report .='       <ul type="square">';
                foreach ($value->products as $key => $value) {
                   $scope_report .='        <li> '.$this->htmlformatter($value->product_name).' </li>';
                }
                $scope_report .='       </ul>';
                $scope_report .='   </td>';
                $scope_report .='</tr>';
            }

            //PRESENT DURING MEETING
            $present_count = 0;
            $present_meeting = "";
            foreach ($present_during_meeting as $key => $value) {
                $present_count++;
                $present_meeting .='<tr>';
                $present_meeting .='   <td colspan="1"></td>';
                $present_meeting .='   <td colspan="1" style="text-align: center;">'. $present_count . ')</td>';
                $present_meeting .='   <td colspan="4">'. $this->htmlformatter($value->name) . '</td>';
                $present_meeting .='   <td colspan="6">';
                if($value->position != ""){
                    $present_meeting .=        $this->htmlformatter($value->position);
                } else {
                    $present_meeting .=        "None";
                }
                
                $present_meeting .='   </td>';
                $present_meeting .='</tr>';
            }


            //PERSONEL MET
            $personel_met = '';
            if(count($get_personel_met) > 0){
                $present_count = 0;
                foreach ($get_personel_met as $key => $value) {
                    $present_count++;
                    $personel_met .='<tr>';
                    $personel_met .='   <td colspan="1"></td>';
                    $personel_met .='   <td colspan="1" style="text-align: center;">'. $present_count . ')</td>';
                    $personel_met .='   <td colspan="4">'. $this->htmlformatter($value->name) . '</td>';
                    $personel_met .='   <td colspan="6">';
                    if($value->designation != ""){
                        $personel_met .=        $this->htmlformatter($value->designation);
                    } else {
                        $personel_met .=        "None";
                    }
                    $personel_met .='   </td>';
                    $personel_met .='</tr>';
                }
            } else {
                $personel_met .='<tr>';
                $personel_met .='   <td colspan="1"></td>';
                $personel_met .='   <td colspan="1"></td>';
                $personel_met .='   <td colspan="4">None</td>';
                $personel_met .='   <td colspan="6"></td>';
                $personel_met .='</tr>';
            }

            //OBSERVATION FINDINGS
            $observation_finding = "";
            if(count($observation_findings) > 0){
                foreach ($observation_findings as $key => $value) {
                    $observation_x = substr(trim($value->Observation), 0, -1);
                    if($observation_x != ""){
                        $observation_finding .= "<li><b>" . $this->htmlformatter($value->Element). "</b> - " . $this->htmlformatter($observation_x). "</li>";
                    } else {
                        $observation_finding .= "<li><b>" . $this->htmlformatter($value->Element). "</b> - None</li>";
                    }
                }
            }
 
            //OBSERVATION
            $observation_cat = "";
            foreach ($observation as $key => $value) {
                $observation_cat .='<tr>';
                $observation_cat .='   <td colspan="12"><u>' . $this->htmlformatter($value->category_name) . '</u></td>';
                $observation_cat .='</tr>';
                $observation_cat .='<tr>';
                $observation_cat .='    <td colspan="12"><i class="italic_font_desc">' . $this->htmlformatter($value->description) . '</i></td>';
                $observation_cat .='</tr>';
                $observation_cat .='<tr>';
                $observation_cat .='   <td colspan="12"></td>';
                $observation_cat .='</tr>';
                $observation_cat .='<tr>';
                $observation_cat .='   <td colspan="12"></td>';
                $observation_cat .='</tr>';
            }


            //LISTING AUDIT OBSERVATION
            $ctrx = 1;
            $critical = null;
            $major = null;
            $minor = null;
            foreach ($details->Template_Elements as $key => $value) {
                foreach ($value->questions as $k => $v) {
                    switch ($v->category) {
                        case 'Critical':
                            $critical .= $v->answer_no . ",";
                            break;
                        
                        case 'Major':
                            $major .= $v->answer_no . ",";
                            break;
                        
                        case 'Minor':
                            $minor .= $v->answer_no . ",";
                            break;
                    }
                }
            }

            if($critical == null) {
                $html_critical = 'None';
            } else {
                $html_critical = 'Please refer to item no(s) ' . substr(trim($critical), 0, -1);
            }

            if($major == null) {
                $html_major = 'None';
            } else {
                $html_major = 'Please refer to item no(s) ' . substr(trim($major), 0, -1);
            }


            if($minor == null) {
                $html_minor = 'None';
            } else {
                $html_minor = 'Please refer to item no(s) ' . substr(trim($minor), 0, -1);
            }


            ///summary recommendation
            $summary_recomendation ='There were';
            if($analysis->no_critical < 1) { 
                $summary_recomendation .=' <b><u>no</u></b> Critical, ';
            } else { 
                $summary_recomendation .= ' <b><u>' . $this->htmlformatter($analysis->no_critical) . '</u></b> Critical';
            }
            if($analysis->no_major < 1) { 
                $summary_recomendation .=' <b><u>no</u></b> Major, ';
            } else { 
                $summary_recomendation .= ' <b><u>' .  $this->htmlformatter($analysis->no_major) . '</u></b> Major';
            }
            if($analysis->no_minor < 1) { 
                $summary_recomendation .=' and <b><u>no</u></b> Minor ';
            } else { 
                $summary_recomendation .= ' and <b><u>' . $this->htmlformatter($analysis->no_minor) . '</u></b> Minor ';
            }
            $summary_recomendation .= 'observations noted at this facility during the inspection.';


            ///RECOMMENDATION
            $recommendation = '';
            if(count($details->Recommendation) > 0){
                $recommendation .='<tr>';
                $recommendation .='   <td colspan="12">We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] observations which can be grouped into the following Quality System and/or GMP elements:</td>';
                $recommendation .='</tr>';
            }


            ///RECOMMENDATION DETAILS
            $recommendation_details = "";
            foreach ($details->Recommendation as $key => $value) {
                if($value->recommendation != ""){
                    $recommendation_details .='   <li><b>' . $this->htmlformatter($value->element_name) .'</b> - ' . $this->htmlformatter($value->recommendation). '</li>';
                } else {
                    $recommendation_details .='   <li><b>' . $this->htmlformatter($value->element_name) .'</b> - None</li>';
                }
            }

            ///dispposition
            $disposition = '';
            foreach ($details->Disposition_Product as $key => $value) {
                $disposition .='<b>'.$this->htmlformatter($value->disposition_name).'</b> ' . $this->htmlformatter($value->disposition_label) . ':';
                $disposition .='    <ul type="square">';
                foreach ($value->disposition_products as $key => $value) {
                    $disposition .='    <li>'.$this->htmlformatter($value->product_name).'</li>';
                }             
                $disposition .='    </ul>';
            }

            //OTHER ISSUES
            $other_issue = "";
            foreach ($details->other_issue_audit as $key => $value){
                $other_issue .='<li> '.$this->htmlformatter($value->other_issues_audit).' </li>';
            }


            //lead auditor stamp
            $lead_date_stamp = "";
            if(isset($signature_stamp[0]->approved_date)) {
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'*<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($lead_stamp[0]->report_submission)) . '<br>' . date("H:i",strtotime($lead_stamp[0]->report_submission)) .'H<br></span></td>';
            } else {
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
            }

            //coauditor stamp
            $coauditor_date_stamp = "";
            foreach ($co_auditor as $key => $value) {
                //co auditor is reviewer
                $coauditor_name = $value->fname. ' ' .$value->lname;
                $reviewer_name = $reviewer_info[0]->fname. ' ' .$reviewer_info[0]->lname;
                $approver_name = $approver[0]->fname. ' ' .$approver[0]->lname;
                if($coauditor_name != $reviewer_name){
                    if($coauditor_name != $approver_name){
                        //check if submitted chanbges
                        $coauditorchanges = $this->global_model->get_data_query("tbl_report_coauditor_submission", "report_id = " . $id . " AND auditor_id = " . $value->auditor_id);
                        if(count($coauditorchanges) > 0){
                            $coauditor_date_stamp .= "<tr>";
                            $coauditor_stamp = $this->Audit_report_model->get_co_auditor_stamp($report[0]->report_id,$value->auditor_id);
                            if(isset($signature_stamp[0]->approved_date)) {
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' .$value->fname. ' ' .$value->lname.'*<br></span></td>';
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' . $value->designation . ' / Co-Auditor <br>' . $value->department . ", " .$value->company . '<br></span></td>';
                                if($coauditor_stamp != null){
                                    $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($coauditor_stamp)) . '<br>' . date("H:i",strtotime($coauditor_stamp)) .'H<br></span></td>';
                                } else {
                                    $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br></span></td>';
                                }
                            } else {
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' .$value->fname. ' ' .$value->lname.'<br></span></td>';
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' . $value->designation . ' / Co-Auditor <br>' . $value->department . ", " .$value->company . '<br></span></td>';
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br></span></td>';
                            }
                            $coauditor_date_stamp .= "</tr>";
                        }
                    }
                }
            }

            //REVIEWER STAMP
            $reviewer_date_stamp = "";
            if(isset($signature_stamp[0]->approved_date)) { 
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '*<br></span></td>';
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$reviewer_info[0]->designation. '<br>' .$reviewer_info[0]->department. ', ' . $reviewer_info[0]->company . '<br></span></td>';
                if($signature_stamp[0]->review_date != null){
                    $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->review_date)) . '<br>' . date("H:i",strtotime($signature_stamp[0]->review_date)) .'H<br></span></td>';
                } else {
                    $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
                }
            } else {
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '<br></span></td>';
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$reviewer_info[0]->designation. '<br>' .$reviewer_info[0]->department. ', ' . $reviewer_info[0]->company . '<br></span></td>'; 
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
            }

            //approver stamp
            $approver_date_stamp = "";
            if(isset($signature_stamp[0]->approved_date)) { 
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '*<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$approver[0]->designation. '<br>' .$approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->approved_date)) .'H<br></span></td>';
            } else {
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$approver[0]->designation. '<br>' .$approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
            }

            //START OF PDF
            $html .= '<style> 
                        .gothic {
                            font-size: 11px;
                            font-family: gothic;
                        }

                        .title {
                            font-size: 14px;
                            font-family: gothic;
                        }

                        .italic_font {
                            font-weight: 700;
                            font-size: 11px;
                            font-family: timesitalic;
                        } 

                        .italic_font_desc {
                            font-weight: 700;
                            font-size: 13px;
                            font-family: timesitalic;
                        } 

                        .gothic_italic {
                            font-weight: 700;
                            font-size: 11px;
                            font-family: cgbi;
                        } 

                        .table, .table > tr, .table > tr > td { 
                            font-size: 11px;
                            font-family: gothic;
                            margin : 0px;
                            padding: 0px;
                        }

                        .table > tr > td {
                            width : 10px;
                        }

                        .center {
                            text-align: center;
                        }

                        .line {
                            border-bottom: 1px solid black;
                        }

                        .bordered {
                            border : 1px solid #000;
                        }

                    </style>';

            $html .= '  <table class="table">
                            <tr>
                                <td colspan="12" class="gothic center"><h2>GMP AUDIT REPORT</h2></td>
                            </tr> 
                            <tr>
                                <td colspan="12" class="gothic center"><b>'.$report[0]->report_no.'</b></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="3"><b>AUDITED SITE:</b></td>
                                <td colspan="9">'.$this->htmlformatter($report[0]->name).'</td>
                            </tr> 
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9">'.$this->htmlformatter($report[0]->address1). ', ' .$this->htmlformatter($report[0]->address2).'</td>
                            </tr> 
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9">'.$this->htmlformatter($report[0]->address3).', '. strtoupper($country[0]->country_name) .'</td>
                            </tr> 
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 

                            '.$activities_carried_out.' 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>AUDIT DATE:</b></td>
                                <td colspan="9">'. $audit_date .'</td>
                            </tr>  
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>AUDITOR/S:</b></td>
                                <td colspan="4">'.$this->htmlformatter($lead_auditor[0]->fname) . ' ' . $this->htmlformatter($lead_auditor[0]->lname) .'</td>
                                <td colspan="5">'.$this->htmlformatter($lead_auditor[0]->designation).'</td>
                            </tr> 
                            '.$co_auditor_table.' 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9"><b>Translator Usage </b> 
                                    (Name of employee, or approved translator, participating in this audit as determined by the Lead Auditor): '.$translator.'
                                </td>
                            </tr> 
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>REFERENCE:</b></td>
                                <td colspan="9"><b>License(s) / Accreditation(s) / Certification(s)</b> held by supplier and verified during the audit</td>
                            </tr> 
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9"><ul type="square">'.$license.'</ul></td>
                            </tr> 
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9"><b>Pre-audit documents</b> provided and reviewed</td>
                            </tr> 
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9"><ul type="square">'.$preaudit_document.'</ul></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9"><b>Regulatory and UNILAB Standards Used</b></td>
                            </tr> 
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9"><ul type="square">'.$standard.'</ul></td>
                            </tr> 
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12" style="font-size: 14px;"><b>SUPPLIER BACKGROUND / HISTORY</b></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12" style="text-align: justify;">'. $this->htmlformatter($report[0]->background) .'</td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><b>Date of previous inspection </b><span class="italic_font">(if applicable)</span></td>
                            </tr> 
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><ul type="square"><li>'.$last_inspection_date.'</li></ul></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><b>Names of Inspectors invloved in previous inspection</b> <span class="italic_font">(if applicable)</span></td>
                            </tr> 
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><ul type="square">'.$inspector_invloved.'</ul></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><b>Major change/s since the previous inspection</b> <span class="italic_font">(if applicable)</span></td>
                            </tr> 
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><ul type="square">'.$major_changes.'</ul></td>
                            </tr> 
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12" style="font-size: 14px;"><b>BRIEF REPORT OF THE AUDIT ACTIVITIES UNDERTAKEN:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"><b>SCOPE: </b></td>
                            </tr>
                            '.$scope_report.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"><b>AUDITED AREA(S): </b></td>
                            </tr>
                            <tr>
                                <td colspan="12">'. $this->htmlformatter($report[0]->audited_areas) .'</td>
                            </tr>
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">The audit did not cover the following areas, to be considered during the next audit:</td>
                            </tr>
                            <tr>
                                <td colspan="12">' . $this->htmlformatter($report[0]->areas_to_consider) . '</td>
                            </tr>
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12" style="font-size: 14px;"><b>PERSONNEL MET DURING THE AUDIT:</b></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">Issues and audit observations were discussed during the wrap-up meeting held on '.$report[0]->wrap_up_date.'. The audit report will focus on the observations that were discussed during the audit.</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">Present during the close-out meeting:</td>
                            </tr>
                            '.$present_meeting.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">Other personnel met during the inspection:</td>
                            </tr>
                            '.$personel_met.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12" style="font-size: 14px;"><b>AUDIT TEAM'. "'" . 'S FINDINGS AND OBSERVATIONS RELEVANT TO THE AUDIT</b></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">The audit consisted of an in-depth review of quality and GMP elements including, but not limited to the following:</td>
                            </tr>
                            <tr>
                                <td colspan="12"><ul type="square">'.$observation_finding.'</ul></td>
                            </tr>
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12" style="font-size: 14px;"><b>DEFINITION / CATEGORIZATION OF AUDIT OBSERVATIONS</b></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            '.$observation_cat.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">LISTING OF AUDIT OBSERVATIONS AND CONCERNS</b> ( in decreasing order of criticality )</td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"><b>Critical Observations - </b><span class="italic_font" style="text-align:justify;">'.$html_critical.'</span></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"><b>Major Observations - </b><span class="italic_font" style="text-align:justify;">'.$html_major.'</span></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"><b>Minor Observations - </b><span class="italic_font" style="text-align:justify;">'.$html_minor.'</span></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">SUMMARY AND RECOMMENDATION</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12">'.$summary_recomendation.'</td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            '.$recommendation.'
                            <tr>
                                <td colspan="12"><ul type="square">'.$recommendation_details.'</ul></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>  
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">CONCLUSION</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12">Based on the results of this assessment, ' . $this->htmlformatter($report[0]->name) . ' is at this time considered:</td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="2">Disposition: </td>
                                <td colspan="10">'.$disposition.'</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>  
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">OTHER ISSUES:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>  
                            <tr>
                                <td colspan="12"><ul type="square">'.$other_issue.'</ul></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="4" class="center bordered"><b>Name</b></td>
                                <td colspan="4" class="center bordered"><b>Department / Designation</b></td>
                                <td colspan="4" class="center bordered"><b>Date / Time</b></td>
                            </tr>
                            <tr>
                                <td colspan="12" class="bordered"> <b>Prepared By</b></td>
                            </tr> 
                            <tr>
                                '.$lead_date_stamp.'
                            </tr>
                            '.$coauditor_date_stamp.'
                            <tr>
                                <td colspan="12" class="bordered"> <b>Reviewed By</b></td>
                            </tr> 
                            <tr>
                                '.$reviewer_date_stamp.'
                            </tr>  
                            <tr>
                                <td colspan="12" class="bordered"> <b>Approved By</b></td>
                            </tr>  
                            <tr>
                                '.$approver_date_stamp.'
                            </tr> 
                            <tr>
                                <td colspan="12"> <b class="gothic_italic">*This document was signed electronically and this is the manifestation of the electronic signature.</b></td>
                            </tr> 
                        </table>
            ';


            $pdf->writeHTML($html,true,false,true,false,'');

            switch ($action) {
                case 'I':
                    $pdf->Output($report[0]->report_no.'-Audit_Report.pdf', 'I');
                    break;
                
                case 'F':
                    if (!file_exists('./json/export/approved/' . $id)) {
                        mkdir('./json/export/approved/' . $id, 0777, true);
                    }

                    if (!file_exists('./json/export/archive/' . $id)) {
                        mkdir('./json/export/archive/' . $id, 0777, true);
                    }

                    $pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Audit_Report.pdf', 'F');
                    $pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Audit_Report.pdf', 'F');
                    break;

                case 'D':
                    $pdf->Output('Audit_Report '.$report[0]->report_no.'.pdf', 'D');
                    break;
                
            }

        }

        function executive_summary()
        {
            $id = $_GET['report_id'];
            $action = $_GET['action'];
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $audit_dates = $details->Audit_Dates;
            $lead_auditor = $details->Lead_Auditor;
            $reviewer = $details->Reviewer;
            $approver = $details->Approver;
            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');
            $lead_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_summary');

            $co_auditor = $this->Audit_report_model->get_data_by_auditor($report[0]->report_id, 'report_id', 'tbl_co_auditors');
            $reviewer_info = $this->Audit_report_model->get_reviewer($report[0]->report_id);
            $approver = $this->Audit_report_model->get_approver($report[0]->report_id);
            

            $this->load->library("Pdf");
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->SetTitle("Executive Summary : " . $report[0]->report_no );
            $pdf->setStatus((int)$report[0]->status );
            $pdf->setFooterRight("CONFIDENTIAL");
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins('20', '32', '20');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $gothic = $pdf->AddFont('gothic');
            $cgi = $pdf->AddFont('cgbi');
            $timesitalic = $pdf->AddFont('timesitalic');
            $italic_font = $pdf->AddFont('helveticaBI');
            $pdf->setListIndentWidth(5);
            $pdf->AddPage();
            $html = '';
            $x = 'A';

            //AUDIT DATE
            $audit_date =   $details->Audit_Dates_Formatted;

            // $len = count($audit_dates);
            // $auddate = "";
            // $audyear = "";
            // $audmonth = "";
            // foreach ($audit_dates as $key => $value) {
            //     $audmonth = date_format(date_create($value->Date),"F");
            //     $audyear = date_format(date_create($value->Date),"Y");
            //     if ($key == $len - 1) {
            //        $auddate .= ' & ' . date_format(date_create($value->Date),"d");
            //     } else {
            //        $auddate .= ', ' . date_format(date_create($value->Date),"d");
            //     }
            // }

            //audit team
            $auditteamcount = 1;
            $coauditor = "";
            foreach ($co_auditor as $key => $value){
                $coauditor .='<tr>';
                if($auditteamcount == 1){
                    $coauditor .='<td colspan="3">Audit Team Member/s</td>';
                } else {
                    $coauditor .='<td colspan="3"></td>';
                }
                
                $coauditor .='   <td colspan="9">'.$this->htmlformatter($value->fname).' '.$this->htmlformatter($value->lname).'</td>';
                $coauditor .='</tr>';
                $auditteamcount++;
            }

            ///scope
            $scope_product_html = "";
            foreach ($details->Scope_Product as $key => $value) {
                $scope_product_html .= '<tr>';
                $scope_product_html .= '  <td colspan="12">';
                $scope_product_html .= '<u>' . $this->htmlformatter($value->scope) . '</u> ' .$this->htmlformatter($value->scope_details).':';
                $scope_product_html .= '   </td>';
                $scope_product_html .= '</tr>';
                $scope_product_html .= '<tr>';
                $scope_product_html .= '  <td colspan="3"></td>';
                $scope_product_html .= '  <td colspan="9">';
                $scope_product_html .='       <ul type="square">';
                foreach ($value->products as $key => $value) {
                   $scope_product_html .='        <li style="margin-left: 200px;"> '.$this->htmlformatter($value->product_name).' </li>';
                }
                $scope_product_html .='       </ul>';
                $scope_product_html .='   </td>';
                $scope_product_html .='</tr>';
            }

            ///
            $html_element = "";
            foreach ($details->Template_Elements as $key => $value) {
                $html_element .='<tr>';
                $html_element .='   <td colspan="1"></td>';
                $html_element .='   <td colspan="11">';
                $html_element .='       <img src="asset/img/bullet.png" width="8"> &nbsp; '.$this->htmlformatter($value->element_name);
                $html_element .='   </td>';
                $html_element .='</tr>';
            }
            $html_element .='<tr>';
            $html_element .='   <td colspan="1"></td>';
            $html_element .='   <td colspan="11">';
            $html_element .='       <img src="asset/img/bullet.png" width="8"> &nbsp; License(s) Accreditation(s)/ Certification(s) held by supplier and verified during the audit. ';
            $html_element .='   </td>';
            $html_element .='</tr>';


            //STANDARD
            $standard_html = '';
            foreach ($details->Template as $key => $value){
                $standard_html .='<li> '.$this->htmlformatter($value->standard_name).' </li>';
            }


            //observation
            if($analysis->no_critical != ""){
                $critical = $analysis->no_critical;
            } else {
                $critical = 0;
            }

            if($analysis->no_major != ""){
                $no_major = $analysis->no_major;
            } else {
                $no_major = 0;
            }

            if($analysis->no_minor != ""){
                $no_minor = $analysis->no_minor;
            } else {
                $no_minor = 0;
            }   


            ///recommendation
            $recommendation = "";
            if(count($details->Recommendation) > 0){
                foreach ($details->Recommendation as $k => $v) {
                    if($v->recommendation != ""){
                        $recommendation .='<li ><b>'.$this->htmlformatter($v->element_name).'</b> - '.$this->htmlformatter($v->recommendation).'</li>';
                    } else {
                        $recommendation .='<li ><b>'.$this->htmlformatter($v->element_name).'</b> - None.</li>';
                    }
                    
                }
            } else {
                $recommendation .='<li>None</li>';
            }

            ///dispposition
            $disposition = '';
            foreach ($details->Disposition_Product as $key => $value) {
                $disposition .='<b>'.$this->htmlformatter($value->disposition_name). ' </b>' . $this->htmlformatter($value->disposition_label) . ':';
                $disposition .='    <ul type="square">';
                foreach ($value->disposition_products as $key => $value) {
                    $disposition .='    <li>'.$this->htmlformatter($value->product_name).'</li>';
                }             
                $disposition .='    </ul>';
            }

            ///other issue
            $other_issue = '';
            foreach ($details->other_issue_exec as $key => $value){
                $other_issue .='<li> '.$this->htmlformatter($value->other_issues_executive).' </li>';
            }


            ///distribution
            $distribution= "";
            foreach ($details->Distribution as $key => $value){
                $distribution .='<li>'.$this->htmlformatter($value->distribution_name).'</li>';
            }
            if(!empty($details->Other_Distribution)){
                foreach ($details->Other_Distribution as $key => $value) {
                    $distribution .='<li>'.$this->htmlformatter($value->other_distribution).'</li>';
                }
            }
            $distribution .='<li>Corporate GMP Department, UNILAB</li>';


            //lead auditor stamp
            $lead_date_stamp = "";
            if(isset($signature_stamp[0]->approved_date)) {
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'*<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($lead_stamp[0]->report_submission)) . '<br>' . date("H:i",strtotime($lead_stamp[0]->report_submission)) .'H<br></span></td>';
            } else {
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->fname . ' ' . $lead_auditor[0]->lname .'<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$lead_auditor[0]->designation.' / Lead Auditor <br>'.$lead_auditor[0]->department.', '.$lead_auditor[0]->company.'<br></span></td>';
                $lead_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
            }

            //coauditor stamp
            $coauditor_date_stamp = "";
            foreach ($co_auditor as $key => $value) {
                //co auditor is reviewer
                $coauditor_name = $value->fname. ' ' .$value->lname;
                $reviewer_name = $reviewer_info[0]->fname. ' ' .$reviewer_info[0]->lname;
                $approver_name = $approver[0]->fname. ' ' .$approver[0]->lname;
                if($coauditor_name != $reviewer_name){
                    if($coauditor_name != $approver_name){
                        //check if submitted chanbges
                        $coauditorchanges = $this->global_model->get_data_query("tbl_report_coauditor_submission", "report_id = " . $id . " AND auditor_id = " . $value->auditor_id);
                        if(count($coauditorchanges) > 0){
                            $coauditor_date_stamp .= "<tr>";
                            $coauditor_stamp = $this->Audit_report_model->get_co_auditor_stamp($report[0]->report_id,$value->auditor_id);
                            if(isset($signature_stamp[0]->approved_date)) {
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' .$value->fname. ' ' .$value->lname.'*<br></span></td>';
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' . $value->designation . ' / Co-Auditor <br>' . $value->department . ", " .$value->company . '<br></span></td>';
                                if($coauditor_stamp != null){
                                    $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($coauditor_stamp)) . '<br>' . date("H:i",strtotime($coauditor_stamp)) .'H<br></span></td>';
                                } else {
                                    $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br></span></td>';
                                }
                            } else {
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' .$value->fname. ' ' .$value->lname.'<br></span></td>';
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br>' . $value->designation . ' / Co-Auditor <br>' . $value->department . ", " .$value->company . '<br></span></td>';
                                $coauditor_date_stamp .='<td colspan="4" class="center bordered"><span><br></span></td>';
                            }
                            $coauditor_date_stamp .= "</tr>";
                        }
                    }
                }
            }

            //REVIEWER STAMP
            $reviewer_date_stamp = "";
            if(isset($signature_stamp[0]->approved_date)) { 
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '*<br></span></td>';
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$reviewer_info[0]->designation. '<br>' .$reviewer_info[0]->department. ', ' . $reviewer_info[0]->company . '<br></span></td>';
                if($signature_stamp[0]->review_date != null){
                    $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->review_date)) . '<br>' . date("H:i",strtotime($signature_stamp[0]->review_date)) .'H<br></span></td>';
                } else {
                    $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
                }
            } else {
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $reviewer_info[0]->fname . ' ' .  $reviewer_info[0]->lname . '<br></span></td>';
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$reviewer_info[0]->designation. '<br>' .$reviewer_info[0]->department. ', ' . $reviewer_info[0]->company . '<br></span></td>'; 
                $reviewer_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
            }

            //approver stamp
            $approver_date_stamp = "";
            if(isset($signature_stamp[0]->approved_date)) { 
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '*<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$approver[0]->designation. '<br>' .$approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. date("d F Y",strtotime($signature_stamp[0]->approved_date)). '<br>' . date("H:i",strtotime($signature_stamp[0]->approved_date)) .'H<br></span></td>';
            } else {
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'. $approver[0]->fname . ' ' .  $approver[0]->lname . '<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br>'.$approver[0]->designation. '<br>' .$approver[0]->department. ', ' . $approver[0]->company . '<br></span></td>';
                $approver_date_stamp .='<td colspan="4" class="center bordered"><span><br><br></span></td>';
            }


            //START OF PDF
            $html .= '<style> 
                        .gothic {
                            font-size: 12px;
                            font-family: gothic;
                        }

                        .title {
                            font-size: 14px;
                            font-family: gothic;
                        }

                        .italic_font {
                            font-weight: 700;
                            font-size: 11px;
                            font-family: timesitalic;
                        } 

                        .table, .table > tr, .table > tr > td { 
                            font-size: 11px;
                            font-family: gothic;
                            margin : 0px;
                            padding: 0px;
                            
                        }

                        .gothic_italic {
                            font-weight: 700;
                            font-size: 11px;
                            font-family: cgbi;
                        } 

                        .table > tr > td {
                            width : 10px;
                        }

                        .center {
                            text-align: center !important;
                        }

                        .line {
                            border-bottom: 1px solid black;
                        }

                        .bordered {
                            border : 1px solid #000;
                        }

                    </style>';

            $html .= '  <table class="table">
                            <tr>
                                <td colspan="12" class="gothic"><h2 class="center">GMP AUDIT REPORT EXECUTIVE SUMMARY REPORT</h2></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3">Company Name</td>
                                <td colspan="9">'.$this->htmlformatter($report[0]->name).'</td>
                            </tr>
                            <tr>
                                <td colspan="3">Site Address</td>
                                <td colspan="9">'.$this->htmlformatter($report[0]->address1).', '.$this->htmlformatter($report[0]->address2).'</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="9">'.$this->htmlformatter($report[0]->address3).', '. strtoupper($country[0]->country_name).'</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">Summary Statement:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">An audit was conducted at the above facility on ' . $audit_date .'. This document serves to attest that the original audit documentation has been reviewed and that this report is an accurate summary of the original audit documentation.</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3">Lead Auditor</td>
                                <td colspan="9">'.$this->htmlformatter($lead_auditor[0]->fname) . ' ' . $this->htmlformatter($lead_auditor[0]->lname).'</td>
                            </tr>
                            '.$coauditor.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">Scope:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            '.$scope_product_html.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">The audit consisted of an in-depth review of their quality systems including, but not limited to the following:</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            '.$html_element.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">The audit did not cover the following areas, to be covered during the next audit:</td>
                            </tr>
                            <tr>
                                <td colspan="12">'.$this->htmlformatter($report[0]->areas_to_consider).'</td>
                            </tr>
                            <tr>
                                <td colspan="12" class="line"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">The standard(s) used during the audit include:</td>
                            </tr>
                            <tr>
                                <td colspan="12"><ul type="square">'.$standard_html.'</ul></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>

                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">Audit Results:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">The following are the observations, categorized based on criticality, noted during '. $audit_date .' inspection.</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr style="background-color:#ccc;">
                                <td colspan="4" class="bordered"><b class="center">No. of Critical <br/> Observations</b></td>
                                <td colspan="4" class="bordered"><b class="center">No. of Major <br/> Observations</b></td>
                                <td colspan="4" class="bordered"><b class="center">No. of Minor <br/> Observations</b></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="bordered"><span class="center">'.$critical.'</span></td>
                                <td colspan="4" class="bordered"><span class="center">'.$no_major.'</span></td>
                                <td colspan="4" class="bordered"><span class="center">'.$no_minor.'</span></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">Observations:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">We recommend that all the findings noted should be addressed giving utmost emphasis on the significant [Critical, Major] Observations which can be grouped into the following Quality System and/or GMP elements:</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><ul type="square">'.$recommendation.'</ul></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12">Based on the results of this asessement, <u>'.$this->htmlformatter($report[0]->name).'</u> is at this time considered:</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="2">Disposition: </td>
                                <td colspan="10">'.$disposition.'</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="12"><b style="font-size: 14px;">Other Issues:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td colspan="11"><ul type="square">'.$other_issue.'</ul></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>

                        <table class="table" nobr="true">
                            <tr>
                                <td colspan="4" class="center bordered"><b>Name</b></td>
                                <td colspan="4" class="center bordered"><b>Department / Designation</b></td>
                                <td colspan="4" class="center bordered"><b>Date / Time</b></td>
                            </tr>
                            <tr>
                                <td colspan="12" class="bordered"> <b>Prepared By</b></td>
                            </tr> 
                            <tr class="center">
                                '.$lead_date_stamp.'
                            </tr>
                            '.$coauditor_date_stamp.'
                            <tr>
                                <td colspan="12" class="bordered"> <b>Reviewed By</b></td>
                            </tr> 
                            <tr class="center">
                                '.$reviewer_date_stamp.'
                            </tr>  
                            <tr>
                                <td colspan="12" class="bordered"> <b>Approved By</b></td>
                            </tr>  
                            <tr class="center">
                                '.$approver_date_stamp.'
                            </tr> 
                            <tr>
                                <td colspan="12"><b class="gothic_italic">*This document was signed electronically and this is the manifestation of the electronic signature.</b></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="12"><b class="italic_font">Distribution List (include the Supplier Quality representative(s) from ALL locations of use associated with this supplier)</b></td>
                            </tr> 
                            <tr>
                                <td colspan="12"></td>
                            </tr> 
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="10"><ul type="square">'.$distribution.'</ul></td>
                            </tr> 
                        </table>
                    ';

            $pdf->writeHTML($html,true,false,true,false,'');


            switch ($action) {
                case 'I':
                     $pdf->Output($report[0]->report_no.'-Executive_Report.pdf', 'I');
                    break;
                
                case 'F':
                    if (!file_exists('./json/export/approved/' . $id)) {
                        mkdir('./json/export/approved/' . $id, 0777, true);
                    }

                    if (!file_exists('./json/export/archive/' . $id)) {
                        mkdir('./json/export/archive/' . $id, 0777, true);
                    }

                    $pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Executive_Report.pdf', 'F');
                    $pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Executive_Report.pdf', 'F');
                    break;

                case 'D':
                    $pdf->Output('Executive_Report '.$report[0]->report_no.'.pdf', 'D');
                    break;
                
            }
        }

        function raw_data()
        {
            $id = $_GET['report_id'];
            $action = $_GET['action'];
            $string = file_get_contents("./listing/".$id.".json");
            $details = json_decode($string, false);

            $anastring = file_get_contents("./listing/".$id."_analysis.json");
            $analysis = json_decode($anastring, false);

            $report = $details->Report_Summary;
            $audit_dates = $details->Audit_Dates;
            $co_auditor = $details->Co_Auditors;
            $lead_auditor = $details->Lead_Auditor;
            $template = $details->Template;
            $reviewer = $details->Reviewer;
            $signature_stamp = $this->Template_model->get_data_by_id($report[0]->report_id,'report_id','tbl_report_signature_stamp');
            $country = $this->Template_model->get_data_by_id("'" . $report[0]->country ."'",'country_code','country_list');


            $this->load->library("Pdf");
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setReportNo($report[0]->report_no);
            $pdf->SetTitle("Raw Data : " . $report[0]->report_no );
            $pdf->setStatus((int)$report[0]->status );
            $pdf->setFooterRight("CONFIDENTIAL");
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins('20', '32', '20');
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetFont('gothic', '', 10);
            $pdf->AddPage();
            $html = '';


             //AUDIT DATE
            $audit_date_html = $details->Audit_Dates_Formatted;

            //coauditor
            $coauditor_count = 0;
            $coauditor_html = "";
            foreach ($co_auditor as $key => $value) {
                $coauditor_count++;
                $coauditor_html .= '<tr>';
                if($coauditor_count == 1){
                    $coauditor_html .= '<td colspan="2"><b>Co-Auditor:</b></td>';
                } else {
                    $coauditor_html .= '<td colspan="2"></td>';
                }
                $coauditor_html .= '<td colspan="10">' . $this->htmlformatter($value->fname). ' ' .$this->htmlformatter($value->lname).'</td>';
                $coauditor_html .= '</tr>';
            }

            ///product
            $product_html = '';
            foreach ($details->Scope_Product as $key => $value) {
                foreach ($value->products as $k => $v) {
                    $product_html .='    <li> '.$this->htmlformatter($v->product_name).' </li>';
                }  
            }

            //elements
            $letters = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
            $element_html = '';
            foreach ($details->Template_Elements as $key => $value) {
                $element_html .= '<table class="table">';
                $element_html .= '<tr>';
                $element_html .= '      <td colspan="12"><h3>Element ' . $letters[$value->order - 1] . ' - ' . $this->htmlformatter($value->element_name) . '</h3></td>';
                $element_html .= '</tr>';
                $element_html .= '<tr style="background-color: #4594cd;">';
                $element_html .= '      <td colspan="1" class="bordered"><br><br></td>';
                $element_html .= '      <td colspan="4" class="bordered"><h4 class="center" style="color: #fff; font-size: 13px;"><br>Questions<br></h4></td>';
                $element_html .= '      <td colspan="2" class="bordered"><h4 class="center" style="color: #fff; font-size: 13px;"><br>Answer<br></h4></td>';
                $element_html .= '      <td colspan="3" class="bordered"><h4 class="center" style="color: #fff; font-size: 13px;"><br>Remarks<br></h4></td>';
                $element_html .= '      <td colspan="2" class="bordered"><h4 class="center" style="color: #fff; font-size: 13px;"><br>Category<br></h4></td>';
                $element_html .= '</tr>';
                $question_count = 0;
                foreach ($value->questions as $a => $b) {
                    $question_count++;
                    $element_html .= '<tr>';
                    $element_html .= '      <td colspan="1" class="bordered"><table cellpadding="6"><tr><td class="center">' . $letters[$value->order - 1]. $question_count . '</td></tr></span></td>';
                    $element_html .= '      <td colspan="4" class="bordered"><table cellpadding="6"><tr><td>' . $this->htmlformatter($b->question) . '</td></tr></table></td>';
                    $element_html .= '      <td colspan="2" class="bordered"><table cellpadding="6"><tr><td class="center">' . $this->htmlformatter($b->answer_name) . '</td></tr></table></td>';
                    $element_html .= '      <td colspan="3" class="bordered"><table cellpadding="6"><tr><td>' . $this->htmlformatter($b->answer_details) . '</td></tr></table></td>';
                    $element_html .= '      <td colspan="2" class="bordered"><table cellpadding="6"><tr><td class="center">' . $this->htmlformatter($b->category) . '</td></tr></span></td>';
                    $element_html .= '</tr>';
                }
                $element_html .= '<tr>';
                $element_html .= '      <td colspan="12"></td>';
                $element_html .= '</tr>';
                $element_html .= '</table>';
            }

            //START OF PDF
            $html .= '<style> 
                        .gothic {
                            font-size: 11px;
                            font-family: gothic;
                        }

                        .title {
                            font-size: 14px;
                            font-family: gothic;
                        }

                        .italic_font {
                            font-weight: 700;
                            font-size: 11px;
                            font-family: timesitalic;
                        } 

                        .table, .table > tr, .table > tr > td { 
                            font-size: 11px;
                            font-family: gothic;
                            margin : 0px;
                            padding: 0px;
                            
                        }

                        .table > tr > td {
                            width : 10px;
                        }

                        .center {
                            text-align: center !important;
                        }

                        .line {
                            border-bottom: 1px solid black;
                        }

                        .bordered {
                            border : .75px solid #000;
                        }

                        .padding {
                            padding : 5px;
                        }

                    </style>';

            $html .= '  <table class="table">
                            <tr>
                                <td colspan="9"></td>
                                <td colspan="2" rowspan="7" class="bordered">
                                <h2 class="center"><span class="center">Rating:<br />' . (number_format($analysis->rating,2) + 0) . '%</span></h2>
                                <span class="center">% Coverage:' . (number_format($analysis->coverage,2) + 0) . '</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9"></td>
                            </tr>
                            <tr>
                                <td colspan="9"></td>
                            </tr>
                            <tr>
                                <td colspan="9"><h2 class="center">GMP RAW DATA REPORT</h2></td>
                            </tr>
                            <tr>
                                <td colspan="9"></td>
                            </tr>
                            <tr>
                                <td colspan="9"></td>
                            </tr>
                            <tr>
                                <td colspan="9"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Company Name: </b></td>
                                <td colspan="4">' . $report[0]->name . '</td>
                                <td colspan="2"><b>Date of Audit: </b></td>
                                <td colspan="4">'.$audit_date_html.'</td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Site Address: </b></td>
                                <td colspan="10">'.$this->htmlformatter($report[0]->address1). ', ' .$this->htmlformatter($report[0]->address2).' '.$this->htmlformatter($report[0]->address3).', '.strtoupper($country[0]->country_name).'</td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Lead Auditor: </b></td>
                                <td colspan="10">' . $this->htmlformatter($lead_auditor[0]->fname) . ' ' . $this->htmlformatter($lead_auditor[0]->lname) . '</td>
                            </tr>
                            '.$coauditor_html.'
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>Product type:</b></td>
                                <td colspan="10">'.$this->htmlformatter($template[0]->classification_name).'</td>
                            </tr>
                            <tr>
                                <td colspan="12"><b>Products:</b></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"><ol>'.$product_html.'</ol></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="3"><b>Standard/Reference:</b></td>
                                <td colspan="9">'.$this->htmlformatter($template[0]->standard_name).'</td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                            </tr>
                        </table>
                            '.$element_html.'

                    ';


            $pdf->writeHTML($html,true,false,true,false,'');     

            switch ($action) {
                case 'I':
                     $pdf->Output($report[0]->report_no.'-Raw_Data.pdf', 'I');
                    break;
                
                case 'F':
                    if (!file_exists('./json/export/approved/' . $id)) {
                        mkdir('./json/export/approved/' . $id, 0777, true);
                    }
                    if (!file_exists('./json/export/archive/' . $id)) {
                        mkdir('./json/export/archive/' . $id, 0777, true);
                    }

                    $pdf->Output(__DIR__.'./../../json/export/approved/' . $id . '/Raw_Data.pdf', 'F');
                    $pdf->Output(__DIR__.'./../../json/export/archive/' . $id . '/Raw_Data.pdf', 'F');
                    break;

                case 'D':
                    $pdf->Output('Raw_Data '.$report[0]->report_no.'.pdf', 'D');
                    break;
                
            }

        }
	}