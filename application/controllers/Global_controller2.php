<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Taipei');

class Global_controller extends CI_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('global_model');

        $this->load->helper('url');

    }

public function getlist_global(){

	$table = $_POST['table'];

	$limit = $_POST['limit'];

	$offset = $_POST['offset'];

  $offset = ($_POST['offset']-1)* $limit;

	$sort = $_POST['order_by'];

	$data = $this->global_model->getlist_global_model($table,$limit,$offset,$sort);

	echo json_encode($data);

}

function getcount_global() {

	$table = $_POST['table'];

	$order_by = $_POST['order_by'];

	$per_page = $_POST['limit'];

	$limit = '9999999';

	$offset = '0';

	$data = count($this->global_model->getlist_global_model($table,$limit,$offset,$order_by));

	$page_count = ceil($data/$per_page);

	echo $page_count;

}

function delete_global(){

	$table = $_POST['table'];

	$field = $_POST['order_by'];

	$id = $_POST['id'];

	$this->global_model->delete_data($id, $table, $field);

}

function inactive_global_update(){

  $table = $_POST['table'];

  $field = $_POST['order_by'];

  $id = $_POST['id'];

  $type = $_POST['type'];

  $data = array('status' => $type);

  $this->global_model->inactive_global($id, $data, $field, $table);

}

public function action_global(){

	$module = $_POST['module'];

	$type = $_POST['type'];

	$data['id'] = $_POST['id'];

	$this->load->view($module.'/'.$type,$data);

}

function edit_global() {

	$table = $_POST['table'];

	$field = $_POST['field'];

	$id = $_POST['id'];

	$data = $this->global_model->getedit_global($id,$table,$field);

	echo json_encode($data);

}

/* image manager */

function getimages(){

  $folder = $_POST['folder'];

  $i= 4;

  $files = glob("images/".$folder."*.*");

  echo "<div class = 'col-md-12 size' style='overflow:scroll; height: 350px;'> ";

 

  for ($i=0; $i<count($files); $i++)

  {

    $path = $files[$i];

     $filename = explode('/', $path);

     $filename =  $filename[count($filename)-1];

     $file_folder = explode('.',$filename);

     $ext = strtolower($file_folder[1]);

     if($ext=='jpeg' || $ext=='jpg' || $ext=='png' || $ext=='gif' || $ext=='doc' || $ext=='docx' || $ext=='xls' || $ext=='xlsx' || $ext=='pdf' ||$ext=='mp4'){

    echo "<div class = 'col-md-2 col-xs-2 col-sm-2 pad-5 image-con' style = 'margin:10px 9px; border-radius:3px;'> <div class = 'col-md-12 pad-0 size-img' style='overflow: hidden; height: 80px;'>

      <img style = 'position:absolute; display:none' class = 'check check_".$i."' width='15px' src = '".base_url()."asset/img/check.png'>

     <p style = 'display:none;'>" . $path;

      

      $file_type = 'file';

      

      if($ext=='doc' || $ext=='docx')

      {

        $path2 = base_url().'asset/img/doc.png';

      }

      elseif($ext=='xls' || $ext=='xlsx')

      {

        $path2 = base_url().'asset/img/excel.png';

      }

      elseif($ext=='pdf')

      {

        $path2 = base_url().'asset/img/pdf.png';

      }

      elseif($ext=='mp4')

      {

        $path2 = base_url().'asset/img/mp4.jpg';

      }

      else

      {

        $path2 = base_url().$path;

        $file_type = 'image';

      }

      echo '</p><img title="'.$filename.'" data-type="'.$file_type.'" file-name = "'.$path.'" class = "image-file-copy img-file_'.$i.'" data-id = "'.$i.'" style = "height:100%; cursor:pointer; text-align:center;" src="'.$path2.'" alt="random image" />'."</div><d style = 'font-size:10px;' title='".$filename."'>".substr($filename, 0, 17)."</d></div>";

     }

  }

  echo "</div>";

  echo "</div>";

}

function create_folder() {

  $directory = 'images/'.$_POST['new_folder'].'/';

  if (!is_dir($directory)) {

    mkdir($directory,0777,true);

    echo "yes";

  }

}

/* image manager */

/* upload module */

function uploadfile(){

  $filename = pathinfo($_FILES["file"]["name"]);

  $image_path = $filename['filename'].'_'.time().'.'.$filename['extension'];

  $path = 'images/'.$_POST['upload_path'];

  if ( 0 < $_FILES['file']['error'] ) {

        echo 'Error: ' . $_FILES['file']['error'] . '<br>';

    }

    else {

        move_uploaded_file($_FILES['file']['tmp_name'], $path.$image_path);

    }

}

/* upload module */

function checkAction($action, $data, $table, $field, $id) {

	if($action=="save"){

		$data = $this->global_model->save_data($table, $data);

	} else {

		$data = $this->global_model->update_data($field, $id, $table, $data);

	}

	return $data;

}

/* Start of User */

function user_array() {

	$table = $_POST['table'];

	$field = $_POST['field'];

	$action = $_POST['action'];

	$id = "";

	if($action=="update") {

		$id = $_POST['id'];

	}

	$data = array(

		'fname' => $_POST['fname'],

		'mname' => $_POST['mname'],

		'lname' => $_POST['lname'],

		'designation' => $_POST['designation'],

		'department' => $_POST['division'],

		'company' => $_POST['company'],

    'administrator' => $_POST['administrator'],

		'email' => $_POST['email'],

		'image' => str_replace('../', '', $_POST['signature'])

	);

	$data = json_decode(json_encode($data), FALSE);

	$this->checkAction($action, $data, $table, $field, $id);

}

/* End of User */

/* Start of User Login */

function user_login() {

  $table = $_POST['table'];

  $field = $_POST['field'];

  $action = $_POST['action'];

  $id = "";

  if($action=="update") {

    $id = $_POST['id'];

  }

  $data = array(

    'name' => $_POST['name'],

    'status' => $_POST['status'],

    'role' => $_POST['role'],

    'email' => $_POST['email']

  );

  $data = json_decode(json_encode($data), FALSE);

  $this->checkAction($action, $data, $table, $field, $id);

}

public function get_where_user(){

        $field = $_POST['field'];

        $where = $_POST['where'];

        $table = $_POST['table'];

        

        $data = $this->global_model->get_where($table, $field, $where);

        echo json_encode(array('count' => count($data), 'result' => $data));

 

}

/* End of user Login */

public function check_email(){ 

      $where = "email = '".$_POST['user']."' ";

      $data = $this->global_model->check_exist('tbl_cms_login', $where);

      $count = $data[0]->count;

      $role = $data[0]->role;

      echo json_encode(array('count' => $count, 'role' => $role));

}

public function get_data(){

  $data = $this->global_model->getdata();

  if($_POST['table']=='country'){   

    $array2 = array();

    foreach ($data as $key => $value) {

      $array = array();

      $array['country_id'] = $value->country_id;

      $array['country'] = $this->country_code_to_country($value->iso2_code);

      array_push($array2, $array);

    }

    $data = array();

    $data = $array2;

  }

  echo json_encode($data);

}

public function get_data_city_province(){

  $id = $_POST['id'];

  $field = $_POST['field'];

  $table = $_POST['table'];

  $data = get_data_city_province_model($table,$field,$id);

  echo json_encode($data);

}

public function convert_iso_name(){

  $names = json_decode(file_get_contents("http://country.io/names.json"), true);

  echo $names[$_POST['country']];

}

public function country_code_to_country( $code ){

    $country = '';

    if( $code == 'AF' ) $country = 'Afghanistan';

    if( $code == 'AX' ) $country = 'Aland Islands';

    if( $code == 'AL' ) $country = 'Albania';

    if( $code == 'DZ' ) $country = 'Algeria';

    if( $code == 'AS' ) $country = 'American Samoa';

    if( $code == 'AD' ) $country = 'Andorra';

    if( $code == 'AO' ) $country = 'Angola';

    if( $code == 'AI' ) $country = 'Anguilla';

    if( $code == 'AQ' ) $country = 'Antarctica';

    if( $code == 'AG' ) $country = 'Antigua and Barbuda';

    if( $code == 'AR' ) $country = 'Argentina';

    if( $code == 'AM' ) $country = 'Armenia';

    if( $code == 'AW' ) $country = 'Aruba';

    if( $code == 'AU' ) $country = 'Australia';

    if( $code == 'AT' ) $country = 'Austria';

    if( $code == 'AZ' ) $country = 'Azerbaijan';

    if( $code == 'BS' ) $country = 'Bahamas the';

    if( $code == 'BH' ) $country = 'Bahrain';

    if( $code == 'BD' ) $country = 'Bangladesh';

    if( $code == 'BB' ) $country = 'Barbados';

    if( $code == 'BY' ) $country = 'Belarus';

    if( $code == 'BE' ) $country = 'Belgium';

    if( $code == 'BZ' ) $country = 'Belize';

    if( $code == 'BJ' ) $country = 'Benin';

    if( $code == 'BM' ) $country = 'Bermuda';

    if( $code == 'BT' ) $country = 'Bhutan';

    if( $code == 'BO' ) $country = 'Bolivia';

    if( $code == 'BA' ) $country = 'Bosnia and Herzegovina';

    if( $code == 'BW' ) $country = 'Botswana';

    if( $code == 'BV' ) $country = 'Bouvet Island (Bouvetoya)';

    if( $code == 'BR' ) $country = 'Brazil';

    if( $code == 'IO' ) $country = 'British Indian Ocean Territory (Chagos Archipelago)';

    if( $code == 'VG' ) $country = 'British Virgin Islands';

    if( $code == 'BN' ) $country = 'Brunei Darussalam';

    if( $code == 'BG' ) $country = 'Bulgaria';

    if( $code == 'BF' ) $country = 'Burkina Faso';

    if( $code == 'BI' ) $country = 'Burundi';

    if( $code == 'KH' ) $country = 'Cambodia';

    if( $code == 'CM' ) $country = 'Cameroon';

    if( $code == 'CA' ) $country = 'Canada';

    if( $code == 'CV' ) $country = 'Cape Verde';

    if( $code == 'KY' ) $country = 'Cayman Islands';

    if( $code == 'CF' ) $country = 'Central African Republic';

    if( $code == 'TD' ) $country = 'Chad';

    if( $code == 'CL' ) $country = 'Chile';

    if( $code == 'CN' ) $country = 'China';

    if( $code == 'CX' ) $country = 'Christmas Island';

    if( $code == 'CC' ) $country = 'Cocos (Keeling) Islands';

    if( $code == 'CO' ) $country = 'Colombia';

    if( $code == 'KM' ) $country = 'Comoros the';

    if( $code == 'CD' ) $country = 'Congo';

    if( $code == 'CG' ) $country = 'Congo the';

    if( $code == 'CK' ) $country = 'Cook Islands';

    if( $code == 'CR' ) $country = 'Costa Rica';

    if( $code == 'CI' ) $country = 'Cote d\'Ivoire';

    if( $code == 'HR' ) $country = 'Croatia';

    if( $code == 'CU' ) $country = 'Cuba';

    if( $code == 'CY' ) $country = 'Cyprus';

    if( $code == 'CZ' ) $country = 'Czech Republic';

    if( $code == 'DK' ) $country = 'Denmark';

    if( $code == 'DJ' ) $country = 'Djibouti';

    if( $code == 'DM' ) $country = 'Dominica';

    if( $code == 'DO' ) $country = 'Dominican Republic';

    if( $code == 'EC' ) $country = 'Ecuador';

    if( $code == 'EG' ) $country = 'Egypt';

    if( $code == 'SV' ) $country = 'El Salvador';

    if( $code == 'GQ' ) $country = 'Equatorial Guinea';

    if( $code == 'ER' ) $country = 'Eritrea';

    if( $code == 'EE' ) $country = 'Estonia';

    if( $code == 'ET' ) $country = 'Ethiopia';

    if( $code == 'FO' ) $country = 'Faroe Islands';

    if( $code == 'FK' ) $country = 'Falkland Islands (Malvinas)';

    if( $code == 'FJ' ) $country = 'Fiji the Fiji Islands';

    if( $code == 'FI' ) $country = 'Finland';

    if( $code == 'FR' ) $country = 'France, French Republic';

    if( $code == 'GF' ) $country = 'French Guiana';

    if( $code == 'PF' ) $country = 'French Polynesia';

    if( $code == 'TF' ) $country = 'French Southern Territories';

    if( $code == 'GA' ) $country = 'Gabon';

    if( $code == 'GM' ) $country = 'Gambia the';

    if( $code == 'GE' ) $country = 'Georgia';

    if( $code == 'DE' ) $country = 'Germany';

    if( $code == 'GH' ) $country = 'Ghana';

    if( $code == 'GI' ) $country = 'Gibraltar';

    if( $code == 'GR' ) $country = 'Greece';

    if( $code == 'GL' ) $country = 'Greenland';

    if( $code == 'GD' ) $country = 'Grenada';

    if( $code == 'GP' ) $country = 'Guadeloupe';

    if( $code == 'GU' ) $country = 'Guam';

    if( $code == 'GT' ) $country = 'Guatemala';

    if( $code == 'GG' ) $country = 'Guernsey';

    if( $code == 'GN' ) $country = 'Guinea';

    if( $code == 'GW' ) $country = 'Guinea-Bissau';

    if( $code == 'GY' ) $country = 'Guyana';

    if( $code == 'HT' ) $country = 'Haiti';

    if( $code == 'HM' ) $country = 'Heard Island and McDonald Islands';

    if( $code == 'VA' ) $country = 'Holy See (Vatican City State)';

    if( $code == 'HN' ) $country = 'Honduras';

    if( $code == 'HK' ) $country = 'Hong Kong';

    if( $code == 'HU' ) $country = 'Hungary';

    if( $code == 'IS' ) $country = 'Iceland';

    if( $code == 'IN' ) $country = 'India';

    if( $code == 'ID' ) $country = 'Indonesia';

    if( $code == 'IR' ) $country = 'Iran';

    if( $code == 'IQ' ) $country = 'Iraq';

    if( $code == 'IE' ) $country = 'Ireland';

    if( $code == 'IM' ) $country = 'Isle of Man';

    if( $code == 'IL' ) $country = 'Israel';

    if( $code == 'IT' ) $country = 'Italy';

    if( $code == 'JM' ) $country = 'Jamaica';

    if( $code == 'JP' ) $country = 'Japan';

    if( $code == 'JE' ) $country = 'Jersey';

    if( $code == 'JO' ) $country = 'Jordan';

    if( $code == 'KZ' ) $country = 'Kazakhstan';

    if( $code == 'KE' ) $country = 'Kenya';

    if( $code == 'KI' ) $country = 'Kiribati';

    if( $code == 'KP' ) $country = 'Korea';

    if( $code == 'KR' ) $country = 'Korea';

    if( $code == 'KW' ) $country = 'Kuwait';

    if( $code == 'KG' ) $country = 'Kyrgyz Republic';

    if( $code == 'LA' ) $country = 'Lao';

    if( $code == 'LV' ) $country = 'Latvia';

    if( $code == 'LB' ) $country = 'Lebanon';

    if( $code == 'LS' ) $country = 'Lesotho';

    if( $code == 'LR' ) $country = 'Liberia';

    if( $code == 'LY' ) $country = 'Libyan Arab Jamahiriya';

    if( $code == 'LI' ) $country = 'Liechtenstein';

    if( $code == 'LT' ) $country = 'Lithuania';

    if( $code == 'LU' ) $country = 'Luxembourg';

    if( $code == 'MO' ) $country = 'Macao';

    if( $code == 'MK' ) $country = 'Macedonia';

    if( $code == 'MG' ) $country = 'Madagascar';

    if( $code == 'MW' ) $country = 'Malawi';

    if( $code == 'MY' ) $country = 'Malaysia';

    if( $code == 'MV' ) $country = 'Maldives';

    if( $code == 'ML' ) $country = 'Mali';

    if( $code == 'MT' ) $country = 'Malta';

    if( $code == 'MH' ) $country = 'Marshall Islands';

    if( $code == 'MQ' ) $country = 'Martinique';

    if( $code == 'MR' ) $country = 'Mauritania';

    if( $code == 'MU' ) $country = 'Mauritius';

    if( $code == 'YT' ) $country = 'Mayotte';

    if( $code == 'MX' ) $country = 'Mexico';

    if( $code == 'FM' ) $country = 'Micronesia';

    if( $code == 'MD' ) $country = 'Moldova';

    if( $code == 'MC' ) $country = 'Monaco';

    if( $code == 'MN' ) $country = 'Mongolia';

    if( $code == 'ME' ) $country = 'Montenegro';

    if( $code == 'MS' ) $country = 'Montserrat';

    if( $code == 'MA' ) $country = 'Morocco';

    if( $code == 'MZ' ) $country = 'Mozambique';

    if( $code == 'MM' ) $country = 'Myanmar';

    if( $code == 'NA' ) $country = 'Namibia';

    if( $code == 'NR' ) $country = 'Nauru';

    if( $code == 'NP' ) $country = 'Nepal';

    if( $code == 'AN' ) $country = 'Netherlands Antilles';

    if( $code == 'NL' ) $country = 'Netherlands the';

    if( $code == 'NC' ) $country = 'New Caledonia';

    if( $code == 'NZ' ) $country = 'New Zealand';

    if( $code == 'NI' ) $country = 'Nicaragua';

    if( $code == 'NE' ) $country = 'Niger';

    if( $code == 'NG' ) $country = 'Nigeria';

    if( $code == 'NU' ) $country = 'Niue';

    if( $code == 'NF' ) $country = 'Norfolk Island';

    if( $code == 'MP' ) $country = 'Northern Mariana Islands';

    if( $code == 'NO' ) $country = 'Norway';

    if( $code == 'OM' ) $country = 'Oman';

    if( $code == 'PK' ) $country = 'Pakistan';

    if( $code == 'PW' ) $country = 'Palau';

    if( $code == 'PS' ) $country = 'Palestinian Territory';

    if( $code == 'PA' ) $country = 'Panama';

    if( $code == 'PG' ) $country = 'Papua New Guinea';

    if( $code == 'PY' ) $country = 'Paraguay';

    if( $code == 'PE' ) $country = 'Peru';

    if( $code == 'PH' ) $country = 'Philippines';

    if( $code == 'PN' ) $country = 'Pitcairn Islands';

    if( $code == 'PL' ) $country = 'Poland';

    if( $code == 'PT' ) $country = 'Portugal, Portuguese Republic';

    if( $code == 'PR' ) $country = 'Puerto Rico';

    if( $code == 'QA' ) $country = 'Qatar';

    if( $code == 'RE' ) $country = 'Reunion';

    if( $code == 'RO' ) $country = 'Romania';

    if( $code == 'RU' ) $country = 'Russian Federation';

    if( $code == 'RW' ) $country = 'Rwanda';

    if( $code == 'BL' ) $country = 'Saint Barthelemy';

    if( $code == 'SH' ) $country = 'Saint Helena';

    if( $code == 'KN' ) $country = 'Saint Kitts and Nevis';

    if( $code == 'LC' ) $country = 'Saint Lucia';

    if( $code == 'MF' ) $country = 'Saint Martin';

    if( $code == 'PM' ) $country = 'Saint Pierre and Miquelon';

    if( $code == 'VC' ) $country = 'Saint Vincent and the Grenadines';

    if( $code == 'WS' ) $country = 'Samoa';

    if( $code == 'SM' ) $country = 'San Marino';

    if( $code == 'ST' ) $country = 'Sao Tome and Principe';

    if( $code == 'SA' ) $country = 'Saudi Arabia';

    if( $code == 'SN' ) $country = 'Senegal';

    if( $code == 'RS' ) $country = 'Serbia';

    if( $code == 'SC' ) $country = 'Seychelles';

    if( $code == 'SL' ) $country = 'Sierra Leone';

    if( $code == 'SG' ) $country = 'Singapore';

    if( $code == 'SK' ) $country = 'Slovakia (Slovak Republic)';

    if( $code == 'SI' ) $country = 'Slovenia';

    if( $code == 'SB' ) $country = 'Solomon Islands';

    if( $code == 'SO' ) $country = 'Somalia, Somali Republic';

    if( $code == 'ZA' ) $country = 'South Africa';

    if( $code == 'GS' ) $country = 'South Georgia and the South Sandwich Islands';

    if( $code == 'ES' ) $country = 'Spain';

    if( $code == 'LK' ) $country = 'Sri Lanka';

    if( $code == 'SD' ) $country = 'Sudan';

    if( $code == 'SR' ) $country = 'Suriname';

    if( $code == 'SJ' ) $country = 'Svalbard & Jan Mayen Islands';

    if( $code == 'SZ' ) $country = 'Swaziland';

    if( $code == 'SE' ) $country = 'Sweden';

    if( $code == 'CH' ) $country = 'Switzerland, Swiss Confederation';

    if( $code == 'SY' ) $country = 'Syrian Arab Republic';

    if( $code == 'TW' ) $country = 'Taiwan';

    if( $code == 'TJ' ) $country = 'Tajikistan';

    if( $code == 'TZ' ) $country = 'Tanzania';

    if( $code == 'TH' ) $country = 'Thailand';

    if( $code == 'TL' ) $country = 'Timor-Leste';

    if( $code == 'TG' ) $country = 'Togo';

    if( $code == 'TK' ) $country = 'Tokelau';

    if( $code == 'TO' ) $country = 'Tonga';

    if( $code == 'TT' ) $country = 'Trinidad and Tobago';

    if( $code == 'TN' ) $country = 'Tunisia';

    if( $code == 'TR' ) $country = 'Turkey';

    if( $code == 'TM' ) $country = 'Turkmenistan';

    if( $code == 'TC' ) $country = 'Turks and Caicos Islands';

    if( $code == 'TV' ) $country = 'Tuvalu';

    if( $code == 'UG' ) $country = 'Uganda';

    if( $code == 'UA' ) $country = 'Ukraine';

    if( $code == 'AE' ) $country = 'United Arab Emirates';

    if( $code == 'GB' ) $country = 'United Kingdom';

    if( $code == 'US' ) $country = 'United States of America';

    if( $code == 'UM' ) $country = 'United States Minor Outlying Islands';

    if( $code == 'VI' ) $country = 'United States Virgin Islands';

    if( $code == 'UY' ) $country = 'Uruguay, Eastern Republic of';

    if( $code == 'UZ' ) $country = 'Uzbekistan';

    if( $code == 'VU' ) $country = 'Vanuatu';

    if( $code == 'VE' ) $country = 'Venezuela';

    if( $code == 'VN' ) $country = 'Vietnam';

    if( $code == 'WF' ) $country = 'Wallis and Futuna';

    if( $code == 'EH' ) $country = 'Western Sahara';

    if( $code == 'YE' ) $country = 'Yemen';

    if( $code == 'ZM' ) $country = 'Zambia';

    if( $code == 'ZW' ) $country = 'Zimbabwe';

    if( $country == '') $country = $code;

    return $country;

}





function update_company(){

  $table = $_POST['table'];

  $field = $_POST['field'];

  $data = array(

    'company_id' => $_POST['id'],

    'type' => $_POST['type'],

    'name' => $_POST['name'],

    'address1' => $_POST['street'],

    'address3' => $_POST['city'],

    'address4' => $_POST['province'],

    'country' => $_POST['country'],

    'background' => $_POST['background'],

    'update_date' => date('Y-m-d H:i:s')

  );

  $data = json_decode(json_encode($data), FALSE);

  $this->global_model->update_data($field,  $_POST['id'], $table, $data);

}





function save_company(){

  $table = $_POST['table'];

  $field = $_POST['field'];

  $data = array(

    'type' => $_POST['type'],

    'name' => $_POST['name'],

    'address1' => $_POST['street'],

    'address3' => $_POST['city'],

    'address4' => $_POST['province'],

    'country' => $_POST['country'],

    'background' => $_POST['background'],

    'create_date' => date('Y-m-d H:i:s')

  );

  $data = json_decode(json_encode($data), FALSE);

  $this->global_model->save_data($table, $data);

}



function get_by_id(){

  $table = $_POST['table'];

  $field = $_POST['field'];

  $where = $_POST['id'];

  $data = $this->global_model->get_where($table, $field, $where);

  echo json_encode($data);

}



}