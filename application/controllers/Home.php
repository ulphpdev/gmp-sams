<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');

class Home extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Audit_trail_model');
		$this->load->model('q_auditreport');
		$this->load->helper('url');
		$this->load->model('Audit_report_model');

		if($this->session->userdata('sess_email')=='' ) {
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}

	}

	public function index()
	{


		if($this->session->userdata('sess_email')=='' ) {
			$this->load->view('login');
		} else {
			$data['dashboard'] = 'dashboard';
			$data['manage_template'] = 'manage_template';
			$data['audit_report'] = 'audit_report';
			$data['data_maintenance'] = 'data_maintenance';
			$data['content'] = 'home';
			$data['audit_reports_cnt'] = count($this->global_model->get_dashboard_filter_status('tbl_report_listing','report_id'));
			$data['templates_cnt'] = count($this->global_model->get_dashboard('tbl_template'));
			$data['audit_report_analysis'] = count($this->global_model->get_dashboard_filter_status('tbl_report_listing', 'report_id'));
			$this->load->view('layout/layout',$data);
		}

	}



	function user(){
		if($this->session->userdata('sess_role') == 1){
			$data['content'] = 'user/list';
			$this->load->view('layout/layout',$data);
		} else {
			$this->index();
		}
	}


	public function template()
	{
		$data['content'] = 'template/list';
		$this->load->view('layout/layout',$data);
	}
	public function dashboard()
	{
		$data['dashboard'] = 'dashboard';
		$this->load->view('layout/dashboard',$data);
	}
	public function manage_template()
	{
		if($this->session->userdata('sess_email')=='' ) {
			$this->load->view('login');
		}else{
			$data['content'] = 'manage_template/list2';
			$this->load->view('layout/layout',$data);
		}

	}



	public function audit_report()
	{
		$data['content' ]= 'audit_report/list';
		$this->load->view('layout/layout',$data);
	}

	public function audit_report2()
	{
		$data['content' ]= 'audit_report/list2';
		$data['type_of_products'] = $this->q_auditreport->group_list("tbl_classification", "classification_name");
		$data['auditors'] = $this->q_auditreport->group_list("tbl_auditor_info", "fname");
		$data['products_list'] = $this->q_auditreport->group_list("tbl_product", "product_name");
		$data['country_list'] = $this->q_auditreport->get_country();
		$this->load->view('layout/layout',$data);
	}



	public function data_maintenance()
	{
		$data['data_maintenance' ]= 'data_maintenance';
		$this->load->view('layout/data_maintenance',$data);
	}

}
