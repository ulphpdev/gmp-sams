<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Audit_trail3 extends CI_Controller{

	public function __construct() {

		parent::__construct();

		$this->load->library('session');

		$this->load->model('Audit_trail_model');

		$this->load->helper('url');

		$this->load->model('global_model');

		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}



	}



	public function index()

	{


		$data['content' ]= 'audit_trail/list';

		$this->load->view('layout/layout',$data);

	}



	public function Audit_trail_list(){

		$limit = $_POST['limit'];

		$offset = $_POST['offset'];

		$offset = ($_POST['offset']-1)* $limit;

		// $sort = $_POST['order_by'];

		$data = $this->Audit_trail_model->Audit_trail_list($limit,$offset);

		echo json_encode($data);

	}



	function Audit_trail_list_count() {

		// $table = $_POST['table'];

		$order_by = $_POST['order_by'];

		$per_page = $_POST['limit'];

		$limit = '9999999';

		$offset = '0';

		$data = count($this->Audit_trail_model->Audit_trail_list($limit,$offset));

		$page_count = ceil($data/$per_page);

		echo $page_count;

	}


	public function get_list_audit_trail(){

		// header('Content-type: Application/JSON');

		$limit = $_POST['limit'];

		$offset = $_POST['offset'];

		$query = $_POST['query'];


		echo json_encode($this->Audit_trail_model->get_audit_trail_list($query,$limit,$offset), JSON_PRETTY_PRINT);

	}
}



?>