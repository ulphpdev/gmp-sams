<?php 
class C_maintenance extends CI_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('q_maintenance');
		$this->load->helper('url');
		$this->load->model('Audit_trail_model');
		date_default_timezone_set('Asia/Manila');
	}
	///////// DISPOSITION
	public function dispotition_get(){
		header('Content-type: Application/JSON');
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$query = $_POST['query'];
		echo json_encode($this->q_maintenance->get_list("tbl_disposition",$query,$limit,$offset, "disposition_id"), JSON_PRETTY_PRINT);
	}
	public function dispotition_get_pagination(){
		header('Content-type: Application/JSON');
		$query = $_POST['query'];
		echo $this->q_maintenance->get_pagination("tbl_disposition",$query);
	}
	public function disposition_add(){
		header('Content-type: Application/JSON');
		$disposition = strip_tags($_POST['disposition']);
		$dispotition_label = strip_tags($_POST['dispotition_label']);
		$data = array(
				"disposition_name"=>$disposition,
				"disposition_label"=>$dispotition_label,
				"create_date"=>date("Y-m-d H:i:s"),
				"update_date"=>date("Y-m-d H:i:s"),
				"status"=>1);
		echo $this->q_maintenance->content_insert("tbl_disposition", $data);
	}
	public function disposition_update(){
		header('Content-type: Application/JSON');
		$disposition = strip_tags($_POST['disposition']);
		$dispotition_label = strip_tags($_POST['dispotition_label']);
		$disposition_id = $_POST['id'];
		$query = "disposition_id = " . $disposition_id;
		$data = array(
				"disposition_name"=>$disposition,
				"disposition_label"=>$dispotition_label,
				"update_date"=>date("Y-m-d H:i:s")
				);
		echo $this->q_maintenance->content_update("tbl_disposition", $query, $data);
	}
	public function dispotition_get_details(){
		header('Content-type: Application/JSON');
		$id = $_POST['id'];
		$query = "disposition_id = " . $id;
		echo json_encode($this->q_maintenance->get_list("tbl_disposition",$query,1,0), JSON_PRETTY_PRINT);
	}
	public function update_status() {
     	$where = $_POST['where'];
      	$status = $_POST['status'];
      	$field = $_POST['field'];
      	$table = $_POST['table'];
      	$data = $this->q_maintenance->update_status($where, $status, $field, $table);
      	 $this->save_audit_trail('Update Disposition Status');
    }
    public function save_audit_trail($action){
    $data['user'] = $this->session->userdata('userid');
    $data['page'] = $this->agent->referrer();
    $data['type'] = $this->session->userdata('type');
    $data['role'] = $this->session->userdata('sess_role');
    $data['email'] = $this->session->userdata('sess_email');
    $data['action'] = ucwords($action);
    $data['date'] = date('Y-m-d H:i:s');
    $table = 'tbl_audit_trail';
    $this->Audit_trail_model->save_data($table,$data);
}
}