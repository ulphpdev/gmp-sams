<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send extends CI_Controller {
	public function __construct() {
		parent::__construct();
		// $this->load->library('session');
		$this->load->helper('url');
		$this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->load->model("Global_model","global_model");
	}

    function email_to_division_head_from_leadauditor()
    {
        $reportid = $_POST['report_id'];

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$reportid);
        $reviewer = $this->global_model->get_emails("email_reviewer",$reportid);
        $approver = $this->global_model->get_emails("email_approver",$reportid);

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($lead_auditor[0]->email,$lead_auditor[0]->fname ." " . $lead_auditor[0]->lname);
        $this->email->to($approver[0]->email);

        $subject = "Division Head's Approval - " . $company;
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi " . $approver[0]->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Please refer to the link below with regards to the audit of ".$company.".";
        $message .= "<br>";
        $message .= "This is for your review and final approval.";
        $message .= "<br>";
        $message .= "<br>";
        $message .= '<a href="'.base_url("approval").'?report_id='.$reportid.'&action=division_head">Link to Audit Report Summary</a>';
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);

        echo $this->email->send();

         if($this->global_model->report_submission_check($reportid) > 0){
            //update
            $data = array("approver"=>date('Y-m-d'));
            $this->global_model->report_submission_update($reportid,$data);
        } else {
            //insert
            $data = array("approver"=>$reportid,"coauditor"=>date('Y-m-d'));
            $this->global_model->report_submission_insert($data);
        }

        if($this->global_model->get_report_status($reportid) < 6){
            $this->global_model->change_report_status($reportid,4);
        }
        

    }

    function email_to_division_head()
    {
        $reportid = $_POST['report_id'];

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$reportid);
        $reviewer = $this->global_model->get_emails("email_reviewer",$reportid);
        $approver = $this->global_model->get_emails("email_approver",$reportid);

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
        $this->email->to($approver[0]->email);

        $subject = "Division Head's Approval - " . $company;
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi " . $approver[0]->fname . ".";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Please refer to the link below with regards to the audit of ".$company.".";
        $message .= "<br>";
        $message .= "This is for your review and final approval.";
        $message .= "<br>";
        $message .= "<br>";
        $message .= '<a href="'.base_url("approval").'?report_id='.$reportid.'&action=division_head">Link to Audit Report Summary</a>';
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();

       

        $this->global_model->change_report_status($reportid,4);

     // $reviewer[0]->fname;

    }

    function email_to_co_auditor_approve_from_approver()
    {
        $reportid = $_POST['report_id'];
        $remarks = $_POST['remarks'];

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$reportid);
        $approver = $this->global_model->get_emails("email_approver",$reportid);

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
        $this->email->to($lead_auditor[0]->email);

        $subject = "STATUS of ".$company."'s  Report";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi " . $lead_auditor[0]->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "The report for ".$company."'s audit was approved by the Division Head.  The audit report is now final and complete.";  
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject); 
        $this->email->message($message);

        $this->email->send();

        $this->global_model->change_report_status($reportid,5);

        //email co-auditors
        foreach($this->global_model->get_emails("email_coauditors",$reportid) as $value){

            $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
            $this->email->to($value->email);

            $subject = "STATUS of ".$company."'s  Report";
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi " . $value->fname . ",";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "The report for ".$company."'s audit was approved by the Division Head.  The audit report is now final and complete.";  
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you.";
            $message .= "</body></html>";

            $this->email->subject($subject); 
            $this->email->message($message);

            $this->email->send();
        }

    }

    function email_to_co_auditor_rejected_from_approver()
    {
        $reportid = $_POST['report_id'];
        $remarks = $_POST['remarks'];

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$reportid);
        $approver = $this->global_model->get_emails("email_approver",$reportid);

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
        $this->email->to($lead_auditor[0]->email);

        $subject = "COMMENTS on ".$company."'s  Report";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi " . $lead_auditor[0]->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "I have reviewed the Audit Report for ".$company.".";
        $message .= "<br>";
        $message .= "Please refer to the comments below:";  
        $message .= "<br>";
        $message .= "<br>";
        $message .= $remarks;
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();


        //email co-auditors
        foreach($this->global_model->get_emails("email_coauditors",$reportid) as $value){

            $this->email->from($approver[0]->email,$approver[0]->fname ." " . $approver[0]->lname);
            $this->email->to($value->email);

            $subject = "COMMENTS on ".$company."'s  Report";
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi " . $value->fname . ",";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "I have reviewed the Audit Report for ".$company.".";
            $message .= "<br>";
            $message .= "Please refer to the comments below:";  
            $message .= "<br>";
            $message .= "<br>";
            $message .= $remarks;
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you.";
            $message .= "</body></html>";

            $this->email->subject($subject); 
            $this->email->message($message);

            $this->email->send();
        }

    }

    function email_to_co_auditor_approve_from_reviewer()
    {
        $reportid = $_POST['report_id'];
        $remarks = $_POST['remarks'];

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$reportid);
        $reviewer = $this->global_model->get_emails("email_reviewer",$reportid);

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
        $this->email->to($lead_auditor[0]->email);

        $subject = "STATUS of ".$company."'s  Report";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi " . $lead_auditor[0]->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "The report for ".$company."'s audit was approved by the Department Head. It was submitted for the Division Head's approval.";  
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();

        //email co-auditors
        foreach($this->global_model->get_emails("email_coauditors",$reportid) as $value){

            $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
            $this->email->to($value->email);

            $subject = "STATUS of ".$company."'s  Report";
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi " . $value->fname . ",";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "The report for ".$company."'s audit was approved by the Department Head. It was submitted for the Division Head's approval.";  
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you.";
            $message .= "</body></html>";

            $this->email->subject($subject); 
            $this->email->message($message);

            $this->email->send();
        }
    }

    function email_to_co_auditor_rejected()
    {
        $reportid = $_POST['report_id'];
        $remarks = $_POST['remarks'];

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$reportid);
        $reviewer = $this->global_model->get_emails("email_reviewer",$reportid);

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
        $this->email->to($lead_auditor[0]->email);

        $subject = "COMMENTS on ".$company."'s  Report";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi " . $lead_auditor[0]->fname . ",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "I have reviewed the Audit Report for ".$company.".";
        $message .= "<br>";
        $message .= "Please see remarks below:";  
        $message .= "<br>";
        $message .= "<br>";
        $message .= $remarks;
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Thank you.";
        $message .= "</body></html>";

        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();

        //email co-auditors
        foreach($this->global_model->get_emails("email_coauditors",$reportid) as $value){

            $this->email->from($reviewer[0]->email,$reviewer[0]->fname ." " . $reviewer[0]->lname);
            $this->email->to($value->email);

            $subject = "COMMENTS on ".$company."'s  Report";
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi " . $value->fname . ",";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "I have reviewed the Audit Report for ".$company.".";
            $message .= "<br>";
            $message .= "Please see remarks below:";  
            $message .= "<br>";
            $message .= "<br>";
            $message .= $remarks;
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you.";
            $message .= "</body></html>";

            $this->email->subject($subject); 
            $this->email->message($message);

            $this->email->send();
        }

    }

    function email_to_coauditor()
    {
        $reportid = $_POST['report_id'];

        foreach($this->global_model->get_emails("email_lead_auditor",$reportid) as $value){
            $lead_auditor_email = $value->email;
            $lead_auditor_name = $value->fname . " " . $value->lname;
        }

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        
        $emails_coaudit = "";
        foreach($this->global_model->get_emails("email_coauditors",$reportid) as $value){

            $this->email->from($lead_auditor_email, $lead_auditor_name);
            $this->email->to($value->email);

            $subject = "Co-Auditor's Review - " . $company;
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi ".$value->fname. ",";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Please log in to DART to view the audit of ".$company.".";
            $message .= "<br>";
            $message .= "This is for your review and comments.";  
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you.";
            $message .= "</body></html>";

            $this->email->subject($subject);
            $this->email->message($message);

            $this->email->send();

            echo $this->email->print_debugger();

        }

        if($this->global_model->report_submission_check($reportid) > 0){
            //update
            $data = array("coauditor"=>date('Y-m-d'));
            $this->global_model->report_submission_update($reportid,$data);
        } else {
            //insert
            $data = array("report_id"=>$reportid,"coauditor"=>date('Y-m-d'));
            $this->global_model->report_submission_insert($data);
        }


        //check if status is greater than 2
        if($this->global_model->get_report_status($reportid) < 6){
            $this->global_model->change_report_status($reportid,1);
        }
        

    }

    function email_to_department_head()
    {
        $reportid = $_POST['report_id'];

        foreach($this->global_model->get_emails("email_lead_auditor",$reportid) as $value){
            $lead_auditor_email = $value->email;
            $lead_auditor_name = $value->fname . " " . $value->lname;
        }

        foreach ($this->global_model->get_report_details($reportid) as $value) {
            $report_no = $value->Report_No;
            $company = $value->name;
        }

        $reviewer = $this->global_model->get_emails("email_reviewer",$reportid);

            //echo $value->email;
            $this->email->from($lead_auditor_email, $lead_auditor_name);
            $this->email->to($reviewer[0]->email);

            $subject = "Department Head's Review - " . $company;
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi ".$reviewer[0]->fname. ".";
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Please refer to the link below with regards to the audit of ".$company.".";
            $message .= "<br>";
            $message .= "This is for your review and approval.";
            $message .= "<br>";
            $message .= "<br>";
            $message .= '<a href="'.base_url("approval").'?report_id='.$reportid.'&action=department_head">Link to Audit Report Summary</a>';
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you.";
            $message .= "</body></html>";

            $this->email->subject($subject);
            $this->email->message($message);

            $this->email->send();

            if($this->global_model->report_submission_check($reportid) > 0){
                //update
                $data = array("reviewer"=>date('Y-m-d'));
                $this->global_model->report_submission_update($reportid,$data);
            } else {
                //insert
                $data = array("report_id"=>$reportid,"reviewer"=>date('Y-m-d'));
                $this->global_model->report_submission_insert($data);
            }

            if($this->global_model->get_report_status($reportid) < 6){
                $this->global_model->change_report_status($reportid,3);
            }
            

            echo $reviewer[0]->email;

    }

	function mail_department_head() {

		$lead_auditor = json_decode($_POST['lead_auditor']);
		foreach ($lead_auditor as $key => $value) {
		$admin = $value->email;
		$lead_fname = $value->fname;
		$lname = $value->lname;
		$report_no = $value->report_no;
		$company = $value->name;
		$report_id = $value->report_id;
		
		


		$department_head = json_decode($_POST['department_head']);
		foreach ($department_head as $key => $value) {
		$email = $value->email;
		$fname = $value->fname;
		$lname = $value->lname;
        $reviewer_id = $value ->reviewer_id;
		// $name2 = $this->session->userdata('sess_fname').' '.$user_id = $this->session->userdata('sess_lname');
		// $hospital = $this->session->userdata('sess_hospital');
		
		// die();
		

		$this->email->from($admin, "GMP");
        $this->email->to($email);
        $subject = "".$report_no." For Department Head's Review ";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi Sir/Madam ".$value->fname.",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Audit Report ".$company." with GMP Number ".$report_no." is ready for your view.";
        $message .= "<br>";
        $message .= "Kindly click on this  to view and add remarks to the report. ";
        $message .= '<a href="'.base_url().'audit_report_link/report_link_remarks_departmenthead?report_id='.$report_id.'&reviewer_id='.$reviewer_id.'">Click here.</a>';
      	$message .= "<br>";
      	$message .= "<br>";
        $message .= "Regards,";
        $message .= "<br>";
        $message .= "".$lead_fname."";
        $message .= "</body></html>";
        $this->email->subject($subject);

        $this->email->message($message);
        if ($this->email->send()) {
        	echo 'success';
        }
        }
        }
        // else{
        // 	echo $this->email->print_debugger();
        // }
	}
	function mail_division_head() {

		$lead_auditor = json_decode($_POST['lead_auditor']);
		foreach ($lead_auditor as $key => $value) {
		$admin = $value->email;
		$lead_fname = $value->fname;
		$lname = $value->lname;
		$report_no = $value->report_no;
		$company = $value->name;
		$report_id = $value->report_id;
		
		


		$division_head = json_decode($_POST['division_head']);
		foreach ($division_head as $key => $value) {
		$email = $value->email;
		$fname = $value->fname;
		$lname = $value->lname;
        $approver_id = $value ->approver_id;

		// $name2 = $this->session->userdata('sess_fname').' '.$user_id = $this->session->userdata('sess_lname');
		// $hospital = $this->session->userdata('sess_hospital');
		
		// die();
		

		$this->email->from($admin, "GMP");
        $this->email->to($email);
        $subject = "".$report_no." For Division Head's Review ";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "Hi Sir/Madam ".$value->fname.",";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Audit Report ".$company." with GMP Number ".$report_no." is ready for your view.";
        $message .= "<br>";
        $message .= "Kindly click on this LINK to view and add remarks to the report. ";
		$message .= '<a href="'.base_url().'audit_report_link/report_link_remarks_divisionhead?report_id='.$report_id.'&approver_id='.$approver_id.'">Click here.</a>';
      	$message .= "<br>";
      	$message .= "<br>";
        $message .= "Regards,";
        $message .= "<br>";
        $message .= "".$lead_fname."";
        $message .= "</body></html>";
        $this->email->subject($subject);

        $this->email->message($message);
        if ($this->email->send()) {
        	echo 'success';
        }
        }
        }
        // else{
        // 	echo $this->email->print_debugger();
        // }
	}
	function coauditor_departmenthead() {
	$this->mail_co_auditor();
	$this->mail_department_head();

	}
	function coauditor_divisionhead() {
	$this->mail_co_auditor();
	$this->mail_division_head();

	}
	function departmenthead_divisionhead() {
	$this->mail_department_head();
	$this->mail_division_head();
	}
	function sendmail_all() {
	$this->mail_co_auditor();
	$this->mail_department_head();
	$this->mail_division_head();
	}

	function mail_division_head_return_email() {

		$lead_auditor = json_decode($_POST['lead_auditor']);
		foreach ($lead_auditor as $key => $value) {
		$admin = $value->email;
		$lead_fname = $value->fname;
		$lname = $value->lname;
		$report_no = $value->report_no;
		$company = $value->name;
		$report_id = $value->report_id;
		$remarks = $value->remarks;
		$status = $value ->status;
		
		if($status == 1 ){
        	$status = 'NA KAY CO-AUDITOR PA';
        }
        if($status == 2){
        	$status = 'NEED PA I APPROVE NI DEPARMENT HEAD';
        }
        if($status == 3){
        	$status = 'NEED PA I APPROVE NI DIVISION HEAD HEAD';
        }
        if($status == 4){
        	$status = 'APPROVED';
        }
        


		$division_head = json_decode($_POST['division_head']);
		foreach ($division_head as $key => $value) {
		$email = $value->email;
		$fname = $value->fname;
		$lname = $value->lname;
		// $name2 = $this->session->userdata('sess_fname').' '.$user_id = $this->session->userdata('sess_lname');
		// $hospital = $this->session->userdata('sess_hospital');
		
		// die();
		

		$this->email->from($email, "GMP");
        $this->email->to($admin);
        $subject = "".$report_no." For Division Head's Review ".$status." ";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "".$lead_fname."";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Done reviewing and our audit with  ".$company." with GMP Number ".$report_no." Please see below remarks:";
        $message .= "<br>";
  //       $message .= "Kindly click on this LINK to view and add remarks to the report. ";
		// $message .= '<a href="'.base_url().'audit_report_link/report_link_remarks?report_id='.$report_id.'">Click here.</a>';
        // $message .= "Kindly click on this LINK to view and add remarks to the report. ";
        // $message .= '<a href="'.base_url().'audit_report_link/report_link_remarks_divisionhead?report_id='.$report_id.'&approver_id='.$approver_id.'">Click here.</a>';
		$message .= "".$status." / ".$remarks."";
      	$message .= "<br>";
      	$message .= "<br>";
        $message .= "Regards,";
        $message .= "<br>";
        $message .= "".$fname."";
        $message .= "</body></html>";
        $this->email->subject($subject);

        $this->email->message($message);
        if ($this->email->send()) {
        	echo 'success';
        }
        }
        }
        // else{
        // 	echo $this->email->print_debugger();
        // }
	}
	function mail_department_head_division_head_email() {


		$lead_auditor = json_decode($_POST['lead_auditor']);
		foreach ($lead_auditor as $key => $value) {
		$admin = $value->email;
		$lead_fname = $value->fname;
		$lname = $value->lname;
		$report_no = $value->report_no;
		$company = $value->name;
		$report_id = $value->report_id;
		$remarks = $value->remarks;
		$status = $value ->status;
		
		
		if($status == 1 ){
        	// $status = 'NA KAY CO-AUDITOR PA';
        }
        if($status == 2){
        	// $status = 'NEED PA I APPROVE NI DEPARMENT HEAD';
        }
        if($status == 3){
        	// $status = 'NEED PA I APPROVE NI DIVISION HEAD HEAD';
        }
        if($status == 4){
        	// $status = 'APPROVED';
        }

		$department_head = json_decode($_POST['department_head']);
		foreach ($department_head as $key => $value) {
		$email = $value->email;
		$fname = $value->fname;
		$lname = $value->lname;
		// $name2 = $this->session->userdata('sess_fname').' '.$user_id = $this->session->userdata('sess_lname');
		// $hospital = $this->session->userdata('sess_hospital');
		
		$division_head = json_decode($_POST['division_head']);
		foreach ($division_head as $key => $value) {
		$email_division = $value->email;
		$fname_division = $value->fname;
		$lname_division = $value->lname;
        $approver_id = $value ->approver_id;

		

		$this->email->from($email, "GMP");
        $this->email->to($email_division);
        
        $subject = "".$report_no." For Division Head's Review (".$status.")";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "".$fname_division."";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Done reviewing and our audit with  ".$company." with GMP Number ".$report_no." Please see below remarks:";
        $message .= "<br>";
  //       $message .= "Kindly click on this LINK to view and add remarks to the report. ";
		// $message .= '<a href="'.base_url().'audit_report_link/report_link_remarks?report_id='.$report_id.'">Click here.</a>';
          $message .= "Kindly click on this LINK to view and add remarks to the report. ";
        $message .= '<a href="'.base_url().'audit_report_link/report_link_remarks_divisionhead?report_id='.$report_id.'&approver_id='.$approver_id.'">Click here.</a>';
		$message .= "".$status." / ".$remarks."";
      	$message .= "<br>";
      	$message .= "<br>";
        $message .= "Regards,";
        $message .= "<br>";
        $message .= "".$fname."";
        $message .= "</body></html>";
        $this->email->subject($subject);

        $this->email->message($message);
        if ($this->email->send()) {
        	echo 'success';

        }
    	}
        }
        }
        // else{
        // 	echo $this->email->print_debugger();
        // }
	}

	function reject_division_head_return_email() {

		$lead_auditor = json_decode($_POST['lead_auditor']);
		foreach ($lead_auditor as $key => $value) {
		$admin = $value->email;
		$lead_fname = $value->fname;
		$lname = $value->lname;
		$report_no = $value->report_no;
		$company = $value->name;
		$report_id = $value->report_id;
		$remarks = $value->remarks;
		$status = $value ->status;
		
		if($status == 1 ){
        	$status = 'NA KAY CO-AUDITOR PA';
        }
        if($status == 2){
        	$status = 'NEED PA I APPROVE NI DEPARMENT HEAD';
        }
        if($status == 3){
        	$status = 'NEED PA I APPROVE NI DIVISION HEAD HEAD';
        }
        if($status == 4){
        	$status = 'APPROVED';
        }


		$division_head = json_decode($_POST['division_head']);
		foreach ($division_head as $key => $value) {
		$email = $value->email;
		$fname = $value->fname;
		$lname = $value->lname;
		// $name2 = $this->session->userdata('sess_fname').' '.$user_id = $this->session->userdata('sess_lname');
		// $hospital = $this->session->userdata('sess_hospital');
		
		// die();
		

		$this->email->from($email);
        $this->email->to($admin);
        $subject = "".$report_no." For Division Head's Review ".$status." ";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "".$lead_fname."";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Done reviewing and our audit with  ".$company." with GMP Number ".$report_no." Please see below remarks:";
        $message .= "<br>";
  //       $message .= "Kindly click on this LINK to view and add remarks to the report. ";
		// $message .= '<a href="'.base_url().'audit_report_link/report_link_remarks?report_id='.$report_id.'">Click here.</a>';
		$message .= "".$status." / ".$remarks."";
      	$message .= "<br>";
      	$message .= "<br>";
        $message .= "Regards,";
        $message .= "<br>";
        $message .= "".$fname."";
        $message .= "</body></html>";
        $this->email->subject($subject);

        $this->email->message($message);
        if ($this->email->send()) {
        	echo 'success';
        }
        }
        }
        // else{
        // 	echo $this->email->print_debugger();
        // }
	}


	function reject_department_head_return_email() {

		$lead_auditor = json_decode($_POST['lead_auditor']);
		foreach ($lead_auditor as $key => $value) {
		$admin = $value->email;
		$lead_fname = $value->fname;
		$lname = $value->lname;
		$report_no = $value->report_no;
		$company = $value->name;
		$report_id = $value->report_id;
		$remarks = $value->remarks;
		$status = $value ->status;
		
		if($status == 1 ){
        	$status = 'NA KAY CO-AUDITOR PA';
        }
        if($status == 2){
        	$status = 'NEED PA I APPROVE NI DEPARMENT HEAD';
        }
        if($status == 3){
        	$status = 'NEED PA I APPROVE NI DIVISION HEAD HEAD';
        }
        if($status == 4){
        	$status = 'APPROVED';
        }


		$department_head = json_decode($_POST['department_head']);
		foreach ($department_head as $key => $value) {
		$email = $value->email;
		$fname = $value->fname;
		$lname = $value->lname;
		// $name2 = $this->session->userdata('sess_fname').' '.$user_id = $this->session->userdata('sess_lname');
		// $hospital = $this->session->userdata('sess_hospital');
		
		// die();
		

		$this->email->from($email);
        $this->email->to($admin);
        $subject = "".$report_no." For Department Head's Review ".$status." ";
        $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
        $message .= "".$lead_fname."";
        $message .= "<br>";
        $message .= "<br>";
        $message .= "Done reviewing and our audit with  ".$company." with GMP Number ".$report_no." Please see below remarks:";
        $message .= "<br>";
  //       $message .= "Kindly click on this LINK to view and add remarks to the report. ";
		// $message .= '<a href="'.base_url().'audit_report_link/report_link_remarks?report_id='.$report_id.'">Click here.</a>';
		$message .= "".$status." / ".$remarks."";
      	$message .= "<br>";
      	$message .= "<br>";
        $message .= "Regards,";
        $message .= "<br>";
        $message .= "".$fname."";
        $message .= "</body></html>";
        $this->email->subject($subject);

        $this->email->message($message);
        if ($this->email->send()) {
        	echo 'success';
        }
        }
        }
        // else{
        // 	echo $this->email->print_debugger();
        // }
	}

	function sendmail_other() {

       


        $report_id = $_POST['report_id'];
        $email = $_POST['sender_other'];
        $subject_other = $_POST['subject_other'];
        $message_other = $_POST['message_other'];
        $email_array = explode(',', $email);


        $pdf = base_url("download/generate_annexure") . "?report_id=" . $report_id;
        $pdf_attach = './email_attach/' . $report_id . '.pdf';
        file_put_contents($pdf_attach, file_get_contents($pdf));

        $doc = base_url("download/annexure_gmp_audit_details") . "?report_id=" . $report_id;
        $doc_attach = './email_attach/' . $report_id . '.docx';
        file_put_contents($doc_attach, file_get_contents($doc));


        $report_details = $this->global_model->get_report_details($report_id);


        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$report_id);

		foreach ($email_array as $key => $sendother) {
    		$this->email->from($lead_auditor[0]->email,$lead_auditor[0]->fname ." " . $lead_auditor[0]->lname);
            $this->email->to($sendother);
            $subject = $subject_other;
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi,";
            $message .= "<br>";
            $message .= "<br>";
            $message .= $message_other;
            $message .= '<br>';
            $message .= '<br>';
            $message .= "</body></html>";
            $this->email->attach($pdf_attach,"",$report_details[0]->Report_No);
            $this->email->attach($doc_attach,"",$report_details[0]->Report_No);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
            echo "success";
        }

	}


    function sendmail_other_approved() {

        $report_id = $_POST['report_id'];
        $email = $_POST['sender_other'];
        $subject_other = $_POST['subject_other'];
        $message_other = $_POST['message_other'];
        $email_array = explode(',', $email);

        $pdf = base_url("generate/annexure_pdf") . "?report_id=" . $report_id;
        $pdf_attach = './email_attach/Template-' . $template_id . '.pdf';
        file_put_contents($pdf_attach, file_get_contents($pdf));
        $this->email->attach($pdf_attach);


        $report_details = $this->global_model->get_report_details($report_id);

        $lead_auditor = $this->global_model->get_emails("email_lead_auditor",$report_id);
        foreach ($email_array as $key => $sendother) {
            $this->email->from($lead_auditor[0]->email,$lead_auditor[0]->fname ." " . $lead_auditor[0]->lname);
            $this->email->to($sendother);
            $subject = $subject_other;
            $message = "<html><head><title></title></head><body style='font-family:Arial'><p>";
            $message .= "Hi,";
            $message .= "<br>";
            $message .= "<br>";
            $message .= $message_other;
            $message .= '<br>';
            $message .= '<br>';
            $message .= "</body></html>";
            $this->email->attach($pdf_attach,"",$report_details[0]->Report_No);
            $this->email->subject($subject);
            $this->email->message($message);
            echo $this->email->send();
        }

    }



    
}


