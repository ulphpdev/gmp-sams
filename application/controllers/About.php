<?php 
class About extends CI_Controller{
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Audit_trail_model');
		$this->load->helper('url');

		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}
		
	}
	function index(){
		$data['content'] = 'about';
		$this->load->view('layout/layout',$data);
	}

}