<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Preview_report extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('Pdf');
		$this->load->model('Template_model');
		$this->load->model('Audit_report_model');
		$this->load->model('Preview_report_model');
		$this->load->model('Global_model');
		$this->load->helper('url');
	}
	public function index(){
		$report_id = $_POST['id'];
		$data = $this->Preview_report_model->get_report_summary($report_id);
		echo json_encode($data);
	}
	function get_audit_dates(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_list("tbl_report_audit_date",$report_id);
		echo json_encode($data);
	}
	function get_translator(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_translator($report_id);
		echo json_encode($data);
	}
	function get_co_auditors(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_co_auditors($report_id);
		echo json_encode($data);
	}
	function get_approvers(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_approvers($report_id);
		echo json_encode($data);
	}
	function get_reviewer(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_reviewer($report_id);
		echo json_encode($data);
	}
	function get_inspectors(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_inspectors($report_id);
		echo json_encode($data);
	}
	function get_inspection_changes(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_inspection_changes($report_id);
		echo json_encode($data);
	}
	function get_activities(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_activities($report_id);
		$array_return = array();
		foreach ($data as $key => $value) {
			$sub = $this->Preview_report_model->get_sub_activities($report_id,$value->activity_id);
			$array_return[] = array(
				"activity_name" => $value->activity_name,
				"sub_activity" => $sub
			);
		}
		echo json_encode($array_return);
	}
	function get_scope_audit(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_scope_audit($report_id);
		echo json_encode($data);
	}
	function get_scope_product(){
		$report_id = $_POST['report_id'];
		$scope_id = $_POST['scope_id'];
		$data = $this->Preview_report_model->get_scope_product($report_id,$scope_id);
		echo json_encode($data);
	}
	function get_license(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_license($report_id);
		echo json_encode($data);
	}
	function get_pre_document(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_pre_document($report_id);
		echo json_encode($data);
	}
	function get_personel_met(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_personel_met($report_id);
		echo json_encode($data);
	}
	function get_present_during_meeting(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_present_during_meeting($report_id);
		echo json_encode($data);
	}
	function get_distribution(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_distribution($report_id);
		echo json_encode($data);
	}
	function get_disposition(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_disposition($report_id);

		foreach ($data as $key => $value) {
			$products = $this->Preview_report_model->get_disposition_product($report_id, $value->disposition_id);
			$result_product = "";
			foreach ($products as $a => $b) {
				$result_product .= ", " . $b->product_name;
			}
			$result[] = array(
				"Disposition" => $value->disposition_name,
				"Products" => substr($result_product, 1)
			);
		}
		// echo "<pre>";
		// echo print_r($result);
		// echo "</pre>";
		echo json_encode($result);
	}
	function get_template(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_template($report_id);
		echo json_encode($data);
	}
	function get_element(){
		$template_id = $_POST['template_id'];
		$data = $this->Preview_report_model->get_element($template_id);
		echo json_encode($data);
	}
	function get_report_answer(){
		$report_id = $_POST['report_id'];
		$element_id = $_POST['element_id'];
		$data = $this->Preview_report_model->get_report_answer($report_id,$element_id);
		echo json_encode($data);
	}
	function get_element_reco(){
		$report_id = $_POST['report_id'];
		$data = $this->Preview_report_model->get_element_reco($report_id);
		echo json_encode($data);
	}

	function get_remarks(){
		$report_id = $_POST['report_id'];
		$result = $this->Preview_report_model->get_remarks($report_id);
		echo json_encode($result);
	}

	function get_audit_observation(){
		$report_id = $_POST['report_id'];
		$result = $this->Preview_report_model->get_audit_observation($report_id);
		echo json_encode($result);
	}

	function get_sub_activities(){
		$report_id = $_POST['report_id'];
		$activityid = $_POST['activityid'];
		$result = $this->Preview_report_model->get_sub_activities($report_id, $activityid);
		echo json_encode($result);
	}

	function get_report_scope_product(){
		$report_id = $_POST['report_id'];
		$scopes = $this->Preview_report_model->get_report_scope_product($report_id);
		$array = array();
		foreach ($scopes as $key => $value) {
			$product = $this->Preview_report_model->get_products($value->audit_scope_id, $report_id);
			$array[] = array(
						"scope"=>$value->scope_name,
						"products"=>$product
					);
		}
		echo json_encode($array);
	} 

	function get_observation_yes(){
		$report_id = $_POST['report_id'];
		$array = array();
		$result_element = $this->Preview_report_model->get_observation_yes_group($report_id);
		foreach ($result_element as $key => $value) {
			$result_elements = $this->Preview_report_model-> get_observation_yes($report_id, $value->element_id);
			$array[] = array(
					"element"=>$value->element_name,
					"details"=>$result_elements
			);
		}
		echo json_encode($array);
	}

	function get_element_questions_answers()
	{	
		$element_order =1 ;
		$count_no = 0;
		$report_id = $_POST['report_id'];
		$template_id = $_POST['template_id'];
		$elements_result = $this->Preview_report_model->preview_report_get_element($template_id);
		$element_array = array();
        foreach ($elements_result as $key => $value) {

        	$question_result = $this->Preview_report_model->get_questions_answers($report_id, $value->element_id);
        	$questions_array = array();
            foreach ($question_result as $k => $v) {

                if($v->answer_id == 2){
                    $count_no ++;
                    $answer_no = $count_no;
                } else {
                    $answer_no = "";
                }

                $questions_array[] = array(
                    "question"=> $v->question,
                    "answer_name"=> $v->answer_name,
                    "answer_details"=> $v->answer_details,
                    "category"=> $v->category_name,
                    "answer_no"=> $answer_no,
                );
            }

            $element_array[] = array(
                "element_id"=> $value->element_id,
                "element_name"=> $value->element_name,
                "order"=> $element_order,
                "questions" => $questions_array
            );

            $element_order++;
        }

		echo json_encode($element_array);


	}

	public function check_stamp()
	{
		$report_id = $_POST['report_id'];
		echo json_encode($this->Preview_report_model->check_stamp($report_id));
	}
	
	public function reset_stamp()
	{
		$report_id = $_POST['report_id'];
		echo json_encode($this->Preview_report_model->reset_stamp($report_id));
	}

	public function report_observation()
    {
      $report_id = $_POST['report_id'];  
      $report= $this->Audit_report_model->get_report($report_id);
      $element = $this->Template_model->get_elements($report[0]->template_id);

      foreach ($element as $key => $value) {
          # code...
        $is_no = 0;
        $is_yes = 0;
        $is_na = 0;
        $is_nc = 0;

        $questions = $this->Global_model->get_element_answers($report_id,$value->element_id);

        //counting all answers
        foreach ($questions as $b => $a) {
            if($a->answer_id == 1){
                $is_yes ++;
            }
            if($a->answer_id == 2){
                $is_no ++;
            }
            if($a->answer_id == 3){
                $is_na ++;
            }
            if($a->answer_id == 4){
                $is_nc ++;
            }
        }

        //generating answers for annexure
        $observation = "";

        //NA only
        if($is_na != 0 && $is_nc == 0 && $is_no == 0 && $is_yes == 0){
            $observation .= "Not Applicable.";
        }

        //NC only
        if($is_na == 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
            $observation .= "Not Covered.";
        }

        //NO ONLy
        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }

        //yes and no
        if($is_na == 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }

        //yes only
        if($is_na == 0 && $is_nc == 0 && $is_no == 0 && $is_yes != 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }

        //na and nc
        if($is_na != 0 && $is_nc != 0 && $is_no == 0 && $is_yes == 0){
            $observation = "Not Covered";
        }

        //nc and no
        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes == 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }

        //nc, no and yes
        if($is_na == 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }

        //na and no
        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes == 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }

        //na, no and yes
        if($is_na != 0 && $is_nc == 0 && $is_no != 0 && $is_yes != 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }

        //na, nc, no and yes
        if($is_na != 0 && $is_nc != 0 && $is_no != 0 && $is_yes != 0){
            foreach ($questions as $c => $d) {
                $observation .= $d->default_yes .". " . $d->answer_details . ", ";
            }
        }


        $array_result[] = array(
            'Element' => $value->element_name, 
            'Observation'=> $observation
        );
      }

      echo json_encode($array_result);

    }


}
?>