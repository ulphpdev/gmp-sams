<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Manage_template extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('Audit_trail_model');
		$this->load->model('Manage_template_model');
		$this->load->model('Template_model');
		$this->load->helper('url');
		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}
	}
	public function index()
	{	
		$data['content'] = 'manage_template/list2';
		$this->load->view('layout/layout',$data);
	}
	function get_product_type(){
		$data = $this->Template_model->get_product_type();
		echo json_encode($data);
	}
	function get_standard_reference(){
		$data = $this->Template_model->get_standard_reference();
		echo json_encode($data);
	}
	function save_template(){
		if($_POST['status'] == 0){
			$version = 0;
		}else{
			$version = 1;
		}
		$data = array(
			'classification_id' =>  $_POST['classification_id'],
			'standard_id' =>  $_POST['standard_id'],
			'create_date' => date('Y-m-d H:i:s'),
			'update_date' => date('Y-m-d H:i:s'),
			'status' => $_POST['status'],
			'version' => $version,
			'sub_version' => 0,
			'user_id' => $this->session->userdata('userid')
			);
		$table = 'tbl_template';
		$data = $this->Template_model->save_data($data,$table);
		echo json_encode($data);
	}
	function save_element(){
		$data = array(
			'element_name' =>  $_POST['element_name'],
			'create_date' => date('Y-m-d H:i:s'),
			'update_date' => date('Y-m-d H:i:s'),
			'order' => $_POST['order']
			);
		$table = 'tbl_elements';
		$data = $this->Template_model->save_data($data,$table);
		// $this->save_audit_trail('Save Element');
		echo json_encode($data);
	}
	function save_questions(){
		$data = array(
			'template_id' =>  $_POST['template_id'],
			'element_id' =>  $_POST['element_id'],
			'question' =>  $_POST['question'],
			'default_yes' =>  $_POST['default_yes'],
			// 'mandatory' =>  $_POST['mandatory'],
			'create_date' => date('Y-m-d H:i:s'),
			'update_date' => date('Y-m-d H:i:s'),
			'order_sort' => $_POST['order'],
			'required_remarks' => $_POST['required_remarks'],
			);
		$table = 'tbl_questions';
		$data = $this->Template_model->save_data($data,$table);
		// $this->save_audit_trail('Save Question');
		echo json_encode($data);
	}
	function save_activity(){
		$data = array(
			'activity_name' =>  $_POST['activity_name'],
			'order' => $_POST['order'],
			'create_date' => date('Y-m-d H:i:s'),
			'update_date' => date('Y-m-d H:i:s'),
			);
		$table = 'tbl_activities';
		$activity_id = $this->Template_model->save_data($data,$table);
		$data1 = array(
			'template_id' =>  $_POST['template_id'],
			'activity_id' =>  $activity_id,
			'create_date' => date('Y-m-d H:i:s'),
			'update_date' => date('Y-m-d H:i:s'),
			);
		$table1 = 'tbl_template_activities';
		$this->Template_model->save_data($data1,$table1);
		// $this->save_audit_trail('Save activity');
		echo json_encode($activity_id);
	}
	function save_subactivity(){
		$data = array(
			'activity_id' =>  $_POST['activity_id'],
			'sub_item_name' =>  $_POST['subActivity'],
			'order' => $_POST['order'],
			'create_date' => date('Y-m-d H:i:s'),
			'update_date' => date('Y-m-d H:i:s'),
			);
		$table = 'tbl_sub_activities';
		$data = $this->Template_model->save_data($data,$table);
		// $this->save_audit_trail('Save Sub-activity');
		echo json_encode($data);
	}
	function get_data_by_id(){
		$id = $_POST['id'];
		$field = $_POST['field'];
		$table = $_POST['table'];
		$data = $this->Template_model->get_data_by_id($id,$field,$table);
		echo json_encode($data);
	}
	function get_elements(){
		$template_id = $_POST['template_id'];
		$data = $this->Template_model->get_elements($template_id);
		echo json_encode($data);
	}
	function get_questions(){
		$template_id = $_POST['template_id'];
		$element_id = $_POST['element_id'];
		$data = $this->Template_model->get_questions($template_id,$element_id);
		echo json_encode($data);
	}
	function get_activity(){
		$template_id = $_POST['template_id'];
		$data = $this->Template_model->get_activity($template_id);
		echo json_encode($data);
	}
	function get_sub_activity(){
		$id = $_POST['activity_id'];
		$data = $this->Template_model->get_subactivity_model($id);
		echo json_encode($data);
	}
	// update
	function update_element(){
		$data = array(
			'element_id' => $_POST['element_id'],
			'element_name' => $_POST['element_name'],
			'update_date' => date('Y-m-d H:i:s'),
			'order' => $_POST['order']
			);
		$table = $_POST['table'];
		$field = $_POST['field'];
		$data = json_decode(json_encode($data), FALSE);
  		$this->Template_model->update_data($field,  $_POST['element_id'], $table, $data);
  		// $this->save_audit_trail('Update Element');
	}
	function update_question(){
		$data = array(
			'question' => $_POST['question'],
			'default_yes' => $_POST['default_yes'],
			'update_date' => date('Y-m-d H:i:s'),
			'order_sort' => $_POST['order'],
			'required_remarks' => $_POST['required_remarks']
			);
		$table = $_POST['table'];
		$field = $_POST['field'];
		$data = json_decode(json_encode($data), FALSE);
  		$this->Template_model->update_data($field,  $_POST['question_id'], $table, $data);
  		print_r($data);
  		echo $_POST['question_id'];
  		// $this->save_audit_trail('Update Question');
	}
	function update_activity(){
		$data = array(
			'activity_name' => $_POST['activity_name'],
			'order' => $_POST['order'],
			'update_date' => date('Y-m-d H:i:s')
			);
		$table = $_POST['table'];
		$field = $_POST['field'];
		$data = json_decode(json_encode($data), FALSE);
  		$this->Template_model->update_data($field,  $_POST['activity_id'], $table, $data);
  		// $this->save_audit_trail('Update Activity');
  		echo $_POST['activity_id'];
	}
	function update_subactivity(){
		$data = array(
			'sub_item_name' => $_POST['sub_item_name'],
			'order' => $_POST['order'],
			'update_date' => date('Y-m-d H:i:s')
			);
		$table = $_POST['table'];
		$field = $_POST['field'];
		$data = json_decode(json_encode($data), FALSE);
  		$this->Template_model->update_data($field,  $_POST['sub_item_id'], $table, $data);
  		// $this->save_audit_trail('Update Sub-activity');
	}
	function delete_data(){
		$id = $_POST['id'];
		$field = $_POST['field'];
		$table = $_POST['table'];
  		$this->Template_model->delete_data($id,$field,$table);
		$this->save_audit_trail('Delete Template');
	}
	function update_template(){
		$data = array(
			'classification_id' =>  $_POST['classification_id'],
			'standard_id' =>  $_POST['standard_id'],
			'version' =>  $_POST['new_version'],
			'sub_version' =>  $_POST['sub_version'],
			'update_date' => date('Y-m-d H:i:s')
			);
		$table = $_POST['table'];
		$field = $_POST['field'];
		$data = json_decode(json_encode($data), FALSE);
  		$this->Template_model->update_data($field,  $_POST['id'], $table, $data);
	}
	function template_as_final(){
		$data = array(
			'status' =>  1,
			'version' =>  1,
			'sub_version' =>  0,
			'update_date' => date('Y-m-d H:i:s')
			);
		$field = $_POST['field'];
		$table = $_POST['table'];
		$data = json_decode(json_encode($data), FALSE);
  		$this->Template_model->update_data($field,  $_POST['id'], $table, $data);
	}
	public function save_audit_trail($action){
		$login['user'] = $this->session->userdata('userid');
		$login['page'] = $this->agent->referrer();
		$login['type'] = $this->session->userdata('type');
		$login['role'] = $this->session->userdata('sess_role');
		$login['email'] = $this->session->userdata('sess_email');
		$login['action'] = ucwords($action);
		$login['date'] = date('Y-m-d H:i:s');
		$data = $login;
		$table = 'tbl_audit_trail';
		$this->Audit_trail_model->save_data($table,$data);
	}
	// get list manage_template 9/19/17
		public function get_list_manage_template(){
		// header('Content-type: Application/JSON');
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$query = $_POST['query'];
		// $table = $_POST['table'];
		// $order_by = $_POST['order_by'];
		echo json_encode($this->Manage_template_model->get_list_manage_template($query,$limit,$offset), JSON_PRETTY_PRINT);
	}

	public function get_pagination_template(){
		header('Content-type: Application/JSON');
		$query = $_POST['query'];
		echo $this->Manage_template_model->get_pagination_template($query);
	}
	public function check_template_standard()
	{
		$classification_id = $_POST['classification_id'];
		$standard_id = $_POST['standard_id'];
		$result = $this->Manage_template_model->get_list_manage_template("standard_id = '" . $standard_id . "' AND classification_id ='" . $classification_id . "' AND status = 1",100,0);
		echo count($result);
	}
	public function update_status(){
		$id = $_POST['template_id'];
		$status = $_POST['status'];
		$this->Manage_template_model->update_status($id,$status);
	}
	public function check_in_use()
	{
		$classification_id = $_POST['classification_id'];
		echo json_encode($this->Manage_template_model->check_in_use($classification_id));
	}

    public function check_in_use1()
    {
        $classification_id = $_POST['classification_id'];
        echo json_encode($this->Manage_template_model->check_in_use($classification_id));
    }



	public function generate_template()
	{
		$classification = $_POST['classification'];
		$standard = $_POST['standard'];
		$elements = $_POST['elements'];
		$activity = $_POST['activity'];
		
		$action = $_POST['action'];

		//template
		if($action == "save"){
            $status = $_POST['status'];
			if($status == 0){
				$version = 0;
			}else{
				$version = 1;
			}

			//save template
			$data = array(
				'classification_id' => $classification,
				'standard_id'       => $standard,
				'create_date'       => date('Y-m-d H:i:s'),
				'update_date'       => date('Y-m-d H:i:s'),
				'status'            => $status,
				'version'           => $version,
				'sub_version'       => 0,
				'user_id'           => $this->session->userdata('userid')
			);
			$template_id = $this->Template_model->save_data($data,'tbl_template');


		} 

        if($action == "update"){
            $new_version = $_POST['new_version'];
            $sub_version = $_POST['sub_version'];
            $template_id = $_POST['template_id'];

            if($_POST['status'] != ""){
                $status = $_POST['status'];
            } else {
                $status = 0;
            }
			//update template
            $data = array(
                'classification_id' =>  $classification,
                'standard_id'       =>  $standard,
                'version'           =>  $new_version,
                'status'            =>  $status,
                'sub_version'       =>  $sub_version,
                'update_date'       =>  date('Y-m-d H:i:s')
            );
            $this->Template_model->update_data("template_id", $template_id, 'tbl_template', $data);



            ///delete
            //element
            foreach ($_POST['del_elements'] as $a => $b) {
                $id = $b['id'];
                $this->Template_model->delete_data($id,'element_id','tbl_elements');


                //remove all questions in this element
                $this->Template_model->delete_data($id,'element_id','tbl_questions');
            }

            //question
            foreach ($_POST['del_questions'] as $a => $b) {
                $id = $b['id'];
                $this->Template_model->delete_data($id,'question_id','tbl_questions');
            }

            //activity
            foreach ($_POST['del_activities'] as $a => $b) {
                $id = $b['id'];
                $this->Template_model->delete_data($id,'activity_id','tbl_activities');
                $this->Template_model->delete_data($id,'activity_id','tbl_template_activities');
            }

            //sub activity
            foreach ($_POST['del_subactivities'] as $a => $b) {
                $id = $b['id'];
                $this->Template_model->delete_data($id,'sub_item_id','tbl_sub_activities');
            }


		}


		//elements
		foreach ($elements as $key => $value) {
			$element_name  = $value['element'];
			$order  = $value['order'];
			$dataid  = $value['dataid'];
			
			if($dataid == 0){
				//save element
				$data = array(
					'element_name'     =>  $element_name,
					'create_date'      => date('Y-m-d H:i:s'),
					'update_date'      => date('Y-m-d H:i:s'),
					'order'            => $order
				);
				$element_id = $this->Template_model->save_data($data,'tbl_elements');

			} else {
				//update element
                $data = array(
                    'element_name'      =>  $element_name,
                    'update_date'       =>  date('Y-m-d H:i:s'),
                    'order'             =>  $order
                );
                $element_id = $dataid;
                $this->Template_model->update_data('element_id', $element_id, 'tbl_elements', $data);

			}

			//questions
			$questions  = $value['questions'];
			foreach ($questions as $k => $v) {
				$question = $v['question'];
				$default_yes = $v['default_yes'];
				$data_id = $v['data-id'];
				$required = $v['required'];
				$order = $v['order'];

				if($data_id == 0){
					//save question
					$data = array(
						'template_id'         => $template_id,
						'element_id'          => $element_id,
						'question'            => $question,
						'default_yes'         => $default_yes,
						'create_date'         => date('Y-m-d H:i:s'),
						'update_date'         => date('Y-m-d H:i:s'),
						'order_sort'          => $order,
						'required_remarks'    => $required
					);
					$this->Template_model->save_data($data,'tbl_questions');

				} else {
					//update question
                    $data = array(
                        'question'          => $question,
                        'default_yes'       => $default_yes,
                        'update_date'       => date('Y-m-d H:i:s'),
                        'order_sort'        => $order,
                        'required_remarks'  => $required
                    );
                    $this->Template_model->update_data('question_id',  $data_id, 'tbl_questions', $data);

				}
			}
		}


		//activities
		foreach ($activity as $a => $b) {
			$activity = $b['activity'];
			$order = $b['order'];
			$data_id = $b['data_id'];

			if($data_id == 0){
				//save activity
				$data = array(
					'activity_name'    => $activity,
					'order'            => $order,
					'create_date'      => date('Y-m-d H:i:s'),
					'update_date'      => date('Y-m-d H:i:s'),
				);
				$activity_id = $this->Template_model->save_data($data,'tbl_activities');

				$data1 = array(
					'template_id'  => $template_id,
					'activity_id'  => $activity_id,
					'create_date'  => date('Y-m-d H:i:s'),
					'update_date'  => date('Y-m-d H:i:s'),
				);
				$this->Template_model->save_data($data1,'tbl_template_activities');

			} else {
				//update activity
                $data = array(
                    'activity_name' => $activity,
                    'order'         => $order,
                    'update_date'   => date('Y-m-d H:i:s')
                );
                $activity_id = $data_id;
                $this->Template_model->update_data('activity_id', $activity_id, 'tbl_activities', $data);

			}



			//sub activities
            if(isset($b['subactivities'])){
                $subactivities = $b['subactivities'];
                foreach ($subactivities as $c => $d) {
                    $subactivity = $d['sub_activity'];
                    $order = $d['order'];
                    $data_id = $d['data_id'];

                    if($data_id == 0){
                        //save data

                        $data = array(
                            'activity_id'     => $activity_id,
                            'sub_item_name'   => $subactivity,
                            'order'           => $order,
                            'create_date'     => date('Y-m-d H:i:s'),
                            'update_date'     => date('Y-m-d H:i:s'),
                        );
                        $data = $this->Template_model->save_data($data,'tbl_sub_activities');

                    } else {
                        //update data

                        $data = array(
                            'sub_item_name' => $subactivity,
                            'order'         => $order,
                            'update_date'   => date('Y-m-d H:i:s')
                        );
                        $this->Template_model->update_data('sub_item_id', $data_id, 'tbl_sub_activities', $data);

                    }
                }
            }
			

            echo $template_id;

		}

		//generate json
        $this->samscurl->load(base_url("api/template_list"));
        $this->samscurl->load(base_url("api/template_info") . "/" . (int) $template_id);

            
        return $template_id;
	}

	public function view_template()
	{
		$id = $_POST['id'];
		$template = $this->Template_model->get_data_by_id($id,'template_id','tbl_template');
		$classification = $template[0]->classification_id;
		$standard = $template[0]->standard_id;
		$version = $template[0]->version;
		$sub_version = $template[0]->sub_version;
		$status = $template[0]->status;


		$elements = $this->Template_model->get_elements($id);
		$elements_array = array();
		foreach ($elements as $key => $value) {


			$questions = $this->Template_model->get_questions($id,$value->element_id);
			$question_array = array();
			foreach ($questions as $a => $b) {
				$question_array[] = array(
					'question_id' 		=> $b->question_id, 
					'question' 			=> $b->question, 
					'default_yes' 		=> $b->default_yes, 
					'order_sort' 		=> $b->order_sort, 
					'required_remarks' 	=> $b->required_remarks 
				);
			}

			$elements_array[] = array(
				'element_id' 	=> 	$value->element_id,
				'element_name' 	=>	$value->element_name,
				'questions'		=>	$question_array
			);
		}


		$activity = $this->Template_model->get_activity($id);
		$activity_array = array();
		foreach ($activity as $key => $value) {

			$sub_activity = $this->Template_model->get_subactivity_model($value->activity_id);
			$sub_activity_array = array();
			foreach ($sub_activity as $a => $b) {
				$sub_activity_array[] = array(
					'sub_activity_id'	=>	$b->sub_item_id,
					'sub_activity'		=>	$b->sub_item_name,
					'order'				=> 	(int) $b->order,
				);
			}
		
			$activity_array[] = array(
				'activity_id'		=>	$value->activity_id,
				'activity'			=>	$value->activity_name,
				'order'				=>	(int) $value->order,
				'sub_activities'	=>	$sub_activity_array,
			);
		}

		$result = array(
			"classification_id"	=>	$classification,
			"standard_id"		=> 	$standard,
			"version"			=> 	$version,
			"sub_version"		=> 	$sub_version,
			"elements"			=> 	$elements_array,
			"activities"		=> 	$activity_array,
			"status"		    => 	$status
		);


		echo json_encode($result);
	}

}
