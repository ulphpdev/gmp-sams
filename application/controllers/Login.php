<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->library('session');

		$this->load->helper('url');

	}

	public function index()

	{

		if($this->session->userdata('sess_email')=='' ) { 

			
			if($this->session->userdata('session_link')) {
				$data['redirect_link'] = $this->session->userdata('session_link');
			} else {
				$data['redirect_link'] = base_url("home");
			}

			$this->load->view('login',$data);

		} else {

			redirect(base_url().'home');

		}

	}



	public function setSession() {

		$username = $_POST['username'];
		$role = $_POST['role'];
		$userid = $_POST['userid'];
		$name = $_POST['name'];

			$newdata = array(
			        'sess_email'  => $username,
			        'sess_role'  => $role,
			        'type' => "Auditor",
			        'userid' => $userid,
			        'logged_in' => TRUE,
			        'name' => $name	    
			);



			$this->session->set_userdata($newdata);

	}

	public function unsetSession() {

		$this->session->sess_destroy();

		header('Location: '.base_url().'login');

	}

}

