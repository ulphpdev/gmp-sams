<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');

class Test extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $this->load->library('session');
    $this->load->model('q_auditreport');
    $this->load->model('Audit_report_model');
    $this->load->model('Global_model');
    $this->load->helper('url');

  }

  public function index()
  {
    //Login
    $id = $_GET['id'];
    $query = "report_id = " . $id;
    $data['audit_dates'] = json_encode($this->Audit_report_model->get_audit_dates($id));
    $this->load->view('layout/header');
    $this->load->view('test',$data);
  }
}