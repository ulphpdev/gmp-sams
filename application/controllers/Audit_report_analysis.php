<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Taipei');
class Audit_report_analysis extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('global_model');
		$this->load->model('Audit_report_analysis_model2');
		$this->load->helper('url');
		$this->load->model('q_auditreport');
		if($this->session->userdata('sess_email')=='' ) { 
			redirect(base_url("login"));
		} else {
			if($this->session->userdata('type')== 'approver'){
				redirect(base_url("login/unsetSession"));
			}
			if($this->session->userdata('type')== 'reviewer'){
				redirect(base_url("login/unsetSession"));
			}
		}
	}
	public function index()
	{	
			$data['content'] = 'audit_report_analysis/list2';
			$this->load->view('layout/layout',$data);
	}
		public function get_list_report(){ 
		// header('Content-type: Application/JSON');
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$query = $_POST['query'];
		$result = $this->Audit_report_analysis_model2->get_report_analysis_list($query,$limit,$offset);
		echo json_encode($result, JSON_PRETTY_PRINT);
	}
		public function get_pagination(){
		$query = $_POST['query'];
		echo $this->Audit_report_analysis_model2->get_pagination($query);
	}
		public function get_pagination_trail(){
		$query = $_POST['query'];
		echo $this->Audit_report_analysis_model2->get_pagination_trail($query);
	}
	public function count_yesno(){
		$report_id = $_POST['report_id'];
		echo $this->Audit_report_analysis_model2->count_yesno($report_id);
	}
}